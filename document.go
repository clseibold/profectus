package main

import (
	"bufio"
	"bytes"
	_ "embed"
	"encoding/csv"
	"errors"
	"fmt"
	"image/gif"
	"io"
	"mime"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/clseibold/go-gemini"
	"github.com/gabriel-vasile/mimetype"
	"github.com/gen2brain/avif"
	"github.com/gen2brain/jpegxl"
	"github.com/gogs/chardet"
	"github.com/gopxl/beep"
	"github.com/gopxl/beep/effects"
	"github.com/gopxl/beep/flac"
	"github.com/gopxl/beep/mp3"
	"github.com/gopxl/beep/speaker"
	"github.com/gopxl/beep/vorbis"
	"github.com/gopxl/beep/wav"
	"github.com/kettek/apng"
	"github.com/kkdai/youtube/v2"
	"github.com/pkg/browser"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/clseibold/gonex/nex_client"
	"gitlab.com/clseibold/profectus/horusui"
	spartan_client "gitlab.com/clseibold/profectus/spartan"
	scroll "gitlab.com/clseibold/scroll-term/scroll_client"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/ianaindex"
	"golang.org/x/text/encoding/unicode"
)

//go:embed help.scroll
var helpFile string

//go:embed licenses.scroll
var licensesFile string

//go:embed changelog.scroll
var changelogFile string

//go:embed themes/README.scroll
var themingFile string

//go:embed newtab.scroll
var newtabFile string

type Certificate []byte
type Line interface{}

type DocumentType uint8

const (
	DocumentType_Scroll DocumentType = iota
	DocumentType_GopherMenu
	DocumentType_Nex
	DocumentType_PlainText
	DocumentType_CSV
	DocumentType_GIF
	DocumentType_PNG // Handles PNGs and Animated PNGs
	DocumentType_JXL // aka. JPEG XL
	DocumentType_AVIF
	DocumentType_WebP
	DocumentType_Image
	DocumentType_Audio
	DocumentType_Video
	DocumentType_Zip
	DocumentType_OtherBinary

	DocumentType_InputPrompt
	DocumentType_InputPrompt_Sensitive
	DocumentType_Error

	DocumentType_BuiltinInteractive // Used for theme building and other things
)

func mediatypeToDocType(mediatype string) DocumentType {
	switch mediatype {
	case "text/gemini", "text/scroll", "text/spartan", "text/markdown":
		return DocumentType_Scroll
	case "text/nex":
		return DocumentType_Nex
	case "application/gopher-menu":
		return DocumentType_GopherMenu
	case "application/ecmascript", "application/javascript", "application/x-ecmascript", "application/x-javascript", "application/json", "application/mbox", "application/postscript", "application/prql", "application/sparql-query", "application/srgs", "application/x-perl", "application/x-sh", "application/x-shar", "application/x-shellscript", "application/x-tcl", "application/x-tex", "application/x-texinfo", "application/xml", "application/xml-dtd", "application/yaml", "application/x-yaml", "chemical/x-cif", "chemical/x-cml", "chemical/x-csml", "chemical/x-xyz":
		return DocumentType_PlainText
	}

	if strings.HasPrefix(mediatype, "text/") || (strings.HasPrefix(mediatype, "appication/") && strings.Contains(mediatype, "+xml")) {
		return DocumentType_PlainText
	} else if IsSupportedAudio(mediatype) || strings.HasPrefix(mediatype, "audio/") {
		return DocumentType_Audio
	} else if strings.HasPrefix(mediatype, "video/") {
		return DocumentType_Video
	} else if mediatype == "image/gif" {
		return DocumentType_GIF
	} else if mediatype == "iamge/apng" || mediatype == "image/png" || mediatype == "image/x-png" || mediatype == "image/vnd.mozilla.apng" {
		return DocumentType_PNG
	} else if mediatype == "image/jxl" {
		return DocumentType_JXL
	} else if mediatype == "image/webp" {
		return DocumentType_WebP
	} else if mediatype == "image/avif" {
		return DocumentType_AVIF
	} else if strings.HasPrefix(mediatype, "image/") {
		return DocumentType_Image
	}

	return DocumentType_OtherBinary
}

type PageCache struct {
	// Whether the page is in the forwards history.
	InForwards bool

	DocType          DocumentType
	Scroll           float32
	Url              *url.URL
	ZipPath          string // The Path within the Zip file.
	Mediatype        string
	Params           map[string]string
	Author           string
	PublishDate      time.Time // Should be in UTC
	ModificationDate time.Time // Should be in UTC
	UDCClass         int
	Title            string
	Abstract         []scroll.ScrollLine
	CSV              [][]string
	CSVWidth         int

	Headings []scroll.ScrollLine_Heading
	Links    []scroll.ScrollLine_Link
	Lines    []scroll.ScrollLine

	Data []byte
}

func saveCurrentPageInCache(tab *Tab) {
	tab.getNextPageCacheInHistory().Value = tab.currentPageCache
	tab.history = tab.history.Next()
}

func resetCurrentDocumentState(state *State, tab *Tab) {
	// Cancel existing connection
	if tab.ongoingConnection != nil {
		ongoingConnection := tab.ongoingConnection
		tab.ongoingConnection = nil
		ongoingConnection.Close()
	}

	// Reset the cursor if tab is active, because elements that affect the cursor (like links) have now disappeared
	if state.getActiveTab() == tab {
		if &tab.documentInput == state.focusedInput {
			resetInputFocus(state)
		}
		resetCursor(state)
	}

	if tab.currentPageCache.DocType == DocumentType_GIF {
		//fmt.Printf("%.2f MB\n", float32(len(tab.currentPageCache.Data))/1024/1024)
	}
	tab.currentPageCache = PageCache{InForwards: false, DocType: DocumentType_Scroll, Scroll: 0, Url: nil, ZipPath: "", Mediatype: "", Params: nil, Author: "", PublishDate: time.Time{}, ModificationDate: time.Time{}, UDCClass: -1, Title: "", Abstract: make([]scroll.ScrollLine, 0, 1), CSV: make([][]string, 0), CSVWidth: 0, Headings: make([]scroll.ScrollLine_Heading, 0, 1), Links: make([]scroll.ScrollLine_Link, 0), Lines: make([]scroll.ScrollLine, 0, 1), Data: make([]byte, 0)}
	tab.spartanLineInputs = nil
	tab.posCache = nil
	tab.firstFrame = true

	// Scroll Animations
	tab.scrollAnimation.SetStaticValue(0)
	tab.outlineScroll.SetStaticValue(0)

	if tab.documentImgSurface != nil {
		surface := tab.documentImgSurface
		state.destroyer <- surface
		go func() {
			time.Sleep(time.Millisecond * 50)
			tab.documentImgSurface = nil
		}()

		//surface.Free()

	}

	if tab.documentGifPlayer != nil {
		gifPlayer := tab.documentGifPlayer
		tab.documentGifPlayer = nil

		go func() {
			gifPlayer.Destroy(state)
		}()
	}

	if tab.documentAPNGPlayer != nil {
		apngPlayer := tab.documentAPNGPlayer
		tab.documentAPNGPlayer = nil

		go func() {
			apngPlayer.Destroy(state)
		}()
	}

	if tab.documentJxlPlayer != nil {
		jxlPlayer := tab.documentJxlPlayer
		tab.documentJxlPlayer = nil

		go func() {
			jxlPlayer.Destroy(state)
		}()
	}

	/*
		if tab.documentWebPPlayer != nil {
			webpPlayer := tab.documentWebPPlayer
			tab.documentWebPPlayer = nil

			go func() {
				webpPlayer.Destroy(state)
			}()
		}
	*/

	if tab.documentAVIFPlayer != nil {
		avifPlayer := tab.documentAVIFPlayer
		tab.documentAVIFPlayer = nil

		go func() {
			avifPlayer.Destroy(state)
		}()
	}
}

// Make sure to only call this on text documents
func getCharsetDecoderReader(charset string, body io.Reader) (io.Reader, error) {
	if charset == "" {
		// Try to detect the charset by sniffing the first 1024 bytes
		var buf [1024]byte
		bytes_read, err := body.Read(buf[:])
		if err != nil && !errors.Is(err, io.EOF) {
			return nil, err
		}

		// Detect charset. Prefer UTF-8, otherwise get the best match.
		detector := chardet.NewTextDetector()
		all_results, err := detector.DetectAll(buf[:bytes_read])
		if err != nil {
			return nil, err
		}
		for _, result := range all_results {
			if result.Charset == "UTF-8" || result.Charset == "ISO-8859-1" {
				//fmt.Printf("Confidence: %v\n", result.Confidence)
				return getCharsetDecoderReader("UTF-8", io.MultiReader(bytes.NewReader(buf[:bytes_read]), body))
			}
		}
		charsetResult := all_results[0]
		//fmt.Printf("Detected Charset: %s\n", charsetResult.Charset)
		return getCharsetDecoderReader(charsetResult.Charset, io.MultiReader(bytes.NewReader(buf[:bytes_read]), body))
	}

	var file_encoding encoding.Encoding
	var err error
	if charset != "UTF-8" && charset != "UTF8" && charset != "ASCII" && charset != "US-ASCII" {
		file_encoding, err = ianaindex.IANA.Encoding(charset)
		if err != nil {
			// TODO: Switch to binary download
			//state.documentType = DocumentType_OtherBinary
			return nil, err
		}
		switch strings.ToUpper(charset) {
		case "UTF-16LE":
			file_encoding = unicode.UTF16(unicode.LittleEndian, unicode.IgnoreBOM)
		case "UTF-16BE":
			file_encoding = unicode.UTF16(unicode.BigEndian, unicode.IgnoreBOM)
		case "UTF-16":
			file_encoding = unicode.UTF16(unicode.LittleEndian, unicode.UseBOM) // Default to this
		case "UTF-32":
		}
	}
	var reader io.Reader = body
	if file_encoding != nil {
		decoder := file_encoding.NewDecoder()
		reader = decoder.Reader(body)
	}

	return reader, nil
}

func getHomepage(state *State, tab *Tab) {
	getPage(state, tab, state.settings.Homepage, false, "about:help")
}

func getPageAbstract(tab *Tab, urlString string) {
	runtime.UnlockOSThread()

	// TODO: Gets a page's abstract
}

func getPage(state *State, tab *Tab, urlString string, keepScroll bool, fallbackUrl string) {
	runtime.UnlockOSThread()

	scrollVal := float32(0)
	if keepScroll {
		scrollVal = tab.currentPageCache.Scroll
	}

	// Save current page's cache to history cache again
	if tab.getCurrentPageCacheInHistory().Value != nil {
		tab.getCurrentPageCacheInHistory().Value = tab.currentPageCache
	}

	prevPageUrl := tab.currentPageCache.Url
	URL, err := url.Parse(urlString)
	if err != nil {
		panic(err)
	}
	if URL.String() == "about:help" {
		getAboutPage(state, tab, "about:help", helpFile)
		return
	} else if URL.String() == "about:licenses" {
		getAboutPage(state, tab, "about:licenses", licensesFile)
		return
	} else if URL.String() == "about:changelog" {
		getAboutPage(state, tab, "about:changelog", changelogFile)
		return
	} else if URL.String() == "about:theming" {
		getAboutPage(state, tab, "about:theming", themingFile)
		return
	} else if URL.String() == "about:about" {
		getAboutPages(state, tab)
		return
	} else if URL.String() == "about:newtab" {
		getAboutPage(state, tab, "about:newtab", newtabFile)
		return
	} else if URL.String() == "about:theme-builder" {
		tab.documentLoading = true
		resetCurrentDocumentState(state, tab)

		tab.currentPageCache.Url, _ = url.Parse("about:theme-builder")
		setInput(state, &tab.addressInput, "about:theme-builder")
		tab.currentPageCache.DocType = DocumentType_BuiltinInteractive
		tab.currentPageCache.Title = "Theme Builder"
		tab.documentLoading = false

		saveCurrentPageInCache(tab)
		return
	} else if strings.HasPrefix(URL.String(), "about:") {
		tab.documentLoading = true
		resetCurrentDocumentState(state, tab)

		tab.currentPageCache.Mediatype = "text/scroll"
		tab.currentPageCache.DocType = DocumentType_Error
		text := "About page not found.\n=> about:about About Pages\n"
		tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(text), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

		tab.documentLoading = false
		saveCurrentPageInCache(tab)
		return
		/*} else if strings.HasPrefix(urlString, "data:") && !strings.HasPrefix(urlString, "data://") {
		URL_String := strings.Replace(urlString, "data:", "data://", 1)
		if commandExists("mpv") {
			VideoDataLink(state, URL_String, VideoProgram_MPV)
		} else if commandExists("ffplay") {
			VideoDataLink(state, URL_String, VideoProgram_FFPLAY)
		} else if commandExists("vlc") {
			VideoDataLink(state, URL_String, VideoProgram_VLC)
		}
		return*/
	} else if strings.HasPrefix(URL.String(), "http://www.youtube.com/") || strings.HasPrefix(URL.String(), "https://www.youtube.com/watch?v=") || strings.HasPrefix(URL.String(), "http://youtube.com/") || strings.HasPrefix(URL.String(), "https://youtube.com/watch?v=") || strings.HasPrefix(URL.String(), "youtube://youtube.com") || strings.HasPrefix(URL.String(), "youtube://www.youtube.com") || strings.HasPrefix(URL.String(), "youtube://watch") || strings.HasPrefix(URL.String(), "ytdl://youtube.com") || strings.HasPrefix(URL.String(), "ytdl://www.youtube.com") {
		videoQualities := []string{"hd1080", "hd720", "medium", "tiny"}
		desiredMaxQuality := "hd720"

		client := youtube.Client{}
		client.HTTPClient = &http.Client{Transport: &http.Transport{
			IdleConnTimeout:       60 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			ForceAttemptHTTP2:     true,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).DialContext,
		}}

		URL_String := URL.String()
		URL_String = strings.Replace(URL_String, "youtube://www.youtube.com", "https://www.youtube.com", 1)
		URL_String = strings.Replace(URL_String, "youtube://youtube.com", "https://youtube.com", 1)
		URL_String = strings.Replace(URL_String, "youtube://watch", "https://youtube.com/watch", 1)
		URL_String = strings.Replace(URL_String, "youtube://watch", "https://youtube.com/watch", 1)
		URL_String = strings.Replace(URL_String, "ytdl://www.youtube.com", "https://youtube.com", 1)
		URL_String = strings.Replace(URL_String, "ytdl://youtube.com", "https://youtube.com", 1)

		video, err := client.GetVideo(URL_String)
		if err == nil {
			audioFormats := video.Formats.WithAudioChannels()
			audioFormats.Sort()

			audioFormats_mediumAudioQuality := filterYT(video.Formats, func(format youtube.Format) bool {
				return format.AudioQuality == "AUDIO_QUALITY_MEDIUM"
			})
			audioFormats_lowAudioQuality := filterYT(video.Formats, func(format youtube.Format) bool {
				return format.AudioQuality == "AUDIO_QUALITY_LOW"
			})

			var format *youtube.Format = nil
			skip := true
			for _, quality := range videoQualities {
				if quality == desiredMaxQuality {
					skip = false
				} else if skip {
					continue
				}

				// Try medium audio quality first
				//format = audioFormats_mediumAudioQuality.FindByQuality(quality)
				var list youtube.FormatList = audioFormats_mediumAudioQuality.Quality(quality)
				if len(list) <= 0 {
					fmt.Printf("Could not find %s-quality video with medium audio. Trying low audio quality.\n", quality)
					// If not found, then try low audio quality
					//format = audioFormats_lowAudioQuality.FindByQuality(quality)
					list = audioFormats_lowAudioQuality.Quality(quality)
					if len(list) <= 0 {
						fmt.Printf("Could not find %s-quality video with audio. Trying next quality.\n", quality)
						continue
					}
				}

				// If a format was found, break
				format = &list[0]
				break
			}

			if format != nil {
				rc, _, err := client.GetStream(video, format)
				if err == nil {
					HandleDocument(state, tab, URL, format.MimeType, nil, rc, rc)
					client.HTTPClient.CloseIdleConnections()
					return
				}
			}
			client.HTTPClient.CloseIdleConnections()
		}
	}

	// Default to gemini scheme
	if URL.Scheme == "" && !strings.ContainsAny(urlString, " \t\n") && strings.IndexRune(urlString, '.') != -1 {
		URL.Scheme = "gemini"
		urlString = URL.String()
	} else if URL.Scheme == "" && (strings.ContainsAny(urlString, " \t\n") || strings.IndexRune(urlString, '.') == -1) {
		// Search string
		searchURL := strings.TrimSuffix(state.settings.SearchURL, "?")
		newURL, err := url.Parse(searchURL + "?" + scroll.QueryEscape(strings.TrimSpace(urlString)))
		if err != nil {
			URL.Scheme = "gemini"
		} else {
			URL = newURL
		}
		urlString = URL.String()
	}

	if URL.Scheme == "http" || URL.Scheme == "https" {
		browser.OpenURL(urlString)
		return
	} else if URL.Scheme == "rtmp" || URL.Scheme == "rtmps" {
		if commandExists("mpv") {
			VideoDataLink(state, URL.String(), VideoProgram_MPV)
		} else if commandExists("ffplay") {
			VideoDataLink(state, URL.String(), VideoProgram_FFPLAY)
		} else if commandExists("vlc") {
			VideoDataLink(state, URL.String(), VideoProgram_VLC)
		}
		return
	} else if URL.Scheme == "scroll" {
		tab.documentLoading = true
		resetCurrentDocumentState(state, tab)
		tab.currentPageCache.Scroll = scrollVal
		tab.scrollAnimation.SetStaticValue(scrollVal)
		tab.scrollAnimation.SetFinished()

		var redirects int8 = 0
		var resp *scroll.Response
		var err error
		for {
			resp, err = scroll.Fetch(urlString, []string{"en"}, false)
			if err != nil || resp == nil {
				break
			}
			if resp.Status == scroll.StatusRedirect || resp.Status == scroll.StatusRedirectPermanent || resp.Status == scroll.StatusRedirectTemporary {
				if redirects >= state.maxRedirects {
					setInput(state, &tab.addressInput, urlString)
					tab.currentPageCache.Url = URL

					tab.currentPageCache.Mediatype = "text/scroll"
					tab.currentPageCache.DocType = DocumentType_Error
					tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Too many redirects. Max redirects: %d\n", state.maxRedirects)), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

					tab.documentLoading = false
					if URL.String() != prevPageUrl.String() {
						saveCurrentPageInCache(tab)
					}
					resp.Body.Close()
					return
				} else {
					URL_redirect, urlError := prevPageUrl.Parse(resp.Description)
					if urlError != nil {
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/scroll"
						tab.currentPageCache.DocType = DocumentType_Error
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Failed to redirect: not a url ('%s')\n", resp.Description)), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL_redirect.String() != prevPageUrl.String() {
							saveCurrentPageInCache(tab)
						}
						resp.Body.Close()
						return
					} else if URL_redirect.Scheme != "scroll" {
						// If cross-protocol redirect, prompt the user
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/scroll"
						tab.currentPageCache.DocType = DocumentType_Scroll
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Cross-protocol redirect. Click to continue:\n=> %s %s\n", URL_redirect.String(), URL_redirect.String())), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL_redirect.String() != prevPageUrl.String() {
							saveCurrentPageInCache(tab)
						}
						resp.Body.Close()
						return
					} else if URL_redirect.RawQuery != "" {
						// If there's a query string, prompt the user to redirect.
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/scroll"
						tab.currentPageCache.DocType = DocumentType_Scroll
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Redirect contains a query string. Click to continue:\n=> %s %s\n", URL_redirect.String(), URL_redirect.String())), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL_redirect.String() != prevPageUrl.String() {
							saveCurrentPageInCache(tab)
						}
						resp.Body.Close()
						return
					} else {
						// Otherwise, just do the redirect.
						redirects += 1
						prevPageUrl = URL
						urlString = URL_redirect.String()
						URL = URL_redirect
						resp.Body.Close()
						time.Sleep(time.Millisecond * 250)
						continue
					}
				}
			} else {
				break
			}
		}

		setInput(state, &tab.addressInput, urlString)
		tab.currentPageCache.Url = URL
		if err != nil || resp == nil {
			if fallbackUrl == "" {
				tab.currentPageCache.Mediatype = "text/scroll"
				tab.currentPageCache.DocType = DocumentType_Error
				tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader("Failed to load page.\n"), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			} else {
				getPage(state, tab, fallbackUrl, keepScroll, "")
				return
			}
		} else if resp.Status >= 20 && resp.Status <= 29 {
			if resp.Description == "" {
				resp.Description = "text/scroll"
			}
			mediatype, params, err := mime.ParseMediaType(resp.Description)
			if err != nil {
				fmt.Printf("Error: Couldn't parse mimetype.\n\n")
				resp.Body.Close()
				return
			}

			reader := resp.Body

			// Handle octet-stream
			if mediatype == "application/octet-stream" {
				detectionData, err := io.ReadAll(io.LimitReader(reader, 3072)) // Read 3072 bytes to try to detect mimetype
				if err != nil {
					fmt.Printf("Error: %s\n", err.Error())
					resp.Body.Close()
					return
				}
				mt, err := mimetype.DetectReader(bytes.NewReader(detectionData))
				if err != nil {
					fmt.Printf("Spartan: Couldn't detect mimetype.\n")
				}
				fmt.Printf("Detected Mimetype: %v\n", mt)
				// If couldn't detect mimetype based on file data, try using the extension instead
				if mt.Is("application/octet-stream") || err != nil {
					mt = mimetype.Lookup(mime.TypeByExtension(path.Ext(URL.Path)))
					mediatype, params, err = mime.ParseMediaType(mt.String())
					if err != nil {
						mediatype = mt.String()
					}
					if path.Ext(URL.Path) == ".gemini" || path.Ext(URL.Path) == ".gmi" {
						mediatype = "text/gemini"
					} else if path.Ext(URL.Path) == ".scroll" || path.Ext(URL.Path) == ".abstract" {
						mediatype = "text/scroll"
					} else if path.Ext(URL.Path) == ".gophermap" || path.Base(URL.Path) == "gophermap" {
						mediatype = "application/gopher-menu"
					}
				} else {
					mediatype, params, err = mime.ParseMediaType(mt.String())
					if err != nil {
						mediatype = mt.String()
					}
				}

				reader = MultiReadCloser(io.NopCloser(bytes.NewReader(detectionData)), resp.Body)
			}

			tab.currentPageCache.UDCClass = resp.Status - 20
			tab.currentPageCache.Mediatype = mediatype
			tab.currentPageCache.Params = params
			tab.currentPageCache.Author = resp.Author
			tab.currentPageCache.PublishDate = resp.PublishDate
			tab.currentPageCache.ModificationDate = resp.ModificationDate

			HandleDocument(state, tab, URL, mediatype, params, reader, resp)
		} else if scroll.CleanStatus(resp.Status) == scroll.StatusInput {
			tab.currentPageCache.Mediatype = "text/scroll"
			tab.currentPageCache.DocType = DocumentType_InputPrompt
			tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(resp.Description), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			resp.Body.Close()
		} else if scroll.CleanStatus(resp.Status) == scroll.StatusSensitiveInput {
			tab.currentPageCache.Mediatype = "text/scroll"
			tab.currentPageCache.DocType = DocumentType_InputPrompt_Sensitive
			tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(resp.Description), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			resp.Body.Close()
		} else {
			tab.currentPageCache.Mediatype = "text/scroll"
			tab.currentPageCache.DocType = DocumentType_Scroll
			tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(scroll.StatusText(resp.Status)), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			resp.Body.Close()
		}

		tab.documentLoading = false
		if prevPageUrl != nil && URL.String() == prevPageUrl.String() {
			// If same URL as previous page, overwrite its existing history
			tab.history = tab.history.Prev()
		}
		saveCurrentPageInCache(tab)
	} else if URL.Scheme == "gemini" {
		tab.documentLoading = true
		resetCurrentDocumentState(state, tab)
		tab.currentPageCache.Scroll = scrollVal
		tab.scrollAnimation.SetStaticValue(scrollVal)
		tab.scrollAnimation.SetFinished()

		var redirects int8 = 0
		var resp *gemini.Response
		var err error
		for {
			resp, err = gemini.Fetch(urlString)
			if err == nil && (resp.Status == gemini.StatusRedirect || resp.Status == gemini.StatusRedirectPermanent || resp.Status == gemini.StatusRedirectTemporary) {
				if redirects >= state.maxRedirects {
					setInput(state, &tab.addressInput, urlString)
					tab.currentPageCache.Url = URL

					tab.currentPageCache.Mediatype = "text/gemini"
					tab.currentPageCache.DocType = DocumentType_Error
					tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Too many redirects. Max redirects: %d\n", state.maxRedirects)), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

					tab.documentLoading = false
					if URL.String() == prevPageUrl.String() {
						// If same URL as previous page, overwrite its existing history
						tab.history = tab.history.Prev()
					}
					saveCurrentPageInCache(tab)
					resp.Body.Close()
					return
				} else {
					fmt.Printf("%v\n", resp.Meta)
					URL_redirect, urlError := prevPageUrl.Parse(resp.Meta)
					if urlError != nil {
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/gemini"
						tab.currentPageCache.DocType = DocumentType_Error
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Failed to redirect: not a url ('%s')\n", resp.Meta)), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL.String() == prevPageUrl.String() {
							// If same URL as previous page, overwrite its existing history
							tab.history = tab.history.Prev()
						}
						saveCurrentPageInCache(tab)
						resp.Body.Close()
						return
					} else if URL_redirect.Scheme != "gemini" {
						// If cross-protocol redirect, prompt the user
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/gemini"
						tab.currentPageCache.DocType = DocumentType_Scroll
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Cross-protocol redirect. Click to continue:\n=> %s %s\n", URL_redirect.String(), URL_redirect.String())), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL.String() == prevPageUrl.String() {
							// If same URL as previous page, overwrite its existing history
							tab.history = tab.history.Prev()
						}
						saveCurrentPageInCache(tab)
						resp.Body.Close()
						return
					} else if URL_redirect.RawQuery != "" {
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/gemini"
						tab.currentPageCache.DocType = DocumentType_Scroll
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Redirect contains a query string. Click to continue:\n=> %s %s\n", URL_redirect.String(), URL_redirect.String())), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL.String() == prevPageUrl.String() {
							// If same URL as previous page, overwrite its existing history
							tab.history = tab.history.Prev()
						}
						saveCurrentPageInCache(tab)
						resp.Body.Close()
						return
					} else {
						// Otherwise, just do the redirect.
						redirects += 1
						prevPageUrl = URL
						urlString = URL_redirect.String()
						URL = URL_redirect
						resp.Body.Close()
						time.Sleep(time.Millisecond * 250)
						continue
					}
				}
			} else {
				break
			}
		}

		setInput(state, &tab.addressInput, urlString)
		tab.currentPageCache.Url = URL
		if err != nil {
			if fallbackUrl == "" {
				tab.currentPageCache.Mediatype = "text/gemini"
				tab.currentPageCache.DocType = DocumentType_Error
				tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader("Failed to load page."), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			} else {
				getPage(state, tab, fallbackUrl, keepScroll, "")
				return
			}
		} else if gemini.CleanStatus(resp.Status) == 20 {
			if resp.Meta == "" {
				resp.Meta = "text/gemini"
			}
			mediatype, params, err := mime.ParseMediaType(resp.Meta)
			if err != nil {
				fmt.Printf("Error: Couldn't parse mimetype.\n\n")
				resp.Body.Close()
				return
			}

			reader := resp.Body

			// Handle octet-stream
			if mediatype == "application/octet-stream" {
				detectionData, err := io.ReadAll(io.LimitReader(reader, 3072)) // Read 3072 bytes to try to detect mimetype
				if err != nil {
					fmt.Printf("Error: %s\n", err.Error())
					resp.Body.Close()
					return
				}
				mt, err := mimetype.DetectReader(bytes.NewReader(detectionData))
				if err != nil {
					fmt.Printf("Spartan: Couldn't detect mimetype.\n")
				}
				fmt.Printf("Detected Mimetype: %v\n", mt)
				// If couldn't detect mimetype based on file data, try using the extension instead
				if mt.Is("application/octet-stream") || err != nil {
					mt = mimetype.Lookup(mime.TypeByExtension(path.Ext(URL.Path)))
					mediatype, params, err = mime.ParseMediaType(mt.String())
					if err != nil {
						mediatype = mt.String()
					}
					if path.Ext(URL.Path) == ".gemini" || path.Ext(URL.Path) == ".gmi" {
						mediatype = "text/gemini"
					} else if path.Ext(URL.Path) == ".scroll" || path.Ext(URL.Path) == ".abstract" {
						mediatype = "text/scroll"
					} else if path.Ext(URL.Path) == ".gophermap" || path.Base(URL.Path) == "gophermap" {
						mediatype = "application/gopher-menu"
					}
				} else {
					mediatype, params, err = mime.ParseMediaType(mt.String())
					if err != nil {
						mediatype = mt.String()
					}
				}

				reader = MultiReadCloser(io.NopCloser(bytes.NewReader(detectionData)), resp.Body)
			}

			tab.currentPageCache.Mediatype = mediatype
			tab.currentPageCache.Params = params

			HandleDocument(state, tab, URL, mediatype, params, reader, resp)
		} else if gemini.CleanStatus(resp.Status) == gemini.StatusInput {
			tab.currentPageCache.Mediatype = "text/gemini"
			tab.currentPageCache.DocType = DocumentType_InputPrompt
			tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(resp.Meta), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			resp.Body.Close()
		} else if gemini.CleanStatus(resp.Status) == gemini.StatusSensitiveInput {
			tab.currentPageCache.Mediatype = "text/gemini"
			tab.currentPageCache.DocType = DocumentType_InputPrompt_Sensitive
			tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(resp.Meta), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			resp.Body.Close()
		} else {
			tab.currentPageCache.Mediatype = "text/gemini"
			tab.currentPageCache.DocType = DocumentType_Scroll
			HandleDocument(state, tab, URL, "text/gemini", nil, io.NopCloser(strings.NewReader(gemini.StatusText(resp.Status))), nil)
			resp.Body.Close()
		}

		tab.documentLoading = false
		if prevPageUrl != nil && URL.String() == prevPageUrl.String() {
			// If same URL as previous page, overwrite its existing history
			tab.history = tab.history.Prev()
		}
		saveCurrentPageInCache(tab)
	} else if URL.Scheme == "spartan" {
		tab.documentLoading = true
		resetCurrentDocumentState(state, tab)
		tab.currentPageCache.Scroll = scrollVal
		tab.scrollAnimation.SetStaticValue(scrollVal)
		tab.scrollAnimation.SetFinished()

		var redirects int8 = 0
		var resp *spartan_client.Response
		var err error
		for {
			resp, err = spartan_client.Request(urlString, []byte{})
			if err == nil && (resp.Status == spartan_client.StatusRedirect) {
				if redirects >= state.maxRedirects {
					setInput(state, &tab.addressInput, urlString)
					tab.currentPageCache.Url = URL

					tab.currentPageCache.Mediatype = "text/gemini"
					tab.currentPageCache.DocType = DocumentType_Error
					tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Too many redirects. Max redirects: %d\n", state.maxRedirects)), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

					tab.documentLoading = false
					if URL.String() == prevPageUrl.String() {
						// If same URL as previous page, overwrite its existing history
						tab.history = tab.history.Prev()
					}
					saveCurrentPageInCache(tab)
					resp.Body.Close()
					return
				} else {
					fmt.Printf("%v\n", resp.Meta)
					URL_redirect, urlError := prevPageUrl.Parse(resp.Meta)
					if urlError != nil {
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/gemini"
						tab.currentPageCache.DocType = DocumentType_Error
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Failed to redirect: not a url ('%s')\n", resp.Meta)), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL.String() == prevPageUrl.String() {
							// If same URL as previous page, overwrite its existing history
							tab.history = tab.history.Prev()
						}
						saveCurrentPageInCache(tab)
						resp.Body.Close()
						return
					} else if URL_redirect.Scheme != "spartan" {
						// If cross-protocol redirect, prompt the user
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/gemini"
						tab.currentPageCache.DocType = DocumentType_Scroll
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Cross-protocol redirect. Click to continue:\n=> %s %s\n", URL_redirect.String(), URL_redirect.String())), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL.String() == prevPageUrl.String() {
							// If same URL as previous page, overwrite its existing history
							tab.history = tab.history.Prev()
						}
						saveCurrentPageInCache(tab)
						resp.Body.Close()
						return
					} else if URL_redirect.RawQuery != "" {
						setInput(state, &tab.addressInput, urlString)
						tab.currentPageCache.Url = URL

						tab.currentPageCache.Mediatype = "text/gemini"
						tab.currentPageCache.DocType = DocumentType_Scroll
						tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fmt.Sprintf("Redirect contains a query string. Click to continue:\n=> %s %s\n", URL_redirect.String(), URL_redirect.String())), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

						tab.documentLoading = false
						if URL.String() == prevPageUrl.String() {
							// If same URL as previous page, overwrite its existing history
							tab.history = tab.history.Prev()
						}
						saveCurrentPageInCache(tab)
						resp.Body.Close()
						return
					} else {
						// Otherwise, just do the redirect.
						redirects += 1
						prevPageUrl = URL
						urlString = URL_redirect.String()
						URL = URL_redirect
						resp.Body.Close()
						time.Sleep(time.Millisecond * 250)
						continue
					}
				}
			} else {
				break
			}
		}

		setInput(state, &tab.addressInput, urlString)
		tab.currentPageCache.Url = URL
		if err != nil {
			if fallbackUrl == "" {
				tab.currentPageCache.Mediatype = "text/gemini"
				tab.currentPageCache.DocType = DocumentType_Error
				tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader("Failed to load page."), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			} else {
				getPage(state, tab, fallbackUrl, keepScroll, "")
				return
			}
		} else if resp.Status == spartan_client.StatusSuccess {
			if resp.Meta == "" {
				resp.Meta = "text/gemini"
			}
			mediatype, params, err := mime.ParseMediaType(resp.Meta)
			if err != nil {
				fmt.Printf("Error: Couldn't parse mimetype.\n\n")
				resp.Body.Close()
				return
			}

			reader := resp.Body

			// Handle octet-stream
			if mediatype == "application/octet-stream" {
				detectionData, err := io.ReadAll(io.LimitReader(reader, 3072)) // Read 3072 bytes to try to detect mimetype
				if err != nil {
					fmt.Printf("Error: %s\n", err.Error())
					resp.Body.Close()
					return
				}
				mt, err := mimetype.DetectReader(bytes.NewReader(detectionData))
				if err != nil {
					fmt.Printf("Spartan: Couldn't detect mimetype.\n")
				}
				fmt.Printf("Detected Mimetype: %v\n", mt)
				// If couldn't detect mimetype based on file data, try using the extension instead
				if mt.Is("application/octet-stream") || err != nil {
					mt = mimetype.Lookup(mime.TypeByExtension(path.Ext(URL.Path)))
					mediatype, params, err = mime.ParseMediaType(mt.String())
					if err != nil {
						mediatype = mt.String()
					}
					if path.Ext(URL.Path) == ".gemini" || path.Ext(URL.Path) == ".gmi" {
						mediatype = "text/gemini"
					} else if path.Ext(URL.Path) == ".scroll" || path.Ext(URL.Path) == ".abstract" {
						mediatype = "text/scroll"
					} else if path.Ext(URL.Path) == ".gophermap" || path.Base(URL.Path) == "gophermap" {
						mediatype = "application/gopher-menu"
					}
				} else {
					mediatype, params, err = mime.ParseMediaType(mt.String())
					if err != nil {
						mediatype = mt.String()
					}
				}

				reader = MultiReadCloser(io.NopCloser(bytes.NewReader(detectionData)), resp.Body)
			}

			tab.currentPageCache.Mediatype = mediatype
			tab.currentPageCache.Params = params

			HandleDocument(state, tab, URL, mediatype, params, reader, resp)
		} else {
			tab.currentPageCache.Mediatype = "text/gemini"
			tab.currentPageCache.DocType = DocumentType_Scroll
			HandleDocument(state, tab, URL, "text/gemini", nil, io.NopCloser(strings.NewReader(spartan_client.StatusText(resp.Status))), nil)
			resp.Body.Close()
		}

		tab.documentLoading = false
		if prevPageUrl != nil && URL.String() == prevPageUrl.String() {
			// If same URL as previous page, overwrite its existing history
			tab.history = tab.history.Prev()
		}
		saveCurrentPageInCache(tab)
	} else if URL.Scheme == "nex" {
		tab.documentLoading = true
		resetCurrentDocumentState(state, tab)
		tab.currentPageCache.Scroll = scrollVal
		tab.scrollAnimation.SetStaticValue(scrollVal)
		tab.scrollAnimation.SetFinished()

		conn, err := nex_client.DefaultClient.Request(urlString)

		setInput(state, &tab.addressInput, urlString)
		tab.currentPageCache.Url = URL
		if err != nil {
			if fallbackUrl == "" {
				tab.currentPageCache.Mediatype = "text/nex"
				tab.currentPageCache.DocType = DocumentType_Error
				tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader("Failed to load page."), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
			} else {
				getPage(state, tab, fallbackUrl, keepScroll, "")
				return
			}
		} else {
			var reader io.Reader = conn
			mediatype := "text/nex"
			if strings.HasSuffix(URL.Path, "/") || URL.Path == "" {
				mediatype = "text/nex"
			} else {
				detectionData, err := io.ReadAll(io.LimitReader(conn, 3072)) // Read 3072 bytes to try to detect mimetype
				if err != nil {
					fmt.Printf("Error: %s\n", err.Error())
					conn.Close()
					return
				}
				mt, err := mimetype.DetectReader(bytes.NewReader(detectionData))
				if err != nil {
					fmt.Printf("Nex: Couldn't detect mimetype.\n")
				}
				// If couldn't detect mimetype based on file data, try using the extension instead
				if mt.Is("application/octet-stream") || err != nil {
					mt = mimetype.Lookup(mime.TypeByExtension(path.Ext(URL.Path)))
					mediatype = mt.String()
					if path.Ext(URL.Path) == ".gemini" || path.Ext(URL.Path) == ".gmi" {
						mediatype = "text/gemini"
					} else if path.Ext(URL.Path) == ".scroll" || path.Ext(URL.Path) == ".abstract" {
						mediatype = "text/scroll"
					} else if path.Ext(URL.Path) == ".gophermap" || path.Base(URL.Path) == "gophermap" {
						mediatype = "application/gopher-menu"
					}
				} else {
					mediatype = mt.String()
				}
				fmt.Printf("Mediatype: %s\n", mediatype)
				reader = io.MultiReader(bytes.NewReader(detectionData), conn)
			}

			mediatype, params, err := mime.ParseMediaType(mediatype)
			if err != nil {
				fmt.Printf("Error: Couldn't parse mimetype.\n\n")
				conn.Close()
				return
			}
			fmt.Printf("Mediatype: %s\n", mediatype)

			tab.currentPageCache.Mediatype = mediatype
			HandleDocument(state, tab, URL, mediatype, params, io.NopCloser(reader), conn)
			//scroll.ParseLines(reader, urlString, &state.document, &state.Headings, &state.Links)
			conn.Close()
		}

		tab.documentLoading = false
		if prevPageUrl != nil && URL.String() == prevPageUrl.String() {
			// If same URL as previous page, overwrite its existing history
			tab.history = tab.history.Prev()
		}
		saveCurrentPageInCache(tab)
	} else {
		resetCurrentDocumentState(state, tab)
		setInput(state, &tab.addressInput, urlString)
		tab.currentPageCache.Url = URL
		tab.currentPageCache.DocType = DocumentType_Error
		tab.currentPageCache.Mediatype = "text/scroll"
		tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader("Scheme not supported."), urlString, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
		if prevPageUrl != nil && URL.String() == prevPageUrl.String() {
			// If same URL as previous page, overwrite its existing history
			tab.history = tab.history.Prev()
		}
		saveCurrentPageInCache(tab)
	}
}

// Resets the input and sets it to a string
func setInput(state *State, input *strings.Builder, s string) { // TODO
	input.Reset()
	input.WriteString(s)
	if state.focusedInput == input {
		state.focusedInputCursor = input.Len()
	}
}

func refreshPage(state *State, tab *Tab) {
	runtime.UnlockOSThread()

	if tab.currentPageCache.Url != nil {
		getPage(state, tab, tab.currentPageCache.Url.String(), true, "")
	}
}

func upPage(state *State, tab *Tab) {
	runtime.UnlockOSThread()

	URL := *tab.currentPageCache.Url
	URL.Path = path.Dir(strings.TrimSuffix(tab.currentPageCache.Url.Path, "/"))
	URL.RawQuery = ""
	getPage(state, tab, URL.String(), false, "")
}

func rootPage(state *State, tab *Tab) {
	runtime.UnlockOSThread()

	URL := *tab.currentPageCache.Url
	URL.Path = "/"
	URL.RawQuery = ""
	getPage(state, tab, URL.String(), false, "")
}

func backPage(state *State, tab *Tab) {
	runtime.UnlockOSThread()

	// Set the currentPage
	tab.pageCache_Back()
	if tab.history.Value != nil && tab.history.Prev().Value != nil { // If current page and previous page are not nil
		// Set current page to be in the forwards history
		tab.currentPageCache.InForwards = true

		// Save to the cache ring
		tab.history.Value = tab.currentPageCache
	} else {
		// Go forward again and return, since the current page or previous page is nil.
		tab.pageCache_Forward()
		return
	}

	// If current page is not nil
	if tab.getCurrentPageCacheInHistory().Value != nil {
		// Set contents as currently-displayed page
		tab.documentLoading = true

		resetCurrentDocumentState(state, tab)
		pageCache := tab.getCurrentPageCacheInHistory().Value.(PageCache)
		tab.currentPageCache = pageCache
		tab.scrollAnimation.SetStaticValue(pageCache.Scroll)

		// Handle spartan input lines
		tab.spartanLineInputs = make(map[string]*strings.Builder, 5)
		for _, line := range tab.currentPageCache.Links {
			tab.spartanLineInputs[line.Url] = &strings.Builder{}
		}

		tab.posCache = make([]horusui.Point[float32], len(tab.currentPageCache.Headings)+2)

		// Set the URL in the addressbar
		setInput(state, &tab.addressInput, tab.currentPageCache.Url.String())

		switch tab.currentPageCache.DocType {
		case DocumentType_Image, DocumentType_GIF, DocumentType_PNG, DocumentType_JXL, DocumentType_WebP, DocumentType_AVIF:
			reconstructImageFromDocumentData(state, tab)
			//case DocumentType_AVIF:
		}

		tab.documentLoading = false
	}
}

func forwardPage(state *State, tab *Tab) {
	runtime.UnlockOSThread()

	// Save current page's cache to history cache again
	if tab.getCurrentPageCacheInHistory().Value != nil {
		tab.getCurrentPageCacheInHistory().Value = tab.currentPageCache
	}

	// Show the next page in the history
	if tab.getNextPageCacheInHistory().Value != nil {
		pageCache := tab.getNextPageCacheInHistory().Value.(PageCache)

		// If the next page in history is not in the forwards history, then don't go forward
		if !pageCache.InForwards {
			return
		} else {
			// Set it so it's not in the forwards history anymore
			pageCache.InForwards = false
			tab.getNextPageCacheInHistory().Value = pageCache

			// Set as currently-displayed page
			tab.documentLoading = true
			resetCurrentDocumentState(state, tab)
			tab.currentPageCache = pageCache
			tab.scrollAnimation.SetStaticValue(pageCache.Scroll)

			// Handle spartan input lines
			tab.spartanLineInputs = make(map[string]*strings.Builder, 5)
			for _, line := range tab.currentPageCache.Links {
				tab.spartanLineInputs[line.Url] = &strings.Builder{}
			}

			tab.posCache = make([]horusui.Point[float32], len(tab.currentPageCache.Headings))

			// Set the URL in the addressbar
			setInput(state, &tab.addressInput, tab.currentPageCache.Url.String())

			switch tab.currentPageCache.DocType {
			case DocumentType_Image, DocumentType_GIF, DocumentType_PNG, DocumentType_JXL, DocumentType_WebP, DocumentType_AVIF:
				reconstructImageFromDocumentData(state, tab)
			}

			tab.documentLoading = false
		}

		tab.history = tab.history.Next()
	}
}

func getAboutPage(state *State, tab *Tab, URL, file string) {
	tab.documentLoading = true
	resetCurrentDocumentState(state, tab)

	tab.currentPageCache.Url, _ = url.Parse(URL)
	setInput(state, &tab.addressInput, URL)

	tab.currentPageCache.Author = "Christian Lee Seibold"
	tab.currentPageCache.PublishDate = time.Time{}
	tab.currentPageCache.ModificationDate = time.Time{}
	tab.currentPageCache.Mediatype = "text/scroll"
	tab.currentPageCache.DocType = DocumentType_Scroll

	tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(file), URL, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
	tab.documentLoading = false

	// Handle spartan input lines
	tab.spartanLineInputs = make(map[string]*strings.Builder, 5)
	for _, line := range tab.currentPageCache.Links {
		tab.spartanLineInputs[line.Url] = &strings.Builder{}
	}

	tab.posCache = make([]horusui.Point[float32], len(tab.currentPageCache.Headings))

	saveCurrentPageInCache(tab)
}

func getAboutPages(state *State, tab *Tab) {
	tab.documentLoading = true
	resetCurrentDocumentState(state, tab)

	tab.currentPageCache.Url, _ = url.Parse("about:about")
	setInput(state, &tab.addressInput, "about:about")

	tab.currentPageCache.Author = "Christian Lee Seibold"
	tab.currentPageCache.PublishDate = time.Time{}
	tab.currentPageCache.ModificationDate = time.Time{}
	tab.currentPageCache.Mediatype = "text/scroll"
	tab.currentPageCache.DocType = DocumentType_Scroll

	fileData := `# About Pages
=> about:newtab New Tab Page
=> about:help Help
=> about:licenses Licenses
=> about:changelog Changelog

=> about:theming Theme Documentation
=> about:theme-builder Theme Builder
`
	tab.currentPageCache.Title, _ = scroll.ParseLines(strings.NewReader(fileData), "about:about", &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
	tab.documentLoading = false

	// Handle spartan input lines
	tab.spartanLineInputs = make(map[string]*strings.Builder, 5)
	for _, line := range tab.currentPageCache.Links {
		tab.spartanLineInputs[line.Url] = &strings.Builder{}
	}

	tab.posCache = make([]horusui.Point[float32], len(tab.currentPageCache.Headings))

	saveCurrentPageInCache(tab)
}

func HandleDocument(state *State, tab *Tab, URL *url.URL, mediatype string, params map[string]string, reader io.ReadCloser, resp interface{}) {
	if mediatype == "text/gemini" || mediatype == "text/nex" || mediatype == "text/scroll" || mediatype == "text/markdown" {
		if mediatype == "text/nex" {
			tab.currentPageCache.DocType = DocumentType_Nex
		} else {
			tab.currentPageCache.DocType = DocumentType_Scroll
		}

		newReader, err := getCharsetDecoderReader(params["charset"], reader)
		if err != nil {
			// TODO: Switch to binary download
			tab.currentPageCache.DocType = DocumentType_OtherBinary
		}

		tab.documentLoading = false
		switch resp := resp.(type) {
		case gemini.Response:
			resp.SetReadTimeout(0)
		case scroll.Response:
			resp.SetReadTimeout(0)
		case net.Conn:
			resp.SetReadDeadline(time.Time{})
		}
		tab.ongoingConnection = reader

		// Add root and ".." to headings
		if URL.Path != "/" && URL.Path != "" {
			tab.currentPageCache.Headings = append(tab.currentPageCache.Headings, scroll.ScrollLine_Heading{Level: 1, Text: "/"})
			tab.currentPageCache.Headings = append(tab.currentPageCache.Headings, scroll.ScrollLine_Heading{Level: 1, Text: ".."})
		}

		tab.currentPageCache.Title, _ = scroll.ParseLines(newReader, URL.String(), &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)

		// Handle spartan input lines
		tab.spartanLineInputs = make(map[string]*strings.Builder, 5)
		for _, line := range tab.currentPageCache.Links {
			tab.spartanLineInputs[line.Url] = &strings.Builder{}
		}

		tab.posCache = make([]horusui.Point[float32], len(tab.currentPageCache.Headings))

		if tab.ongoingConnection != nil {
			reader.Close()
			tab.ongoingConnection = nil
		}
	} else if mediatype == "application/gopher-menu" {
		tab.currentPageCache.DocType = DocumentType_GopherMenu
		tab.documentLoading = false
		tab.ongoingConnection = reader
		if tab.ongoingConnection != nil {
			tab.ongoingConnection = nil
			reader.Close()
		}
	} else if mediatype == "text/csv" {
		tab.currentPageCache.DocType = DocumentType_CSV
		tab.documentLoading = false
		csvReader := csv.NewReader(reader)
		csvReader.FieldsPerRecord = -1
		for {
			record, err := csvReader.Read()
			if errors.Is(err, io.EOF) {
				break
			} else if err != nil {
				tab.currentPageCache.CSV = append(tab.currentPageCache.CSV, []string{"Error: " + err.Error()})
			} else {
				if len(record) > int(tab.currentPageCache.CSVWidth) {
					tab.currentPageCache.CSVWidth = len(record)
				}
				tab.currentPageCache.CSV = append(tab.currentPageCache.CSV, record)
			}
		}
		reader.Close()
	} else if mediatype == "text/tsv" {
		tab.currentPageCache.DocType = DocumentType_CSV
		tab.documentLoading = false
		csvReader := csv.NewReader(reader)
		csvReader.Comma = '\t'
		csvReader.FieldsPerRecord = -1
		for {
			record, err := csvReader.Read()
			if errors.Is(err, io.EOF) {
				break
			} else if err != nil {
				tab.currentPageCache.CSV = append(tab.currentPageCache.CSV, []string{"Error: " + err.Error()})
			} else {
				if len(record) > int(tab.currentPageCache.CSVWidth) {
					tab.currentPageCache.CSVWidth = len(record)
				}
				tab.currentPageCache.CSV = append(tab.currentPageCache.CSV, record)
			}
		}
		reader.Close()
	} else if mediatype == "text/psv" {
		tab.currentPageCache.DocType = DocumentType_CSV
		tab.documentLoading = false
		csvReader := csv.NewReader(reader)
		csvReader.Comma = '|'
		csvReader.FieldsPerRecord = -1
		for {
			record, err := csvReader.Read()
			if errors.Is(err, io.EOF) {
				break
			} else if err != nil {
				tab.currentPageCache.CSV = append(tab.currentPageCache.CSV, []string{"Error: " + err.Error()})
			} else {
				if len(record) > int(tab.currentPageCache.CSVWidth) {
					tab.currentPageCache.CSVWidth = len(record)
				}
				tab.currentPageCache.CSV = append(tab.currentPageCache.CSV, record)
			}
		}
		reader.Close()
	} else if mediatype == "text/plain" || strings.HasPrefix(mediatype, "text/") {
		fmt.Printf("Plain text\n")
		newReader, err := getCharsetDecoderReader(params["charset"], reader)
		if err != nil {
			// TODO: Switch to binary download
			tab.currentPageCache.DocType = DocumentType_OtherBinary
		} else {
			tab.currentPageCache.DocType = DocumentType_PlainText
		}

		tab.documentLoading = false
		tab.ongoingConnection = reader
		tab.currentPageCache.Title = ParsePlainTextLines(newReader, &tab.currentPageCache.Lines, &tab.currentPageCache.Headings, &tab.currentPageCache.Links)
		if tab.ongoingConnection != nil {
			tab.ongoingConnection = nil
			reader.Close()
		}

		tab.posCache = make([]horusui.Point[float32], len(tab.currentPageCache.Headings))
	} else if IsSupportedAudio(mediatype) {
		tab.currentPageCache.DocType = DocumentType_Audio
		tab.documentLoading = false
		switch resp := resp.(type) {
		case gemini.Response:
			resp.SetReadTimeout(0)
		case scroll.Response:
			resp.SetReadTimeout(0)
		case net.Conn:
			resp.SetReadDeadline(time.Time{})
		}
		tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Audio playing in background..."))
		StreamAudio(state, mediatype, params, reader)
	} else if strings.HasPrefix(mediatype, "image/") {
		if mediatype == "image/gif" {
			tab.currentPageCache.DocType = DocumentType_GIF
		} else if mediatype == "image/apng" || mediatype == "image/png" || mediatype == "image/x-png" || mediatype == "image/vnd.mozilla.apng" {
			tab.currentPageCache.DocType = DocumentType_PNG
		} else if mediatype == "image/jxl" {
			tab.currentPageCache.DocType = DocumentType_JXL
		} else if mediatype == "image/webp" {
			tab.currentPageCache.DocType = DocumentType_WebP
		} else if mediatype == "image/avif" {
			tab.currentPageCache.DocType = DocumentType_AVIF
		} else {
			tab.currentPageCache.DocType = DocumentType_Image
		}
		//image, _, _ := image.Decode(reader)
		tab.ongoingConnection = reader
		var err error
		tab.currentPageCache.Data, err = io.ReadAll(reader)
		if err != nil {
			tab.documentLoading = false
			return
		}
		if tab.ongoingConnection != nil {
			tab.ongoingConnection = nil
			reader.Close()
		}
		reconstructImageFromDocumentData(state, tab)
		tab.documentLoading = false
	} else if strings.HasPrefix(mediatype, "video/") {
		tab.currentPageCache.DocType = DocumentType_Video
		tab.documentLoading = false
		switch resp := resp.(type) {
		case gemini.Response:
			resp.SetReadTimeout(0)
		case scroll.Response:
			resp.SetReadTimeout(0)
		case net.Conn:
			resp.SetReadDeadline(time.Time{})
		}
		if commandExists("mpv") {
			state.videoPlayerAnimation.Show()
			state.videoPlaying = reader
			tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Video playing in mpv..."))
			StreamVideo(state, mediatype, params, reader, VideoProgram_MPV)
			if state.videoPlaying != nil {
				state.videoPlayerAnimation.Hide()
				go func() {
					// Give video player animation a chance to run before closing the video.
					time.Sleep(time.Millisecond * time.Duration(state.videoPlayerAnimation.TotalMS()+5))
					reader.Close()
					state.videoPlaying = nil
				}()
			}
		} else if commandExists("ffplay") {
			state.videoPlayerAnimation.Show()
			state.videoPlaying = reader
			tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Video playing in ffplay..."))
			StreamVideo(state, mediatype, params, reader, VideoProgram_FFPLAY)
			if state.videoPlaying != nil {
				state.videoPlayerAnimation.Hide()
				go func() {
					// Give video player animation a chance to run before closing the video.
					time.Sleep(time.Millisecond * time.Duration(state.videoPlayerAnimation.TotalMS()+5))
					reader.Close()
					state.videoPlaying = nil
				}()
			}
		} else if commandExists("vlc") {
			state.videoPlayerAnimation.Show()
			state.videoPlaying = reader
			tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Video playing in vlc..."))
			StreamVideo(state, mediatype, params, reader, VideoProgram_VLC)
			if state.videoPlaying != nil {
				state.videoPlayerAnimation.Hide()
				go func() {
					// Give video player animation a chance to run before closing the video.
					time.Sleep(time.Millisecond * time.Duration(state.videoPlayerAnimation.TotalMS()+5))
					reader.Close()
					state.videoPlaying = nil
				}()
			}
		} else {
			// Just download the file
			tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Mpv, ffplay, and vlc not found. Downloading video..."))
			DownloadFile(reader, URL)
			tab.currentPageCache.Lines = nil
			tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Downloaded."))
			reader.Close()
		}
	} else {
		tab.currentPageCache.DocType = DocumentType_OtherBinary
		tab.documentLoading = false
		tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Downloading file..."))
		DownloadFile(reader, URL)
		tab.currentPageCache.Lines = nil
		tab.currentPageCache.Lines = append(tab.currentPageCache.Lines, scroll.ScrollLine_Text("Downloaded."))
		//state.document = append(state.document, scroll.ScrollLine_Link{"file://"}))
		reader.Close()
	}
}

// as util
func commandExists(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}

func reconstructImageFromDocumentData(state *State, tab *Tab) {
	data := tab.currentPageCache.Data
	if tab.currentPageCache.DocType == DocumentType_GIF {
		img, err := gif.DecodeAll(bytes.NewReader(data))
		if err != nil {
			return
		}
		animation := NewGifAnimation(img)
		tab.documentGifPlayer = NewGifPlayer(state.renderer, animation)
	} else if tab.currentPageCache.DocType == DocumentType_PNG {
		img, err := apng.DecodeAll(bytes.NewReader(data))
		if err != nil {
			return
		}
		animation := NewAPNGAnimation(img)
		tab.documentAPNGPlayer = NewAPNGPlayer(state.renderer, animation)
	} else if tab.currentPageCache.DocType == DocumentType_JXL {
		img, err := jpegxl.DecodeAll(bytes.NewReader(data))
		if err != nil {
			return
		}
		animation := NewJxlAnimation(img)
		tab.documentJxlPlayer = NewJxlPlayer(state.renderer, animation)
		/*} else if tab.currentPageCache.DocType == DocumentType_WebP {
		decoder, err := webp.NewAnimationDecoder(data)
		if err != nil {
			return
		}
		animation, err := decoder.Decode()
		if err != nil {
			return
		}
		tab.documentWebPPlayer = NewWebPPlayer(state.renderer, NewWebPAnimation(animation))
		decoder.Close()*/
	} else if tab.currentPageCache.DocType == DocumentType_AVIF {
		img, err := avif.DecodeAll(bytes.NewReader(data))
		if err != nil {
			return
		}
		animation := NewAVIFAnimation(img)
		tab.documentAVIFPlayer = NewAVIFPlayer(state.renderer, animation)
	} else {
		rwops, err := sdl.RWFromMem(data)
		if err != nil {
			return
		}
		tab.documentImgSurface, err = img.LoadRW(rwops, true)
		if err != nil {
			return
		}
	}
}

// TODO: Check if the abstract of the file is available.
func StreamAudio(state *State, mediatype string, params map[string]string, reader io.ReadCloser) {
	speakerSampleRate := beep.SampleRate(44100)
	cacheDuration := time.Duration(float32(time.Second) * 1.5)

	state.audioPlayerAnimation.Show()

	if mediatype == "audio/mpeg" || mediatype == "audio/mp3" || mediatype == "audio/x-mpeg" {
		if !state.speakerInited {
			speaker.Init(speakerSampleRate, speakerSampleRate.N(cacheDuration))
			state.speakerInited = true
		} else {
			speaker.Resume()
		}
		initialBuffer, err := io.ReadAll(io.LimitReader(reader, 40*1024*2)) // 320 kbps * 2 seconds (assumes 320 kbps mp3 files)
		if err != nil {
			fmt.Printf("Error: Failed to stream mp3 file.\n\n")
			return
		}

		multireadcloser := MultiReadCloser(io.NopCloser(bytes.NewReader(initialBuffer)), reader)
		state.audioReader = reader
		stream, format, err := mp3.Decode(multireadcloser)
		if err != nil {
			fmt.Printf("Error: Failed to stream mp3 file.\n\n")
			return
		}
		state.currentStream = stream
		state.currentStreamFormat = format

		resampled := beep.Resample(4, format.SampleRate, speakerSampleRate, stream)
		state.currentStreamVolume = effects.Volume{Streamer: resampled, Base: 2, Volume: 0, Silent: false}
		speaker.PlayAndWait(&state.currentStreamVolume)
		stream.Close()
		speaker.Suspend()
	} else if mediatype == "audio/ogg" || mediatype == "audio/x-ogg" || mediatype == "application/ogg" || mediatype == "application/x-ogg" {
		if !state.speakerInited {
			speaker.Init(speakerSampleRate, speakerSampleRate.N(cacheDuration))
			state.speakerInited = true
		} else {
			speaker.Resume()
		}
		initialBuffer, err := io.ReadAll(io.LimitReader(reader, 8*1024*2)) // 128 kbps * 2 seconds (assumes 128 kbps vorbis files)
		if err != nil {
			fmt.Printf("Error: Failed to stream mp3 file.\n\n")
			return
		}

		// TODO: Use params["codecs"]?
		state.audioReader = MultiReadCloser(io.NopCloser(bytes.NewReader(initialBuffer)), reader)
		stream, format, err := vorbis.Decode(state.audioReader)
		if err != nil {
			fmt.Printf("Error: Failed to stream ogg file.\n\n")
			return
		}
		state.currentStream = stream
		state.currentStreamFormat = format

		resampled := beep.Resample(4, format.SampleRate, speakerSampleRate, stream)
		speaker.PlayAndWait(resampled)
		stream.Close()
		speaker.Suspend()
	} else if mediatype == "audio/flac" || mediatype == "audio/x-flac" {
		if !state.speakerInited {
			speaker.Init(speakerSampleRate, speakerSampleRate.N(cacheDuration))
			state.speakerInited = true
		} else {
			speaker.Resume()
		}
		initialBuffer, err := io.ReadAll(io.LimitReader(reader, 175*1024*2)) // 1400 kbps * 2 seconds (assumes 1400 kbps flac files)
		if err != nil {
			fmt.Printf("Error: Failed to stream flac file.\n\n")
			return
		}

		state.audioReader = MultiReadCloser(io.NopCloser(bytes.NewReader(initialBuffer)), reader)
		stream, format, err := flac.Decode(state.audioReader)
		if err != nil {
			fmt.Printf("Error: Failed to stream flac file.\n\n")
			return
		}
		state.currentStream = stream
		state.currentStreamFormat = format

		resampled := beep.Resample(4, format.SampleRate, speakerSampleRate, stream)
		speaker.PlayAndWait(resampled)
		stream.Close()
		speaker.Suspend()
	} else if mediatype == "audio/wav" || mediatype == "audio/wave" || mediatype == "audio/x-wav" || mediatype == "audio/vnd.wave" {
		if !state.speakerInited {
			speaker.Init(speakerSampleRate, speakerSampleRate.N(cacheDuration))
			state.speakerInited = true
		} else {
			speaker.Resume()
		}
		initialBuffer, err := io.ReadAll(io.LimitReader(reader, 177*1024*2)) // 1411 kbps * 2 seconds (assumes 1411 kbps wav files)
		if err != nil {
			fmt.Printf("Error: Failed to stream wav file.\n\n")
			return
		}

		stream, format, err := wav.Decode(MultiReadCloser(io.NopCloser(bytes.NewReader(initialBuffer)), reader))
		if err != nil {
			fmt.Printf("Error: Failed to stream wav file.\n\n")
			return
		}
		state.currentStream = stream
		state.currentStreamFormat = format

		resampled := beep.Resample(4, format.SampleRate, speakerSampleRate, stream)
		speaker.PlayAndWait(resampled)
		stream.Close()
		speaker.Suspend()
	}

	state.audioPlayerAnimation.Hide()
	go func() {
		// Give audio player animation a chance to run before closing the audio.
		time.Sleep(time.Millisecond * time.Duration(state.audioPlayerAnimation.TotalMS()+5))
		state.currentStream = nil
		state.currentStreamVolume = effects.Volume{}
	}()
}

type VideoProgram uint8

const (
	VideoProgram_MPV VideoProgram = iota
	VideoProgram_VLC
	VideoProgram_FFPLAY
)

func StreamVideo(state *State, mediatype string, params map[string]string, readCloser io.ReadCloser, program VideoProgram) {
	var cmd *exec.Cmd
	if program == VideoProgram_MPV {
		cmd = exec.Command("mpv", "-", "--demuxer-lavf-probesize=32000", "--demuxer-thread=yes", "--cache=yes")
	} else if program == VideoProgram_VLC {
		cmd = exec.Command("vlc", "-")
	} else if program == VideoProgram_FFPLAY {
		cmd = exec.Command("ffplay", "-")
	}
	stdin, err := cmd.StdinPipe()
	if err != nil {
		fmt.Printf("Error: Cannot open pipe to mpv.\n")
	}

	err = cmd.Start()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
	}
	io.Copy(stdin, readCloser)
	fmt.Printf("Finished downloading video...\n")
	err = cmd.Wait()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
	}
}

func VideoDataLink(state *State, URL string, program VideoProgram) {
	var cmd *exec.Cmd
	if program == VideoProgram_MPV {
		cmd = exec.Command("mpv", URL)
	} else if program == VideoProgram_VLC {
		cmd = exec.Command("vlc", URL)
	} else if program == VideoProgram_FFPLAY {
		cmd = exec.Command("ffplay", URL)
	}
	err := cmd.Start()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
	}
	err = cmd.Wait()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
	}
}

func DownloadFile(reader io.Reader, URL *url.URL) string {
	homeDir, home_err := os.UserHomeDir()
	if home_err != nil {
		return ""
	}
	// Download the file
	newfile := filepath.Join(homeDir, "Downloads", path.Base(URL.Path))
	var file *os.File
	var err error
	i := 0
	for {
		file, err = os.OpenFile(newfile, os.O_CREATE|os.O_SYNC|os.O_WRONLY, 0600)
		if errors.Is(err, os.ErrExist) && i < 15 {
			// Retry by appending a random integer to the file.
			base := path.Base(URL.Path)
			without_extension := strings.TrimSuffix(base, path.Ext(base))
			i += 1
			newfile = filepath.Join(homeDir, "Downloads", without_extension+"_"+strconv.Itoa(i)+path.Ext(base))
			continue
		} else if err != nil {
			fmt.Printf("Error: Failed to create file.\n")
			return ""
		}
		break
	}
	defer file.Close()
	_, err = io.Copy(file, reader)
	if err != nil {
		fmt.Printf("Error: Failed to write to file: %v\n", err.Error())
		return ""
	}
	file.Sync()
	fmt.Printf("File downloaded to %s.\n", newfile)

	return newfile
}

func ParsePlainTextLines(reader io.Reader, lines *[]scroll.ScrollLine, headings *[]scroll.ScrollLine_Heading, links *[]scroll.ScrollLine_Link) string {
	if links == nil {
		links = &[]scroll.ScrollLine_Link{}
	}

	title := ""

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		scanned_text := scanner.Text()

		line := strings.TrimRight(scanned_text, "\r\n")
		if title == "" && strings.TrimSpace(line) != "" {
			if scroll.ContainsLetterRunes(line) {
				// Assume for nex documents that the first non-blank line is the title
				title = strings.TrimSpace(line)
			}
		}

		*lines = append(*lines, scroll.ScrollLine_Text(line))
	}

	return title
}
