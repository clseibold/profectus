package main

import (
	"bytes"
	"fmt"
	"io"
	"math"
	"net/url"
	"os"
	"path/filepath"
	"time"
	"unicode/utf8"

	alan "github.com/amitybell/piper-voice-alan"
	"github.com/nabbl/piper"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/clseibold/profectus/horusui"
)

type Events struct {
}

var _mouseDown = [5]bool{false, false, false, false, false} // Left, Middle, Right, Back, Forward
var _mouseDownX int32 = 0
var _mouseDownY int32 = 0

func (state *State) FilterEvent(e sdl.Event, userdata interface{}) bool {
	if state.guiState == nil {
		return false
	}
	switch e := e.(type) {
	case sdl.QuitEvent:
		state.running = false
	case sdl.WindowEvent:
		switch e.Event {
		case sdl.WINDOWEVENT_RESIZED, sdl.WINDOWEVENT_SIZE_CHANGED:
			handleWindowResize(state, e, state.Window)
			state.getActiveTab().firstFrame = true
		case sdl.WINDOWEVENT_SHOWN, sdl.WINDOWEVENT_MAXIMIZED, sdl.WINDOWEVENT_RESTORED, sdl.WINDOWEVENT_FOCUS_GAINED, sdl.WINDOWEVENT_ENTER:
			state.noGuiUpdate = false
		case sdl.WINDOWEVENT_EXPOSED:
			// Redraw the window on expose, because SDL finally added a workaround to the main thread being blocked on resize.
			// NOTE: Commented out because it doesn't work properly'
			// state.lastTime = state.nowTime
			// state.nowTime = sdl.GetPerformanceCounter()
			// state.deltaTime = float64(float64(state.nowTime-state.lastTime) / float64(sdl.GetPerformanceFrequency()))

			state.noGuiUpdate = false
			// update(state, state.deltaTime)
			// render(state, state.deltaTime)
		case sdl.WINDOWEVENT_HIDDEN, sdl.WINDOWEVENT_MINIMIZED:
			state.noGuiUpdate = true
		case sdl.WINDOWEVENT_DISPLAY_CHANGED:
			displayIndex, err := state.Window.GetDisplayIndex()
			if err != nil {
				panic(err)
			}
			_, hdpi, _, dpi_err := sdl.GetDisplayDPI(displayIndex)
			if dpi_err != nil {
				fmt.Println("Failed to get Window's Display DPI.")
				return false
			}
			fmt.Printf("Display changed; new dpi: %f\n", hdpi)

			state.guiState.UserScale = float64(pixelRatio(state, true)) //+ 0.5
			state.noGuiUpdate = true
			freeFonts(state)
			setupFonts(state, state.guiState.UserScale)
			state.getActiveTab().firstFrame = true
			state.noGuiUpdate = false
		}
	case sdl.MouseMotionEvent:
		state.guiState.SetMouseInput(int32(float32(e.X)*pixelRatio(state, false)), int32(float32(e.Y)*pixelRatio(state, false)))

		// If mouse moved while a mouse button is down *and* the mouse moved more than 5 (scaled) pixels, then set drag to true.
		if math.Abs(float64(e.X)*float64(pixelRatio(state, false))-float64(_mouseDownX)) > 10 || math.Abs(float64(e.Y)*float64(pixelRatio(state, false))-float64(_mouseDownY)) > 10 {
			if _mouseDown[0] { // Left
				state.guiState.Drag[horusui.MouseButton_Left] = true
				state.guiState.DragOrigX = _mouseDownX
				state.guiState.DragOrigY = _mouseDownY
			} else if _mouseDown[1] { // Middle
				state.guiState.Drag[horusui.MouseButton_Middle] = true
				state.guiState.DragOrigX = _mouseDownX
				state.guiState.DragOrigY = _mouseDownY
			} else if _mouseDown[2] { // Right
				state.guiState.Drag[horusui.MouseButton_Right] = true
				state.guiState.DragOrigX = _mouseDownX
				state.guiState.DragOrigY = _mouseDownY
			}
		}

		if state.guiState.Click[horusui.MouseButton_Left] {
			state.guiState.Click[horusui.MouseButton_Left] = false
		}
		if state.guiState.Click[horusui.MouseButton_Right] {
			state.guiState.Click[horusui.MouseButton_Right] = false
		}
		if state.guiState.Click[horusui.MouseButton_Middle] {
			state.guiState.Click[horusui.MouseButton_Middle] = false
		}
		if state.guiState.Click[horusui.MouseButton_Back] {
			state.guiState.Click[horusui.MouseButton_Back] = false
		}
		if state.guiState.Click[horusui.MouseButton_Forward] {
			state.guiState.Click[horusui.MouseButton_Forward] = false
		}
	case sdl.MouseWheelEvent:
		if e.Which == sdl.TOUCH_MOUSEID { // For Touchscreens
			state.activeTab.scrollAnimation.SetNewTotalMS(20, true)
			state.activeTab.outlineScroll.SetNewTotalMS(20, true)
		} else { // Touchpads and physical mice
			state.activeTab.scrollAnimation.SetNewTotalMS(55, true)
			state.activeTab.outlineScroll.SetNewTotalMS(55, true)
		}
		state.guiState.WheelDir = e.PreciseY * pixelRatio(state, false)
	case sdl.MouseButtonEvent:
		switch e.Type {
		case sdl.MOUSEBUTTONDOWN:
			state.guiState.SetMouseInput(int32(float32(e.X)*pixelRatio(state, false)), int32(float32(e.Y)*pixelRatio(state, false)))

			if e.Button == sdl.ButtonLeft { // Left Button
				_mouseDown[0] = true
			} else if e.Button == sdl.ButtonMiddle { // Middle Button
				_mouseDown[1] = true
			} else if e.Button == sdl.ButtonRight { // Right Button
				_mouseDown[2] = true
			} else if e.Button == sdl.ButtonX1 { // Back Button
				_mouseDown[3] = true
			} else if e.Button == sdl.ButtonX2 { // Forward Button
				_mouseDown[4] = true
			}
			//mouseDown = true;
			_mouseDownX = int32(float32(e.X) * pixelRatio(state, false))
			_mouseDownY = int32(float32(e.Y) * pixelRatio(state, false))
		case sdl.MOUSEBUTTONUP:
			state.guiState.SetMouseInput(int32(float32(e.X)*pixelRatio(state, false)), int32(float32(e.Y)*pixelRatio(state, false)))

			if e.Button == sdl.ButtonLeft { // Left Button
				_mouseDown[0] = false
				if !state.guiState.Drag[horusui.MouseButton_Left] {
					state.guiState.Click[horusui.MouseButton_Left] = true
				}
			} else if e.Button == sdl.ButtonMiddle { // Middle Button
				_mouseDown[1] = false
				if !state.guiState.Drag[horusui.MouseButton_Middle] {
					state.guiState.Click[horusui.MouseButton_Middle] = true
				}
			} else if e.Button == sdl.ButtonRight { // Right Button
				_mouseDown[2] = false
				if !state.guiState.Drag[horusui.MouseButton_Right] {
					state.guiState.Click[horusui.MouseButton_Right] = true
				}
			} else if e.Button == sdl.ButtonX1 { // Back Button
				_mouseDown[3] = false
				//if !state.guiState.Drag[horusui.MouseButton_Back] {
				state.guiState.Click[horusui.MouseButton_Back] = true
				//}
				//flipPage(state, state.prevPages);
			} else if e.Button == sdl.ButtonX2 { // Forward Button
				_mouseDown[4] = false
				//if !state.guiState.Drag[horusui.MouseButton_Forward] {
				state.guiState.Click[horusui.MouseButton_Forward] = true
				//}
			}

			if state.guiState.Drag[horusui.MouseButton_Left] {
				state.guiState.Drag[horusui.MouseButton_Left] = false
			} else if state.guiState.Drag[horusui.MouseButton_Middle] {
				state.guiState.Drag[horusui.MouseButton_Middle] = false
			} else if state.guiState.Drag[horusui.MouseButton_Right] {
				state.guiState.Drag[horusui.MouseButton_Right] = false
			}
		}
	case sdl.KeyboardEvent:
		switch e.Type {
		case sdl.KEYDOWN:
			keysym := e.Keysym
			// char := rune(e.Keysym.Sym)

			// shift := (sdl.KMOD_LSHIFT & keysym.Mod) == sdl.KMOD_LSHIFT
			// caps := (sdl.KMOD_CAPS & keysym.Mod) == sdl.KMOD_CAPS
			ctrl := (sdl.KMOD_LCTRL&keysym.Mod) == sdl.KMOD_LCTRL || (sdl.KMOD_RCTRL&keysym.Mod) == sdl.KMOD_RCTRL

			if state.focusedInput != nil {
				switch keysym.Sym {
				case sdl.K_BACKSPACE:
					if state.focusedInput.Len() > 0 {
						currentString := state.focusedInput.String()
						_, newEnd := utf8.DecodeLastRuneInString(currentString)
						currentString = currentString[:len(currentString)-newEnd]
						state.focusedInput.Reset()
						state.focusedInput.WriteString(currentString)
						state.focusedInputCursor -= newEnd
						if state.focusedInputCursor < 0 {
							state.focusedInputCursor = 0
						}
					}
				case sdl.K_v:
					if ctrl {
						clipboardText, err := sdl.GetClipboardText()
						if err != nil {
							return false
						}
						state.focusedInput.WriteString(clipboardText)
						state.focusedInputCursor += len(clipboardText)
					}
				case sdl.K_KP_ENTER, sdl.K_RETURN: // Keypad Enter and main Enter key
					activeTab := state.getActiveTab()
					if state.focusedInput == &activeTab.addressInput {
						go getPage(state, activeTab, activeTab.addressInput.String(), false, "")
					} else if state.focusedInput == &activeTab.documentInput && !activeTab.documentLoading {
						newUrl := *activeTab.currentPageCache.Url
						newUrl.RawQuery = url.QueryEscape(activeTab.documentInput.String())
						go getPage(state, activeTab, newUrl.String(), false, "")
						activeTab.documentInput.Reset()
					}

					// Check for Spartan Input textboxes
					if activeTab.spartanLineInputs != nil {
						for URL, builder := range activeTab.spartanLineInputs {
							if state.focusedInput == builder {
								u, err := url.Parse(URL)
								if err == nil {
									u.RawQuery = builder.String()
									go getPage(state, activeTab, u.String(), false, "")
								}
								break
							}
						}
					}
				}
			} else {
				switch keysym.Sym {
				case sdl.K_LCTRL, sdl.K_RCTRL:
					state.ctrl = true
				case sdl.K_LALT, sdl.K_RALT:
					state.alt = true
				case sdl.K_LSHIFT, sdl.K_RSHIFT:
					state.shift = true
				case sdl.K_DOWN:
					activeTab := state.getActiveTab()
					activeTab.currentPageCache.Scroll += float32(1.5) * 2.75 * float32(state.documentFonts[ScrollLineType_Body].Font.LineSkip()) /*horusui.Scale_value(guiState, float32(37))*/
					if activeTab.currentPageCache.Scroll < 0 {
						activeTab.currentPageCache.Scroll = 0
					}
					activeTab.scrollAnimation.SetNewTotalMS(150, false)
					activeTab.scrollAnimation.SaveCurrentAndSetNewEnd(activeTab.currentPageCache.Scroll)
				case sdl.K_UP:
					activeTab := state.getActiveTab()
					activeTab.currentPageCache.Scroll -= float32(1.5) * 2.75 * float32(state.documentFonts[ScrollLineType_Body].Font.LineSkip()) /*horusui.Scale_value(guiState, float32(37))*/
					if activeTab.currentPageCache.Scroll < 0 {
						activeTab.currentPageCache.Scroll = 0
					}
					activeTab.scrollAnimation.SetNewTotalMS(150, false)
					activeTab.scrollAnimation.SaveCurrentAndSetNewEnd(activeTab.currentPageCache.Scroll)
				}
			}
		case sdl.KEYUP:
			keysym := e.Keysym
			shift := (sdl.KMOD_LSHIFT&keysym.Mod) == sdl.KMOD_LSHIFT || (sdl.KMOD_LSHIFT&keysym.Mod) == sdl.KMOD_RSHIFT
			ctrl := (sdl.KMOD_LCTRL&keysym.Mod) == sdl.KMOD_LCTRL || (sdl.KMOD_RCTRL&keysym.Mod) == sdl.KMOD_RCTRL
			alt := (sdl.KMOD_LALT&keysym.Mod) == sdl.KMOD_LALT || (sdl.KMOD_RALT&keysym.Mod) == sdl.KMOD_RALT || (sdl.KMOD_ALT&keysym.Mod) == sdl.KMOD_ALT
			if state.focusedInput == nil {
				switch keysym.Sym {
				case sdl.K_LCTRL, sdl.K_RCTRL:
					state.ctrl = false
				case sdl.K_LALT, sdl.K_RALT:
					state.alt = false
				case sdl.K_LSHIFT, sdl.K_RSHIFT:
					state.shift = false
				case sdl.K_F5:
					go refreshPage(state, state.getActiveTab())
				case sdl.K_PLUS, sdl.K_KP_PLUS, sdl.K_EQUALS:
					if ctrl && shift && !alt {
						state.noGuiUpdate = true
						newScale := state.guiState.UserScale + 0.125
						state.userScaleAnimation.SaveCurrentAndSetNewEnd(newScale)
						freeFonts(state)
						setupFonts(state, newScale)
						state.getActiveTab().firstFrame = true
						state.noGuiUpdate = false
					} else if ctrl && !shift && alt {
						if !currentThemeAnimation.Animating {
							newTheme := currentTheme
							newTheme.Page.MaxWidth += 105
							newTheme.Page.MaxImageWidth += 105
							newTheme.Page.NexCharWidth += 5
							currentThemeAnimation = NewThemeAnimation(currentTheme, newTheme, 200 /*140*/)
						}
					}
				case sdl.K_MINUS, sdl.K_KP_MINUS, sdl.K_UNDERSCORE:
					if ctrl && shift && !alt {
						state.noGuiUpdate = true
						newScale := state.guiState.UserScale - 0.125
						state.userScaleAnimation.SaveCurrentAndSetNewEnd(newScale)
						freeFonts(state)
						setupFonts(state, newScale)
						state.getActiveTab().firstFrame = true
						state.noGuiUpdate = false
					} else if ctrl && !shift && alt {
						if !currentThemeAnimation.Animating {
							newTheme := currentTheme
							newTheme.Page.MaxWidth -= 105
							newTheme.Page.MaxImageWidth -= 105
							newTheme.Page.NexCharWidth -= 5
							currentThemeAnimation = NewThemeAnimation(currentTheme, newTheme, 200 /*140*/)
						}
					}
				case sdl.K_w:
					if ctrl {
						activeTab := state.getActiveTab()
						activeTab.widthPercentAnimation.Hide()
						go func(tab *Tab) {
							time.Sleep(time.Millisecond*time.Duration(tab.widthPercentAnimation.TotalMS()) + 5)
							tab.Close(state)
						}(state.activeTab)
					}
				case sdl.K_b, sdl.K_KP_B:
					if ctrl {
						state.sidebarAnimation.Toggle()
					}
				case sdl.K_s:
					if ctrl {
						fmt.Printf("Testing\n")
						tts, tts_err := piper.New(filepath.Join(state.configDirectory, "tts"), alan.Asset)
						if tts_err != nil {
							panic(tts_err)
						}
						go func() {
							//var wave []byte
							//buf := bytes.NewBuffer(wave)
							for _, line := range state.activeTab.currentPageCache.Lines {
								if line.String() == "" || line.String() == "\n" {
									continue
								}
								w, _ := tts.Synthesize(line.String())
								StreamAudio(state, "audio/wav", map[string]string{}, io.NopCloser(bytes.NewReader(w)))
								//buf.Write(w)
							}
							//go StreamAudio(state, "audio/wav", map[string]string{}, io.NopCloser(bytes.NewReader(buf.Bytes())))
						}()
					}
				case sdl.K_t:
					if ctrl && !alt {
						// Open new tab and switch to it
						newTab := state.NewTab()
						state.setActiveTab(newTab)
					} else if alt {
						// Toggle through themes
						backwards := false
						if shift {
							backwards = true
						}
						dir, err := os.ReadDir(state.themesDirectory)
						if err == nil {
							var nextTheme Theme
							nextPath := ""
							firstPath := ""
							getNext := false
							getLast := false
							for i, entry := range dir {
								if entry.IsDir() {
									continue
								} else if filepath.Ext(entry.Name()) != ".json" || entry.Name() == "." || entry.Name() == ".." {
									continue
								}

								entryPath := filepath.Join(state.themesDirectory, entry.Name())
								if getLast && i == len(dir)-1 {
									nextPath = entryPath
									break
								}
								if firstPath == "" {
									firstPath = entryPath
								}
								if entryPath == filepath.Clean(currentTheme.Filepath) {
									if backwards && i != 0 {
										// Use previous path, if it's set
										break
									} else if backwards && i == 0 {
										// Get the last file
										getLast = true
										continue
									} else if !backwards && i == len(dir)-1 {
										nextPath = firstPath
										break
									} else {
										getNext = true
										continue
									}
								}
								if backwards {
									nextPath = entryPath
								}
								if getNext {
									nextPath = entryPath
									break
								}
							}
							nextTheme = LoadThemeFromFile(nextPath)
							state.settings.ThemeName = nextTheme.Name
							SaveSettingsToFile(filepath.Join(state.configDirectory, "settings.json"), state.settings)
							currentThemeAnimation = NewThemeAnimation(currentTheme, nextTheme, 200 /*140*/)
						}
					}
				}
			}
		}
	case sdl.TextInputEvent:
		if state.focusedInput == nil {
			break
		}

		// TODO: Write the text at the cursor location
		text := e.GetText()
		state.focusedInput.WriteString(text)
		state.focusedInputCursor += len(text)
	case sdl.TextEditingEvent:
		// TODO
		if state.focusedInput == nil {
			break
		}
	case sdl.OSEvent:
		/*if e.Type == sdl.APP_LOWMEMORY {

		}*/
	}
	return false
}

func handleWindowResize(state *State, e sdl.WindowEvent, window *sdl.Window) {
	ow, oh, _ := state.renderer.GetOutputSize()
	state.guiState.WindowSize.W, state.guiState.WindowSize.H = ow, oh
}
