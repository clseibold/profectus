// Package spartan_client provides an easy interface to create client and servers that
// speak the Spartan protocol.
//
// It will automatically handle URLs that have IDNs in them, i.e. domains with Unicode.
// It will convert to punycode for DNS and for sending to the server, but accept
// certs with either punycode or Unicode as the hostname.
//
// This also applies to hosts, for functions where a host can be passed specifically.
package spartan_client
