package spartan_client

import (
	"bufio"
	"fmt"
	"io"
	"net/url"
	"strings"
	"time"
	"unicode"
)

type SpartanLine interface {
	String() string
	ImplementsScrollLine()
}

type SpartanLine_Heading struct {
	Level int
	Text  string
}
type SpartanLine_Quote struct {
	Level int
	Text  string
}
type SpartanLine_UnorderedListItem struct {
	Level int
	Text  string
}
type SpartanLine_OrderedListItem struct {
	Level int
	Label string
	Text  string
}
type SpartanLine_Link struct {
	Url      string
	Title    string
	Relation int
	Tag      string
	Input    bool
	Time     time.Time
}
type SpartanLine_Text string
type SpartanLine_PreformattedText string
type SpartanLine_PreformattedToggle string

func (heading SpartanLine_Heading) ImplementsScrollLine()                       {}
func (quote SpartanLine_Quote) ImplementsScrollLine()                           {}
func (listItem SpartanLine_UnorderedListItem) ImplementsScrollLine()            {}
func (listItem SpartanLine_OrderedListItem) ImplementsScrollLine()              {}
func (link SpartanLine_Link) ImplementsScrollLine()                             {}
func (text SpartanLine_Text) ImplementsScrollLine()                             {}
func (preformattedText SpartanLine_PreformattedText) ImplementsScrollLine()     {}
func (preformattedToggle SpartanLine_PreformattedToggle) ImplementsScrollLine() {}

func (heading SpartanLine_Heading) String() string {
	return fmt.Sprintf("%s%s", strings.Repeat("#", heading.Level), heading.Text)
}
func (quote SpartanLine_Quote) String() string {
	return fmt.Sprintf("%s%s", strings.Repeat(">", quote.Level), quote.Text)
}
func (listItem SpartanLine_UnorderedListItem) String() string {
	return fmt.Sprintf("%s %s", strings.Repeat("*", listItem.Level), listItem.Text)
}
func (listItem SpartanLine_OrderedListItem) String() string {
	return fmt.Sprintf("%s %s. %s", strings.Repeat("*", listItem.Level), listItem.Label, listItem.Text)
}
func (link SpartanLine_Link) String() string {
	if link.Input {
		return fmt.Sprintf("=: %s %s", link.Url, link.Title)
	} else {
		return fmt.Sprintf("=> %s %s", link.Url, link.Title)
	}
}
func (text SpartanLine_Text) String() string {
	return strings.Clone(string(text))
}
func (preformattedText SpartanLine_PreformattedText) String() string {
	return string(preformattedText)
}
func (preformattedToggle SpartanLine_PreformattedToggle) String() string {
	return fmt.Sprintf("```%s", string(preformattedToggle))
}

/*
func ParseLines(reader io.Reader) []ScrollLine {
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()

	}
}*/

func ParseLines(reader io.Reader, currentURLString string, lines *[]SpartanLine, headings *[]SpartanLine_Heading, links *[]SpartanLine_Link) (string, bool) {
	currentURL, err := url.Parse(currentURLString)
	if err != nil { // TODO: Return some error
		return "", false
	}
	if links == nil {
		links = &[]SpartanLine_Link{}
	}

	title := ""
	lastTitleLevel := 5
	isFeed := false

	scanner := bufio.NewScanner(reader)
	inPreformat := false
	for scanner.Scan() {
		scanned_text := scanner.Text()

		line := strings.TrimRight(scanned_text, "\r\n")
		if title == "" && strings.TrimSpace(line) != "" {
			if ContainsLetterRunes(line) {
				// Assume for nex documents that the first non-blank line is the title
				title = strings.TrimSpace(line)
			}
		}
		if inPreformat {
			if strings.HasPrefix(line, "```") {
				*lines = append(*lines, SpartanLine_PreformattedToggle(strings.TrimPrefix(line, "```")))
				inPreformat = false
				continue
			}
			*lines = append(*lines, SpartanLine_PreformattedText(line))
			continue
		}

		if strings.HasPrefix(line, "```") {
			*lines = append(*lines, SpartanLine_PreformattedToggle(strings.TrimPrefix(line, "```")))
			inPreformat = !inPreformat
		} else if strings.HasPrefix(line, "####") {
			text := strings.TrimLeft(strings.TrimPrefix(line, "####"), " \t")
			if title == "" || lastTitleLevel > 4 {
				title = text
				lastTitleLevel = 4
			}
			if headings != nil {
				*headings = append(*headings, SpartanLine_Heading{4, text})
			}

			*lines = append(*lines, SpartanLine_Heading{4, text})
		} else if strings.HasPrefix(line, "###") {
			text := strings.TrimLeft(strings.TrimPrefix(line, "###"), " \t")
			if title == "" || lastTitleLevel > 3 {
				title = text
				lastTitleLevel = 3
			}
			if headings != nil {
				*headings = append(*headings, SpartanLine_Heading{3, text})
			}

			*lines = append(*lines, SpartanLine_Heading{3, text})
		} else if strings.HasPrefix(line, "##") {
			text := strings.TrimLeft(strings.TrimPrefix(line, "##"), " \t")
			if title == "" || lastTitleLevel > 2 {
				title = text
				lastTitleLevel = 2
			}
			if headings != nil {
				*headings = append(*headings, SpartanLine_Heading{2, text})
			}

			*lines = append(*lines, SpartanLine_Heading{2, text})
		} else if strings.HasPrefix(line, "#") {
			text := strings.TrimLeft(strings.TrimPrefix(line, "#"), " \t")
			if title == "" || lastTitleLevel > 1 {
				title = text
				lastTitleLevel = 1
			}
			if headings != nil {
				*headings = append(*headings, SpartanLine_Heading{1, text})
			}

			*lines = append(*lines, SpartanLine_Heading{1, text})
		} else if strings.HasPrefix(line, "=:") {
			// Input Link: Don't put in urls to crawl
			line = strings.TrimSpace(strings.TrimPrefix(line, "=:"))
			link, title, hasTitle := CutAny(line, " \t")

			link = strings.TrimSpace(link)
			//link_without_fragment, _, _ := strings.Cut(link, "#")

			URL, err := currentURL.Parse(link)
			if err != nil {
				continue
			}
			/*crossHost := false
			if URL.Scheme != "" && URL.Scheme != currentURL.Scheme && URL.Hostname() != currentURL.Hostname() {
				crossHost = true
			}*/

			title = strings.TrimSpace(title)
			tag := ""
			relation := 0
			if hasTitle && strings.HasSuffix(title, "]") {
				title, tag, _ = strings.Cut(title, "[")
				tag = strings.TrimSuffix(strings.TrimPrefix(tag, "["), "]")
				if strings.HasPrefix(tag, "-") {
					relation = -1
					tag = strings.TrimPrefix(tag, "-")
				} else if strings.HasPrefix(tag, "+") {
					relation = 1
					tag = strings.TrimPrefix(tag, "+")
				}
			}

			time := getTimeDate(title, false)
			if links != nil {
				*links = append(*links, SpartanLine_Link{URL.String(), title, relation, tag, true, time})
			}
			*lines = append(*lines, SpartanLine_Link{URL.String(), title, relation, tag, true, time})

			if isTimeDate(title) { // TODO
				isFeed = true
			}
		} else if strings.HasPrefix(line, "=>") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "=>"))
			link, title, hasTitle := CutAny(line, " \t")

			link = strings.TrimSpace(link)
			URL, err := currentURL.Parse(link)
			if err != nil {
				continue
			}

			title = strings.TrimSpace(title)
			tag := ""
			relation := 0
			if hasTitle && strings.HasSuffix(title, "]") {
				title, tag, _ = strings.Cut(title, "[")
				tag = strings.TrimSuffix(strings.TrimPrefix(tag, "["), "]")
				if strings.HasPrefix(tag, "-") {
					relation = -1
					tag = strings.TrimPrefix(tag, "-")
				} else if strings.HasPrefix(tag, "+") {
					relation = 1
					tag = strings.TrimPrefix(tag, "+")
				}
			}

			time := getTimeDate(title, false)
			if links != nil {
				*links = append(*links, SpartanLine_Link{URL.String(), title, relation, tag, false, time})
			}
			*lines = append(*lines, SpartanLine_Link{URL.String(), title, relation, tag, false, time})

			if isTimeDate(title) {
				isFeed = true
			}
		} else if strings.HasPrefix(line, ">") {
			line = strings.TrimLeft(strings.TrimPrefix(line, ">"), " \t")
			*lines = append(*lines, SpartanLine_Quote{1, line})
		} else if strings.HasPrefix(line, "* ") || strings.HasPrefix(line, "*\t") {
			level := strings.IndexFunc(line, func(r rune) bool {
				return r != '*'
			})

			ordered, label, text := bullet_is_ordered(line[level:])
			if ordered {
				*lines = append(*lines, SpartanLine_OrderedListItem{level, label, text})
			} else {
				*lines = append(*lines, SpartanLine_UnorderedListItem{level, text})
			}
		} else {
			*lines = append(*lines, SpartanLine_Text(line))
		}
	}

	return title, isFeed
}

// Pass in a line string slice with the bullet prefix (e.g., "* ") trimmed. Returns whether it's an
// ordered or unordered bullet, the label, and the text. If unordered, the label is an empty slice.
func bullet_is_ordered(line string) (bool, string, string) {
	text := strings.TrimLeft(line, " \t")
	label_end := strings.IndexFunc(text, func(r rune) bool {
		return !unicode.IsDigit(r) && r != '.'
	})

	if label_end == 0 { // Unordered
		return false, "", text
	} else if label_end == -1 {
		return false, "", text
	} else { // Ordered
		if !strings.HasSuffix(text[:label_end], ".") { // No dot, unordered
			return false, "", text
		} else { // Ordered
			label := strings.TrimRight(text[:label_end], ".")
			text = text[label_end:]
			return true, label, text
		}
	}
}

type TextParsingState struct {
	previousRune rune
	inStrong     bool
	inEmphasis   bool
	inMonospace  bool
}
