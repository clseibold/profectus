package spartan_client

import (
	"fmt"
	"net/url"
	"strings"
)

const (
	URLMaxLength  = 1024
	MetaMaxLength = 1024
)

// Scroll status codes as defined in the Gemini spec Appendix 1.
const (
	StatusSuccess  = 2
	StatusRedirect = 3
	ClientError    = 4
	ServerError    = 5
)

var statusText = map[int]string{
	StatusSuccess:  "Success",
	StatusRedirect: "Redirect - Temporary",
	ClientError:    "Client Error",
	ServerError:    "Server Error",
}

// StatusText returns a text for the Scroll status code. It returns the empty
// string if the code is unknown.
func StatusText(code int) string {
	return statusText[code]
}

// IsStatusValid checks whether an int status is covered by the spec.
// Note that:
//
//	A client SHOULD deal with undefined status codes
//	between '10' and '69' per the default action of the initial digit.
func IsStatusValid(status int) bool {
	_, found := statusText[status]
	return found
}

// StatusInRange returns true if the status has a valid first digit.
// This means it can be handled even if it's not defined by the spec,
// because it has a known category
func StatusInRange(status int) bool {
	if status < 2 || status > 5 {
		return false
	}
	return true
}

// QueryEscape provides URL query escaping in a way that follows the Spartan spec.
// It is the same as url.PathEscape, but it also replaces the +, because Spartan
// requires percent-escaping for queries.
func QueryEscape(query string) string {
	return strings.Replace(url.PathEscape(query), "+", "%2B", -1)
}

// QueryUnescape is the same as url.PathUnescape
func QueryUnescape(query string) (string, error) {
	return url.PathUnescape(query)
}

type Error struct {
	Err    error
	Status int
}

func (e Error) Error() string {
	return fmt.Sprintf("Status %d: %v", e.Status, e.Err)
}

func (e Error) Unwrap() error {
	return e.Err
}
