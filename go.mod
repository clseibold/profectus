module gitlab.com/clseibold/profectus

go 1.21.9

require (
	github.com/amitybell/piper-voice-alan v0.0.0-20231118093148-059963c24dbd
	github.com/clseibold/go-gemini v0.0.0-20240314051634-436d3e54df5c
	github.com/gen2brain/avif v0.3.0
	github.com/gen2brain/jpegxl v0.2.6
	github.com/gogs/chardet v0.0.0-20211120154057-b7413eaefb8f
	github.com/gonutz/w32/v2 v2.11.1
	github.com/gopxl/beep v1.4.1
	github.com/iangudger/ilist v0.0.0-20230905231755-6e70bcc31ea3
	github.com/nabbl/piper v0.0.0-20240807143155-14d7a439759f
	github.com/pkg/browser v0.0.0-20240102092130-5ac0b6a4141c
	github.com/veandco/go-sdl2 v0.5.0-alpha.7
	gitlab.com/clseibold/gonex v0.0.0-20240510073418-5058258917b4
	gitlab.com/clseibold/scroll-term v0.0.0-20240520162440-6ae2e4ba051b
	golang.org/x/image v0.15.0
	golang.org/x/text v0.14.0
	tlog.app/go/loc v0.7.0
)

require (
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/amitybell/piper-asset v0.0.0-20231030194325-d36a29e3b1fd // indirect
	github.com/amitybell/piper-bin-linux v0.0.0-20231118093037-92b3de178ad8 // indirect
	github.com/amitybell/piper-bin-windows v0.0.0-20231118093113-cc2cef2f6b74 // indirect
	github.com/bitly/go-simplejson v0.5.1 // indirect
	github.com/dlclark/regexp2 v1.11.0 // indirect
	github.com/dop251/goja v0.0.0-20240220182346-e401ed450204 // indirect
	github.com/ebitengine/purego v0.7.1 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/google/pprof v0.0.0-20240227163752-401108e1b7e7 // indirect
	github.com/klauspost/compress v1.17.3 // indirect
	github.com/nabbl/piper-bin-macos v0.0.0-20240805085459-7f1b1df8c68d // indirect
	github.com/tetratelabs/wazero v1.7.1 // indirect
)

require (
	github.com/ebitengine/oto/v3 v3.1.0 // indirect
	github.com/gabriel-vasile/mimetype v1.4.3
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/hajimehoshi/go-mp3 v0.3.4 // indirect
	github.com/icza/bitio v1.1.0 // indirect
	github.com/jfreymuth/oggvorbis v1.0.5 // indirect
	github.com/jfreymuth/vorbis v1.0.2 // indirect
	github.com/kettek/apng v0.0.0-20220823221153-ff692776a607
	github.com/kkdai/youtube/v2 v2.10.1
	github.com/mewkiz/flac v1.0.8 // indirect
	github.com/mewkiz/pkg v0.0.0-20230226050401-4010bf0fec14 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.22.0
	golang.org/x/sys v0.29.0 // indirect
)
