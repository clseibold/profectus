package main

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"

	// _ "net/http/pprof"
	"strings"

	"github.com/gopxl/beep"
	"github.com/gopxl/beep/effects"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/clseibold/profectus/horusui"
	"golang.org/x/image/math/fixed"
)

type ScrollLineType uint8

const (
	ScrollLineType_Heading1 ScrollLineType = iota
	ScrollLineType_Heading2
	ScrollLineType_Heading3
	ScrollLineType_Heading4
	ScrollLineType_Body
	ScrollLineType_Quote
	ScrollLineType_Preformat
	ScrollLineType_Link
	ScrollLineType_Link_Nex
	//ScrollLineType_Link_Hover // TODO: Remove
	ScrollLineType_ListItem
	ScrollLineType_Max
)

type State struct {
	// Misc.
	running                  bool
	maxRedirects             int8
	currentLanguageDirection horusui.TextDirection
	speakerInited            bool
	ctrl                     bool
	shift                    bool
	alt                      bool
	//searchString             string

	// Directories and Settings
	themesDirectory string
	fontsDirectory  string
	configDirectory string
	settings        Settings

	// SDL and GUI Stuff
	noGuiUpdate bool
	lastTime    uint64
	deltaTime   float64
	nowTime     uint64
	fps         float64
	ms          float64
	destroyer   chan interface{}

	Window   *sdl.Window
	renderer *sdl.Renderer
	Cursors  []*sdl.Cursor
	guiState *horusui.GuiState

	// Animation Stuff
	userScaleAnimation   horusui.NumberAnimation[float64]
	sidebarAnimation     horusui.ShowHideAnimation
	videoPlayerAnimation horusui.ShowHideAnimation
	audioPlayerAnimation horusui.ShowHideAnimation

	// GUI Styling
	theme           Theme
	documentColors  [ScrollLineType_Max]horusui.Color
	documentFonts   [ScrollLineType_Max]*horusui.Font
	defaultFont     *horusui.Font
	tinyFont        *horusui.Font
	arial15         *horusui.Font
	bold_arial15    *horusui.Font
	topBarUiFont    *horusui.Font
	topBarIconsFont *horusui.Font
	emojiFont       *horusui.Font

	tabs      []*Tab
	activeTab *Tab

	focusedInput       *strings.Builder // TODO: Move both to horusui
	focusedInputCursor int
	inputFocusSet      bool
	inputFocusReset    bool

	// Audio Playing Stuff - Not tab-based
	audioReader         io.ReadCloser
	currentStream       beep.StreamSeekCloser
	currentStreamFormat beep.Format
	currentStreamVolume effects.Volume

	// Video Playing Stuff - Not tab-based
	videoPlaying io.ReadCloser
}

func (state *State) getActiveTab() *Tab {
	//return state.tabs[state.activeTab]
	return state.activeTab
}

func (state *State) setActiveTab(newTab *Tab) {
	state.activeTab = newTab
}

func main() {
	state := State{}

	state.configDirectory = getConfigDirectory()
	_, stat_err := os.Stat(state.configDirectory)
	state.themesDirectory = getUserThemesDirectory()
	state.fontsDirectory = getUserFontsDirectory()

	// Check if themes/default_dark.json exists
	if errors.Is(stat_err, fs.ErrNotExist) {
		// If it doesn't, then save all of the theme files to the user's themes directory
		SaveThemeToFile(filepath.Join(state.themesDirectory, "default_dark.json"), DefaultDark)

		themeReadmeFile, _ := builtinThemes.Open("themes/README.scroll")
		themeReadme, _ := io.ReadAll(themeReadmeFile)
		os.WriteFile(filepath.Join(state.themesDirectory, "README.scroll"), themeReadme, 0600)

		dir, _ := builtinThemes.ReadDir("themes")
		for _, entry := range dir {
			fmt.Printf("%s\n", entry.Name())
			if entry.IsDir() {
				continue
			}

			if entry.Name() != "." && entry.Name() != ".." && strings.HasSuffix(entry.Name(), ".json") {
				file, _ := builtinThemes.Open("themes/" + entry.Name())
				fileData, _ := io.ReadAll(file)
				os.WriteFile(filepath.Join(state.themesDirectory, entry.Name()), fileData, 0600)
			}
		}
	}

	// Get settings, then get all themes from themes directory, and set currentTheme to the theme in the settings (based on the theme name)
	state.settings = LoadSettingsFromFile(filepath.Join(state.configDirectory, "settings.json"))
	themes := LoadThemes(state.themesDirectory)
	if setTheme, ok := themes[state.settings.ThemeName]; ok {
		currentTheme = setTheme
	} else {
		//currentTheme = LoadThemeFromFile(filepath.Join(state.themesDirectory, "default_dark.json"))
		currentTheme = DefaultDark
	}

	if state.settings.SearchURL == "scroll://auragem.letz.dev/search/s" {
		state.settings.SearchURL = "scroll://auragem.ddns.net/search/s"
	}

	state.destroyer = make(chan interface{})

	// Animation stuff
	state.sidebarAnimation = horusui.CreateShowHideAnimation(horusui.AnimationType_EaseOutQuad, horusui.AnimationType_EaseInQuad, 90, true)
	state.videoPlayerAnimation = horusui.CreateShowHideAnimation(horusui.AnimationType_EaseOutQuad, horusui.AnimationType_EaseInQuad, 90, false)
	state.audioPlayerAnimation = horusui.CreateShowHideAnimation(horusui.AnimationType_EaseOutQuad, horusui.AnimationType_EaseInQuad, 90, false)

	// Misc.
	state.noGuiUpdate = true
	state.maxRedirects = 5

	// Create first tab, don't animate it, and set it to active tab
	firstTab := state.NewTab()
	firstTab.widthPercentAnimation.SetFinished()
	state.setActiveTab(firstTab)

	setupPlatform()
	err := sdl.Init(sdl.INIT_TIMER | sdl.INIT_VIDEO | sdl.INIT_EVENTS | sdl.INIT_AUDIO)
	if err != nil {
		panic(err)
	}

	v := sdl.Version{}
	sdl.GetVersion(&v)
	fmt.Printf("SDL Version: %2d.%d.%d\n", v.Major, v.Minor, v.Patch)

	title := "Profectus Browser"
	state.Window, err = sdl.CreateWindow(title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, 300, 300, sdl.WINDOW_RESIZABLE|sdl.WINDOW_ALLOW_HIGHDPI|sdl.WINDOW_HIDDEN)
	if err != nil {
		panic(err)
	}
	state.Window.SetResizable(true)
	sdl.AddEventWatch(&state, nil)
	sdl.EventState(sdl.SYSWMEVENT, sdl.ENABLE)
	sdl.SetHint(sdl.HINT_RENDER_SCALE_QUALITY, "1")
	sdl.SetHint(sdl.HINT_IME_INTERNAL_EDITING, "1")
	sdl.SetHint(sdl.HINT_MAC_CTRL_CLICK_EMULATE_RIGHT_CLICK, "1")

	// Use GetDisplayDPI just for the Window Size
	displayIndex, err := state.Window.GetDisplayIndex()
	if err != nil {
		panic(err)
	}
	_, _, vdpi, dpi_err := sdl.GetDisplayDPI(displayIndex)
	if dpi_err != nil {
		fmt.Println("Failed to get Window's Display DPI.")
		vdpi = 1.0
	}

	// Calculate Window Size, Create Gui State, and Show Window
	ww, wh := int32(dpiScalePixels(float64(vdpi), 950)), int32(dpiScalePixels(float64(vdpi), 575)) // 660
	state.Window.SetSize(ww, wh)
	state.Window.SetPosition(sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED)

	state.renderer, err = sdl.CreateRenderer(state.Window, -1, sdl.RENDERER_ACCELERATED|sdl.RENDERER_PRESENTVSYNC)
	if err != nil {
		panic(err)
	}
	ow, oh, _ := state.renderer.GetOutputSize()
	fmt.Printf("Window Size: %v x %v\nOutput Size: %v x %v\n", ww, wh, ow, oh)

	state.guiState = horusui.MakeGuiState(currentTheme.Default.Panel.background[Selector_Default], title, ow, oh)
	state.guiState.UserScale = float64(pixelRatio(&state, true))
	state.userScaleAnimation = horusui.CreateNumberAnimation(horusui.AnimationType_EaseOutCubic, 200, state.guiState.UserScale, state.guiState.UserScale)
	state.userScaleAnimation.SetFinished()
	fmt.Printf("Current DPI: %f\nUser Scale: %v\n", vdpi, state.guiState.UserScale)

	ttf.Init()
	img.Init(img.INIT_JPG | img.INIT_TIF)
	state.Window.Show()

	state.renderer.SetDrawColor(0, 0, 0, 255)
	state.renderer.DrawRect(nil)

	// Setup FreeType
	//state.FtCtx = freetype.NewContext()
	setupFonts(&state, state.guiState.UserScale)
	setupGui(&state)

	setupCursors(&state)

	state.running = true
	state.nowTime = sdl.GetPerformanceCounter()

	sdl.StopTextInput()

	/*
		state.renderer.SetDrawColor(255, 255, 255, 255)
		state.renderer.Clear()
		width, height, _ := horusui.SizeUTF8_Wrapped(state.documentFonts[ScrollLineType_Body], "This is a test to see how this works.", 50)
		fmt.Printf("%v, %v\n", width, height)
		state.renderer.Present()
		for {
			event := sdl.WaitEvent()
			state.FilterEvent(event, nil)
		}*/

	// runtime.LockOSThread()
	/*go func() {
		runtime.LockOSThread()
		for state.running {
			//sdl.WaitEvent()
			sdl.PumpEvents()
		}
	}()*/
	//runtime.UnlockOSThread()

	/*go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()*/

	var time_showMs float64 = 1
	var currentMs float64 = 0
	for state.running {
		state.lastTime = state.nowTime
		state.nowTime = sdl.GetPerformanceCounter()

		state.deltaTime = float64(float64(state.nowTime-state.lastTime) / float64(sdl.GetPerformanceFrequency()))
		currentMs = state.deltaTime * 1000

		time_showMs -= state.deltaTime
		if time_showMs <= 0 {
			time_showMs = 1
			var spf float64 = currentMs / 1000.0
			state.fps = 1 / spf
			state.ms = currentMs
			//fmt.Printf("MS: %f; FPS %f; deltaTime: %f\n", state.ms, state.fps, state.deltaTime*1000)
		}

		if !state.noGuiUpdate {
			state.guiState.UserScale, _ = state.userScaleAnimation.Update(state.deltaTime * 1000)
			go currentThemeAnimation.Update(&state, state.deltaTime*1000)
			update(&state, state.deltaTime)
			render(&state, state.deltaTime)
			sdl.PumpEvents()
		} else {
			event := sdl.WaitEvent()
			state.FilterEvent(event, nil)
		}

		state.guiState.ClearElements()
		// runtime.GC()
	}

	sdl.Quit()
}

func setupCursors(state *State) {
	state.Cursors = make([]*sdl.Cursor, sdl.NUM_SYSTEM_CURSORS)
	// Setup System Cursors
	for i := 0; i < int(sdl.NUM_SYSTEM_CURSORS); i++ {
		c := sdl.SystemCursor(i)
		state.Cursors[c] = sdl.CreateSystemCursor(c)
	}
}
func resetCursor(state *State) {
	if len(state.Cursors) > 0 && state.Cursors[sdl.SYSTEM_CURSOR_ARROW] != nil {
		sdl.SetCursor(state.Cursors[sdl.SYSTEM_CURSOR_ARROW])
	}
}
func setCursor(state *State, c sdl.SystemCursor) {
	sdl.SetCursor(state.Cursors[c])
}

func update(state *State, deltaTime float64) {
	// var spf float64 = (currentMs / 1000.0)
	// var fps float64 = 1 / spf

	handleFocusExit_startFrame(state)
	updateGui(state, deltaTime)
	handleFocusExit_endFrame(state)
	/*if !state.noGuiUpdate {
	}*/
}

func render(state *State, deltaTime float64) {
	state.renderer.SetDrawColor(41, 41, 41, 255)
	state.renderer.Clear()

	// Check if textures or surfaces need destroying and destroy them. This is used for the animated image renderers.
	select {
	case resource := <-state.destroyer:
		switch resource := resource.(type) {
		case *sdl.Texture:
			err := resource.Destroy()
			if err != nil {
				panic(err)
			}
		case *sdl.Surface:
			resource.Free()
		}
	default:
	}

	horusui.UpdateDimensions(state.guiState, float32(state.guiState.WindowSize.W), float32(state.guiState.WindowSize.H))

	// NOTE: Times by two as a hack because of pt-to-pixel for highdpi crap.
	state.guiState.HandleEvents_Start()
	state.guiState.Render(state.renderer, state.guiState.Root, sdl.Rect{X: 0, Y: 0, W: state.guiState.WindowSize.W, H: state.guiState.WindowSize.H})
	state.guiState.HandleEvents_End()
	//state.guiState.HandleEvents()

	/*
		robotoFile, _ := os.OpenFile("fonts/arial.ttf", os.O_RDONLY, 0600)
		defer robotoFile.Close()
		robotoLoader, _ := loader.NewLoader(robotoFile)
		robotoFont, _ := font.NewFont(robotoLoader)
		ppem := uint16(dpiScalePixels(float64(hdpi), 15))
		robotoFace := &font.Face{robotoFont, []tables.Coord{}, ppem, ppem}
		robotoHBFont := harfbuzz.NewFont(robotoFace)
		buf := harfbuzz.NewBuffer()
		buf.AddRunes([]rune("This is some example text!"), 0, -1)
		buf.GuessSegmentProperties()
		buf.Shape(robotoHBFont, []harfbuzz.Feature{harfbuzz.Feature{loader.MustNewTag("kern"), 1, harfbuzz.FeatureGlobalStart, harfbuzz.FeatureGlobalEnd}, harfbuzz.Feature{loader.MustNewTag("sbix"), 1, harfbuzz.FeatureGlobalStart, harfbuzz.FeatureGlobalEnd}})
		glyphinfo := buf.Info
		glyphpos := buf.Pos

		//textRenderer := textRender.Renderer{FontSize: 15, Color: color.Black, PixScale: 2.0}
		//dest := image.NewRGBA()
		//textRenderer.DrawString("This is some example text!")

		var cursorX int32 = 0
		var cursorY int32 = 0
		scale := *r.PixScale / float32(robotoFace.Upem())
		for i, info := range glyphinfo {
			pos := glyphpos[i]
			switch data := robotoFace.GlyphData(info.Glyph).(type) {
			case api.GlyphBitmap:
				rwops, _ := sdl.RWFromMem(data.Data)
				surface, _ := img.LoadRW(rwops, false)
				texture, _ := state.renderer.CreateTextureFromSurface(surface)
				state.renderer.Copy(texture, nil, &sdl.Rect{cursorX + pos.XOffset, cursorY + pos.YOffset, int32(data.Width), int32(data.Height)})
			case api.GlyphOutline:
				var _x float32 = float32(cursorX)
				var _y float32 = float32(cursorX)
				fmt.Printf("----------\n")

				imgX := state.guiState.WindowSize.X
				imgY := state.guiState.WindowSize.Y
				scanner := rasterx.NewScannerGV(b.Dx(), b.Dy(), img, b)
				f := rasterx.NewFiller(b.Dx(), b.Dy(), scanner)
			case api.GlyphSVG:
			}

			cursorX += pos.XAdvance
			fmt.Printf("CursorX: %v\n", cursorX)
			cursorY += pos.YAdvance
		}*/

	/*if true {
		stringToRender := fmt.tprintf("%.2f fps (%.2f ms)", state.fps, state.ms);
		sdl.SetRenderDrawColor(state.renderer, 255, 255, 255, 255);
		horusui.renderText(state.renderer, &state.arialFont_16pt, cast(f32) dpiScalePixels(f64(dpi), 10), 0, stringToRender, false, 0);
	}*/

	state.renderer.Present()
}

/*
func drawOutline(r *textRender.Renderer, g shaping.Glyph, bitmap api.GlyphOutline, f *rasterx.Filler, scale float32, x, y float32) {
	for _, s := range bitmap.Segments {
		switch s.Op {
		case api.SegmentOpMoveTo:
			f.Start(fixed.Point26_6{X: floatToFixed266(s.Args[0].X*scale + x), Y: floatToFixed266(-s.Args[0].Y*scale + y)})
		case api.SegmentOpLineTo:
			f.Line(fixed.Point26_6{X: floatToFixed266(s.Args[0].X*scale + x), Y: floatToFixed266(-s.Args[0].Y*scale + y)})
		case api.SegmentOpQuadTo:
			f.QuadBezier(fixed.Point26_6{X: floatToFixed266(s.Args[0].X*scale + x), Y: floatToFixed266(-s.Args[0].Y*scale + y)},
				fixed.Point26_6{X: floatToFixed266(s.Args[1].X*scale + x), Y: floatToFixed266(-s.Args[1].Y*scale + y)})
		case api.SegmentOpCubeTo:
			f.CubeBezier(fixed.Point26_6{X: floatToFixed266(s.Args[0].X*scale + x), Y: floatToFixed266(-s.Args[0].Y*scale + y)},
				fixed.Point26_6{X: floatToFixed266(s.Args[1].X*scale + x), Y: floatToFixed266(-s.Args[1].Y*scale + y)},
				fixed.Point26_6{X: floatToFixed266(s.Args[2].X*scale + x), Y: floatToFixed266(-s.Args[2].Y*scale + y)})
		}
	}
	f.Stop(true)
}
*/

func fixed266ToFloat(i fixed.Int26_6) float32 {
	return float32(float64(i) / 64)
}

func floatToFixed266(f float32) fixed.Int26_6 {
	return fixed.Int26_6(int(float64(f) * 64))
}
