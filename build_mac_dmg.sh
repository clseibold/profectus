mkdir -p Profectus.app
mkdir -p Profectus.app/Contents
mkdir -p Profectus.app/Contents/MacOS
mkdir -p Profectus.app/Contents/Resources

cp Info.plist Profectus.app/Contents/Info.plist
cp profectus Profectus.app/Contents/MacOS/exec
cp README.md Profectus.app/Contents/Resources/
cp LICENSE Profectus.app/Contents/Resources/

hdiutil create -volname Profectus -srcfolder Profectus.app -ov -format UDZO profectus_darwin_x64.dmg
