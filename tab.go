package main

import (
	"container/ring"
	"io"
	"net/url"
	"strings"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/clseibold/profectus/horusui"
)

type Tab struct {
	// Current page Cache
	widthPercentAnimation   horusui.ShowHideAnimation
	documentLoading         bool
	firstFrame              bool
	outlineScroll           horusui.NumberAnimation[float32]
	elementOffset           int
	ScrollOffset_StartIndex int
	ScrollOffset_StopIndex  int

	scrollAnimation  horusui.NumberAnimation[float32]
	history          *ring.Ring
	currentPageCache PageCache

	// Gets reconstructed based on the page cache Data of the current page
	documentImgSurface *sdl.Surface // For currently-displayed image that is not a GIF. Use GifPlayer for GIFs.
	documentGifPlayer  *GifPlayer
	documentAPNGPlayer *APNGPlayer
	documentJxlPlayer  *JxlPlayer
	//documentWebPPlayer *WebPPlayer
	documentAVIFPlayer *AVIFPlayer

	// Ongoing connection for text streams
	ongoingConnection io.ReadCloser

	// Keyboard Input Stuff
	addressInput  strings.Builder
	documentInput strings.Builder

	spartanLineInputs map[string]*strings.Builder // Key is the URL; Map is only initialized for spartan documents
	posCache          []horusui.Point[float32]
}

// Opens new tab, adding it to the tab list, and goes to configured new tab page
func (state *State) NewTab() *Tab {
	tab := &Tab{history: ring.New(10)}
	//tab.widthPercentAnimation = horusui.CreateNumberAnimation(horusui.AnimationType_EaseOutQuad, 80, float32(0), 1)
	tab.widthPercentAnimation = horusui.CreateShowHideAnimation(horusui.AnimationType_EaseInQuad, horusui.AnimationType_EaseOutQuad, 70, false)
	tab.widthPercentAnimation.Show()
	tab.outlineScroll = horusui.CreateNumberAnimation(horusui.AnimationType_EaseOutQuad, 55, float32(0), float32(0))
	tab.scrollAnimation = horusui.CreateNumberAnimation(horusui.AnimationType_EaseOutQuad, 55, float32(0), float32(0)) //horusui.CreateStaticNumberAnimation(float32(0))
	var err error
	tab.currentPageCache.Url, err = url.Parse("about:newtab")
	if err != nil {
		panic(err)
	}
	tab.firstFrame = true
	state.tabs = append(state.tabs, tab)
	go getPage(state, tab, "about:newtab", false, "")
	//go getHomepage(state, tab)

	return tab
}

// Opens new tab, adding it to the tab list, and goes to passed-in address
func (state *State) NewTabWithPage(address string) *Tab {
	tab := &Tab{history: ring.New(10)}
	//tab.widthPercentAnimation = horusui.CreateNumberAnimation(horusui.AnimationType_EaseOutQuad, 80, float32(0), 1)
	tab.widthPercentAnimation = horusui.CreateShowHideAnimation(horusui.AnimationType_EaseInQuad, horusui.AnimationType_EaseOutQuad, 70, false)
	tab.widthPercentAnimation.Show()
	tab.outlineScroll = horusui.CreateNumberAnimation(horusui.AnimationType_EaseOutQuad, 55, float32(0), float32(0))
	tab.scrollAnimation = horusui.CreateNumberAnimation(horusui.AnimationType_EaseOutQuad, 55, float32(0), float32(0))
	tab.firstFrame = true
	go getPage(state, tab, address, false, "")
	state.tabs = append(state.tabs, tab)
	return tab
}

// The cache of the current page, which is always one behind the current ring position.
func (tab *Tab) getCurrentPageCacheInHistory() *ring.Ring {
	return tab.history.Prev()
}

// Sets the page cache history back one (to the current page).
func (tab *Tab) pageCache_Back() {
	tab.history = tab.getCurrentPageCacheInHistory()
}

func (tab *Tab) pageCache_Forward() {
	tab.history = tab.history.Next()
}

// The cache of the previous page in history
func (tab *Tab) getPrevPageCacheInHistory() *ring.Ring {
	return tab.history.Prev().Prev()
}

// The cache of the next page (which is always where the ring is currently at)
func (tab *Tab) getNextPageCacheInHistory() *ring.Ring {
	return tab.history
}

// Closes any ongoing connections, destroys cached image data if applicable, and removes from the tabs array
func (tab *Tab) Close(state *State) {
	if len(state.tabs) == 1 {
		// Don't close, just go to newtab page
		tab.widthPercentAnimation.Show()
		tab.widthPercentAnimation.Reset()
		//go getHomepage(state, tab)
		go getPage(state, tab, "about:newtab", false, "")
		return
	}
	if tabIndex := state.findTabIndex(tab); tabIndex != -1 {
		if state.activeTab == tab {
			if tabIndex >= len(state.tabs)-1 {
				state.setActiveTab(state.tabs[tabIndex-1])
			} else {
				state.setActiveTab(state.tabs[tabIndex+1])
			}
		}
		state.tabs = remove(state.tabs, tabIndex)
	}
	resetCurrentDocumentState(state, tab)
}

func remove(slice []*Tab, s int) []*Tab {
	return append(slice[:s], slice[s+1:]...)
}
func (state *State) findTabIndex(tab *Tab) int {
	for i, t := range state.tabs {
		if tab == t {
			return i
		}
	}
	return -1
}
