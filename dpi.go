package main

import (
	"fmt"
	"runtime"

	"github.com/veandco/go-sdl2/sdl"
)

func defaultDpi() float64 {
	if runtime.GOOS == "windows" {
		return 96
		//return windows.USER_DEFAULT_SCREEN_DPI
	} else if runtime.GOOS == "darwin" {
		return 72
	} else if runtime.GOOS == "freebsd" || runtime.GOOS == "linux" {
		// return 120
		//return 96
		return 96
	}
	return 96
}
func dpiScale(dpi float64) float64 {
	//linuxDefaultDPI = dpi
	return dpi / defaultDpi()
	//return 1;
}
func scaleToDpi(scale float64) float64 {
	return scale * defaultDpi()
}
func dpiScalePixels(dpi float64, pixels float64) float64 {
	return pixels * dpiScale(dpi)
}
func pixelsToInches(dpi float64, pixels float64) float64 {
	return pixels / dpi
}
func inchesToPixels(dpi float64, inches float64) float64 {
	return dpi * inches
}

// Returns pixels per point
func pixelRatio(state *State, fallback bool) float32 {
	width, _, _ := state.renderer.GetOutputSize() // Pixels
	ww, _ := state.Window.GetSize()               // Points

	// If the output size matches the window size, get the display dpi instead.
	if ww == width && fallback {
		displayIndex, err := state.Window.GetDisplayIndex()
		if err != nil {
			panic(err)
		}
		_, _, vdpi, dpi_err := sdl.GetDisplayDPI(displayIndex)
		if dpi_err != nil {
			fmt.Println("Failed to get Window's Display DPI.")
			return 1.0
		}

		return float32(dpiScale(float64(vdpi)))
	}

	return float32(width) / float32(ww)
}

func PtToPixelF(pixelRatio float32, pts float32) float32 {
	return pts * pixelRatio
}
func PtToPixelI(pixelRatio float32, pts int32) int32 {
	return int32(float32(pts) * pixelRatio)
}

func PixelToPointF(pixelRatio float32, pixels float32) float32 {
	return pixels / pixelRatio
}
func PixelToPointI(pixelRatio float32, pixels int32) int32 {
	return int32(float32(pixels) / pixelRatio)
}
