package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	_ "embed"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/clseibold/profectus/horusui"
)

// Builtin Theme Ideas:
// * Light and Dark
// * Lagrange Light, and Dark - Has page themes as well...
// * GemiNaut Fabric, Dark, Plain, and Terminal - Needs the ability to do background images
// * Kristall
// * Castor
// * Fafi
// * Firefox Dark
// * Vim
// * Mozz.us Portal Theme

var currentTheme Theme = DefaultDark
var currentThemeAnimation = ThemeAnimation{}

// Builtin Theme files

//go:embed themes/README.scroll
//go:embed themes/*.json
var builtinThemes embed.FS

var DefaultDark Theme = Theme{
	Name:                    "Dark",
	ContentContainerPadding: horusui.GuiPadding{Top: 14, Bottom: 14, Left: 14, Right: 14},
	//SidebarDivider:          14,
	Default: SectionTheme{
		Panel: ElementTheme{
			background: [Selector_Max]horusui.Color{
				Selector_Default: horusui.Color{41, 41, 41, 255},
			},
			border: [Selector_Max]horusui.GuiBorder{
				Selector_Default: horusui.GuiBorder{0, horusui.Color{21, 21, 21, 255}},
			},
			textColors: [Selector_Max]horusui.Color{
				Selector_Default:  horusui.OffWhite,
				Selector_Disabled: horusui.Color{150, 150, 150, 255},
			},
		},
		Button: ButtonTheme{
			Outset: false,
			ElementTheme: ElementTheme{
				background: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{41 - 4, 41 - 4, 41 - 4, 255},
					Selector_Hover:   horusui.Color{41 - 8, 41 - 8, 41 - 8, 255},
				},
				border: [Selector_Max]horusui.GuiBorder{
					Selector_Default: horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}},
					Selector_Hover:   horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}},
				},
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.OffWhite,
				},
				padding: horusui.GuiPadding{Top: 4.2, Bottom: 4.2, Left: 14, Right: 14},
			},
		},
		Textbox: ElementTheme{
			radius: 2,
			background: [Selector_Max]horusui.Color{
				Selector_Default: horusui.Color{41 - 8, 41 - 8, 41 - 8, 255},
			},
			border: [Selector_Max]horusui.GuiBorder{
				Selector_Default: horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}},
				Selector_Hover:   horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}},
				Selector_Focused: horusui.GuiBorder{1, horusui.Color{R: 50, G: 150, B: 50, A: 255}},
			},
			textColors: [Selector_Max]horusui.Color{
				Selector_Default:     horusui.OffWhite,
				Selector_Placeholder: horusui.Color{150, 150, 150, 255},
			},
			padding: horusui.GuiPadding{Top: 4.2, Bottom: 4.2, Left: 10, Right: 10},
		},
		Checkbox:    ElementTheme{jsonOmitted: true},
		List:        ElementTheme{jsonOmitted: true},
		jsonOmitted: false,
	},
	Topbar: SectionTheme{
		Panel: ElementTheme{
			background: [Selector_Max]horusui.Color{
				Selector_Default: horusui.Color{21, 21, 21, 255},
			},
			/*border: [Selector_Max]horusui.GuiBorder{
				Selector_Default: horusui.GuiBorder{1, horusui.Color{20, 100, 20, 255}},
			},*/
			textColors: [Selector_Max]horusui.Color{
				Selector_Default: horusui.OffWhite,
			},
			spacing: horusui.GuiSpacing{X: 7, Y: 7},
			padding: horusui.GuiPadding{Top: 7, Bottom: 7, Left: 7, Right: 7},
		},
		Button: ButtonTheme{
			Outset: true,
			ElementTheme: ElementTheme{
				background: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{21 - 4, 21 - 4, 21 - 4, 255},
					Selector_Hover:   horusui.Color{21 - 8, 21 - 8, 21 - 8, 255},
				},
				border: [Selector_Max]horusui.GuiBorder{
					Selector_Default: horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}},
					Selector_Hover:   horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}},
				},
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.OffWhite,
				},
				padding: horusui.GuiPadding{Top: 4.2, Bottom: 4.2, Left: 14, Right: 14},
			},
		},
		Textbox:     ElementTheme{jsonOmitted: true},
		Checkbox:    ElementTheme{jsonOmitted: true},
		List:        ElementTheme{jsonOmitted: true},
		jsonOmitted: false,
	},
	Tabbar: TabbarTheme{
		Position:          "below",
		AddButtonPosition: 1, // opposite
		SectionTheme: SectionTheme{
			Panel: ElementTheme{
				background: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{21, 21, 21, 255},
				},
				/*border: [Selector_Max]horusui.GuiBorder{
					Selector_Default: horusui.GuiBorder{1, horusui.Color{20, 100, 20, 255}},
				},*/
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.OffWhite,
				},
				padding: horusui.GuiPadding{Left: 7, Right: 7},
				spacing: horusui.GuiSpacing{X: 1, Y: 1},
			},
			Button: ButtonTheme{
				Outset: true,
				ElementTheme: ElementTheme{
					background: [Selector_Max]horusui.Color{
						Selector_Default: horusui.Color{21 - 4, 21 - 4, 21 - 4, 255},
						Selector_Hover:   horusui.Color{21 - 8, 21 - 8, 21 - 8, 255},
						Selector_Active:  horusui.Color{41, 41, 41, 255}, //horusui.Color{21 - 8, 21 - 8, 21 - 8, 255},
					},
					border: [Selector_Max]horusui.GuiBorder{
						Selector_Default: horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}},
						Selector_Hover:   horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}},
						Selector_Active:  horusui.GuiBorder{1, horusui.Color{20, 100, 20, 255}},
					},
					textColors: [Selector_Max]horusui.Color{
						Selector_Default: horusui.Gray,
						Selector_Active:  horusui.OffWhite,
					},
					padding: horusui.GuiPadding{Left: 21, Right: 21, Top: 7, Bottom: 7},
				},
			},
			Textbox:     ElementTheme{jsonOmitted: true},
			Checkbox:    ElementTheme{jsonOmitted: true},
			List:        ElementTheme{jsonOmitted: true},
			jsonOmitted: false,
		},
	},
	Sidebar: SidebarTheme{
		MaxWidth: 245,
		MinWidth: 140,
		Dock:     "default",
		SectionTheme: SectionTheme{
			Panel: ElementTheme{
				radius: 2,
				background: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{21, 21, 21, 255},
				},
				border: [Selector_Max]horusui.GuiBorder{
					Selector_Default: horusui.GuiBorder{1, horusui.Color{20, 100, 20, 255}},
				},
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.OffWhite,
				},
				spacing: horusui.GuiSpacing{X: 7, Y: 7},
				padding: horusui.GuiPadding{14, 14, 14, 14},
			},
			Button: ButtonTheme{
				Outset: true,
				ElementTheme: ElementTheme{
					background: [Selector_Max]horusui.Color{
						Selector_Default: horusui.Color{21 - 4, 21 - 4, 21 - 4, 255},
						Selector_Hover:   horusui.Color{21 - 8, 21 - 8, 21 - 8, 255},
					},
					border: [Selector_Max]horusui.GuiBorder{
						Selector_Default: horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}},
						Selector_Hover:   horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}},
					},
					textColors: [Selector_Max]horusui.Color{
						Selector_Default: horusui.OffWhite,
					},
					padding: horusui.GuiPadding{Top: 4.2, Bottom: 4.2, Left: 14, Right: 14},
				},
			},
			Textbox:  ElementTheme{jsonOmitted: true},
			Checkbox: ElementTheme{jsonOmitted: true},
			List: ElementTheme{
				background: [Selector_Max]horusui.Color{
					Selector_Hover: horusui.Color{R: 24, G: 77, B: 97, A: 255},
				},
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.OffWhite,
				},
				padding:    horusui.GuiPadding{Top: 4.2, Bottom: 4.2, Left: 14, Right: 14},
				indentSize: 21,
			},
			jsonOmitted: false,
		},
	},
	Page: PageTheme{
		MaxWidth:      861,
		MaxImageWidth: 1260,
		NexCharWidth:  80,
		Center:        1,
		SectionTheme: SectionTheme{
			Panel: ElementTheme{
				padding: horusui.GuiPadding{Top: 1, Bottom: 0, Left: 14, Right: 14},
			},
			Button:   ButtonTheme{ElementTheme: ElementTheme{jsonOmitted: true}},
			Textbox:  ElementTheme{jsonOmitted: true},
			Checkbox: ElementTheme{jsonOmitted: true},
			List:     ElementTheme{jsonOmitted: true},
		},
		Document: [ScrollLineType_Max]ElementTheme{
			ScrollLineType_Heading1: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.White,
				},
			},
			ScrollLineType_Heading2: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{R: 151, G: 171, B: 180, A: 255},
				},
			},
			ScrollLineType_Heading3: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{R: 103, G: 153, B: 178, A: 255},
				},
			},
			ScrollLineType_Heading4: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{R: 103, G: 153, B: 178, A: 255},
				},
			},
			ScrollLineType_Body: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{R: 180, G: 180, B: 180, A: 255},
				},
			},
			ScrollLineType_Quote: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{R: 155, G: 155, B: 155, A: 255},
				},
				border: [Selector_Max]horusui.GuiBorder{
					Selector_Default: horusui.GuiBorder{Thickness: 1, Color: horusui.Gray},
				},
				padding:    horusui.GuiPadding{Top: 10, Bottom: 14, Left: 0, Right: 0},
				indentSize: 21,
			},
			ScrollLineType_Preformat: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{R: 165, G: 165, B: 165, A: 255},
				},
			},
			ScrollLineType_Link: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.OffWhite,
					Selector_Hover:   horusui.Color{R: 32, G: 191, B: 253, A: 255},
				},
			},
			ScrollLineType_Link_Nex: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.OffWhite,
					Selector_Hover:   horusui.Color{R: 32, G: 191, B: 253, A: 255},
				},
				spacing: horusui.GuiSpacing{X: 5, Y: 5},
			},
			ScrollLineType_ListItem: ElementTheme{
				textColors: [Selector_Max]horusui.Color{
					Selector_Default: horusui.Color{R: 180, G: 180, B: 180, A: 255},
				},
				indentSize: 21,
			},
		},
	},
	PageInlined: SectionTheme{
		Panel:       ElementTheme{jsonOmitted: true},
		Button:      ButtonTheme{ElementTheme: ElementTheme{jsonOmitted: true}},
		Textbox:     ElementTheme{jsonOmitted: true},
		Checkbox:    ElementTheme{jsonOmitted: true},
		List:        ElementTheme{jsonOmitted: true},
		jsonOmitted: true,
	},
	Popup: SectionTheme{
		Panel:       ElementTheme{jsonOmitted: true},
		Button:      ButtonTheme{ElementTheme: ElementTheme{jsonOmitted: true}},
		Textbox:     ElementTheme{jsonOmitted: true},
		Checkbox:    ElementTheme{jsonOmitted: true},
		List:        ElementTheme{jsonOmitted: true},
		jsonOmitted: true,
	},
}

type Theme struct {
	Name                    string
	Filepath                string
	ContentContainerPadding horusui.GuiPadding
	SidebarDivider          float32

	Default     SectionTheme // Default Fallback
	Topbar      SectionTheme
	Tabbar      TabbarTheme // Add location setting (below_topbar, above_topbar, bottom, top)
	Sidebar     SidebarTheme
	Page        PageTheme
	PageInlined SectionTheme
	Popup       SectionTheme

	// AnimationTimings: tab_open_close, sidebar_open_close
	// Sidebar Spacing and Position/Dock, Tabbar Position
	// Document alignment (centering/left/right of page, or entire screen like in Kristall) and max-width
	// Document background image
}

type TabbarTheme struct {
	Position          string  // above vs. below
	AddButtonPosition float32 // after_tabs = 0, before_tabs = -1, and opposite = 1
	SectionTheme
}

type SidebarTheme struct {
	MaxWidth int
	MinWidth int
	Dock     string // default, opposite, left, right
	SectionTheme
}

type PageTheme struct {
	MaxWidth      int
	MaxImageWidth int
	NexCharWidth  int
	Center        float32 // 0 = not centered; 1 = centered; 0 < val < 1 = animating the centering
	SectionTheme
	Document [ScrollLineType_Max]ElementTheme
}

type SectionTheme struct {
	// animationTotalMS float32 // TODO: Might be unnecessary to store this here
	Panel    ElementTheme
	Button   ButtonTheme
	Textbox  ElementTheme
	Checkbox ElementTheme
	List     ElementTheme
	//Document    [ScrollLineType_Max]ElementTheme
	jsonOmitted bool
}

type Selector uint8

const (
	Selector_Default Selector = iota
	Selector_Hover
	Selector_DragHover // When hovered over while an element is being dragged
	Selector_Dragged   // The original location, when being dragged
	Selector_Active
	Selector_Focused
	Selector_Placeholder
	Selector_Checked
	// Selector_Unchecked // Use Default instead
	Selector_Disabled
	Selector_Max
)

type ButtonTheme struct {
	Outset bool
	ElementTheme
}

type ElementTheme struct {
	background [Selector_Max]horusui.Color     // Change to ColorAnimation
	border     [Selector_Max]horusui.GuiBorder // Animate Border Color at the very least
	textColors [Selector_Max]horusui.Color     // Change to ColorAnimation
	// textFonts  [Selector_Max]horusui.Font

	padding     horusui.GuiPadding
	spacing     horusui.GuiSpacing
	indentSize  float32 // Animate?
	radius      int16   // For rounding elements
	jsonOmitted bool
}

var CheckboxColor = horusui.Color{R: 81, G: 81, B: 81, A: 255} //horusui.Color { 81, 81, 81, 255 };
var CheckboxColor_Unchecked = horusui.Color{R: 81, G: 81, B: 81, A: 0}
var CheckboxColor_Hover = horusui.Color{R: 110, G: 110, B: 110, A: 255}

func LoadThemes(directory string) map[string]Theme {
	dir, err := os.ReadDir(directory)
	if err != nil {
		return map[string]Theme{
			"Default Dark": DefaultDark,
		}
	}

	result := make(map[string]Theme, 0)
	for _, entry := range dir {
		if entry.IsDir() || !strings.HasSuffix(entry.Name(), ".json") || entry.Name() == "" {
			continue
		}
		theme := LoadThemeFromFile(filepath.Join(directory, entry.Name()))
		result[theme.Name] = theme
	}

	return result
}

// Loads a theme from a json file.
func LoadThemeFromFile(filepath string) Theme {
	data, _ := os.ReadFile(filepath)
	var theme ThemeJson
	err := json.Unmarshal(data, &theme)
	if err != nil {
		fmt.Printf("Error loading theme '%s'.\n", filepath)
		return DefaultDark
	}
	result := ThemeJsonToTheme(theme)
	result.Filepath = filepath

	return result
}

func SaveThemeToFile(filepath string, theme Theme) {
	themeJson := ThemeToThemeJson(theme)
	data, err := json.MarshalIndent(themeJson, "", "    ")
	if err != nil {
		panic(err)
	}
	os.WriteFile(filepath, data, 0600)
}

/*
func setDefaultTheme(guiState *horusui.GuiState) {
	// The lightest color, used for some focused input elements, like textboxes.
	// Other elements will use a darker variant of this.
	accentColor := horusui.Color{R: 50, G: 150, B: 50, A: 255}

	// Toolbar
	toolbarTheme.background[Selector_Default] = sidebarTheme.background[Selector_Default]
	toolbarTheme.background[Selector_Hover] = horusui.Color{31, 31, 31, 255}
	toolbarTheme.background[Selector_Focused] = horusui.Color{31, 31, 31, 255}
	toolbarTheme.border[Selector_Default] = sidebarTheme.border[Selector_Default]
	//toolbarTheme.border[Selector_Focused] = { 1, horusui.Color { 200, 0, 0, 255 } };
	toolbarTheme.border[Selector_Focused] = horusui.GuiBorder{1, accentColor}
	toolbarTheme.textColors[Selector_Default] = horusui.OffWhite
}
*/

/*
func setLightTheme(guiState *horusui.GuiState) {
	accentColor := horusui.Color{R: 50, G: 150, B: 50, A: 255}

	// Page
	pageTheme.background[Selector_Default] = horusui.White
	pageTheme.border[Selector_Default] = horusui.GuiBorder{0, horusui.Color{200, 0, 0, 0}} // horusui.Color { 200, 0, 0, 255 }
	pageTheme.textColors[Selector_Default] = horusui.Black
	pageTheme.textColors[Selector_Disabled] = horusui.Color{150, 150, 150, 255}

	// Page Inside
	pageInsideTheme.border[Selector_Default] = horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}} // horusui.Color { 150, 0, 0, 255 }
	pageInsideTheme.border[Selector_Hover] = horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}}
	pageInsideTheme.border[Selector_Focused] = horusui.GuiBorder{1, accentColor}

	// Sidebar
	sidebarTheme.background[Selector_Default] = horusui.Color{21, 21, 21, 255}
	sidebarTheme.border[Selector_Default] = horusui.GuiBorder{1, horusui.Color{0, 200, 0, 255}}
	sidebarTheme.textColors[Selector_Default] = horusui.OffWhite

	// Toolbar
	toolbarTheme.background[Selector_Default] = sidebarTheme.background[Selector_Default]
	toolbarTheme.background[Selector_Focused] = horusui.Color{31, 31, 31, 255}
	toolbarTheme.background[Selector_Focused] = horusui.Color{31, 31, 31, 255}
	toolbarTheme.border[Selector_Default] = sidebarTheme.border[Selector_Default]
	toolbarTheme.border[Selector_Focused] = horusui.GuiBorder{1, accentColor }
	toolbarTheme.textColors[Selector_Default] = horusui.OffWhite

	// Buttons
	defaultButtonTheme.background[Selector_Default] = horusui.Color{41 - 4, 41 - 4, 41 - 4, 255}
	defaultButtonTheme.background[Selector_Hover] = horusui.Color{41 - 8, 41 - 8, 41 - 8, 255}
	defaultButtonTheme.border[Selector_Default] = horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}}
	defaultButtonTheme.border[Selector_Hover] = horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}}
	defaultButtonTheme.textColors[Selector_Default] = horusui.OffWhite
	defaultButtonTheme.padding = guiState.Scale_padding(horusui.GuiPadding{3, 3, 10, 10})
	pageTheme.buttonTheme = defaultButtonTheme
	sidebarTheme.buttonTheme = defaultButtonTheme
	sidebarTheme.buttonTheme.background[Selector_Default] = horusui.Color{21 - 4, 21 - 4, 21 - 4, 255}
	sidebarTheme.buttonTheme.background[Selector_Hover] = horusui.Color{21 - 8, 21 - 8, 21 - 8, 255}

	// Textboxes
	defaultTextboxTheme.background[Selector_Default] = horusui.Color{250, 250, 250, 255}
	defaultTextboxTheme.border[Selector_Default] = horusui.GuiBorder{1, horusui.Color{81, 81, 81, 255}}
	defaultTextboxTheme.border[Selector_Hover] = horusui.GuiBorder{1, horusui.Color{110, 110, 110, 255}}
	defaultTextboxTheme.border[Selector_Focused] = horusui.GuiBorder{1, horusui.Color{140, 140, 140, 255}}
	defaultTextboxTheme.textColors[Selector_Default] = horusui.OffBlack
	defaultTextboxTheme.textColors[Selector_Placeholder] = horusui.Color{150, 150, 150, 255}
	defaultTextboxTheme.padding = guiState.Scale_padding(horusui.GuiPadding{3, 3, 10, 10})
	pageTheme.textboxTheme = defaultTextboxTheme
	sidebarTheme.textboxTheme = defaultTextboxTheme
}
*/

func updateTheme(state *State) {
}

//go:embed fonts/NotoColorEmoji.ttf
var notocoloremoji []byte

//go:embed fonts/Inter/Inter-Regular.ttf
var roboto_regular []byte

//go:embed fonts/Inter/Inter-Bold.ttf
var roboto_bold []byte

//go:embed fonts/Inter/Inter-Italic.ttf
var roboto_italic []byte

//go:embed fonts/MaterialDesignIcons/MaterialIconsRound-Regular.otf
var material_icons []byte

//go:embed fonts/PkgTTC-Iosevka-29.0.5/Iosevka-Regular.ttc
var iosevka []byte

func OpenFontFromMemory(data []byte, size int) *ttf.Font {
	rw, _ := sdl.RWFromMem(data)
	font, _ := ttf.OpenFontRW(rw, 1, size)
	return font
}

func setupFonts(state *State, userScale float64) {
	var dpiFix float64 = 1.0

	// Emoji
	emojiFont := OpenFontFromMemory(notocoloremoji, 109)
	state.emojiFont = &horusui.Font{Font: emojiFont, EmojiFont: emojiFont, Renderer: state.renderer}
	//emojiFont, _ := ttf.OpenFont(filepath.Join(state.fontsDirectory, "NotoColorEmoji.ttf") /*int(float64(15)*userScale*dpiFix)*/, 109)

	// Default // TODO
	font := OpenFontFromMemory(roboto_regular, int(float64(22)*userScale*dpiFix))
	font.SetKerning(true)
	font.SetHinting(ttf.HINTING_NORMAL)
	state.defaultFont = &horusui.Font{Font: font, EmojiFont: emojiFont, Renderer: state.renderer}
	//font, _ := ttf.OpenFont(filepath.Join(state.fontsDirectory, "Roboto/Roboto-Regular.ttf"), int(float64(22)*userScale*dpiFix))
	//font.SetStyle(ttf.STYLE_ITALIC)

	// Tiny Font
	font = OpenFontFromMemory(roboto_regular, int(float64(10.5)*userScale*dpiFix))
	font.SetKerning(true)
	font.SetHinting(ttf.HINTING_NORMAL)
	state.tinyFont = &horusui.Font{Font: font, EmojiFont: emojiFont, Renderer: state.renderer}

	// UI Font
	font = OpenFontFromMemory(roboto_regular, int(float64(15)*userScale*dpiFix))
	font.SetKerning(true)
	font.SetHinting(ttf.HINTING_NORMAL)
	//font.SetStyle(ttf.STYLE_ITALIC)
	state.arial15 = &horusui.Font{Font: font, EmojiFont: emojiFont, Renderer: state.renderer}

	// UI Font - Bold
	font = OpenFontFromMemory(roboto_bold, int(float64(15)*userScale*dpiFix))
	font.SetKerning(true)
	font.SetHinting(ttf.HINTING_NORMAL)
	//font.SetStyle(ttf.STYLE_BOLD)
	//font.SetStyle(ttf.STYLE_ITALIC)
	state.bold_arial15 = &horusui.Font{Font: font, EmojiFont: emojiFont, Renderer: state.renderer}

	// Top Bar UI
	font = OpenFontFromMemory(roboto_regular, int(float64(17)*userScale*dpiFix)) // 16.5
	font.SetKerning(true)
	font.SetHinting(ttf.HINTING_NORMAL)
	state.topBarUiFont = &horusui.Font{Font: font, EmojiFont: emojiFont, Renderer: state.renderer}

	// Top Bar Icons
	font = OpenFontFromMemory(material_icons, int(float64(20)*userScale*dpiFix)) // 16.5
	font.SetKerning(true)
	font.SetHinting(ttf.HINTING_NORMAL)
	state.topBarIconsFont = &horusui.Font{Font: font, EmojiFont: emojiFont, Renderer: state.renderer}

	// -- Document Fonts --
	// Heading 1
	//heading1Font, _ := ttf.OpenFont("fonts/Roboto/Roboto-Bold.ttf", int(float64(15)*state.guiState.UserScale*1.87*dpiFix))
	heading1Font := OpenFontFromMemory(roboto_bold, int(float64(39)*userScale*dpiFix))
	heading1Font.SetKerning(true)
	heading1Font.SetHinting(ttf.HINTING_NORMAL)
	state.documentFonts[ScrollLineType_Heading1] = &horusui.Font{Font: heading1Font, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Heading1] = horusui.White

	// Heading 2
	heading2Font := OpenFontFromMemory(roboto_regular, int(float64(32)*userScale*dpiFix)) // 31
	heading2Font.SetKerning(true)
	heading2Font.SetHinting(ttf.HINTING_NORMAL)
	state.documentFonts[ScrollLineType_Heading2] = &horusui.Font{Font: heading2Font, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Heading2] = horusui.Color{R: 151, G: 171, B: 180, A: 255}

	// Heading 3
	heading3Font := OpenFontFromMemory(roboto_regular, int(float64(24)*userScale*dpiFix))
	heading3Font.SetKerning(true)
	heading3Font.SetHinting(ttf.HINTING_NORMAL)
	state.documentFonts[ScrollLineType_Heading3] = &horusui.Font{Font: heading3Font, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Heading3] = horusui.Color{R: 103, G: 153, B: 178, A: 255}

	// Heading 4
	heading4Font := OpenFontFromMemory(roboto_italic, int(float64(20 /*16.05*/)*userScale*dpiFix))
	heading4Font.SetKerning(true)
	heading4Font.SetHinting(ttf.HINTING_NORMAL)
	heading4Font.SetStyle(ttf.STYLE_ITALIC)
	state.documentFonts[ScrollLineType_Heading4] = &horusui.Font{Font: heading4Font, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Heading4] = horusui.Color{R: 103, G: 153, B: 178, A: 255}

	// Preformatted Text and Plain Text Files
	preformatFont := OpenFontFromMemory(iosevka, int(float64(17)*userScale*dpiFix))
	preformatFont.SetKerning(true)
	preformatFont.SetHinting(ttf.HINTING_NORMAL)
	state.documentFonts[ScrollLineType_Preformat] = &horusui.Font{Font: preformatFont, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Preformat] = horusui.Color{R: 165, G: 165, B: 165, A: 255}

	// Body
	bodyFont := OpenFontFromMemory(roboto_regular, int(float64(19)*userScale*dpiFix))
	bodyFont.SetKerning(true)
	bodyFont.SetHinting(ttf.HINTING_NORMAL)
	state.documentFonts[ScrollLineType_Body] = &horusui.Font{Font: bodyFont, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Body] = horusui.Color{R: 180, G: 180, B: 180, A: 255}

	// Blockquotes
	state.documentFonts[ScrollLineType_Quote] = &horusui.Font{Font: bodyFont, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Quote] = horusui.Color{R: 155, G: 155, B: 155, A: 255}

	// Links
	state.documentFonts[ScrollLineType_Link] = &horusui.Font{Font: bodyFont, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentFonts[ScrollLineType_Link_Nex] = &horusui.Font{Font: preformatFont, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_Link] = horusui.OffWhite
	state.documentColors[ScrollLineType_Link_Nex] = horusui.OffWhite
	//state.documentColors[ScrollLineType_Link_Hover] = horusui.Color{R: 32, G: 191, B: 253, A: 255}

	// List Items
	state.documentFonts[ScrollLineType_ListItem] = &horusui.Font{Font: bodyFont, EmojiFont: emojiFont, Renderer: state.renderer}
	state.documentColors[ScrollLineType_ListItem] = horusui.Color{R: 180, G: 180, B: 180, A: 255}
}

// Frees all of the fonts and all of the guiState's textCache.
func freeFonts(state *State) {
	state.guiState.FreeTextCache()

	state.defaultFont.Font.Close()
	state.defaultFont.EmojiFont.Close()
	state.tinyFont.Font.Close()
	state.arial15.Font.Close()
	state.topBarUiFont.Font.Close()
	state.documentFonts[ScrollLineType_Heading1].Font.Close()
	state.documentFonts[ScrollLineType_Heading2].Font.Close()
	state.documentFonts[ScrollLineType_Heading3].Font.Close()
	state.documentFonts[ScrollLineType_Heading4].Font.Close()
	state.documentFonts[ScrollLineType_Body].Font.Close()
	state.documentFonts[ScrollLineType_Preformat].Font.Close()
}
