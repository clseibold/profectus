package horusui

import "math"

type AnimationType uint8

const (
	AnimationType_Linear AnimationType = iota
	AnimationType_Smoothstep
	AnimationType_Smootherstep
	AnimationType_Step_3
	AnimationType_Step_4
	AnimationType_Step_5
	AnimationType_Jump // Jumps to the ending value at the end of the animation.

	AnimationType_EaseInQuad
	AnimationType_EaseOutQuad
	AnimationType_EaseInOutQuad

	AnimationType_EaseInCubic
	AnimationType_EaseOutCubic
	AnimationType_EaseInOutCubic

	AnimationType_EaseInSine
	AnimationType_EaseOutSine
	AnimationType_EaseInOutSine

	AnimationType_EaseInExpo
	AnimationType_EaseOutExpo
	AnimationType_EaseInOutExpo

	AnimationType_EaseOutBounce
	AnimationType_EaseOutElastic
)

// ----- Show/Hide Animation -----

type ShowHideAnimation struct {
	visibility        bool // If true, shown or in process of being shown. If false, hidden or in process of being hidden.
	showAnimationType AnimationType
	hideAnimationType AnimationType
	NumberAnimation[float32]
}

func CreateShowHideAnimation(showAnimationType AnimationType, hideAnimationType AnimationType, ms_total float64, startVisible bool) (animation ShowHideAnimation) {
	animation.showAnimationType = showAnimationType
	animation.hideAnimationType = hideAnimationType
	animation.NumberAnimation = CreateNumberAnimation(showAnimationType, ms_total, float32(0), float32(1))
	if startVisible {
		animation.visibility = true
		animation.SetForwards()
		animation.SetFinished()
	} else {
		animation.visibility = false
		animation.SetBackwards()
		animation.SetFinished()
	}

	return animation
}

func (anim *ShowHideAnimation) Show() {
	if anim.visibility {
		// Already shown or in process of being shown
		return
	}
	anim.visibility = true
	anim.SetAnimationType(anim.showAnimationType)
	anim.SetForwards()
	anim.Reset()
}
func (anim *ShowHideAnimation) Hide() {
	if !anim.visibility {
		// Already hidden or in process of being hidden
		return
	}
	anim.visibility = false
	anim.SetAnimationType(anim.hideAnimationType)
	anim.SetBackwards()
	anim.Reset()
}
func (anim *ShowHideAnimation) Toggle() {
	if anim.visibility {
		anim.Hide()
	} else {
		anim.Show()
	}
}

func (anim *ShowHideAnimation) Visibility() bool {
	return anim.visibility
}

// ----- Number Animation -----

type NumberAnimation[T Number] struct {
	animationType AnimationType
	paused        bool
	backwards     bool
	ms            float64
	ms_total      float64

	start T
	end   T
}

// Creates a new animation with an end
func CreateNumberAnimation[T Number](animationType AnimationType, ms_total float64, start T, end T) (animation NumberAnimation[T]) {
	animation.animationType = animationType

	animation.ms = 0
	animation.ms_total = ms_total
	animation.start = start
	animation.end = end

	return animation
}

// An animation that is static, it doesn't animate yet. It will register as always at its ending value.
func CreateStaticNumberAnimation[T Number](value T) (animation NumberAnimation[T]) {
	animation.start = value
	animation.end = value
	animation.ms_total = 0 // Static animation
	animation.ms = 0

	return animation
}

// Sets the animation type.
func (anim *NumberAnimation[T]) SetAnimationType(animationType AnimationType) {
	anim.animationType = animationType
}

// Sets the animation to be static, using a static value.
func (anim *NumberAnimation[T]) SetStaticValue(value T) {
	anim.start = value
	anim.end = value
	//anim.ms_total = 0
	//anim.ms = 0
}

// Sets the animation details (type and ms_total). Turns a static "animation" to a proper animation.
func (anim *NumberAnimation[T]) SetAnimation(animationType AnimationType, ms_total float64) {
	anim.animationType = animationType
	anim.ms_total = ms_total
}

// Sets the starting value to the current value, reset's the animation, then sets the new ending value.
func (anim *NumberAnimation[T]) SaveCurrentAndSetNewEnd(end T) {
	anim.start = anim.CurrentValue()
	anim.Reset()
	anim.end = end
}

// Sets new starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *NumberAnimation[T]) SetNewStartingValue(start T, keepCurrent bool) {
	valueBefore := anim.CurrentValue()
	if valueBefore < start {
		valueBefore = start
	}
	anim.start = start
	if keepCurrent {
		newPercent := InterpPercentFromValue[T, float64](start, anim.end, valueBefore)
		anim.ms = newPercent * anim.ms_total
	}
}

// Add to starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *NumberAnimation[T]) AddToStartingValue(start T, keepCurrent bool) {
	anim.SetNewStartingValue(anim.start+start, keepCurrent)
}

// Sets new ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *NumberAnimation[T]) SetNewEndingValue(end T, keepCurrent bool) {
	valueBefore := anim.CurrentValue()
	if valueBefore > end {
		valueBefore = end
	}
	anim.end = end
	if keepCurrent {
		newPercent := InterpPercentFromValue[T, float64](anim.start, end, valueBefore)
		anim.ms = newPercent * anim.ms_total
	}
}

// Add to ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *NumberAnimation[T]) AddToEndingValue(end T, keepCurrent bool) {
	anim.SetNewEndingValue(anim.end+end, keepCurrent)
}

// Sets new ending time of animation. If translateProgress is set to true, it will keep the same progress percentage.
// Otherwise, the progress will not be kept and will decrease/increse depending on if the new end value is higher/lower
// then the previous one (respectively).
func (anim *NumberAnimation[T]) SetNewTotalMS(ms_total float64, translateProgress bool) {
	progressBefore := anim.CurrentProgress()
	anim.ms_total = ms_total
	if translateProgress {
		anim.ms = progressBefore * anim.ms_total
	}
}

func (anim *NumberAnimation[T]) TotalMS() float64 {
	return anim.ms_total
}

// Percentage of current progress.
func (anim *NumberAnimation[T]) CurrentProgress() float64 {
	if anim.ms_total == 0 {
		return 1.0
	}
	return anim.ms / anim.ms_total
}

// Percentage left until completion.
func (anim *NumberAnimation[T]) PercentToCompletion() float64 {
	return (anim.ms_total - anim.ms) / anim.ms_total
}

// Reset animation back to starting value. If set to backwards, resets to ending value.
func (anim *NumberAnimation[T]) Reset() {
	if anim.backwards {
		anim.ms = anim.ms_total
	} else {
		anim.ms = 0
	}
}

func (anim *NumberAnimation[T]) ToggleDirection() {
	if anim.backwards {
		anim.SetForwards()
	} else {
		anim.SetBackwards()
	}
}

// Set the animation to go backwards. It will move backwards from the ending value to the starting value.
func (anim *NumberAnimation[T]) SetBackwards() {
	anim.backwards = true
}

// Set the animation to go forwards. It will move forwards from the starting value to the ending value.
func (anim *NumberAnimation[T]) SetForwards() {
	anim.backwards = false
}

func (anim *NumberAnimation[T]) IsBackwards() bool {
	return anim.backwards
}

// Pause the animation.
func (anim *NumberAnimation[T]) Pause() {
	anim.paused = true
}

// Resume the animation.
func (anim *NumberAnimation[T]) Resume() {
	anim.paused = false
}

func (anim *NumberAnimation[T]) IsPaused() bool {
	return anim.paused
}

// Update the animation, returning the current value and whether the animation is at its ending value.
func (anim *NumberAnimation[T]) Update(deltaMS float64) (T, bool) {
	finished := anim.IsFinished()
	if !finished && !anim.paused && anim.ms_total != 0 {
		if anim.backwards {
			anim.ms -= deltaMS
			if anim.ms < 0 {
				anim.ms = 0
			}
		} else {
			anim.ms += deltaMS
			if anim.ms >= anim.ms_total {
				anim.ms = anim.ms_total
			}
		}
	}
	return anim.CurrentValue(), finished
}

// Returns the current value.
func (anim *NumberAnimation[T]) CurrentValue() T {
	percentToCompletion := anim.CurrentProgress()
	return Interp(anim.start, anim.end, Ease(anim.animationType, percentToCompletion))
}

func (anim *NumberAnimation[T]) IsFinished() bool {
	if anim.backwards && anim.ms <= 0 {
		return true
	} else if !anim.backwards && anim.ms >= anim.ms_total {
		return true
	} else {
		return false
	}
}

// Sets the animation to its finished value
func (anim *NumberAnimation[T]) SetFinished() {
	if anim.backwards {
		anim.ms = 0
	} else {
		anim.ms = anim.ms_total
	}
}

func (anim *NumberAnimation[T]) StartValue() T {
	return anim.start
}

func (anim *NumberAnimation[T]) EndValue() T {
	return anim.end
}

// ----- Color Animation -----

type ColorAnimation struct {
	r NumberAnimation[uint8]
	g NumberAnimation[uint8]
	b NumberAnimation[uint8]
	a NumberAnimation[uint8]
}

func CreateColorAnimation(animationType AnimationType, ms_total float64, start Color, end Color) (animation ColorAnimation) {
	// If colors are the same, use a Jump animation instead
	if start == end {
		animationType = AnimationType_Jump
	}

	// Just animate the alpha if the ending alpha will be 0. Use a jump for the other colors to jump to them at the end when the alpha is 0. This should improve color animations.
	if end.A == 0 {
		animation.r = CreateNumberAnimation(AnimationType_Jump, ms_total, start.R, end.R)
		animation.g = CreateNumberAnimation(AnimationType_Jump, ms_total, start.G, end.G)
		animation.b = CreateNumberAnimation(AnimationType_Jump, ms_total, start.B, end.B)
	} else {
		animation.r = CreateNumberAnimation(animationType, ms_total, start.R, end.R)
		animation.g = CreateNumberAnimation(animationType, ms_total, start.G, end.G)
		animation.b = CreateNumberAnimation(animationType, ms_total, start.B, end.B)
	}

	animation.a = CreateNumberAnimation(animationType, ms_total, start.A, end.A)

	return animation
}

// Sets the animation type.
func (anim *ColorAnimation) SetAnimationType(animationType AnimationType) {
	anim.r.SetAnimationType(animationType)
	anim.g.SetAnimationType(animationType)
	anim.b.SetAnimationType(animationType)
	anim.a.SetAnimationType(animationType)
}

// Sets the animation to be static, using a static value.
func (anim *ColorAnimation) SetStaticValue(value Color) {
	anim.r.SetStaticValue(value.R)
	anim.g.SetStaticValue(value.G)
	anim.b.SetStaticValue(value.B)
	anim.a.SetStaticValue(value.A)
}

// Sets the animation details (type and ms_total). Turns a static "animation" to a proper animation.
func (anim *ColorAnimation) SetAnimation(animationType AnimationType, ms_total float64) {
	anim.r.SetAnimation(animationType, ms_total)
	anim.g.SetAnimation(animationType, ms_total)
	anim.b.SetAnimation(animationType, ms_total)
	anim.a.SetAnimation(animationType, ms_total)
}

// Sets the starting value to the current value, reset's the animation, then sets the new ending value.
func (anim *ColorAnimation) SaveCurrentAndSetNewEnd(end Color) {
	anim.r.SaveCurrentAndSetNewEnd(end.R)
	anim.g.SaveCurrentAndSetNewEnd(end.G)
	anim.b.SaveCurrentAndSetNewEnd(end.B)
	anim.a.SaveCurrentAndSetNewEnd(end.A)
}

// Sets new starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *ColorAnimation) SetNewStartingValue(start Color, keepCurrent bool) {
	anim.r.SetNewStartingValue(start.R, keepCurrent)
	anim.g.SetNewStartingValue(start.G, keepCurrent)
	anim.b.SetNewStartingValue(start.B, keepCurrent)
	anim.a.SetNewStartingValue(start.A, keepCurrent)
}

// Add to starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *ColorAnimation) AddToStartingValue(start Color, keepCurrent bool) {
	anim.r.AddToStartingValue(start.R, keepCurrent)
	anim.g.AddToStartingValue(start.G, keepCurrent)
	anim.b.AddToStartingValue(start.B, keepCurrent)
	anim.a.AddToStartingValue(start.A, keepCurrent)
}

// Sets new ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *ColorAnimation) SetNewEndingValue(end Color, keepCurrent bool) {
	anim.r.SetNewEndingValue(end.R, keepCurrent)
	anim.g.SetNewEndingValue(end.G, keepCurrent)
	anim.b.SetNewEndingValue(end.B, keepCurrent)
	anim.a.SetNewEndingValue(end.A, keepCurrent)
}

// Add to ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *ColorAnimation) AddToEndingValue(end Color, keepCurrent bool) {
	anim.r.AddToEndingValue(end.R, keepCurrent)
	anim.g.AddToEndingValue(end.G, keepCurrent)
	anim.b.AddToEndingValue(end.B, keepCurrent)
	anim.a.AddToEndingValue(end.A, keepCurrent)
}

// Sets new ending time of animation. If translateProgress is set to true, it will keep the same progress percentage.
// Otherwise, the progress will not be kept and will decrease/increse depending on if the new end value is higher/lower
// then the previous one (respectively).
func (anim *ColorAnimation) SetNewTotalMS(ms_total float64, translateProgress bool) {
	anim.r.SetNewTotalMS(ms_total, translateProgress)
	anim.g.SetNewTotalMS(ms_total, translateProgress)
	anim.b.SetNewTotalMS(ms_total, translateProgress)
	anim.a.SetNewTotalMS(ms_total, translateProgress)
}

func (anim *ColorAnimation) TotalMS() float64 {
	return anim.r.TotalMS()
}

// Percentage of current progress.
func (anim *ColorAnimation) CurrentProgress() float64 {
	return anim.r.CurrentProgress()
}

// Percentage left until completion.
func (anim *ColorAnimation) PercentToCompletion() float64 {
	return anim.r.PercentToCompletion()
}

// Reset animation back to starting value. If set to backwards, resets to ending value.
func (anim *ColorAnimation) Reset() {
	anim.r.Reset()
	anim.g.Reset()
	anim.b.Reset()
	anim.a.Reset()
}

// Set the animation to go backwards. It will move backwards from the ending value to the starting value.
func (anim *ColorAnimation) SetBackwards() {
	anim.r.SetBackwards()
	anim.g.SetBackwards()
	anim.b.SetBackwards()
	anim.a.SetBackwards()
}

func (anim *ColorAnimation) ToggleDirection() {
	anim.r.ToggleDirection()
	anim.g.ToggleDirection()
	anim.b.ToggleDirection()
	anim.a.ToggleDirection()
}

// Set the animation to go forwards. It will move forwards from the starting value to the ending value.
func (anim *ColorAnimation) SetForwards() {
	anim.r.SetForwards()
	anim.g.SetForwards()
	anim.b.SetForwards()
	anim.a.SetForwards()
}

func (anim *ColorAnimation) IsBackwards() bool {
	return anim.r.IsBackwards()
}

// Pause the animation.
func (anim *ColorAnimation) Pause() {
	anim.r.Pause()
	anim.g.Pause()
	anim.b.Pause()
	anim.a.Pause()
}

// Resume the animation.
func (anim *ColorAnimation) Resume() {
	anim.r.Resume()
	anim.g.Resume()
	anim.b.Resume()
	anim.a.Resume()
}

func (anim *ColorAnimation) IsPaused() bool {
	return anim.r.IsPaused()
}

func (anim *ColorAnimation) Update(deltaTime float64) (color Color, finished bool) {
	color.R, finished = anim.r.Update(deltaTime)
	color.G, _ = anim.g.Update(deltaTime)
	color.B, _ = anim.b.Update(deltaTime)
	color.A, _ = anim.a.Update(deltaTime)

	return color, finished
}

func (anim *ColorAnimation) CurrentValue() (color Color) {
	color.R = anim.r.CurrentValue()
	color.G = anim.g.CurrentValue()
	color.B = anim.b.CurrentValue()
	color.A = anim.a.CurrentValue()

	return color
}

func (anim *ColorAnimation) IsFinished() bool {
	return anim.r.IsFinished()
}

func (anim *ColorAnimation) SetFinished() {
	anim.r.SetFinished()
	anim.g.SetFinished()
	anim.b.SetFinished()
	anim.a.SetFinished()
}

func (anim *ColorAnimation) StartValue() (color Color) {
	color.R = anim.r.StartValue()
	color.G = anim.g.StartValue()
	color.B = anim.b.StartValue()
	color.A = anim.a.StartValue()

	return color
}

func (anim *ColorAnimation) EndValue() (color Color) {
	color.R = anim.r.EndValue()
	color.G = anim.g.EndValue()
	color.B = anim.b.EndValue()
	color.A = anim.a.EndValue()

	return color
}

// ----- Border Animation -----

type BorderAnimation struct {
	thickness NumberAnimation[uint16]
	color     ColorAnimation
}

func CreateBorderAnimation(animationType AnimationType, ms_total float64, start GuiBorder, end GuiBorder) (animation BorderAnimation) {
	if start.Thickness == end.Thickness || math.Abs(float64(start.Thickness-end.Thickness)) == 1 {
		animation.thickness = CreateNumberAnimation(AnimationType_Jump, ms_total, start.Thickness, end.Thickness)
	} else {
		animation.thickness = CreateNumberAnimation(animationType, ms_total, start.Thickness, end.Thickness)
	}

	animation.color = CreateColorAnimation(animationType, ms_total, start.Color, end.Color)

	return animation
}

// Sets the animation type.
func (anim *BorderAnimation) SetAnimationType(animationType AnimationType) {
	anim.thickness.SetAnimationType(animationType)
	anim.color.SetAnimationType(animationType)
}

// Sets the animation to be static, using a static value.
func (anim *BorderAnimation) SetStaticValue(value GuiBorder) {
	anim.thickness.SetStaticValue(value.Thickness)
	anim.color.SetStaticValue(value.Color)
}

// Sets the animation details (type and ms_total). Turns a static "animation" to a proper animation.
func (anim *BorderAnimation) SetAnimation(animationType AnimationType, ms_total float64) {
	anim.thickness.SetAnimation(animationType, ms_total)
	anim.color.SetAnimation(animationType, ms_total)
}

// Sets the starting value to the current value, reset's the animation, then sets the new ending value.
func (anim *BorderAnimation) SaveCurrentAndSetNewEnd(end GuiBorder) {
	anim.thickness.SaveCurrentAndSetNewEnd(end.Thickness)
	anim.color.SaveCurrentAndSetNewEnd(end.Color)
}

// Sets new starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *BorderAnimation) SetNewStartingValue(start GuiBorder, keepCurrent bool) {
	anim.thickness.SetNewStartingValue(start.Thickness, keepCurrent)
	anim.color.SetNewStartingValue(start.Color, keepCurrent)
}

// Add to starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *BorderAnimation) AddToStartingValue(start GuiBorder, keepCurrent bool) {
	anim.thickness.AddToStartingValue(start.Thickness, keepCurrent)
	anim.color.AddToStartingValue(start.Color, keepCurrent)
}

// Sets new ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *BorderAnimation) SetNewEndingValue(end GuiBorder, keepCurrent bool) {
	anim.thickness.SetNewEndingValue(end.Thickness, keepCurrent)
	anim.color.SetNewEndingValue(end.Color, keepCurrent)
}

// Add to ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *BorderAnimation) AddToEndingValue(end GuiBorder, keepCurrent bool) {
	anim.thickness.AddToEndingValue(end.Thickness, keepCurrent)
	anim.color.AddToEndingValue(end.Color, keepCurrent)
}

// Sets new ending time of animation. If translateProgress is set to true, it will keep the same progress percentage.
// Otherwise, the progress will not be kept and will decrease/increse depending on if the new end value is higher/lower
// then the previous one (respectively).
func (anim *BorderAnimation) SetNewTotalMS(ms_total float64, translateProgress bool) {
	anim.thickness.SetNewTotalMS(ms_total, translateProgress)
	anim.color.SetNewTotalMS(ms_total, translateProgress)
}

func (anim *BorderAnimation) TotalMS() float64 {
	return anim.color.TotalMS()
}

// Percentage of current progress.
func (anim *BorderAnimation) CurrentProgress() float64 {
	return anim.color.CurrentProgress()
}

// Percentage left until completion.
func (anim *BorderAnimation) PercentToCompletion() float64 {
	return anim.color.PercentToCompletion()
}

// Reset animation back to starting value. If set to backwards, resets to ending value.
func (anim *BorderAnimation) Reset() {
	anim.thickness.Reset()
	anim.color.Reset()
}

// Set the animation to go backwards. It will move backwards from the ending value to the starting value.
func (anim *BorderAnimation) SetBackwards() {
	anim.thickness.SetBackwards()
	anim.color.SetBackwards()
}

func (anim *BorderAnimation) ToggleDirection() {
	anim.thickness.ToggleDirection()
	anim.color.ToggleDirection()
}

// Set the animation to go forwards. It will move forwards from the starting value to the ending value.
func (anim *BorderAnimation) SetForwards() {
	anim.thickness.SetForwards()
	anim.color.SetForwards()
}

func (anim *BorderAnimation) IsBackwards() bool {
	return anim.color.IsBackwards()
}

// Pause the animation.
func (anim *BorderAnimation) Pause() {
	anim.thickness.Pause()
	anim.color.Pause()
}

// Resume the animation.
func (anim *BorderAnimation) Resume() {
	anim.thickness.Resume()
	anim.color.Resume()
}

func (anim *BorderAnimation) IsPaused() bool {
	return anim.color.IsPaused()
}

func (anim *BorderAnimation) Update(deltaTime float64) (border GuiBorder, finished bool) {
	border.Thickness, _ = anim.thickness.Update(deltaTime)
	border.Color, finished = anim.color.Update(deltaTime)

	return border, finished
}

func (anim *BorderAnimation) CurrentValue() (border GuiBorder) {
	border.Thickness = anim.thickness.CurrentValue()
	border.Color = anim.color.CurrentValue()

	return border
}

func (anim *BorderAnimation) IsFinished() bool {
	return anim.color.IsFinished()
}

func (anim *BorderAnimation) SetFinished() {
	anim.thickness.SetFinished()
	anim.color.SetFinished()
}

func (anim *BorderAnimation) StartValue() (border GuiBorder) {
	border.Thickness = anim.thickness.StartValue()
	border.Color = anim.color.StartValue()

	return border
}

func (anim *BorderAnimation) EndValue() (border GuiBorder) {
	border.Thickness = anim.thickness.EndValue()
	border.Color = anim.color.EndValue()

	return border
}

// ----- Padding Animation -----

type PaddingAnimation struct {
	top    NumberAnimation[float32]
	bottom NumberAnimation[float32]
	left   NumberAnimation[float32]
	right  NumberAnimation[float32]
}

func CreatePaddingAnimation(animationType AnimationType, ms_total float64, start GuiPadding, end GuiPadding) (animation PaddingAnimation) {
	animation.top = CreateNumberAnimation(animationType, ms_total, start.Top, end.Top)
	animation.bottom = CreateNumberAnimation(animationType, ms_total, start.Bottom, end.Bottom)
	animation.left = CreateNumberAnimation(animationType, ms_total, start.Left, end.Left)
	animation.right = CreateNumberAnimation(animationType, ms_total, start.Right, end.Right)

	return animation
}

// Sets the animation type.
func (anim *PaddingAnimation) SetAnimationType(animationType AnimationType) {
	anim.top.SetAnimationType(animationType)
	anim.bottom.SetAnimationType(animationType)
	anim.left.SetAnimationType(animationType)
	anim.right.SetAnimationType(animationType)
}

// Sets the animation to be static, using a static value.
func (anim *PaddingAnimation) SetStaticValue(value GuiPadding) {
	anim.top.SetStaticValue(value.Top)
	anim.bottom.SetStaticValue(value.Bottom)
	anim.left.SetStaticValue(value.Left)
	anim.right.SetStaticValue(value.Right)
}

// Sets the animation details (type and ms_total). Turns a static "animation" to a proper animation.
func (anim *PaddingAnimation) SetAnimation(animationType AnimationType, ms_total float64) {
	anim.top.SetAnimation(animationType, ms_total)
	anim.bottom.SetAnimation(animationType, ms_total)
	anim.left.SetAnimation(animationType, ms_total)
	anim.right.SetAnimation(animationType, ms_total)
}

// Sets the starting value to the current value, reset's the animation, then sets the new ending value.
func (anim *PaddingAnimation) SaveCurrentAndSetNewEnd(end GuiPadding) {
	anim.top.SaveCurrentAndSetNewEnd(end.Top)
	anim.bottom.SaveCurrentAndSetNewEnd(end.Bottom)
	anim.left.SaveCurrentAndSetNewEnd(end.Left)
	anim.right.SaveCurrentAndSetNewEnd(end.Right)
}

// Sets new starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *PaddingAnimation) SetNewStartingValue(start GuiPadding, keepCurrent bool) {
	anim.top.SetNewStartingValue(start.Top, keepCurrent)
	anim.bottom.SetNewStartingValue(start.Bottom, keepCurrent)
	anim.left.SetNewStartingValue(start.Left, keepCurrent)
	anim.right.SetNewStartingValue(start.Right, keepCurrent)
}

// Add to starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *PaddingAnimation) AddToStartingValue(start GuiPadding, keepCurrent bool) {
	anim.top.AddToStartingValue(start.Top, keepCurrent)
	anim.bottom.AddToStartingValue(start.Bottom, keepCurrent)
	anim.left.AddToStartingValue(start.Left, keepCurrent)
	anim.right.AddToStartingValue(start.Right, keepCurrent)
}

// Sets new ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *PaddingAnimation) SetNewEndingValue(end GuiPadding, keepCurrent bool) {
	anim.top.SetNewEndingValue(end.Top, keepCurrent)
	anim.bottom.SetNewEndingValue(end.Bottom, keepCurrent)
	anim.left.SetNewEndingValue(end.Left, keepCurrent)
	anim.right.SetNewEndingValue(end.Right, keepCurrent)
}

// Add to ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *PaddingAnimation) AddToEndingValue(end GuiPadding, keepCurrent bool) {
	anim.top.AddToEndingValue(end.Top, keepCurrent)
	anim.bottom.AddToEndingValue(end.Bottom, keepCurrent)
	anim.left.AddToEndingValue(end.Left, keepCurrent)
	anim.right.AddToEndingValue(end.Right, keepCurrent)
}

// Sets new ending time of animation. If translateProgress is set to true, it will keep the same progress percentage.
// Otherwise, the progress will not be kept and will decrease/increse depending on if the new end value is higher/lower
// then the previous one (respectively).
func (anim *PaddingAnimation) SetNewTotalMS(ms_total float64, translateProgress bool) {
	anim.top.SetNewTotalMS(ms_total, translateProgress)
	anim.bottom.SetNewTotalMS(ms_total, translateProgress)
	anim.left.SetNewTotalMS(ms_total, translateProgress)
	anim.right.SetNewTotalMS(ms_total, translateProgress)
}

func (anim *PaddingAnimation) TotalMS() float64 {
	return anim.top.TotalMS()
}

// Percentage of current progress.
func (anim *PaddingAnimation) CurrentProgress() float64 {
	return anim.top.CurrentProgress()
}

// Percentage left until completion.
func (anim *PaddingAnimation) PercentToCompletion() float64 {
	return anim.top.PercentToCompletion()
}

// Reset animation back to starting value. If set to backwards, resets to ending value.
func (anim *PaddingAnimation) Reset() {
	anim.top.Reset()
	anim.bottom.Reset()
	anim.left.Reset()
	anim.right.Reset()
}

// Set the animation to go backwards. It will move backwards from the ending value to the starting value.
func (anim *PaddingAnimation) SetBackwards() {
	anim.top.SetBackwards()
	anim.bottom.SetBackwards()
	anim.left.SetBackwards()
	anim.right.SetBackwards()
}

func (anim *PaddingAnimation) ToggleDirection() {
	anim.top.ToggleDirection()
	anim.bottom.ToggleDirection()
	anim.left.ToggleDirection()
	anim.right.ToggleDirection()
}

// Set the animation to go forwards. It will move forwards from the starting value to the ending value.
func (anim *PaddingAnimation) SetForwards() {
	anim.top.SetForwards()
	anim.bottom.SetForwards()
	anim.left.SetForwards()
	anim.right.SetForwards()
}

func (anim *PaddingAnimation) IsBackwards() bool {
	return anim.top.IsBackwards()
}

// Pause the animation.
func (anim *PaddingAnimation) Pause() {
	anim.top.Pause()
	anim.bottom.Pause()
	anim.left.Pause()
	anim.right.Pause()
}

// Resume the animation.
func (anim *PaddingAnimation) Resume() {
	anim.top.Resume()
	anim.bottom.Resume()
	anim.left.Resume()
	anim.right.Resume()
}

func (anim *PaddingAnimation) IsPaused() bool {
	return anim.top.IsPaused()
}

func (anim *PaddingAnimation) Update(deltaTime float64) (padding GuiPadding, finished bool) {
	padding.Top, finished = anim.top.Update(deltaTime)
	padding.Bottom, finished = anim.bottom.Update(deltaTime)
	padding.Left, finished = anim.left.Update(deltaTime)
	padding.Right, finished = anim.right.Update(deltaTime)

	return padding, finished
}

func (anim *PaddingAnimation) CurrentValue() (padding GuiPadding) {
	padding.Top = anim.top.CurrentValue()
	padding.Bottom = anim.bottom.CurrentValue()
	padding.Left = anim.left.CurrentValue()
	padding.Right = anim.right.CurrentValue()

	return padding
}

func (anim *PaddingAnimation) IsFinished() bool {
	return anim.top.IsFinished()
}

func (anim *PaddingAnimation) SetFinished() {
	anim.top.SetFinished()
	anim.bottom.SetFinished()
	anim.left.SetFinished()
	anim.right.SetFinished()
}

func (anim *PaddingAnimation) StartValue() (padding GuiPadding) {
	padding.Top = anim.top.StartValue()
	padding.Bottom = anim.bottom.StartValue()
	padding.Left = anim.left.StartValue()
	padding.Right = anim.right.StartValue()

	return padding
}

func (anim *PaddingAnimation) EndValue() (padding GuiPadding) {
	padding.Top = anim.top.EndValue()
	padding.Bottom = anim.bottom.EndValue()
	padding.Left = anim.left.EndValue()
	padding.Right = anim.right.EndValue()

	return padding
}

// ----- Spacing Animation -----

type SpacingAnimation struct {
	x NumberAnimation[float32]
	y NumberAnimation[float32]
}

func CreateSpacingAnimation(animationType AnimationType, ms_total float64, start GuiSpacing, end GuiSpacing) (animation SpacingAnimation) {
	animation.x = CreateNumberAnimation(animationType, ms_total, start.X, end.X)
	animation.y = CreateNumberAnimation(animationType, ms_total, start.Y, end.Y)

	return animation
}

// Sets the animation type.
func (anim *SpacingAnimation) SetAnimationType(animationType AnimationType) {
	anim.x.SetAnimationType(animationType)
	anim.y.SetAnimationType(animationType)
}

// Sets the animation to be static, using a static value.
func (anim *SpacingAnimation) SetStaticValue(value GuiSpacing) {
	anim.x.SetStaticValue(value.X)
	anim.y.SetStaticValue(value.Y)
}

// Sets the animation details (type and ms_total). Turns a static "animation" to a proper animation.
func (anim *SpacingAnimation) SetAnimation(animationType AnimationType, ms_total float64) {
	anim.x.SetAnimation(animationType, ms_total)
	anim.y.SetAnimation(animationType, ms_total)
}

// Sets the starting value to the current value, reset's the animation, then sets the new ending value.
func (anim *SpacingAnimation) SaveCurrentAndSetNewEnd(end GuiSpacing) {
	anim.x.SaveCurrentAndSetNewEnd(end.X)
	anim.y.SaveCurrentAndSetNewEnd(end.Y)
}

// Sets new starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *SpacingAnimation) SetNewStartingValue(start GuiSpacing, keepCurrent bool) {
	anim.x.SetNewStartingValue(start.X, keepCurrent)
	anim.y.SetNewStartingValue(start.Y, keepCurrent)
}

// Add to starting value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *SpacingAnimation) AddToStartingValue(start GuiSpacing, keepCurrent bool) {
	anim.x.AddToStartingValue(start.X, keepCurrent)
	anim.y.AddToStartingValue(start.Y, keepCurrent)
}

// Sets new ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *SpacingAnimation) SetNewEndingValue(end GuiSpacing, keepCurrent bool) {
	anim.x.SetNewEndingValue(end.X, keepCurrent)
	anim.y.SetNewEndingValue(end.Y, keepCurrent)
}

// Add to ending value of animation. If keepCurrent is set to true, it will keep the same current value, changing
// the current progress to match. If the current value is out of bounds, it will be clipped between the starting
// and ending values. If keepCurrent is false, the current value will change based on the animation's timer progress.
func (anim *SpacingAnimation) AddToEndingValue(end GuiSpacing, keepCurrent bool) {
	anim.x.AddToEndingValue(end.X, keepCurrent)
	anim.y.AddToEndingValue(end.Y, keepCurrent)
}

// Sets new ending time of animation. If translateProgress is set to true, it will keep the same progress percentage.
// Otherwise, the progress will not be kept and will decrease/increse depending on if the new end value is higher/lower
// then the previous one (respectively).
func (anim *SpacingAnimation) SetNewTotalMS(ms_total float64, translateProgress bool) {
	anim.x.SetNewTotalMS(ms_total, translateProgress)
	anim.y.SetNewTotalMS(ms_total, translateProgress)
}

func (anim *SpacingAnimation) TotalMS() float64 {
	return anim.x.TotalMS()
}

// Percentage of current progress.
func (anim *SpacingAnimation) CurrentProgress() float64 {
	return anim.x.CurrentProgress()
}

// Percentage left until completion.
func (anim *SpacingAnimation) PercentToCompletion() float64 {
	return anim.x.PercentToCompletion()
}

// Reset animation back to starting value. If set to backwards, resets to ending value.
func (anim *SpacingAnimation) Reset() {
	anim.x.Reset()
	anim.y.Reset()
}

// Set the animation to go backwards. It will move backwards from the ending value to the starting value.
func (anim *SpacingAnimation) SetBackwards() {
	anim.x.SetBackwards()
	anim.y.SetBackwards()
}

func (anim *SpacingAnimation) ToggleDirection() {
	anim.x.ToggleDirection()
	anim.y.ToggleDirection()
}

// Set the animation to go forwards. It will move forwards from the starting value to the ending value.
func (anim *SpacingAnimation) SetForwards() {
	anim.x.SetForwards()
	anim.y.SetForwards()
}

func (anim *SpacingAnimation) IsBackwards() bool {
	return anim.x.IsBackwards()
}

// Pause the animation.
func (anim *SpacingAnimation) Pause() {
	anim.x.Pause()
	anim.y.Pause()
}

// Resume the animation.
func (anim *SpacingAnimation) Resume() {
	anim.x.Resume()
	anim.y.Resume()
}

func (anim *SpacingAnimation) IsPaused() bool {
	return anim.x.IsPaused()
}

func (anim *SpacingAnimation) Update(deltaTime float64) (spacing GuiSpacing, finished bool) {
	spacing.X, finished = anim.x.Update(deltaTime)
	spacing.Y, finished = anim.y.Update(deltaTime)

	return spacing, finished
}

func (anim *SpacingAnimation) CurrentValue() (spacing GuiSpacing) {
	spacing.X = anim.x.CurrentValue()
	spacing.Y = anim.y.CurrentValue()

	return spacing
}

func (anim *SpacingAnimation) IsFinished() bool {
	return anim.x.IsFinished()
}

func (anim *SpacingAnimation) SetFinished() {
	anim.x.SetFinished()
	anim.y.SetFinished()
}

func (anim *SpacingAnimation) StartValue() (spacing GuiSpacing) {
	spacing.X = anim.x.StartValue()
	spacing.Y = anim.y.StartValue()

	return spacing
}

func (anim *SpacingAnimation) EndValue() (spacing GuiSpacing) {
	spacing.X = anim.x.EndValue()
	spacing.Y = anim.y.EndValue()

	return spacing
}
