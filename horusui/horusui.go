package horusui

// import "core:mem"

// import sdl "vendor:sdl2"
import (
	"crypto/sha256"
	"fmt"
	"os"

	"github.com/iangudger/ilist"
	"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"tlog.app/go/loc"
)

// ----- Font Stuff -----

type Font struct {
	Font      *ttf.Font
	EmojiFont *ttf.Font
	Renderer  *sdl.Renderer

	// Custom_scale float32
}

// ----- GuiState data and functions -----

type MouseButton uint8

const (
	MouseButton_Left MouseButton = iota
	MouseButton_Right
	MouseButton_Middle
	MouseButton_Back
	MouseButton_Forward
	MouseButton_Max
)

/*
func ButtonsBitset(buttons ...MouseButton) (result *BitSet[MouseButton]) {
	for _, button := range buttons {
		result.Set(button)
	}
	return result
}
*/

type GuiState struct {
	// allocator mem.Allocator,
	Root *GuiElement
	//floatingElements    *GuiElement   // Linked List
	globalTextDirection TextDirection // Default TextDirection
	UserScale           float64       // NOTE: Not used automatically by horusui. Used when you call the Scale_* functions.

	windowTitle string
	WindowSize  Rect[int32]

	loopId           [4]int
	elements_backing []GuiElement
	elements         map[GuiElementId]*GuiElement
	//animations: map[AnimationId]^Animation, // TODO

	calculateEverything bool

	// Input Info
	Drag      [MouseButton_Max]bool
	Click     [MouseButton_Max]bool
	mx        int32
	my        int32
	DragOrigX int32 // TODO: dragOrigElementId
	DragOrigY int32
	WheelDir  float32 // Set to 0 when handled, and value moved to scroll field.

	// Internal Input Info
	hoverHandled bool
	focusHandled bool
	dragHandled  [MouseButton_Max]bool // TODO
	clickHandled [MouseButton_Max]bool // TODO: Turn into Bitset

	hoveredElement    GuiElementId
	hoverEnterElement GuiElementId
	hoverExitElement  GuiElementId

	focusedElement    GuiElementId
	focusEnterElement GuiElementId
	focusExitElement  GuiElementId

	clickedElement [MouseButton_Max]GuiElementId

	scrolledElement GuiElementId
	scroll          float32

	dragElement      [MouseButton_Max]GuiElementId
	dragStartElement [MouseButton_Max]GuiElementId
	dragEndElement   [MouseButton_Max]GuiElementId

	textCache map[GuiElementId]TextCache

	// TODO: When element is in the process of being clicked, but not fully clicked yet.
	//activeElement: [MouseButton]GuiElementId,
}

type TextCache struct {
	size      FSize
	color     Color
	bounds    Rect[float32]
	text_hash string
	texture   *sdl.Texture
	//id        GuiElementId
}

// Grows the element backing to make space for a large number of elements.
func (guiState *GuiState) GrowElementBacking(num int) {
	initialCap := cap(guiState.elements_backing)
	space := initialCap - len(guiState.elements_backing)
	if num < space {
		return
	}

	newSlice := make([]GuiElement, 0, (initialCap+num-space)*2)
	copy(newSlice, guiState.elements_backing)
	guiState.elements_backing = newSlice
}

func (guiState *GuiState) ClearElements() {
	len := len(guiState.elements)
	guiState.elements = nil
	guiState.elements_backing = nil
	guiState.elements = make(map[GuiElementId]*GuiElement, len)
	//guiState.elements_backing = make([]GuiElement, len)
	guiState.elements_backing = guiState.elements_backing[:0] // Retains the cap, but resets the length to 0
	//clear(guiState.elements)
	guiState.calculateEverything = false
}

// NOTE: Not used by HorusGUI. A utility field for use by the user.
func Scale_value[T ~float64 | ~float32 | ~int64 | ~int32 | ~int](guiState *GuiState, v T) T {
	return T(float64(v) * guiState.UserScale)
}
func (guiState *GuiState) Scale_padding(v GuiPadding) GuiPadding {
	return GuiPadding{Scale_value(guiState, v.Top), Scale_value(guiState, v.Bottom), Scale_value(guiState, v.Left), Scale_value(guiState, v.Right)}
}
func (guiState *GuiState) Scale_spacing(v GuiSpacing) GuiSpacing {
	return GuiSpacing{Scale_value(guiState, v.X), Scale_value(guiState, v.Y)}
}
func Scale_FPoint(guiState *GuiState, v FPoint) FPoint {
	return FPoint{Scale_value(guiState, v.X), Scale_value(guiState, v.Y)}
}
func Scale_array[E ~float64 | ~float32 | ~int64 | ~int32 | ~int](guiState *GuiState, v [2]E) [2]E {
	return [2]E{Scale_value(guiState, v[0]), Scale_value(guiState, v[1])}
}

// NOTE: Be careful with this
func (guiState *GuiState) scale_sizeType(v SizeType) SizeType {
	//v := v
	if v.width == SizeMode_Pixel {
		v.values.X = Scale_value(guiState, v.values.X)
	}
	if v.height == SizeMode_Pixel {
		v.values.Y = Scale_value(guiState, v.values.Y)
	}

	v.max.X = Scale_value(guiState, v.max.X)
	v.max.Y = Scale_value(guiState, v.max.Y)

	v.min.X = Scale_value(guiState, v.min.X)
	v.min.Y = Scale_value(guiState, v.min.Y)

	return v
}

// scale :: proc{scale_value, scale_padding, scale_spacing, scale_sizeType};

func MakeGuiState(background Color /*allocator: mem.Allocator,*/, windowTitle string, windowWidth int32, windowHeight int32) *GuiState {
	state := &GuiState{}
	//state.allocator = allocator;
	state.elements = make(map[GuiElementId]*GuiElement)
	state.elements_backing = make([]GuiElement, 0, 100)
	state.windowTitle = windowTitle
	state.WindowSize = createRect(0, 0, windowWidth, windowHeight)
	state.loopId = [4]int{-1, -1, -1, -1}
	state.globalTextDirection = TextDirection_LTR_TTB
	state.textCache = make(map[GuiElementId]TextCache, 10)

	return state
}
func (guiState *GuiState) SetGlobalTextDirection(direction TextDirection) {
	guiState.globalTextDirection = direction
}
func (guiState *GuiState) GetGlobalTextDirection() TextDirection {
	return guiState.globalTextDirection
}
func (guiState *GuiState) SetMouseInput(mx int32, my int32) {
	guiState.mx = mx
	guiState.my = my
}
func (guiState *GuiState) SetClickInput(click [MouseButton_Max]bool) {
	guiState.Click = click
}
func (guiState *GuiState) SetClick(button MouseButton, val bool) {
	guiState.Click[button] = val
}

// Call this before creating elements within loops so that
// they each get a unique id.
func (guiState *GuiState) SetLoopId(ids ...int) {
	for i := 0; i < min(len(ids), 4); i++ {
		guiState.loopId[i] = ids[i]
	}
}

// Call this at the end of a loop so that the loop id is no longer used.
// TODO: Pop from the stack!
func (guiState *GuiState) EndLoopId() {
	guiState.loopId = [4]int{-1, -1, -1, -1}
}

// ----- Element data and functions -----

// TODO
type GuiElementProperty uint8

const (
	Property_Visible GuiElementProperty = iota
	property_dorender

	// Event Properties
	Property_Clickable
	Property_Draggable
	Property_Hoverable
	Property_Focusable
	Property_Scrollable

	// Visual Properties
	Property_Background
	Property_Border
	Property_Padding
	Property_Spacing

	Property_Text
	Property_Flip
	//Property_CustomData
	Property_Floating
	// Property_Expandable // collapsible vs. expandable
	// Property_Max

	/*margin, size,
	text,
	bValue, iValue, fValue,
	input,
	animation,*/ // Set this when animating, unset when not animating
)

// TODO: Idea - FillStatic - Fills parent's bounds, but does not take into account percent of parent.
//
// Modes:
//   - Auto mode sizes based on the inner content size.
//   - AutoAdd mdoe sizes based on the inner content size, and then adds the pixel value from the values field.
//   - Fill
//   - Flex
//   - Match will match the width to the height or the height to the width.
//   - Rest will take the rest of the available space *without* splitting between the other flex elements. This
//     is useful for animating elements where an element takes all of the space *unless* it is animating, then
//     other elements (like a spacer, for example) can take the unused space. There cannot be multiple Rest
//     elements. // TODO: Implement This
//   - Pixel
//   - Percent
//   - MatchContent - matches the content size calculated from the sibling elements.
//
// TODO: Add MaxSibling mode to match the size to the maximum size after all other siblings have been computed, almost like Flex
type SizeMode uint8

const (
	SizeMode_Auto SizeMode = iota
	// SizeMode_AutoAdd
	SizeMode_Fill
	SizeMode_Flex
	SizeMode_Pixel
	SizeMode_Percent
	SizeMode_Match
	SizeMode_Rest
	SizeMode_MatchContent
)

type FPoint Point[float32]

type SizeTypeProperty uint8

const (
	SizeTypeProperty_HideOnMin SizeTypeProperty = iota // If size is smaller than minimum, then hide element
	SizeTypeProperty_AutoAsMin
	SizeTypeProperty_HideOnEmpty
)

//   - values is a percentage of the container when in Flex, Fill, or Percent modes. It is a pixel amount
//     in Pixel mode, and nothing in Auto mode (atm).
//   - Percent will always be the percentage of the calculated size after it's been calculated with the
//     mode and values. Note that it does not obey min and max sizes (because this is very useful for animations of hiding and showing elements).
type SizeType struct {
	properties    BitSet[uint8, SizeTypeProperty]
	width, height SizeMode
	values        FPoint
	percent       FPoint
	max           FPoint
	min           FPoint
}

func CreateSizeType(width, height SizeMode, values FPoint) SizeType {
	return SizeType{BitSet[uint8, SizeTypeProperty]{}, width, height, values, FPoint{1, 1}, FPoint{0, 0}, FPoint{0, 0}}
}
func SizeFill() SizeType {
	return SizeType{BitSet[uint8, SizeTypeProperty]{}, SizeMode_Fill, SizeMode_Fill, FPoint{1, 1}, FPoint{1, 1}, FPoint{0, 0}, FPoint{0, 0}}
}
func SizeRow(heightMode SizeMode, heightValue float32) SizeType {
	return SizeType{BitSet[uint8, SizeTypeProperty]{}, SizeMode_Fill, heightMode, FPoint{1, heightValue}, FPoint{1, 1}, FPoint{0, 0}, FPoint{0, 0}}
}
func SizeColumn(widthMode SizeMode, widthValue float32) SizeType {
	return SizeType{BitSet[uint8, SizeTypeProperty]{}, widthMode, SizeMode_Fill, FPoint{widthValue, 1}, FPoint{1, 1}, FPoint{0, 0}, FPoint{0, 0}}
}
func SizeAutoRow(heightMode SizeMode, heightValue float32) SizeType {
	return SizeType{BitSet[uint8, SizeTypeProperty]{}, SizeMode_Auto, heightMode, FPoint{1, heightValue}, FPoint{1, 1}, FPoint{0, 0}, FPoint{0, 0}}
}
func SizeAutoColumn(widthMode SizeMode, widthValue float32) SizeType {
	return SizeType{BitSet[uint8, SizeTypeProperty]{}, widthMode, SizeMode_Auto, FPoint{widthValue, 1}, FPoint{1, 1}, FPoint{0, 0}, FPoint{0, 0}}
}

func (sizeType SizeType) SetValues(values FPoint) SizeType {
	sizeType.values = values
	return sizeType
}
func (sizeType SizeType) SetPercent(percent FPoint) SizeType {
	sizeType.percent = percent
	return sizeType
}
func (sizeType SizeType) SetMin(min FPoint) SizeType {
	sizeType.min = min
	return sizeType
}
func (sizeType SizeType) SetMax(max FPoint) SizeType {
	sizeType.max = max
	return sizeType
}
func (sizeType SizeType) HideOnMin() SizeType {
	sizeType.properties.Set(SizeTypeProperty_HideOnMin)
	return sizeType
}
func (sizeType SizeType) AutoAsMin() SizeType {
	sizeType.properties.Set(SizeTypeProperty_AutoAsMin)
	return sizeType
}
func (sizeType SizeType) HiseOnEmpty() SizeType {
	sizeType.properties.Set(SizeTypeProperty_HideOnEmpty)
	return sizeType
}

// Default and Opposite are based on whether the text is LTR or RTL
type TextAlignment uint8

const (
	TextAlignment_Default TextAlignment = iota
	TextAlignment_Middle
	TextAlignment_Opposite
	TextAlignment_LeftOrTop
	TextAlignment_RightOrBottom
)

type TextDirection uint8

const (
	TextDirection_UseGlobal TextDirection = iota
	TextDirection_LTR_TTB
	TextDirection_LTR_BTT
	TextDirection_RTL_TTB
	TextDirection_RTL_BTT
	TextDirection_TTB_LTR
	TextDirection_BTT_LTR
	TextDirection_TTB_RTL
	TextDirection_BTT_RTL
)

type TextAttributeProperty uint8

const (
	TextAttributeProperty_Bold TextAttributeProperty = iota
	TextAttributeProperty_Italic
	TextAttributeProperty_Underline
	TextAttributeProperty_Strikethrough
	// TextAttributeProperty_Color
	TextAttributeProperty_Max
)

type TextAttribute struct {
	//properties: bit_set[TextAttributeProperty]
	color Color
	font  *Font // TODO: use rawptr
}

// NOTE: Hide will hide all of the text if it doesn't fit, HideElement will
// hide the text and the containing element, and Clip will just cutoff/clip
// the stuff that is outside of the bounds.
type GuiTextOverflow uint8

const (
	GuiTextOverflow_Show        GuiTextOverflow = iota // Shows the text unclipped
	GuiTextOverflow_Hide                               // Hides the text only
	GuiTextOverflow_HideElement                        // Hides the whole element (?) // TODO
	GuiTextOverflow_Wrap
	GuiTextOverflow_Clip        // Clips the text
	GuiTextOverflow_ClipElipses // Clips the text, adding an elipses // TODO
)

type GuiText struct {
	overflow    GuiTextOverflow
	alignmentX  TextAlignment
	alignmentY  TextAlignment
	direction   TextDirection
	_textSize   FSize
	cursorIndex int
	TextAttribute
	text      string
	shortText string

	cachedTexture *sdl.Texture
}

type GuiPadding struct {
	Top    float32
	Bottom float32
	Left   float32
	Right  float32
}
type GuiSpacing FPoint

type GuiPanelDirection uint8

const (
	GuiPanelDirection_Horizontal GuiPanelDirection = iota
	GuiPanelDirection_Vertical
	GuiPanelDirection_Linear
)

// The border/outline thickness will be automatically calculated as padding.
// You can enter 0 thickness and set a color to create a border that takes no space in the layout system,
// and it will render behind children elements.
type GuiBorder struct {
	Thickness uint16 // If 0, the border takes no space
	Color     Color  // Defaults to invisible black ({ 0, 0, 0, 0 })
}

type GuiStyles struct {
	background   Color // TODO: make sure border roundedness affects the background also
	Padding      GuiPadding
	borderLeft   GuiBorder
	borderRight  GuiBorder
	borderTop    GuiBorder
	borderBottom GuiBorder
	spacing      GuiSpacing // Spacing of children elements
}

// Overflow left or right (which side elements are clipped off of when they overflow)
// GuiElementId :: string;
type GuiElementIdParent struct {
	loop [4]int
	line int
	file string
	// loopString string
}
type GuiElementId struct {
	loop [4]int
	line int
	file string
	// loopString string // TODO: For loops where the indexes of elements change, so we need a different way to uniquely identify them.
	parent GuiElementIdParent
}
type GuiElement struct {
	direction    GuiPanelDirection
	rounded      int16 // Radius, for creating rounded borders and rounded boxes
	clickButtons BitSet[uint8, MouseButton]
	dragButtons  BitSet[uint8, MouseButton]
	properties   BitSet[uint16, GuiElementProperty] // TODO: Change to uint32 in the future when this gets expanded
	GuiStyles

	scrollPixelOffset float32 // Number of pixels to scroll the rendering of the children elements
	// ScrollElement_StartIndex *int          // Index of the child element rendered at the top of this element
	// ScrollElement_StopIndex  *int          // Not inclusive.
	_pos     Rect[float32]   // Internal Usage
	posCache *Point[float32] // Relative to the parent element. Takes into account the current scroll of the parent

	tooltip      *GuiElement // An element can only get one tooltip
	customRender RenderFunc
	ilist.Entry[GuiElement, *GuiElement]

	id       GuiElementId
	sizeType SizeType // Also has max and min sizes
	//scrollElement *float32 // The child index of the element on the top-left after applying the scroll offset.

	children         ilist.List[GuiElement, *GuiElement]
	floatingChildren ilist.List[GuiElement, *GuiElement] // TODO

	//textNode GuiTextNode // TODO: make into slice or map or something later on

	text GuiText
	//customData   rawptr
}

/*
type GuiTextNode struct {
	color Color
	style BitSet[uint8, TextAttributeProperty]
	index [2]int // Start and stop
	text  string
	font  *Font
	_pos  Rect[float32]

	cachedTexture *sdl.Texture
}
*/

type RenderFunc func(renderer *sdl.Renderer, element *GuiElement, bounds sdl.Rect, innerBounds Rect[float32])

// Checks if second argument (parentCheck) is the parent element to first argument (child)
func idIsParent(child GuiElementId, parentCheck GuiElementId) bool {
	return child.parent == (GuiElementIdParent{parentCheck.loop, parentCheck.line, parentCheck.file /*parentCheck.loopString*/})
}

// In repetedly called element functions, the parent is always
// the id passed in to that function to make sure all instances
// within the function remain unique.
func (guiState *GuiState) CreateId(parent GuiElementId) GuiElementId {
	pc := loc.Caller(1)
	_, file, line := pc.NameFileLine()
	//_, file, line, _ := runtime.Caller(1)
	return GuiElementId{
		guiState.loopId,
		line,
		file,
		//"",
		GuiElementIdParent{parent.loop, parent.line, parent.file /*parent.loopString*/},
	}
}
func (guiState *GuiState) CreateId_skip(parent GuiElementId, skip int) GuiElementId {
	pc := loc.Caller(1 + skip)
	_, file, line := pc.NameFileLine()
	//_, file, line, _ := runtime.Caller(1 + skip)
	return GuiElementId{
		guiState.loopId,
		line,
		file,
		//"",
		GuiElementIdParent{parent.loop, parent.line, parent.file /*parent.loopString*/},
	}
}

// Adds the element to the map.
func (guiState *GuiState) addElement(id GuiElementId, element *GuiElement) {
	// Check if id already in map, and print an error
	if _, ok := guiState.elements[id]; ok {
		fmt.Printf("%s:%d: Duplicate Element Id.\n", id.file, id.line)
		os.Exit(1)
	}
	guiState.elements[id] = element
}

// Gets element from map via GuiElementId
func (guiState *GuiState) GetElement(id GuiElementId) *GuiElement {
	return guiState.elements[id]
}

// ----- Element Creation Functions -----

// Adds new GuiElement to backing array
func (guiState *GuiState) newElementBacking() *GuiElement {
	guiState.elements_backing = append(guiState.elements_backing, GuiElement{})
	return &guiState.elements_backing[len(guiState.elements_backing)-1]
}
func (guiState *GuiState) Element(id GuiElementId, sizeType SizeType, direction GuiPanelDirection, parent GuiElementId) GuiElementId {
	element := guiState.newElementBacking()

	//context.temp_allocator = guiState.string_allocator;
	/*if guiState.loopId > -1 do element.id = fmt.tprintf("%s[%d]", id, guiState.loopId);
	  else do element.id = id;*/
	element.id = id

	element.direction = direction
	addProperty(element, Property_Visible)
	//element.visible = true
	element.text.cursorIndex = -1

	guiState.addElement(element.id, element)
	guiState.SetSizeType(element.id, sizeType)

	/*if padding.top != 0 || padding.bottom != 0 || padding.left != 0 || padding.right != 0 {
	    addPadding(element.id, padding);
	}*/

	/*if parent != (GuiElementId {}) do*/
	guiState.setParent_ids(element.id, parent)

	return element.id
}
func (guiState *GuiState) CreateTooltip(id GuiElementId, sizeType SizeType, direction GuiPanelDirection, parent GuiElementId) GuiElementId {
	element := guiState.newElementBacking()

	element.id = id

	element.direction = direction
	addProperty(element, Property_Visible)
	element.text.cursorIndex = -1

	if guiState.IsHovered(parent) {
		addProperty(element, Property_Visible)
	} else {
		removeProperty(element, Property_Visible)
	}

	/*
	   if isDeepHovered(parent) {
	       element.visible = true;
	   } else {
	       element.visible = false;
	   }
	*/

	guiState.addElement(element.id, element)
	guiState.SetSizeType(element.id, sizeType)
	guiState.SetTooltip(parent, element.id)

	return element.id
}
func (guiState *GuiState) SetTooltip(parent GuiElementId, tooltip GuiElementId) {
	parentElem := guiState.GetElement(parent)
	tooltipElem := guiState.GetElement(tooltip)

	parentElem.tooltip = tooltipElem
}
func (guiState *GuiState) RootElement(background Color) GuiElementId {
	guiState.Root = guiState.newElementBacking()
	guiState.Root.id = GuiElementId{}
	guiState.Root.sizeType = CreateSizeType(SizeMode_Fill, SizeMode_Fill, FPoint{1, 1})
	guiState.Root.direction = GuiPanelDirection_Horizontal
	addProperty(guiState.Root, Property_Visible)

	addProperty(guiState.Root, Property_Background)
	guiState.Root.background = background
	addProperty(guiState.Root, Property_Focusable)

	// TODO
	guiState.elements = make(map[GuiElementId]*GuiElement, 0)
	guiState.elements[guiState.Root.id] = guiState.Root

	return guiState.Root.id
}

func (guiState *GuiState) addProperty_id(element GuiElementId, property GuiElementProperty) {
	e := guiState.elements[element]
	//e.properties += {property}
	/*e.properties =*/
	e.properties.Set(property)
}
func addProperty(element *GuiElement, property GuiElementProperty) {
	//element.properties += {property}
	/*element.properties =*/
	element.properties.Set(property)
}

// addProperty :: proc{addProperty_id, addProperty_ptr};
func removeProperty(element *GuiElement, property GuiElementProperty) {
	//element.properties -= {property};
	/*element.properties = */
	element.properties.Clear(property)
}

// Position is relative to parent's position
func (guiState *GuiState) SetFloating(id GuiElementId, position FPoint, parent GuiElementId) {
	p := guiState.elements[parent]
	e := guiState.elements[id]

	// Move element to end of parent's children linked list
	p.children.Remove(e)
	p.floatingChildren.PushBack(e)

	e._pos.X = position.X
	e._pos.Y = position.Y

	guiState.addProperty_id(id, Property_Floating)
}

func (guiState *GuiState) SetDirection(id GuiElementId, direction GuiPanelDirection) {
	guiState.elements[id].direction = direction
}

func (guiState *GuiState) SetRounded(id GuiElementId, rounded int16) {
	e := guiState.elements[id]
	e.rounded = rounded
}
func (guiState *GuiState) SetUnrounded(id GuiElementId) {
	e := guiState.elements[id]
	e.rounded = 0
}
func (guiState *GuiState) SetPosCache(id GuiElementId, posCache *Point[float32]) {
	e := guiState.elements[id]
	e.posCache = posCache
}
func (guiState *GuiState) CalculateEverything() {
	guiState.calculateEverything = true
}

/*
func (guiState *GuiState) addCustomData_id(element GuiElementId, d rawptr) {
	e := guiState.elements[element]
	e.customData = d
}
func addCustomData_ptr(element *GuiElement, d rawptr) {
	element.customData = d
}*/

// addCustomData :: proc{addCustomData_id, addCustomData_ptr};
func (guiState *GuiState) SetCustomRender(element GuiElementId, f RenderFunc) {
	e := guiState.elements[element]
	e.customRender = f
}
func (guiState *GuiState) SetPadding(element GuiElementId, padding GuiPadding) {
	e := guiState.elements[element]
	addProperty(e, Property_Padding)
	e.Padding = padding
}

// var setPadding = addPadding

func (guiState *GuiState) SetSpacing(element GuiElementId, spacing GuiSpacing) {
	e := guiState.elements[element]
	addProperty(e, Property_Spacing)
	e.spacing = spacing
}

// var setSpacing = addSpacing

func (guiState *GuiState) HideTopBorder(element GuiElementId) {
	e := guiState.elements[element]
	e.borderTop.Thickness = 0
}
func (guiState *GuiState) HideBottomBorder(element GuiElementId) {
	e := guiState.elements[element]
	e.borderBottom.Thickness = 0
}
func (guiState *GuiState) HideLeftBorder(element GuiElementId) {
	e := guiState.elements[element]
	e.borderLeft.Thickness = 0
}
func (guiState *GuiState) HideRightBorder(element GuiElementId) {
	e := guiState.elements[element]
	e.borderRight.Thickness = 0
}
func (guiState *GuiState) HideBorder(element GuiElementId) {
	e := guiState.elements[element]
	removeProperty(e, Property_Border)
	e.borderTop.Thickness = 0
	e.borderBottom.Thickness = 0
	e.borderLeft.Thickness = 0
	e.borderRight.Thickness = 0
}
func (guiState *GuiState) SetBorder_sides(element GuiElementId, borderLeft GuiBorder, borderRight GuiBorder, borderTop GuiBorder, borderBottom GuiBorder) {
	e := guiState.elements[element]
	addProperty(e, Property_Border)
	e.borderLeft = borderLeft
	e.borderRight = borderRight
	e.borderTop = borderTop
	e.borderBottom = borderBottom
}
func (guiState *GuiState) SetBorder_all(element GuiElementId, border GuiBorder) {
	e := guiState.elements[element]
	addProperty(e, Property_Border)
	e.borderLeft = border
	e.borderRight = border
	e.borderTop = border
	e.borderBottom = border
}

// addBorder :: proc{addBorder_sides, addBorder_all};
// var setBorder_sides = addBorder_sides
// var setBorder_all = addBorder_all

func (guiState *GuiState) SetBackground(element GuiElementId, background Color) {
	e := guiState.elements[element]
	addProperty(e, Property_Background)
	e.background = background
}

// var setBackground = addBackground

/*
func (guiState *GuiState) AddTextNode(element GuiElementId, text string, color Color, font *Font, alignmentX, alignmentY TextAlignment, overflow GuiTextOverflow, direction TextDirection) {
	e := guiState.elements[element]

	if direction == TextDirection_UseGlobal {
		direction = guiState.globalTextDirection
	}

	addProperty(e, Property_Text)
	e.text.overflow = overflow
	e.text.alignmentX = alignmentX
	e.text.alignmentY = alignmentY
	e.text.direction = direction
	e.text._textSize = FSize{}
	e.text.cursorIndex = -1

	e.textNode.text = text
	e.textNode.color = color
	e.textNode.font = font
	e.textNode.index = [2]int{0, 0}
	e.textNode.style
	e.textNode.cachedTexture = nil
	e.text = GuiText{overflow, alignmentX, alignmentY, direction, FSize{}, -1, TextAttribute{ color, font}, text, nil}
}
*/

func (guiState *GuiState) SetText(element GuiElementId, text string, shortText string, color Color, font *Font, alignmentX TextAlignment, alignmentY TextAlignment, overflow GuiTextOverflow, direction TextDirection) {
	//direction := direction
	e := guiState.elements[element]

	if direction == TextDirection_UseGlobal {
		direction = guiState.globalTextDirection
	}

	addProperty(e, Property_Text)
	e.text = GuiText{overflow, alignmentX, alignmentY, direction, FSize{}, -1, TextAttribute{ /*{},*/ color, font}, text, shortText, nil}
}
func (guiState *GuiState) SetCursorIndex(element GuiElementId, index int) {
	e := guiState.elements[element]
	e.text.cursorIndex = index
}
func (guiState *GuiState) SetSizeType(element GuiElementId, sizeType SizeType) {
	pc := loc.Caller(1)
	_, file, line := pc.NameFileLine()
	//_, file, line, _ := runtime.Caller(1)
	e := guiState.elements[element]
	if e == nil {
		fmt.Printf("e (%v) is nil at %s:%d!\n", element, file, line)
	} else {
		e.sizeType = sizeType
	}
}
func (guiState *GuiState) SetSize(element GuiElementId, widthMode SizeMode, heightMode SizeMode, values FPoint) {
	e := guiState.elements[element]
	e.sizeType.width = widthMode
	e.sizeType.height = heightMode
	e.sizeType.values = values
}
func (guiState *GuiState) SetSizeWidth(element GuiElementId, widthMode SizeMode, value float32) {
	e := guiState.elements[element]
	e.sizeType.width = widthMode
	e.sizeType.values.X = value
}
func (guiState *GuiState) SetSizeHeight(element GuiElementId, heightMode SizeMode, value float32) {
	e := guiState.elements[element]
	e.sizeType.height = heightMode
	e.sizeType.values.Y = value
}
func (guiState *GuiState) SetPercentSize(element GuiElementId, percent FPoint) {
	e := guiState.elements[element]
	e.sizeType.percent = percent
}
func (guiState *GuiState) SetMaxSize(element GuiElementId, max FPoint) {
	e := guiState.elements[element]
	e.sizeType.max = max
}
func (guiState *GuiState) SetMinSize(element GuiElementId, min FPoint) {
	e := guiState.elements[element]
	e.sizeType.min = min
}
func (guiState *GuiState) AutoAsMin(id GuiElementId, autoAsMin bool) GuiElementId {
	e := guiState.elements[id]
	if autoAsMin {
		e.sizeType.properties.Set(SizeTypeProperty_AutoAsMin)
	} else {
		e.sizeType.properties.Clear(SizeTypeProperty_AutoAsMin)
	}
	return e.id
}
func (guiState *GuiState) HideOnMin(id GuiElementId, hideOnMin bool) GuiElementId {
	e := guiState.elements[id]
	if hideOnMin {
		e.sizeType.properties.Set(SizeTypeProperty_HideOnMin)
	} else {
		e.sizeType.properties.Clear(SizeTypeProperty_HideOnMin)
	}
	return e.id
}

// Calls hideOnMin and autoAsMin
func (guiState *GuiState) HideOnAutoMin(id GuiElementId, hideOnAutoMin bool) GuiElementId {
	guiState.HideOnMin(id, hideOnAutoMin)
	guiState.AutoAsMin(id, hideOnAutoMin)
	return id
}
func (guiState *GuiState) HideOnEmpty(id GuiElementId, hideOnEmpty bool) GuiElementId {
	e := guiState.elements[id]
	if hideOnEmpty {
		e.sizeType.properties.Set(SizeTypeProperty_HideOnEmpty)
	} else {
		e.sizeType.properties.Clear(SizeTypeProperty_HideOnEmpty)
	}
	return e.id
}
func (guiState *GuiState) Hide(element GuiElementId) {
	e := guiState.elements[element]
	removeProperty(e, Property_Visible)
}
func (guiState *GuiState) Show(element GuiElementId) {
	e := guiState.elements[element]
	addProperty(e, Property_Visible)
}
func (guiState *GuiState) ToggleVisibility(element GuiElementId) {
	e := guiState.elements[element]
	if e.properties.Test(Property_Visible) {
		removeProperty(e, Property_Visible)
	} else {
		addProperty(e, Property_Visible)
	}
}
func (guiState *GuiState) SetFlip(element GuiElementId, flip bool) {
	e := guiState.elements[element]
	if flip {
		addProperty(e, Property_Flip)
	} else {
		removeProperty(e, Property_Flip)
	}
}
func (guiState *GuiState) setFocusable(element GuiElementId, value bool) {
	e := guiState.elements[element]
	if value {
		addProperty(e, Property_Focusable)
	} else {
		removeProperty(e, Property_Focusable)
	}
}
func (guiState *GuiState) setHoverable(element GuiElementId, value bool) {
	e := guiState.elements[element]
	if value {
		addProperty(e, Property_Hoverable)
	} else {
		removeProperty(e, Property_Hoverable)
	}
}
func (guiState *GuiState) SetClickable(element GuiElementId /*buttons bit_set[MouseButton]*/, value bool, buttons ...MouseButton) {
	e := guiState.elements[element]
	if value {
		addProperty(e, Property_Clickable)
	} else {
		removeProperty(e, Property_Clickable)
	}
	//e.clickButtons = e.clickButtons + buttons;
	for _, b := range buttons {
		e.clickButtons.Set(b)
		//e.clickButtons[b] = true
	}
	guiState.setFocusable(element, value)
}
func (guiState *GuiState) SetDraggable(element GuiElementId, value bool, buttons ...MouseButton) {
	e := guiState.elements[element]
	if value {
		addProperty(e, Property_Draggable)
	} else {
		removeProperty(e, Property_Draggable)
	}
	for _, b := range buttons {
		e.dragButtons.Set(b)
	}
	guiState.setFocusable(element, value) // TODO
}
func (guiState *GuiState) setScrollable(element GuiElementId, value bool) {
	e := guiState.elements[element]
	if value {
		addProperty(e, Property_Scrollable)
	} else {
		removeProperty(e, Property_Scrollable)
	}
}
func (guiState *GuiState) SetScrollPixelOffset(element GuiElementId, value float32) {
	e := guiState.elements[element]
	e.scrollPixelOffset = value
}
func (guiState *GuiState) AddScrollPixelOffset(element GuiElementId, value float32) {
	e := guiState.elements[element]
	e.scrollPixelOffset += value
}
func (guiState *GuiState) SubtractScrollPixelOffset(element GuiElementId, value float32) {
	e := guiState.elements[element]
	e.scrollPixelOffset -= value
}

// ----- Event Functions -----
func (guiState *GuiState) ResetFocus() {
	guiState.SetFocus(GuiElementId{}, true)
}
func (guiState *GuiState) SetFocus(id GuiElementId, value bool) {
	//fmt.printf("Set focus to %q\n", id);

	if value {
		// The currently focused element becomes defocused, but only if it does not match the current element
		if guiState.focusedElement != id {
			guiState.focusExitElement = guiState.focusedElement
		}

		// If element is not already focused, set focusEnter
		if guiState.focusedElement != id {
			guiState.focusEnterElement = id
		}

		guiState.focusedElement = id
	} else if !value && guiState.focusedElement == id {
		// The currently focused element becomes defocused, but only if it does not match "root"
		if (guiState.focusedElement != GuiElementId{}) {
			guiState.focusExitElement = guiState.focusedElement
		}

		// If element is not already focused, set focusEnter
		if (guiState.focusedElement != GuiElementId{}) {
			guiState.focusEnterElement = GuiElementId{}
		}

		guiState.focusedElement = GuiElementId{}
	}
}
func (guiState *GuiState) IsFocused(id GuiElementId) bool {
	guiState.setFocusable(id, true)
	return id == guiState.focusedElement
}
func (guiState *GuiState) IsFocusEnter(id GuiElementId) bool {
	guiState.setFocusable(id, true)
	return id == guiState.focusEnterElement
}
func (guiState *GuiState) IsFocusExit(id GuiElementId) bool {
	guiState.setFocusable(id, true)
	return id == guiState.focusExitElement
}
func (guiState *GuiState) SetHover(id GuiElementId, value bool) {
	if value {
		// Set previous hover to hoverExit as long as not same as current element
		if guiState.hoveredElement != id {
			guiState.SetHoverExit(guiState.hoveredElement)
		}

		// If not already hovered, set hoverEnter
		if guiState.hoveredElement != id {
			guiState.hoverEnterElement = id
		}

		guiState.hoveredElement = id
	} else {
		// If not already hovered, set hoverEnter
		if (guiState.hoveredElement != GuiElementId{}) {
			guiState.hoverEnterElement = GuiElementId{}
		}

		guiState.hoveredElement = GuiElementId{}
	}
}
func (guiState *GuiState) SetHoverExit(id GuiElementId) {
	guiState.hoverExitElement = id
}
func (guiState *GuiState) IsHovered(id GuiElementId) bool {
	guiState.setHoverable(id, true)
	return id == guiState.hoveredElement
}
func (guiState *GuiState) IsDescendantHovered(parent GuiElementId) bool {
	return idIsParent(guiState.hoveredElement, parent)
}
func (guiState *GuiState) IsHoverEnter(id GuiElementId) bool {
	guiState.setHoverable(id, true)
	return id == guiState.hoverEnterElement
}
func (guiState *GuiState) IsHoverExit(id GuiElementId) bool {
	guiState.setHoverable(id, true)
	return id == guiState.hoverExitElement
}

// TODO: Add ability to set specific Mouse Buttons in a bit_set. Union the bit_set with the one passed in.
func (guiState *GuiState) setClicked(id GuiElementId, button MouseButton, value bool) {
	if value {
		guiState.clickedElement[button] = id
	} else {
		guiState.clickedElement[button] = GuiElementId{}
	}
}

// This sets clickHandled, which tells the internals of the library to clear the clicks just before handling the next events, at the end of the frame.
func (guiState *GuiState) clearClickedElement(button MouseButton) {
	guiState.clickHandled[button] = true
}

// TODO: Make sure that the descendant stuff works correctly with composed/compound elements
func (guiState *GuiState) isDescendantClicked(parent GuiElementId, buttons BitSet[uint8, MouseButton]) bool {
	if buttons.Test(MouseButton_Left) && idIsParent(guiState.clickedElement[MouseButton_Left], parent) {
		guiState.clearClickedElement(MouseButton_Left)
		return true
	}
	if buttons.Test(MouseButton_Right) && idIsParent(guiState.clickedElement[MouseButton_Right], parent) {
		guiState.clearClickedElement(MouseButton_Right)
		return true
	}
	if buttons.Test(MouseButton_Middle) && idIsParent(guiState.clickedElement[MouseButton_Middle], parent) {
		guiState.clearClickedElement(MouseButton_Middle)
		return true
	}
	if buttons.Test(MouseButton_Back) && idIsParent(guiState.clickedElement[MouseButton_Back], parent) {
		guiState.clearClickedElement(MouseButton_Back)
		return true
	}
	if buttons.Test(MouseButton_Forward) && idIsParent(guiState.clickedElement[MouseButton_Forward], parent) {
		guiState.clearClickedElement(MouseButton_Forward)
		return true
	}

	return false
}

// TODO: Handle distinguishing which mouse buttons looking for
func (guiState *GuiState) IsClicked(id GuiElementId, buttons ...MouseButton) bool {
	guiState.SetClickable(id, true, buttons...)
	for _, b := range buttons {
		if guiState.clickedElement[b] == id {
			guiState.clearClickedElement(b)
			return true
		}
	}

	return false
}

// This will check for a click that is not the given element's descendants. This does not return true if there was no click happening at all.
func (guiState *GuiState) IsDescendantNotClicked(parent GuiElementId, buttons BitSet[uint8, MouseButton]) bool {
	//setClickable(parent, buttons);
	if buttons.Test(MouseButton_Left) && !idIsParent(guiState.clickedElement[MouseButton_Left], parent) && (guiState.clickedElement[MouseButton_Left] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Right) && !idIsParent(guiState.clickedElement[MouseButton_Right], parent) && (guiState.clickedElement[MouseButton_Right] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Middle) && !idIsParent(guiState.clickedElement[MouseButton_Middle], parent) && (guiState.clickedElement[MouseButton_Middle] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Back) && !idIsParent(guiState.clickedElement[MouseButton_Back], parent) && (guiState.clickedElement[MouseButton_Back] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Forward) && !idIsParent(guiState.clickedElement[MouseButton_Forward], parent) && (guiState.clickedElement[MouseButton_Forward] != GuiElementId{}) {
		return true
	}

	return false
}

// This will check for a click that is not the given element. This does not return true if there was no click happening at all.
func (guiState *GuiState) IsNotClicked(id GuiElementId, buttons BitSet[uint8, MouseButton]) bool {
	if buttons.Test(MouseButton_Left) && guiState.clickedElement[MouseButton_Left] != id && (guiState.clickedElement[MouseButton_Left] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Right) && guiState.clickedElement[MouseButton_Right] != id && (guiState.clickedElement[MouseButton_Right] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Middle) && guiState.clickedElement[MouseButton_Middle] != id && (guiState.clickedElement[MouseButton_Middle] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Back) && guiState.clickedElement[MouseButton_Back] != id && (guiState.clickedElement[MouseButton_Back] != GuiElementId{}) {
		return true
	}
	if buttons.Test(MouseButton_Forward) && guiState.clickedElement[MouseButton_Forward] != id && (guiState.clickedElement[MouseButton_Forward] != GuiElementId{}) {
		return true
	}
	return false
}

func (guiState *GuiState) setDragged(id GuiElementId, button MouseButton, value bool) {
	if value {
		// If not already hovered, set hoverEnter
		if guiState.dragElement[button] != id {
			guiState.dragStartElement[button] = id
		}

		guiState.dragElement[button] = id
	} else {
		guiState.dragElement[button] = GuiElementId{}
	}
}
func (guiState *GuiState) SetDragEnd(id GuiElementId, button MouseButton) {
	guiState.dragEndElement[button] = id
}

// ClearDraggedElement should be called by the user.
func (guiState *GuiState) ClearDraggedElement(id GuiElementId) {
	for b := MouseButton(0); b < MouseButton_Max; b++ {
		if guiState.dragElement[b] == id {
			guiState.dragElement[b] = GuiElementId{}
			guiState.dragHandled[b] = true
		}
	}
}

// TODO: Handle distinguishing which mouse buttons looking for
func (guiState *GuiState) IsDragging(id GuiElementId, buttons ...MouseButton) bool {
	guiState.SetDraggable(id, true, buttons...)
	for _, b := range buttons {
		if guiState.dragElement[b] == id {
			//guiState.clearClickedElement(b)
			return true
		}
	}

	return false
}

func (guiState *GuiState) IsDragStart(id GuiElementId, buttons ...MouseButton) bool {
	guiState.SetDraggable(id, true, buttons...)
	for _, b := range buttons {
		if guiState.dragStartElement[b] == id {
			//guiState.clearClickedElement(b)
			return true
		}
	}

	return false
}

func (guiState *GuiState) IsDragEnd(id GuiElementId, buttons ...MouseButton) bool {
	guiState.SetDraggable(id, true, buttons...)
	for _, b := range buttons {
		if guiState.dragEndElement[b] == id {
			return true
		}
	}

	return false
}

func (guiState *GuiState) setScrolled(id GuiElementId, value bool) {
	if value {
		guiState.scrolledElement = id
	} else {
		guiState.scrolledElement = GuiElementId{}
	}
}
func (guiState *GuiState) IsScrolled(id GuiElementId) bool {
	guiState.setScrollable(id, true)
	if guiState.scrolledElement == id {
		guiState.scrolledElement = GuiElementId{} // Scrolled Element has been handled.
		return true
	}
	return false
}
func (guiState *GuiState) GetScroll() float32 {
	return guiState.scroll
}

// ----- Linked List Functions -----

/*
type GuiChildrenIterator struct {
	index   int
	parent  *GuiElement
	current *GuiElement
}*/

/*
makeGuiFloatingChildrenIterator :: proc(parent *GuiElement) -> GuiChildrenIterator {
    return GuiChildrenIterator { 0, parent, parent.floatingChildren };
}
makeGuiChildrenIterator :: proc(parent *GuiElement) -> GuiChildrenIterator {
    return GuiChildrenIterator { 0, parent, parent.children };
}
resetGuiChildrenIterator :: proc(it *GuiChildrenIterator) {
    it.index = 0;
    it.current = it.parent.children;
}
resetGuiFloatingChildrenIterator :: proc(it *GuiChildrenIterator) {
    it.index = 0;
    it.current = it.parent.floatingChildren;
}
iterateChildren :: proc(it *GuiChildrenIterator) -> (val *GuiElement, idx: int, cond: bool) {
    if cond = (it.current != nil); cond {
        val = it.current;
        idx = it.index;
        it.current = it.current.next;
        it.index += 1;
    }
    return;
}
*/

func (guiState *GuiState) setParent_ids(element GuiElementId, parent GuiElementId) {
	e := guiState.elements[element]
	p := guiState.elements[parent]
	//return linkedList_addChild(p, e);
	p.children.PushBack(e)
}
func setParent_noids(element *GuiElement, parent *GuiElement) {
	//return linkedList_addChild(parent, element);
	parent.children.PushBack(element)
}

// setParent :: proc{setParent_ids, setParent_noids};
func GetChild(parent *GuiElement, index int) *GuiElement {
	//if index >= parent.childrenCount do return nil;

	i := 0
	for child := parent.children.Front(); child != nil; child = child.Next() {
		if i == index {
			return child
		}
		i += 1
	}

	fmt.Printf("Couldn't find child\n")
	return nil
}
func (guiState *GuiState) CreateElement(sizeType SizeType, direction GuiPanelDirection, parent GuiElementId) GuiElementId {
	element := guiState.newElementBacking()

	//context.temp_allocator = guiState.string_allocator;
	/*if guiState.loopId > -1 do element.id = fmt.tprintf("%v[%d]", loc, guiState.loopId);
	  else do element.id = fmt.tprintf("%v", loc);*/
	element.id = guiState.CreateId_skip(parent, 1)

	element.direction = direction
	addProperty(element, Property_Visible)
	element.text.cursorIndex = -1

	guiState.addElement(element.id, element)
	guiState.SetSizeType(element.id, sizeType)

	/*if parent != (GuiElementId {}) do*/
	guiState.setParent_ids(element.id, parent)

	return element.id
}
func (guiState *GuiState) CreatePane(id GuiElementId, sizeType SizeType, direction GuiPanelDirection, padding GuiPadding, parent GuiElementId) GuiElementId {
	element := guiState.newElementBacking()

	//context.temp_allocator = guiState.string_allocator;
	/*if guiState.loopId > -1 do element.id = fmt.tprintf("%s[%d]", id, guiState.loopId);
	  else do element.id = id;*/
	element.id = id

	element.direction = direction
	addProperty(element, Property_Visible)
	element.text.cursorIndex = -1

	guiState.addElement(element.id, element)
	guiState.SetSizeType(element.id, sizeType)

	if padding.Top != 0 || padding.Bottom != 0 || padding.Left != 0 || padding.Right != 0 {
		guiState.SetPadding(element.id, padding)
	}

	/*if parent != (GuiElementId {}) do*/
	guiState.setParent_ids(element.id, parent)

	return element.id
}

// ----- Action Functions -----

func UpdateDimensions(state *GuiState, windowWidth float32, windowHeight float32) {
	currentBounds := Rect[float32]{Point[float32]{0, 0}, windowWidth, windowHeight}

	state.Root._pos = currentBounds
	addProperty(state.Root, property_dorender)
	state.updateChildrenDimensions3(state.Root)
	updateChildrenPositions(state, state.Root)

	// Clear from textCache all of the elements that were not used this frame
	for key, val := range state.textCache {
		if _, ok := state.elements[key]; !ok {
			if val.texture == nil {
				continue
			}

			err := val.texture.Destroy()
			if err != nil {
				panic(err)
			}

			delete(state.textCache, key)
			//fmt.Printf("Deleting\n")
		}
	}

	/*
		elementSize := int(unsafe.Sizeof(*state.Root))
		elementPtrSize := int(unsafe.Sizeof(state.Root))
		totalSize := (elementSize * len(state.elements)) + (elementPtrSize * len(state.elements))
		fmt.Printf("Size of GuiElement: %v\nSize of All Elements: %v\nText Cache Length: %v\n\n", elementSize, totalSize, len(state.textCache))
	*/
}

func getBounds(element *GuiElement) Rect[float32] {
	var bounds Rect[float32] = element._pos

	// Take into account padding
	if element.properties.Test(Property_Padding) {
		bounds.X += element.Padding.Left
		bounds.Y += element.Padding.Top
		bounds.W -= (element.Padding.Left + element.Padding.Right)
		bounds.H -= (element.Padding.Top + element.Padding.Bottom)
	}

	// Take into account border
	if element.properties.Test(Property_Border) {
		bounds.X += float32(element.borderLeft.Thickness)
		bounds.Y += float32(element.borderTop.Thickness)
		bounds.W = bounds.W - float32(element.borderLeft.Thickness) - float32(element.borderRight.Thickness)
		bounds.H = bounds.H - float32(element.borderTop.Thickness) - float32(element.borderBottom.Thickness)
	}

	return bounds
}

// When wrapping is turned on, the height takes into account newlines.
// bounds are given to know the maximum size to wrap to, if wrapping is turned on and needed
func (guiState *GuiState) getTextSize(element *GuiElement, bounds Rect[float32], requireSurface bool) FSize {
	var textSize FSize
	h := sha256.New()
	h.Write([]byte(element.text.text))
	hash := fmt.Sprintf("%x", h.Sum(nil))

	cache, cached := guiState.textCache[element.id]
	if cached && (!requireSurface || cache.texture != nil) && cache.text_hash == hash && cache.bounds == bounds && (cache.color == element.text.color && !requireSurface) {
		element.text.cachedTexture = cache.texture
		return cache.size
	} else if cached && cache.texture != nil && (cache.text_hash != hash || cache.bounds != bounds || cache.color != element.text.color) {
		//fmt.Printf("Cached changed: From {%v, %v, %v} to {%v, %v, %v}\n", cache.text_hash, cache.bounds, cache.color, hash, bounds, element.text.color)
		//fmt.Printf("Cache changed: %v\n", element.id)
		err := cache.texture.Destroy()
		if err != nil {
			panic(err)
		}
	}

	cache = TextCache{}
	if element.properties.Test(Property_Text) {
		if element.text.font.Renderer == nil {
			// fmt.Printf("Test")
			return FSize{0, 0}
		}

		var textWidth, textHeight int
		if element.text.overflow == GuiTextOverflow_Wrap {
			//textWidth, textHeight = measureTextWithWrap(element.text.font, element.text.text, bounds);

			surface, err := element.text.font.Font.RenderUTF8BlendedWrapped(element.text.text, colorToSdlColor(element.text.color), int(bounds.W))
			defer surface.Free()
			element.text.cachedTexture, _ = element.text.font.Renderer.CreateTextureFromSurface(surface)
			if err != nil {
				/*fmt.Printf("Wrap Length: %d", int(bounds.W))
				panic(err)*/
				textWidth = 0
				textHeight = element.text.font.Font.LineSkip()
			} else {
				textWidth = surface.Bounds().Dx()
				textHeight = surface.Bounds().Dy()
			}

			//textWidth, textHeight, _ = element.text.font.Font.SizeUTF8(element.text.text)
			if textWidth > int(bounds.W) {
				textWidth = int(bounds.W)
			}
			//textWidth, textHeight = measureText(element.text.font, element.text.text)
		} else {
			/*if !requireSurface {
				textWidth, textHeight, _ = element.text.font.Font.SizeUTF8(element.text.text)
			} else {*/
			surface, err := element.text.font.Font.RenderUTF8Blended(element.text.text, colorToSdlColor(element.text.color))
			defer surface.Free()
			element.text.cachedTexture, _ = element.text.font.Renderer.CreateTextureFromSurface(surface)
			if err != nil {
				textWidth = 0
				textHeight = element.text.font.Font.LineSkip()
			} else {
				textWidth = surface.Bounds().Dx()
				textHeight = surface.Bounds().Dy()
			}
			//}
			//textWidth, textHeight, _ = element.text.font.Font.SizeUTF8(element.text.text)
			//textWidth, textHeight = measureText(element.text.font, element.text.text)
		}
		//textHeight, descent := getFontHeight(element.text.font, false, false);

		textSize = FSize{float32(textWidth), float32(textHeight) /*- descent*/}
	}

	cache.size = textSize
	cache.bounds = bounds
	//cache.id = element.id
	cache.texture = element.text.cachedTexture
	cache.text_hash = hash
	cache.color = element.text.color
	guiState.textCache[element.id] = cache
	return textSize
}

// Frees all of the surfaces within the text cache and clears all keys
func (guiState *GuiState) FreeTextCache() {
	for _, val := range guiState.textCache {
		if val.texture != nil {
			err := val.texture.Destroy()
			if err != nil {
				panic(err)
			}
		}
	}
	clear(guiState.textCache)
}

func (guiState *GuiState) updateChildrenDimensions3(element *GuiElement) {
	// Takes bounds set by parent and subtracts off border and padding
	bounds := getBounds(element)

	// Spacing for cildren elements
	var spacing = FPoint{0, 0}
	if element.properties.Test(Property_Spacing) {
		if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
			spacing.X = element.spacing.X
		} else if element.direction == GuiPanelDirection_Vertical || element.direction == GuiPanelDirection_Linear {
			spacing.Y = element.spacing.Y
		}
	}

	var contentSize FSize
	// Get text size, set as content Size as baseline
	bounds_scaled := bounds
	element.text._textSize = guiState.getTextSize(element, bounds_scaled, false)
	contentSize = FSize{element.text._textSize.W + 2, element.text._textSize.H}

	var split FPoint
	hiddenCount := 0
	var hasFlexOrFill = Point[bool]{false, false}

	// Used for Linear layouts. Otherwise, currentRowContextSize should be the exact same as contentSize.
	//currentRow := 0
	var currentRowContentSize float32

	// TODO: bounds act as a max size clamp on child size

	// Iterate through all children
	for child := element.children.Front(); child != nil; child = child.Next() {
		isFloating := child.properties.Test(Property_Floating) // TODO: Completely broken now

		// Hide child if percent (or value) is 0 for x or y
		/*if child.sizeType.percent.X == 0 || child.sizeType.percent.Y == 0 || child.sizeType.values.X == 0 || child.sizeType.values.Y == 0 {
			hiddenCount += 1
			child.visible = false
			continue
		}*/

		if element.direction == GuiPanelDirection_Horizontal {
			if bounds.W-(contentSize.W-element.scrollPixelOffset-Scale_value(guiState, float32(20))) <= 0 {
				hiddenCount += 1
				removeProperty(child, Property_Visible)
				// Remove elements from here on from the tree
				if !guiState.calculateEverything {
					child.SetNext(nil)
					element.children.Remove(child)
					delete(guiState.elements, child.id)
					break
				}
			}
		}
		if element.direction == GuiPanelDirection_Vertical {
			if bounds.H-(contentSize.H-element.scrollPixelOffset-Scale_value(guiState, float32(20))) <= 0 {
				hiddenCount += 1
				removeProperty(child, Property_Visible)
				// Remove elements from here on from the tree
				if !guiState.calculateEverything {
					child.SetNext(nil)
					element.children.Remove(child)
					delete(guiState.elements, child.id)
					break
				}
			}
		}

		hideX := false
		hideY := false
		var splitValue FPoint = FPoint{}
		hideX, hasFlexOrFill.X, splitValue.X = setChildWidth(child, bounds, isFloating, element)
		hideY, hasFlexOrFill.Y, splitValue.Y = setChildHeight(child, bounds, isFloating, element)
		split.X += splitValue.X
		split.Y += splitValue.Y
		if hideX || hideY {
			hiddenCount += 1
			removeProperty(child, Property_Visible)
			continue
		}

		// Update children and content sizes
		// What happens if width is auto and height is flex???
		// TODO: Update dimensions of child if not .Flex
		if child.sizeType.width == SizeMode_Auto || child.sizeType.height == SizeMode_Auto {
			guiState.updateChildrenDimensions3(child)
		}

		// Save these for use later
		autoWidth := child._pos.W
		autoHeight := child._pos.H

		// Handle .Match
		// TODO: Why is this here in this location?
		if child.sizeType.width == SizeMode_Match {
			child._pos.W = child._pos.H

			if child.sizeType.max.X != 0 && child._pos.W >= child.sizeType.max.X {
				child._pos.W = child.sizeType.max.X
			}
			if child.sizeType.min.X != 0 && child._pos.W <= child.sizeType.min.X {
				child._pos.W = child.sizeType.min.X
			}
		}
		if child.sizeType.height == SizeMode_Match {
			child._pos.H = child._pos.W

			if child.sizeType.max.Y != 0 && child._pos.H >= child.sizeType.max.Y {
				child._pos.H = child.sizeType.max.Y
			}
			if child.sizeType.min.Y != 0 && child._pos.H <= child.sizeType.min.Y {
				child._pos.H = child.sizeType.min.Y
			}
		}

		// Percent of child size
		if child.sizeType.width != SizeMode_Flex {
			child._pos.W *= child.sizeType.percent.X
		}
		if child.sizeType.height != SizeMode_Flex {
			child._pos.H *= child.sizeType.percent.Y
		}

		// Clamp size to parent bounds
		// TODO: Doesn't seem to work with pixel sizes
		if child.sizeType.width != SizeMode_Flex && child.sizeType.width != SizeMode_Fill && child.sizeType.width != SizeMode_Percent /*&& child.sizeType.width != .Auto*/ {
			newContentWidth := currentRowContentSize + child._pos.W + spacing.X
			if element.direction != GuiPanelDirection_Horizontal && element.direction != GuiPanelDirection_Linear {
				newContentWidth = max(currentRowContentSize, child._pos.W)
			}

			if newContentWidth > bounds.W {
				if element.direction == GuiPanelDirection_Linear {
					// Go to new row, but first set contentSize.x to this row's currentRowContentSize if it's greater
					//if currentRowContentSize > contentSize.x do contentSize.x = currentRowContentSize;
					currentRowContentSize = 0
				} else {
					child._pos.W = bounds.W - currentRowContentSize - spacing.X
					if element.direction != GuiPanelDirection_Horizontal {
						child._pos.W = bounds.W - currentRowContentSize // TODO: Does this make sense?
					}

					if /*child._pos.W < 0 ||*/ child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child._pos.W < child.sizeType.min.X {
						child._pos.W = 0
						hiddenCount += 1
						removeProperty(child, Property_Visible)
						continue
					}
				}
			}
		}
		if child.sizeType.height != SizeMode_Flex && child.sizeType.height != SizeMode_Fill && child.sizeType.height != SizeMode_Percent /*&& child.sizeType.height != .Auto*/ {
			newContentHeight := contentSize.H + child._pos.H + spacing.Y
			if element.direction != GuiPanelDirection_Vertical {
				newContentHeight = max(contentSize.H, child._pos.H)
			}

			if newContentHeight > bounds.H {
				child._pos.H = bounds.H - contentSize.H - spacing.Y
				if element.direction != GuiPanelDirection_Vertical {
					child._pos.H = bounds.H - contentSize.H // TODO: Does this make sense?
				}

				if /*child._pos.H < 0 ||*/ child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child._pos.H < child.sizeType.min.Y {
					child._pos.H = 0
					hiddenCount += 1
					removeProperty(child, Property_Visible)
					continue
				}
			}
		}

		// Handle Auto Minimums
		// TODO: Put before calculating in percents??
		if child.sizeType.width == SizeMode_Auto && child.sizeType.properties.Test(SizeTypeProperty_AutoAsMin) {
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child._pos.W < autoWidth {
				child._pos.W = autoWidth
			}
			if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child._pos.W < autoWidth {
				hiddenCount += 1
				removeProperty(child, Property_Visible)
				continue
			}
		}
		if child.sizeType.height == SizeMode_Auto && child.sizeType.properties.Test(SizeTypeProperty_AutoAsMin) {
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child._pos.H < autoHeight {
				child._pos.H = autoHeight
			}
			if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child._pos.H < autoHeight {
				hiddenCount += 1
				removeProperty(child, Property_Visible)
				continue
			}
		}

		addProperty(child, property_dorender)

		// Add to contentsize
		if !isFloating {
			if element.direction == GuiPanelDirection_Horizontal {
				contentSize.W += child._pos.W + spacing.X
				currentRowContentSize += child._pos.W
				currentRowContentSize += spacing.X
			} else if element.direction == GuiPanelDirection_Linear {
				currentRowContentSize += child._pos.W
				currentRowContentSize += spacing.X
			} else if child._pos.W > contentSize.W {
				contentSize.W = child._pos.W
				contentSize.W += spacing.X
			}

			if element.direction == GuiPanelDirection_Vertical {
				contentSize.H += child._pos.H
				contentSize.H += spacing.Y
			} else if child._pos.H > contentSize.H {
				contentSize.H = child._pos.H
				contentSize.H += spacing.Y
			}
		}
		// TODO: If there's flex or fills, then contentsize can just be set to parent bounds. This is already done basically for fills, so just need to do this for flex
	}

	if element.direction == GuiPanelDirection_Linear && currentRowContentSize > contentSize.W {
		contentSize.W = currentRowContentSize
	}

	// Make sure last element's added spacing is subtracted since it doesn't get spacing
	// TODO: When clamping pixel elements to the parent bounds, it assumes the last element gets spacing.
	contentSize.W -= spacing.X
	contentSize.H -= spacing.Y

	rest := FPoint{bounds.W - contentSize.W, bounds.H - contentSize.H} // TODO: This probably should be an FSize

	// Handle flex children
	for child := element.children.Front(); child != nil; child = child.Next() {
		if !child.properties.Test(Property_Visible) {
			continue
		}

		/*#partial */
		switch child.sizeType.width {
		case SizeMode_Flex:
			{
				if child.sizeType.values.X == 0 {
					continue
				}

				child._pos.W += rest.X / split.X
				child._pos.W *= child.sizeType.values.X
				child._pos.W *= child.sizeType.percent.X

				if element.direction == GuiPanelDirection_Horizontal {
					contentSize.W += child._pos.W
				}
				//else if child._pos.w > contentSize.x do contentSize.x = child._pos.w;
			}
		case SizeMode_MatchContent:
			{
				if element.direction == GuiPanelDirection_Vertical {
					child._pos.W = contentSize.W

					// Percent of current content size
					child._pos.W *= child.sizeType.values.X

					if child.sizeType.max.X != 0 && child._pos.W >= child.sizeType.max.X {
						child._pos.W = child.sizeType.max.X
					}
					if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.X != 0 && child._pos.W <= child.sizeType.min.X {
						child._pos.W = child.sizeType.min.X
					}
					if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.X != 0 && child._pos.W < child.sizeType.min.X {
						hiddenCount += 1
						removeProperty(child, Property_Visible)
						continue
					}
				}
			}
		}
		/*#partial */ switch child.sizeType.height {
		case SizeMode_Flex:
			{
				if child.sizeType.values.Y == 0 {
					continue
				}

				child._pos.H += rest.Y / split.Y
				child._pos.H *= child.sizeType.values.Y
				child._pos.H *= child.sizeType.percent.Y

				if element.direction == GuiPanelDirection_Vertical {
					contentSize.H += child._pos.H
				}
				//else if child._pos.h > contentSize.y do contentSize.y = child._pos.h;
			}
		case SizeMode_MatchContent:
			{
				if element.direction == GuiPanelDirection_Horizontal {
					child._pos.H = contentSize.H

					// Percent of current content size
					child._pos.H *= child.sizeType.values.Y

					if child.sizeType.max.Y != 0 && child._pos.H >= child.sizeType.max.Y {
						child._pos.H = child.sizeType.max.Y
					}
					if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.Y != 0 && child._pos.H <= child.sizeType.min.Y {
						child._pos.H = child.sizeType.min.Y
					}
					if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.Y != 0 && child._pos.H < child.sizeType.min.Y {
						hiddenCount += 1
						removeProperty(child, Property_Visible)
						continue
					}
				}
			}
		}

		addProperty(child, property_dorender)
		if child.sizeType.width != SizeMode_Auto && child.sizeType.height != SizeMode_Auto {
			guiState.updateChildrenDimensions3(child)
		}
	}

	// Set element's width or height if Auto
	// Note that the (parent) element's bounds *does* include padding and border
	if (element.sizeType.width == SizeMode_Auto && element.direction != GuiPanelDirection_Horizontal) || (element.sizeType.width == SizeMode_Auto && element.direction == GuiPanelDirection_Horizontal && !hasFlexOrFill.X) {
		element._pos.W = contentSize.W

		element._pos.W += element.Padding.Left + element.Padding.Right + (float32(element.borderLeft.Thickness) + float32(element.borderRight.Thickness))
	}
	if (element.sizeType.height == SizeMode_Auto && element.direction != GuiPanelDirection_Vertical) || (element.sizeType.height == SizeMode_Auto && element.direction == GuiPanelDirection_Vertical && !hasFlexOrFill.Y) {
		element._pos.H = contentSize.H

		element._pos.H += element.Padding.Top + element.Padding.Bottom + (float32(element.borderTop.Thickness) + float32(element.borderBottom.Thickness))
	}
}

func setChildWidth(child *GuiElement, bounds Rect[float32], isFloating bool, parent *GuiElement) (bool, bool, float32) {
	var hide bool
	var hasFlexOrFill bool
	var splitValue float32
	hide = false
	hasFlexOrFill = false
	splitValue = 0

	/*#partial */
	switch child.sizeType.width {
	case SizeMode_Pixel:
		{
			child._pos.W = child.sizeType.values.X

			if child.sizeType.max.X != 0 && child._pos.W >= child.sizeType.max.X {
				child._pos.W = child.sizeType.max.X
			}
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.X != 0 && child._pos.W <= child.sizeType.min.X {
				child._pos.W = child.sizeType.min.X
			}
			if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.X != 0 && child._pos.W < child.sizeType.min.X {
				return true, false, 0
			}
		}
	case SizeMode_Fill, SizeMode_Percent:
		{
			// Set width to parent bounds
			child._pos.W = bounds.W

			// Percent of container size
			child._pos.W *= child.sizeType.values.X

			if child.sizeType.max.X != 0 && child._pos.W >= child.sizeType.max.X {
				child._pos.W = child.sizeType.max.X
			}
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.X != 0 && child._pos.W <= child.sizeType.min.X {
				child._pos.W = child.sizeType.min.X
			}
			if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.X != 0 && child._pos.W < child.sizeType.min.X {
				return true, false, 0
			}

			if parent.direction == GuiPanelDirection_Horizontal || parent.direction == GuiPanelDirection_Linear {
				hasFlexOrFill = true
			}
		}
	case SizeMode_MatchContent:
		{
			if parent.direction == GuiPanelDirection_Horizontal {
				// TODO: If horizontal direction, set to rest of size needed to fill minimum or bounds.
			}
			// Vertical direction is handled in the second for loop.
		}
	case SizeMode_Flex:
		{ // TODO: handle maximum size for flex
			// Handle minimum size and add to contentSize
			child._pos.W = 0
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.X != 0 && child._pos.W <= child.sizeType.min.X {
				child._pos.W = child.sizeType.min.X
			}

			// Handle Split info
			if !isFloating && parent.direction == GuiPanelDirection_Horizontal /*|| element.direction == .Linear*/ {
				hasFlexOrFill = true
				splitValue = 1 * child.sizeType.values.X * child.sizeType.percent.X
			}
		}
	case SizeMode_Auto:
		{
			// Set to full bounds
			child._pos.W = bounds.W

			//if element.direction == .Horizontal do child._pos.w -= contentSize.x;
			if child.sizeType.max.X != 0 && child._pos.W >= child.sizeType.max.X {
				child._pos.W = child.sizeType.max.X
			}
			if !child.sizeType.properties.Test(SizeTypeProperty_AutoAsMin) && child.sizeType.min.X != 0 && child._pos.W <= child.sizeType.min.X {
				child._pos.W = child.sizeType.min.X // TODO: Not sure this is needed, since parent bounds is already minimum
			}
		}
	}

	return hide, hasFlexOrFill, splitValue
}

func setChildHeight(child *GuiElement, bounds Rect[float32], isFloating bool, parent *GuiElement) (bool, bool, float32) {
	var hide bool
	var hasFlexOrFill bool
	var splitValue float32

	hide = false
	hasFlexOrFill = false
	splitValue = 0

	/*#partial*/
	switch child.sizeType.height {
	case SizeMode_Pixel:
		{
			child._pos.H = child.sizeType.values.Y

			if child.sizeType.max.Y != 0 && child._pos.H >= child.sizeType.max.Y {
				child._pos.H = child.sizeType.max.Y
			}
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.Y != 0 && child._pos.H <= child.sizeType.min.Y {
				child._pos.H = child.sizeType.min.Y
			}
			if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.Y != 0 && child._pos.H < child.sizeType.min.Y {
				return true, false, 0
			}
		}
	case SizeMode_Fill, SizeMode_Percent:
		{
			child._pos.H = bounds.H

			// Percent of container size
			child._pos.H *= child.sizeType.values.Y

			if child.sizeType.max.Y != 0 && child._pos.H >= child.sizeType.max.Y {
				child._pos.H = child.sizeType.max.Y
			}
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.Y != 0 && child._pos.H <= child.sizeType.min.Y {
				child._pos.H = child.sizeType.min.Y
			}
			if child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.Y != 0 && child._pos.H < child.sizeType.min.Y {
				return true, false, 0
			}

			if parent.direction == GuiPanelDirection_Vertical {
				hasFlexOrFill = true
			}
		}
	case SizeMode_MatchContent:
		{
			if parent.direction == GuiPanelDirection_Vertical {
				// TODO: If vertical direction, set to rest of size needed to fill minimum or bounds.
			}
			// Horizontal direction is handled in the second for loop.
		}
	case SizeMode_Flex:
		{ // TODO: handle maximum size for flex
			// Handle minimum size and add to contentSize
			child._pos.H = 0
			if !child.sizeType.properties.Test(SizeTypeProperty_HideOnMin) && child.sizeType.min.Y != 0 && child._pos.H <= child.sizeType.min.Y {
				child._pos.H = child.sizeType.min.Y
			}

			// Handle Split info
			if !isFloating && parent.direction == GuiPanelDirection_Vertical {
				hasFlexOrFill = true
				splitValue = 1 * child.sizeType.values.Y * child.sizeType.percent.Y
			}
		}
	case SizeMode_Auto:
		{
			child._pos.H = bounds.H
			//if element.direction == .Vertical do child._pos.h -= contentSize.y; // TODO: Subtracts contentSize from parent bounds, What is this for?
			if child.sizeType.max.Y != 0 && child._pos.H >= child.sizeType.max.Y {
				child._pos.H = child.sizeType.max.Y
			}
			if !child.sizeType.properties.Test(SizeTypeProperty_AutoAsMin) && child.sizeType.min.Y != 0 && child._pos.H <= child.sizeType.min.Y {
				child._pos.H = child.sizeType.min.Y
			}
		}
	}

	return hide, hasFlexOrFill, splitValue
}

func updateChildrenPositions(guiState *GuiState, element *GuiElement) {
	bounds := getBounds(element)

	var scrollOffsetX float32 = 0
	var scrollOffsetY float32 = 0
	if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
		scrollOffsetX = element.scrollPixelOffset
	} else if element.direction == GuiPanelDirection_Vertical {
		scrollOffsetY = element.scrollPixelOffset
	}

	var spacing FPoint
	if element.properties.Test(Property_Spacing) {
		if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
			spacing.X = element.spacing.X
		} else if element.direction == GuiPanelDirection_Vertical {
			spacing.Y = element.spacing.Y
		}
	}

	offset := FPoint{bounds.X - scrollOffsetX, bounds.Y - scrollOffsetY}
	if element.properties.Test(Property_Flip) {
		x2 := bounds.X + bounds.W - 1
		y2 := bounds.Y + bounds.H - 1
		offset = FPoint{x2, y2}
	}
	/*if !(GuiElementProperty.flip in element.properties) {
	      if element.direction == .Horizontal || element.direction == .Linear do offset = bounds.x;
	      else if element.direction == .Vertical do offset = bounds.y;
	  } else {
	      // ofset becomes *right* or *bottom* of elements
	      if element.direction == .Horizontal || element.direction == .Linear do offset = bounds.x + bounds.w;
	      else if element.direction == .Vertical do offset = bounds.y + bounds.h;
	  }*/

	var rowMaxY float32 = 0

	childIndex := 0
	for child := element.children.Front(); child != nil; child = child.Next() {
		isFloating := child.properties.Test(Property_Floating) // TODO: Completely broken now
		if /*!child.properties.Test(Property_Visible) ||*/ !child.properties.Test(property_dorender) {
			/*if element.ScrollElement_StopIndex != nil {
				*element.ScrollElement_StopIndex = childIndex
			}*/
			// Remove elements from here on from the tree
			if !guiState.calculateEverything {
				child.SetNext(nil)
				element.children.Remove(child)
				delete(guiState.elements, child.id)
				break
			} else {
			}
		} else if !child.properties.Test(Property_Visible) && !guiState.calculateEverything {
			continue
		}

		// Used for custom positionings of floating elements
		customOffset := FPoint{child._pos.X, child._pos.Y}
		if !isFloating {
			customOffset = FPoint{0, 0}
		}

		if !element.properties.Test(Property_Flip) {
			// Starting Position as Fallback
			child._pos.X = bounds.X
			child._pos.Y = bounds.Y

			child._pos.X += customOffset.X
			child._pos.Y += customOffset.Y

			if !isFloating {
				// Set element pos to Current Offset
				if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
					child._pos.X = offset.X
				} else if element.direction == GuiPanelDirection_Vertical {
					child._pos.Y = offset.Y
				}

				// Go to next row
				if element.direction == GuiPanelDirection_Linear && child._pos.X+child._pos.W >= bounds.X+bounds.W {
					child._pos.X = bounds.X
					offset.X = bounds.X // TODO
					child._pos.Y = rowMaxY
					offset.Y = rowMaxY
					if child._pos.Y+child._pos.H+spacing.Y > rowMaxY {
						rowMaxY = child._pos.Y + child._pos.H - 1 + spacing.Y
					}
				} else if element.direction == GuiPanelDirection_Linear {
					child._pos.Y = offset.Y
					if child._pos.Y+child._pos.H+spacing.Y > rowMaxY {
						rowMaxY = child._pos.Y + child._pos.H - 1 + spacing.Y
					}
				}

				// Set Current Offset for Next Element
				// NOTE: The - 1 + 1 cancel each other out, since + 1 is needed to move element over 1 pixel from the end of previous element.
				if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
					offset.X = (child._pos.X + child._pos.W + spacing.X)
				} else if element.direction == GuiPanelDirection_Vertical {
					offset.Y = (child._pos.Y + child._pos.H + spacing.Y)
				}
			}
		} else {
			// Starting Position as Fallback
			// bounds.x + bounds.w - 1 - child._pos.w + 1 = bounds.x + bounds.w - child._pos.w
			child._pos.X = bounds.X + bounds.W - child._pos.W
			child._pos.Y = bounds.Y + bounds.H - child._pos.H

			child._pos.X += customOffset.X
			child._pos.Y += customOffset.Y

			if !isFloating {
				// Set element pos to Current Offset
				if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
					child._pos.X = offset.X - child._pos.W
				} else if element.direction == GuiPanelDirection_Vertical {
					child._pos.Y = offset.Y - child._pos.H
				}

				// Set Current Offset for Next element
				if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
					offset.X = (child._pos.X - spacing.X)
				} else if element.direction == GuiPanelDirection_Vertical {
					offset.Y = (child._pos.Y - spacing.Y)
				}
			}
		}

		/*if element.ScrollElement_StartIndex != nil {
			if element.direction == GuiPanelDirection_Horizontal && child._pos.X < element._pos.X {
				*element.ScrollElement_StartIndex = childIndex
			} else if element.direction == GuiPanelDirection_Vertical && child._pos.Y < element._pos.Y {
				*element.ScrollElement_StartIndex = childIndex
			}
		}*/

		/*
			if !isFloating && element.direction == GuiPanelDirection_Horizontal && child._pos.X >= bounds.X + bounds.W {
				break
			}
			if !isFloating && element.direction == GuiPanelDirection_Vertical && child._pos.Y >= bounds.Y + bounds.H {
				break
			}
		*/

		// Set pos cache for user's use during next frame.
		if child.posCache != nil {
			child.posCache.X = child._pos.X - bounds.X
			if element.direction == GuiPanelDirection_Horizontal {
				child.posCache.X = child._pos.X - bounds.X + element.scrollPixelOffset
			}
			child.posCache.Y = child._pos.Y - bounds.Y
			if element.direction == GuiPanelDirection_Vertical {
				child.posCache.Y = child._pos.Y - bounds.Y + element.scrollPixelOffset
			}
		}

		updateChildrenPositions(guiState, child)
		childIndex++
	}

	// Handle tooltip
	if element.tooltip != nil {
		child := element.tooltip
		if child.properties.Test(Property_Visible) {
			// Starting Position as Fallback
			child._pos.X = bounds.X
			child._pos.Y = bounds.Y

			updateChildrenPositions(guiState, child)
		}
	}
}

// ----- Render -----

// Render handles the rendering *and* the events. Returns whether element was hovered
func (guiState *GuiState) Render(renderer *sdl.Renderer, element *GuiElement, clipRect_inner sdl.Rect) bool {
	elementX, elementY, elementW, elementH := element._pos.X, element._pos.Y, element._pos.W, element._pos.H

	bounds := rectToSdlRect(&element._pos)
	clipRect_outer, intersect := bounds.Intersect(&clipRect_inner) // NOTE: Used for the events system

	//bounds := rectToSdlFRect(&element._pos); // Includes padding and border
	innerBounds := getBounds(element) // Bounds of inner content. Does not include padding and border

	innerBounds.H += 1
	sdlInnerBounds := rectToSdlRect(&innerBounds)

	if int(bounds.W) < 0 || int(bounds.H) < 0 {
		return false
	}

	/*
		renderer.SetDrawColor(255, 255, 255, 255)
		renderer.DrawRect(&clipRect)
	*/

	renderer.SetDrawBlendMode(sdl.BLENDMODE_BLEND)

	// Background
	if element.properties.Test(Property_Background) && element.background.A > 0 {
		background := &element.background
		if element.rounded > 0 {
			gfx.RoundedBoxRGBA(renderer, bounds.X, bounds.Y, bounds.X+bounds.W-1, bounds.Y+bounds.H-1, int32(element.rounded), background.R, background.G, background.B, background.A)
		} else {
			renderer.SetDrawColor(background.R, background.G, background.B, background.A)
			renderer.FillRect(&bounds)
		}
	}

	// Border
	// TODO: Implement the thickness functionality
	if element.properties.Test(Property_Border) {
		x2 := bounds.X + bounds.W - 1
		y2 := bounds.Y + bounds.H - 1

		if element.rounded > 0 && element.borderLeft.Color == element.borderRight.Color && element.borderLeft.Color == element.borderTop.Color && element.borderLeft.Color == element.borderBottom.Color && element.borderLeft.Thickness > 0 && element.borderLeft.Thickness == element.borderRight.Thickness && element.borderLeft.Thickness == element.borderTop.Thickness && element.borderLeft.Thickness == element.borderBottom.Thickness {
			borderColor := &element.borderLeft.Color
			gfx.RoundedRectangleRGBA(renderer, bounds.X, bounds.Y, bounds.X+bounds.W-1, bounds.Y+bounds.H-1, int32(element.rounded), borderColor.R, borderColor.G, borderColor.B, borderColor.A)
		} else {
			if element.borderLeft.Color.A > 0 && element.borderLeft.Thickness > 0 {
				borderColor := &element.borderLeft.Color
				renderer.SetDrawColor(borderColor.R, borderColor.G, borderColor.B, borderColor.A)
				renderer.DrawLine(bounds.X, bounds.Y, bounds.X, y2)
			}
			if element.borderRight.Color.A > 0 && element.borderRight.Thickness > 0 {
				borderColor := &element.borderRight.Color
				renderer.SetDrawColor(borderColor.R, borderColor.G, borderColor.B, borderColor.A)
				renderer.DrawLine(x2, bounds.Y, x2, y2)
			}
			if element.borderTop.Color.A > 0 && element.borderTop.Thickness > 0 {
				borderColor := &element.borderTop.Color
				renderer.SetDrawColor(borderColor.R, borderColor.G, borderColor.B, borderColor.A)
				renderer.DrawLine(bounds.X, bounds.Y, x2, bounds.Y)
			}
			if element.borderBottom.Color.A > 0 && element.borderBottom.Thickness > 0 {
				borderColor := &element.borderBottom.Color
				renderer.SetDrawColor(borderColor.R, borderColor.G, borderColor.B, borderColor.A)
				renderer.DrawLine(bounds.X, y2, x2, y2)
			}
		}

		// sdl.SetRenderDrawColor(renderer, 0, 255, 0, 255);

		/*borderColor := &element.borderBottom.color;
		  sdl.SetRenderDrawColor(renderer, borderColor.r, borderColor.g, borderColor.b, borderColor.a);
		  sdl.RenderDrawRect(renderer, &bounds);*/
	}

	// Text
	if element.properties.Test(Property_Text) {
		clipRect_inner, intersect := clipRect_inner.Intersect(&sdlInnerBounds)
		if intersect {
			prevClip := renderer.GetClipRect()

			renderer.SetClipRect(&clipRect_inner)
			guiState.renderTextOfElement(renderer, element)

			renderer.SetClipRect(&prevClip)
		}
	}

	if element.customRender != nil {
		bounds := bounds
		innerBounds := innerBounds
		if element.direction == GuiPanelDirection_Horizontal || element.direction == GuiPanelDirection_Linear {
			bounds.X -= int32(element.scrollPixelOffset)
			innerBounds.X -= element.scrollPixelOffset
		} else if element.direction == GuiPanelDirection_Vertical {
			bounds.Y -= int32(element.scrollPixelOffset)
			innerBounds.Y -= element.scrollPixelOffset
		}
		clipRect, intersect := clipRect_inner.Intersect(&sdlInnerBounds)
		if intersect {
			renderer.SetClipRect(&clipRect)
			element.customRender(renderer, element, bounds, innerBounds)
		}
	}

	// Render Children
	childHovered := false
	clipRect_inner, intersect_inner := clipRect_inner.Intersect(&sdlInnerBounds)
	for child := element.children.Front(); child != nil; child = child.Next() {
		if !child.properties.Test(property_dorender) {
			break
		}

		if intersect_inner {
			renderer.SetClipRect(&clipRect_inner)

			childX, childY, childW, childH := child._pos.X, child._pos.Y, child._pos.W, child._pos.H
			if childX <= elementX+elementW && childY <= elementY+elementH && childX+childW >= elementX && childY+childH >= elementY {
				if guiState.Render(renderer, child, clipRect_inner) {
					if !child.properties.Test(Property_Floating) {
						childHovered = true
					}
				}
			} else if cache, ok := guiState.textCache[child.id]; ok && cache.texture != nil && child.properties.Test(Property_Text) {
				err := child.text.cachedTexture.Destroy()
				if err != nil {
					panic(err)
				}
				child.text.cachedTexture = nil
				cache.texture = nil
				guiState.textCache[child.id] = cache
			}
		} else if cache, ok := guiState.textCache[child.id]; ok && cache.texture != nil && child.properties.Test(Property_Text) {
			err := child.text.cachedTexture.Destroy()
			if err != nil {
				panic(err)
			}
			child.text.cachedTexture = nil
			cache.texture = nil
			guiState.textCache[child.id] = cache
		}
	}

	// Render Floating Elements on top of other elements
	/*
	   it = makeGuiFloatingChildrenIterator(element);
	   for child, i in iterateChildren(&it) {
	       if !child.visible do continue;

	       render(renderer, child);
	   }*/

	renderer.SetClipRect(nil)

	// Render Tooltip
	if element.tooltip != nil && element.tooltip.properties.Test(Property_Visible) {
		guiState.Render(renderer, element.tooltip, rectToSdlRect(&guiState.WindowSize))
	}

	// Handle Events
	hovered := guiState.Element_HandleEvent(element, sdlRectToRect[float32](&clipRect_outer), intersect, childHovered)

	return hovered
}

// TODO: Implement ClipElipses
func (guiState *GuiState) renderTextOfElement(renderer *sdl.Renderer, element *GuiElement) {
	bounds := getBounds(element) // Bounds of inner content. Inner content size does not include padding and border

	// Scaled bounds used *only* for wrapping
	bounds_scaled := bounds

	canUseShortText := element.text.shortText != ""

	// Re-render and cache the surface again when it comes back into view
	if element.text.cachedTexture == nil {
		textSize := guiState.getTextSize(element, bounds_scaled, true)
		element.text._textSize.W = textSize.W
		element.text._textSize.H = textSize.H
	}

	width := element.text._textSize.W
	height := element.text._textSize.H
	if canUseShortText && (height > bounds_scaled.H || width > bounds_scaled.W) {
		//text = element.text.shortText
		element.text.text = element.text.shortText

		//height, descent = getFontHeight(element.text.font, false, false); // TODO: false, true?
		size := guiState.getTextSize(element, bounds_scaled, true)
		width, height = size.W, size.H
		//int_width, int_height, _ := element.text.font.Font.SizeUTF8(element.text.text)
		//int_width, int_height := measureText(element.text.font, text)
		//width = float32(int_width)
		//height = float32(int_height)
	}

	textBounds := Rect[float32]{Point[float32]{bounds_scaled.X, bounds_scaled.Y}, width, height}
	if element.text.direction == TextDirection_LTR_TTB {
		switch element.text.alignmentX {
		case TextAlignment_Default, TextAlignment_LeftOrTop:
			{
				textBounds.X = bounds_scaled.X + 1 // TODO
			}
		case TextAlignment_Middle:
			{
				textBounds.X = bounds_scaled.X + (bounds_scaled.W / 2) - (width / 2)
			}
		case TextAlignment_Opposite, TextAlignment_RightOrBottom:
			{
				textBounds.X = bounds_scaled.X + bounds_scaled.W - width
			}
		}
		switch element.text.alignmentY {
		case TextAlignment_Default, TextAlignment_LeftOrTop:
			{
				textBounds.Y = bounds_scaled.Y
			}
		case TextAlignment_Middle:
			{
				textBounds.Y = bounds_scaled.Y + (bounds_scaled.H / 2) - (height / 2)
			}
		case TextAlignment_Opposite, TextAlignment_RightOrBottom:
			{
				textBounds.Y = bounds_scaled.Y + bounds_scaled.H - height
			}
		}

		// Make sure the text origin doesn't go outside of the bounding box
		if textBounds.X < bounds_scaled.X {
			textBounds.X = bounds_scaled.X
		}
		if textBounds.Y < bounds_scaled.Y {
			textBounds.Y = bounds_scaled.Y
		}
	} else if element.text.direction == TextDirection_RTL_TTB {
		switch element.text.alignmentX {
		case TextAlignment_Default, TextAlignment_RightOrBottom:
			{
				textBounds.X = bounds_scaled.X + bounds_scaled.W - 1
			}
		case TextAlignment_Middle:
			{
				textBounds.X = bounds_scaled.X + (bounds_scaled.W / 2) + (width / 2)
			}
		case TextAlignment_Opposite, TextAlignment_LeftOrTop:
			{
				textBounds.X = bounds_scaled.X + width
			}
		}
		switch element.text.alignmentY {
		case TextAlignment_Default, TextAlignment_LeftOrTop:
			{
				textBounds.Y = bounds_scaled.Y
			}
		case TextAlignment_Middle:
			{
				textBounds.Y = bounds_scaled.Y + (bounds_scaled.H / 2) - (height / 2)
			}
		case TextAlignment_Opposite, TextAlignment_RightOrBottom:
			{
				textBounds.Y = bounds_scaled.Y + bounds_scaled.H - height
			}
		}

		// Fix x position to take into account bounds width
		if textBounds.W > bounds_scaled.W {
			textBounds.X -= (textBounds.W - bounds_scaled.W)
		}

		// Make sure the text origin doesn't go outside of the bounding box
		if textBounds.X > bounds_scaled.X+bounds_scaled.W-1 {
			textBounds.X = bounds_scaled.X + bounds_scaled.W - 1
		}
		if textBounds.Y < bounds_scaled.Y {
			textBounds.Y = bounds_scaled.Y
		}
	}

	renderCursor := element.text.cursorIndex != -1
	debug := false // Used to render rectangle around text bounds

	// Get cursor position from within text // TODO: Currently assumes one line of non-wrapped text
	cursorPos := textBounds.X
	if renderCursor {
		if element.text.cursorIndex > len(element.text.text) {
			element.text.cursorIndex = len(element.text.text)
		}
		cursorPosX, _, _ := element.text.font.Font.SizeUTF8(element.text.text[:element.text.cursorIndex])
		cursorPos += float32(cursorPosX)
	}

	// TODO: Implement .HideElement
	if element.text.overflow == GuiTextOverflow_Show {
		renderer.SetDrawColor(element.text.color.R, element.text.color.G, element.text.color.B, element.text.color.A)

		//surface, _ := element.text.font.Font.RenderUTF8Blended(text, colorToSdlColor(element.text.color))
		texture := element.text.cachedTexture
		//defer texture.Destroy()
		dstRect := rectToSdlRect[float32](&textBounds)
		renderer.Copy(texture, nil, &dstRect)
		//_, _, _, cursorPos := renderText(renderer, element.text.font, textBounds.X, textBounds.Y, text, false, element.text.cursorIndex, 0, element.text.direction == TextDirection_RTL_TTB || element.text.direction == TextDirection_RTL_BTT)

		if renderCursor {
			//renderer.SetDrawColor(250, 250, 250, 250)
			renderer.DrawLineF(cursorPos, textBounds.Y, cursorPos, textBounds.Y+textBounds.H-1)
		}
	} else if element.text.overflow == GuiTextOverflow_Hide && height <= bounds.H && width <= bounds.W {
		renderer.SetDrawColor(element.text.color.R, element.text.color.G, element.text.color.B, element.text.color.A)
		//_, _, _, cursorPos := renderText(renderer, element.text.font, textBounds.X, textBounds.Y, text, false, element.text.cursorIndex, 0, element.text.direction == TextDirection_RTL_TTB || element.text.direction == TextDirection_RTL_BTT)

		//surface, _ := element.text.font.Font.RenderUTF8Blended(text, colorToSdlColor(element.text.color))
		texture := element.text.cachedTexture
		//defer texture.Destroy()
		dstRect := rectToSdlRect[float32](&textBounds)
		renderer.Copy(texture, nil, &dstRect)

		if renderCursor {
			//renderer.SetDrawColor(250, 250, 250, 250)
			renderer.DrawLineF(cursorPos, textBounds.Y, cursorPos, textBounds.Y+textBounds.H-1)
		}
	} else if /*(height <= bounds.H) &&*/ element.text.overflow == GuiTextOverflow_Clip || element.text.overflow == GuiTextOverflow_ClipElipses {
		// boundsWidth := bounds.W

		renderer.SetDrawColor(element.text.color.R, element.text.color.G, element.text.color.B, element.text.color.A)

		//surface, _ := element.text.font.Font.RenderUTF8Blended(text, colorToSdlColor(element.text.color))
		texture := element.text.cachedTexture
		//defer texture.Destroy()
		dstRect := rectToSdlRect[float32](&textBounds)
		renderer.Copy(texture, nil, &dstRect)
		//_, _, _, cursorPos := renderText(renderer, element.text.font, textBounds.X, textBounds.Y, text, false, element.text.cursorIndex, boundsWidth, element.text.direction == TextDirection_RTL_TTB || element.text.direction == TextDirection_RTL_BTT)

		if renderCursor {
			//renderer.SetDrawColor(250, 250, 250, 250)
			renderer.DrawLineF(cursorPos, textBounds.Y, cursorPos, textBounds.Y+textBounds.H-1)
		}
	} else if /*(height <= bounds.H) &&*/ element.text.overflow == GuiTextOverflow_Wrap {
		renderer.SetDrawColor(element.text.color.R, element.text.color.G, element.text.color.B, element.text.color.A)

		//surface, _ := element.text.font.Font.RenderUTF8BlendedWrapped(text, colorToSdlColor(element.text.color), int(bounds.W))
		texture := element.text.cachedTexture
		//defer texture.Destroy()
		dstRect := rectToSdlRect[float32](&textBounds)
		renderer.Copy(texture, nil, &dstRect)
		//_, _, _, cursorPos := renderText(renderer, element.text.font, textBounds.X, textBounds.Y, text, false, element.text.cursorIndex, boundsWidth, element.text.direction == TextDirection_RTL_TTB || element.text.direction == TextDirection_RTL_BTT)

		if renderCursor {
			//renderer.SetDrawColor(250, 250, 250, 250)
			renderer.DrawLineF(cursorPos, textBounds.Y, cursorPos, textBounds.Y+textBounds.H-1)
		}
	}

	if debug {
		textBoundsSdl := rectToSdlFRect(&textBounds)
		renderer.DrawRectF(&textBoundsSdl)
	}
}
