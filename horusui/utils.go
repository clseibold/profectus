package horusui

import (
	"github.com/veandco/go-sdl2/sdl"
)

type Number interface {
	~int | ~int32 | ~int64 | ~float32 | ~float64 | ~uint8 | ~int8 | ~uint16 | ~int16 | ~uint | ~uint32 | ~uint64
}

type Point[T Number | ~bool] struct {
	X T
	Y T
}

type FSize Size[float32]
type Size[T Number] struct {
	W T
	H T
}

type Rect[T Number] struct {
	Point[T]
	W T
	H T
}

func (rect *Rect[T]) Intersect(s Rect[T]) (Rect[T], bool) {
	result := *rect
	if rect.X < s.X {
		result.X = s.X
	}
	if rect.Y < s.Y {
		result.Y = s.Y
	}
	if rect.X+rect.W > s.X+s.W {
		//r.Max.X = s.Max.X
		result.W = (s.X + s.W) - result.X
	} else {
		result.W = rect.X + rect.W - result.X
	}

	if rect.Y+rect.H > s.Y+s.H {
		//r.Max.Y = s.Max.Y
		result.H = (s.Y + s.H) - result.Y
	} else {
		result.H = rect.Y + rect.H - result.Y
	}

	/*if result.W <= 0 || result.H <= 0 {
	return Rect[T]{}, false
	}*/

	// Letting r0 and s0 be the values of r and s at the time that the method
	// is called, this next line is equivalent to:
	//
	// if max(r0.Min.X, s0.Min.X) >= min(r0.Max.X, s0.Max.X) || likewiseForY { etc }
	if result.Empty() {
		return Rect[T]{}, false
	}
	return result, true

}

func (rect *Rect[T]) Empty() bool {
	return rect.X >= rect.X+rect.W || rect.Y >= rect.Y+rect.H
}

func createRect[T Number](x, y, w, h T) Rect[T] {
	return Rect[T]{Point[T]{x, y}, w, h}
}
func sdlRectToRect[T Number](r *sdl.Rect) Rect[T] {
	return Rect[T]{Point[T]{X: T(r.X), Y: T(r.Y)}, T(r.W), T(r.H)}
}
func rectToSdlRect[T Number](fr *Rect[T]) sdl.Rect {
	return sdl.Rect{X: int32(fr.X), Y: int32(fr.Y), W: int32(fr.W), H: int32(fr.H)}
}
func rectToSdlFRect[T Number](fr *Rect[T]) sdl.FRect {
	return sdl.FRect{X: float32(fr.X), Y: float32(fr.Y), W: float32(fr.W), H: float32(fr.H)}
}
func pointInRect[T Number](r *Rect[T], x T, y T) bool {
	return x >= r.X && x <= r.X+r.W-1 && y >= r.Y && y <= r.Y+r.H-1
}
func pointInSdlRect(r *sdl.Rect, x int32, y int32) bool {
	return x >= r.X && x <= r.X+r.W-1 && y >= r.Y && y <= r.Y+r.H-1
}

type Color struct {
	R uint8
	G uint8
	B uint8
	A uint8
}

func colorToSdlColor(c Color) sdl.Color {
	return sdl.Color{R: c.R, G: c.G, B: c.B, A: c.A}
}
func sdlColorToColor(c sdl.Color) Color {
	return Color{c.R, c.G, c.B, c.A}
}
