package horusui

import (
	"fmt"
	"image/color"
	"strings"

	"github.com/veandco/go-sdl2/sdl"
)

func SizeUTF8_Wrapped(font *Font, text string, wrapLength int) (maxWidth, height int, err error) {
	surface, _ := font.Font.RenderUTF8BlendedWrapped(text, sdl.Color(color.NRGBA{R: 0, G: 0, B: 0, A: 0}), wrapLength)
	texture, _ := font.Renderer.CreateTextureFromSurface(surface)
	font.Renderer.Copy(texture, &sdl.Rect{0, 0, surface.W, surface.H}, &sdl.Rect{0, 0, surface.W, surface.H})
	fmt.Printf("Size Using Render Function: %v, %v\n", surface.W, surface.H)

	lineSpacing := font.Font.LineSkip() - font.Font.Height()
	fmt.Printf("Line Skip: %v\nLine Height: %v\nLine Spacing: %v\n", font.Font.LineSkip(), font.Font.Height(), lineSpacing)

	wordStartIndex := 0
	currentWidth := 0

	i := 0
	for wordStartIndex < len(text) && wordStartIndex != -1 {
		// Skip beginning spaces
		wordStart_withoutSpaces := strings.IndexFunc(text[wordStartIndex:], func(r rune) bool {
			return r != ' '
		})
		if wordStartIndex == -1 {
			wordStart_withoutSpaces = wordStartIndex
		}
		wordStart_withoutSpaces += wordStartIndex

		wordEndIndex := strings.Index(text[wordStart_withoutSpaces:], " ")
		if wordEndIndex == -1 {
			wordEndIndex = len(text) - wordStart_withoutSpaces
		}

		word := text[wordStartIndex : wordEndIndex+wordStart_withoutSpaces]
		wordStartIndex = wordEndIndex + wordStart_withoutSpaces

		fmt.Printf("Word: '%v'\n", word)
		word_width, word_height, err := font.Font.SizeUTF8(word)
		if err != nil {
			return maxWidth, height, err
		}
		if i == 0 {
			height += word_height
		}

		if currentWidth+word_width > wrapLength {
			// New Line
			height += word_height + lineSpacing
			if currentWidth > maxWidth {
				maxWidth = currentWidth
			}
			currentWidth = 0
		} else {
			currentWidth += word_width
		}
		if i == 8 {
			break
		}
		i++
	}

	return maxWidth, height, err
}
