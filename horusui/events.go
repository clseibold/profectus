package horusui

/*
type EventProperty uint8

const (
	EventProperty_Clickable EventProperty = iota
	EventProperty_Hoverable
	EventProperty_Focusable
	EventProperty_Scrollable
)

type EventNode struct {
	element GuiElementId
	ilist.Entry[EventNode, *EventNode]
	children ilist.List[EventNode, *EventNode]
}
*/

func (guiState *GuiState) HandleEvents_Start() {
	// Use clickHandled here to reset clickedElement based on calls to isClicked() in usercode that have happened.
	// The isClicked() function uses clearClick(), which sets clickHandled, so that the clickedElement can be cleared at the end of frame.
	// We want clickElement to be cleared at the end of frame so that multiple isClicked() functions can be used for one element.
	if guiState.clickHandled[MouseButton_Left] {
		guiState.clickedElement[MouseButton_Left] = GuiElementId{}
	}
	if guiState.clickHandled[MouseButton_Right] {
		guiState.clickedElement[MouseButton_Right] = GuiElementId{}
	}
	if guiState.clickHandled[MouseButton_Middle] {
		guiState.clickedElement[MouseButton_Middle] = GuiElementId{}
	}
	if guiState.clickHandled[MouseButton_Back] {
		guiState.clickedElement[MouseButton_Back] = GuiElementId{}
	}
	if guiState.clickHandled[MouseButton_Forward] {
		guiState.clickedElement[MouseButton_Forward] = GuiElementId{}
	}

	/*
		if guiState.dragHandled[MouseButton_Left] {
			guiState.dragElement[MouseButton_Left] = GuiElementId{}
		}
		if guiState.dragHandled[MouseButton_Right] {
			guiState.dragElement[MouseButton_Right] = GuiElementId{}
		}
		if guiState.dragHandled[MouseButton_Middle] {
			guiState.dragElement[MouseButton_Middle] = GuiElementId{}
		}
		if guiState.dragHandled[MouseButton_Back] {
			guiState.dragElement[MouseButton_Back] = GuiElementId{}
		}
		if guiState.dragHandled[MouseButton_Forward] {
			guiState.dragElement[MouseButton_Forward] = GuiElementId{}
		}
	*/

	guiState.hoverHandled = false
	guiState.focusHandled = false
	// Reset clickHandled so that it can be used to keep track of which clicks have been associated with a gui element.
	guiState.clickHandled = [MouseButton_Max]bool{MouseButton_Left: false, MouseButton_Right: false, MouseButton_Middle: false, MouseButton_Back: false, MouseButton_Forward: false}
	guiState.dragHandled = [MouseButton_Max]bool{MouseButton_Left: false, MouseButton_Right: false, MouseButton_Middle: false, MouseButton_Back: false, MouseButton_Forward: false}

	// Drag Start/End reset
	guiState.dragStartElement = [MouseButton_Max]GuiElementId{}
	guiState.dragEndElement = [MouseButton_Max]GuiElementId{}

	// HoverExit reset
	guiState.hoverEnterElement = GuiElementId{}
	guiState.hoverExitElement = GuiElementId{}

	// Handle FocusEnter and FocusExit reset
	// TODO: Should an element stay defocused until the defocus has been handled or replaced???
	guiState.focusEnterElement = GuiElementId{}
	guiState.focusExitElement = GuiElementId{}

	// Handle click resets
	if guiState.Click[MouseButton_Left] {
		guiState.clickedElement[MouseButton_Left] = GuiElementId{}
	}
	if guiState.Click[MouseButton_Right] {
		guiState.clickedElement[MouseButton_Right] = GuiElementId{}
	}
	if guiState.Click[MouseButton_Middle] {
		guiState.clickedElement[MouseButton_Middle] = GuiElementId{}
	}
	if guiState.Click[MouseButton_Back] {
		guiState.clickedElement[MouseButton_Back] = GuiElementId{}
	}
	if guiState.Click[MouseButton_Forward] {
		guiState.clickedElement[MouseButton_Forward] = GuiElementId{}
	}
}

func (guiState *GuiState) HandleEvents_End() {
	// No matter if scroll was or was not handled, reset it.
	guiState.WheelDir = 0

	// No matter if clicks were or were not handled, set all clicks as being handled.
	guiState.Click = [MouseButton_Max]bool{MouseButton_Left: false, MouseButton_Right: false, MouseButton_Middle: false, MouseButton_Back: false, MouseButton_Forward: false}

	// If hover was not handled, set hover to root
	if !guiState.hoverHandled {
		guiState.hoveredElement = GuiElementId{}
	}
}

func (guiState *GuiState) HandleEvents() {
	guiState.HandleEvents_Start()

	// Handle Element Events
	guiState.HandleElementEvents(guiState.Root, Rect[float32]{Point[float32]{0, 0}, float32(guiState.WindowSize.W), float32(guiState.WindowSize.H)})

	guiState.HandleEvents_End()
}

func (guiState *GuiState) Element_HandleEvent(element *GuiElement, clipRect Rect[float32], intersect, childHovered bool) bool {
	hovered := intersect && (pointInRect(&clipRect, float32(guiState.mx), float32(guiState.my)) || childHovered) //pointInRect(&element._pos, float32(guiState.mx), float32(guiState.my)) || childHovered

	// If hovered element is no longer behing hovered over, handle hoverExit
	if element.id == guiState.hoveredElement && !hovered {
		guiState.SetHoverExit(element.id)
	}

	// If dragged element is not longer being dragged, handle dragEnd
	for b := MouseButton(0); b < MouseButton_Max; b++ {
		if element.id == guiState.dragElement[b] && !guiState.Drag[b] && (element.id != GuiElementId{}) {
			guiState.SetDragEnd(element.id, b)
			guiState.setDragged(element.id, b, false)
		}
	}

	// Handle Mouse Hover
	// Additionally, set hoverExit for the element current set in guiState.hoveredElement (as long as not same as current element).
	if !guiState.hoverHandled && element.properties.Test(Property_Hoverable) && hovered {
		guiState.SetHover(element.id, true)
		guiState.hoverHandled = true
	}

	if hovered {
		for b := MouseButton(0); b < MouseButton_Max; b++ {
			// Click
			if guiState.Click[b] {
				if !guiState.focusHandled && element.properties.Test(Property_Focusable) && b != MouseButton_Back && b != MouseButton_Forward {
					guiState.SetFocus(element.id, true)
					guiState.focusHandled = true
				}

				if !guiState.clickHandled[b] && element.properties.Test(Property_Clickable) && element.clickButtons.Test(b) {
					guiState.setClicked(element.id, b, true)
					guiState.clickHandled[b] = true
				}
			}
		}

		// Scroll
		if element.properties.Test(Property_Scrollable) && guiState.WheelDir != 0 {
			guiState.setScrolled(element.id, true)
			guiState.scroll = guiState.WheelDir
			guiState.WheelDir = 0
		}
	}

	// Handle drag if the original points when starting the drag were inside the element.
	dragClicked := pointInRect(&clipRect, float32(guiState.DragOrigX), float32(guiState.DragOrigY))
	if dragClicked {
		for b := MouseButton(0); b < MouseButton_Max; b++ {
			if guiState.Drag[b] {
				if !guiState.dragHandled[b] && element.properties.Test(Property_Draggable) && element.dragButtons.Test(b) {
					guiState.setDragged(element.id, b, true)
					guiState.dragHandled[b] = true
				}
			}
		}
	}

	return hovered
}

// TODO: Do "clip rect" for inputs?
// On click - click event and hover exit, then hover enter on next frame if element is still being hovered.
func (guiState *GuiState) HandleElementEvents(element *GuiElement, clipRect Rect[float32]) bool {
	if !element.properties.Test(property_dorender) || !element.properties.Test(Property_Visible) {
		return false
	}

	childHovered := false
	// floatingChildHovered := false

	bounds := element._pos
	innerBounds := getBounds(element)
	clipRect, intersect := clipRect.Intersect(bounds)
	innerBounds_clipRect, _ := clipRect.Intersect(innerBounds)

	if intersect {
		for child := element.children.Front(); child != nil; child = child.Next() {
			isFloating := child.properties.Test(Property_Floating)

			if !child.properties.Test(Property_Visible) || !child.properties.Test(property_dorender) {
				break
			}

			// Only handle input when the element is displayed on the screen.
			if child._pos.X <= element._pos.X+element._pos.W && child._pos.Y <= element._pos.Y+element._pos.H && child._pos.X+child._pos.W >= element._pos.X && child._pos.Y+child._pos.H >= element._pos.Y {
				if guiState.HandleElementEvents(child, innerBounds_clipRect) {
					// If children are hovered, then so is the parent
					if isFloating {
						// floatingChildHovered = true
					} else {
						childHovered = true
					}
				}
			}
		}
	}

	hovered := intersect && (pointInRect(&clipRect, float32(guiState.mx), float32(guiState.my)) || childHovered) //pointInRect(&element._pos, float32(guiState.mx), float32(guiState.my)) || childHovered

	// If hovered element is no longer behing hovered over, handle hoverExit
	if element.id == guiState.hoveredElement && !hovered {
		guiState.SetHoverExit(element.id)
	}

	// Handle Mouse Hover
	// Additionally, set hoverExit for the element current set in guiState.hoveredElement (as long as not same as current element).
	if !guiState.hoverHandled && element.properties.Test(Property_Hoverable) && hovered {
		guiState.SetHover(element.id, true)
		guiState.hoverHandled = true
	}

	if hovered {
		for b := MouseButton(0); b < MouseButton_Max; b++ {
			if guiState.Click[b] {
				if !guiState.focusHandled && element.properties.Test(Property_Focusable) && b != MouseButton_Back && b != MouseButton_Forward {
					guiState.SetFocus(element.id, true)
					guiState.focusHandled = true
				}

				if !guiState.clickHandled[b] && element.properties.Test(Property_Clickable) && element.clickButtons.Test(b) {
					guiState.setClicked(element.id, b, true)
					guiState.clickHandled[b] = true
				}
			}
		}
		// TODO: Handle drag

		// Scroll
		if element.properties.Test(Property_Scrollable) && guiState.WheelDir != 0 {
			guiState.setScrolled(element.id, true)
			guiState.scroll = guiState.WheelDir
			guiState.WheelDir = 0
		}
	}

	return hovered
}
