package horusui

import (
	"math"
)

func Interp[T Number, U Number](from T, to T, percent U) T {
	return T(U(from)*(1.0-percent)) + T(U(to)*percent)
}

func InterpPercentFromValue[T Number, U Number](from T, to T, value T) U {
	return U(-((value - from) / (from + to)))
}

/*
	interpColor :: proc(from, to: Color, percent: f32) -> Color {
		// TODO
	}
*/
func Ease[T Number](t AnimationType, x T) T {
	switch t {
	case AnimationType_Linear:
		return x
	case AnimationType_Smoothstep:
		return Smoothstep(x)
	case AnimationType_Smootherstep:
		return Smootherstep(x)
	case AnimationType_Step_3:
		return Step3(x)
	case AnimationType_Step_4:
		return Step4(x)
	case AnimationType_Step_5:
		return Step5(x)
	case AnimationType_Jump:
		return Jump(x)

	case AnimationType_EaseInQuad:
		return EaseIn(x, 2)
	case AnimationType_EaseOutQuad:
		return EaseOut(x, 2)
	case AnimationType_EaseInOutQuad:
		return EaseInOut(x, 2)

	case AnimationType_EaseInCubic:
		return EaseIn(x, 3)
	case AnimationType_EaseOutCubic:
		return EaseOut(x, 3)
	case AnimationType_EaseInOutCubic:
		return EaseInOut(x, 3)

	case AnimationType_EaseInSine:
		return EaseInSine(x)
	case AnimationType_EaseOutSine:
		return EaseOutSine(x)
	case AnimationType_EaseInOutSine:
		return easeInOutSine(x)

	case AnimationType_EaseInExpo:
		return EaseInExpo(x)
	case AnimationType_EaseOutExpo:
		return EaseOutExpo(x)
	case AnimationType_EaseInOutExpo:
		return EaseInOutExpo(x)

	case AnimationType_EaseOutBounce:
		return EaseOutBounce(x)
	case AnimationType_EaseOutElastic:
		return EaseOutElastic(x)
	}

	return x
}
func Smoothstep[T Number](x T) T {
	return x * x * (3 - 2*x)
}
func Smootherstep[T Number](x T) T {
	return x * x * x * (x*(x*6-15) + 10)
}
func Step3[T Number](x T) T {
	if float32(x) < float32(1)/3 {
		return 0
	} else if float32(x) < float32(2)/3 {
		return T(1) / 2
	} else if float32(x) < 1 {
		return 1
	}

	return 1
}
func Step4[T Number](x T) T {
	if float32(x) < float32(1)/4 {
		return 0
	} else if float32(x) < float32(2)/4 {
		return T(1) / 3
	} else if float32(x) < float32(3)/4 {
		return T(2) / 3
	} else if float32(x) < 1 {
		return 1
	}

	return 1
}
func Step5[T Number](x T) T {
	if float32(x) < float32(1)/5 {
		return 0
	} else if float32(x) < float32(2)/5 {
		return T(1) / 4
	} else if float32(x) < float32(3)/5 {
		return T(2) / 4
	} else if float32(x) < float32(4)/5 {
		return T(3) / 4
	} else if float32(x) < 1 {
		return 1
	}

	return 1
}
func Jump[T Number](x T) T {
	if x < 1 {
		return 0
	} else {
		return 1
	}
}
func EaseIn[T Number](x T, p float64) T {
	return T(math.Pow(float64(x), p))
}
func EaseOut[T Number](x T, p float64) T {
	return T(1 - math.Pow(1-float64(x), p))
}
func EaseInOut[T Number](x T, p float64) T {
	x_float := float64(x)
	if x_float < 0.5 {
		return T(math.Pow(2, p-1) * math.Pow(x_float, p))
	} else {
		return T(1 - math.Pow(-2*x_float+2, p)/2)
	}
}
func EaseInSine[T Number](x T) T {
	return T(1 - math.Cos((float64(x)*math.Pi)/2))
}
func EaseOutSine[T Number](x T) T {
	return T(math.Sin((float64(x) * math.Pi) / 2))
}
func easeInOutSine[T Number](x T) T {
	return T(-(math.Cos(float64(x)*math.Pi) - 1) / 2)
}
func EaseInExpo[T Number](x T) T {
	if x == 0 {
		return 0
	} else {
		return T(math.Pow(2, 10*float64(x)-10))
	}
}
func EaseOutExpo[T Number](x T) T {
	if x == 1 {
		return 1
	} else {
		return T(1 - math.Pow(2, -10*float64(x)))
	}
}
func EaseInOutExpo[T Number](x T) T {
	if x == 1 {
		return 1
	} else {
		return T(1 - math.Pow(2, -10*float64(x)))
	}
}
func EaseOutBounce[T Number](x T) T {
	const n1 = 7.5625
	const d1 = 2.75

	x_float := float64(x)

	if x_float < 1/d1 {
		return T(n1 * x_float * x_float)
	} else if x_float < 2/d1 {
		a := (x_float - 1.5/d1)
		return T(n1*a*a + 0.75)
	} else if x_float < 2.5/d1 {
		a := (x_float - 2.25/d1)
		return T(n1*a*a + 0.9375)
	} else {
		a := (x_float - 2.625/d1)
		return T(n1*a*a + 0.984375)
	}
}
func EaseOutElastic[T Number](x T) T {
	const c4 = (2 * math.Pi) / 3

	if x == 0 {
		return 0
	} else if x == 1 {
		return 1
	} else {
		return T(math.Pow(2, -10*float64(x))*math.Sin((float64(x)*10-0.75)*c4) + 1)
	}
}

func ClipRect[T Number](r Rect[T], clip Rect[T]) Rect[T] {
	intersection := IntersectRect(r, clip)
	return Rect[T]{Point[T]{T(math.Abs(float64(r.X) - float64(intersection.X))), T(math.Abs(float64(r.Y) - float64(intersection.Y)))}, intersection.W, intersection.H}
}
func IntersectRect[T Number](r1 Rect[T], r2 Rect[T]) (intersection Rect[T]) {
	intersection.X = max(r1.X, r2.X)
	intersection.Y = max(r1.Y, r2.Y)
	intersection.W = min(r1.X+r1.W-1, r2.X+r2.W-1) - intersection.X
	intersection.H = min(r1.Y+r1.H-1, r2.Y+r2.H-1) - intersection.Y

	return intersection
}
