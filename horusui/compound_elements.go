package horusui

import (
	"strings"
)

var White = Color{255, 255, 255, 255}
var OffWhite = Color{250, 250, 250, 255}
var Gray = Color{250 / 2, 250 / 2, 250 / 2, 255}
var Grey = Gray
var Black = Color{0, 0, 0, 255}
var OffBlack = Color{25, 25, 25, 255}

var Red = Color{255, 0, 0, 255}
var Green = Color{0, 255, 0, 255}
var Blue = Color{0, 0, 255, 255}
var Yellow = Color{255, 255, 0, 255}
var Magenta = Color{255, 0, 255, 255}
var Cyan = Color{0, 255, 255, 255}
var Orange = Color{255, 165, 0, 255}

func (guiState *GuiState) CreateHDivider(id GuiElementId, color Color, margin GuiPadding, parent GuiElementId) (divider GuiElementId, container GuiElementId) {
	container = guiState.CreatePane(id, CreateSizeType(SizeMode_Fill, SizeMode_Auto, FPoint{1, 1}), GuiPanelDirection_Vertical, margin, parent)
	divider = guiState.CreatePane(guiState.CreateId(id), CreateSizeType(SizeMode_Fill, SizeMode_Auto, FPoint{1, 1}), GuiPanelDirection_Vertical, GuiPadding{}, container)
	guiState.SetBorder_sides(divider, GuiBorder{}, GuiBorder{}, GuiBorder{}, GuiBorder{1, color})

	return divider, container
}

func (guiState *GuiState) CenterPaneHorizontally(id GuiElementId, sizeType SizeType, paneDirection GuiPanelDirection, panePadding GuiPadding, parent GuiElementId) (pane GuiElementId, container GuiElementId) {
	return guiState.CenterPane(id, sizeType, GuiPanelDirection_Horizontal, paneDirection, panePadding, parent)
}
func (guiState *GuiState) CenterPaneVertically(id GuiElementId, sizeType SizeType, paneDirection GuiPanelDirection, panePadding GuiPadding, parent GuiElementId) (pane GuiElementId, container GuiElementId) {
	return guiState.CenterPane(id, sizeType, GuiPanelDirection_Vertical, paneDirection, panePadding, parent)
}

// TODO: Ability to pass in a function constructing an element as the container.
func (guiState *GuiState) CenterPane(id GuiElementId, sizeType SizeType, containerDirection GuiPanelDirection, paneDirection GuiPanelDirection, panePadding GuiPadding, parent GuiElementId) (pane GuiElementId, container GuiElementId) {
	container = guiState.CreatePane(id, sizeType, containerDirection, GuiPadding{}, parent)

	paneWidthMode := SizeMode_Auto
	var paneWidthValue float32 = 1
	paneHeightMode := SizeMode_Auto
	var paneHeightValue float32 = 1

	spacerWidthMode := SizeMode_Fill
	spacerHeightMode := SizeMode_Fill

	if containerDirection == GuiPanelDirection_Horizontal {
		spacerWidthMode = SizeMode_Flex
		paneHeightMode = SizeMode_Fill
	} else if containerDirection == GuiPanelDirection_Vertical {
		spacerHeightMode = SizeMode_Flex
		paneWidthMode = SizeMode_Fill
	}

	guiState.CreatePane(guiState.CreateId(id), CreateSizeType(spacerWidthMode, spacerHeightMode, FPoint{1, 1}), GuiPanelDirection_Vertical, GuiPadding{}, container)
	pane = guiState.CreatePane(guiState.CreateId(id), CreateSizeType(paneWidthMode, paneHeightMode, FPoint{paneWidthValue, paneHeightValue}), paneDirection, panePadding, container)
	guiState.CreatePane(guiState.CreateId(id), CreateSizeType(spacerWidthMode, spacerHeightMode, FPoint{1, 1}), GuiPanelDirection_Vertical, GuiPadding{}, container)

	return pane, container
}

func (guiState *GuiState) CreateRow(id GuiElementId, heightMode SizeMode, heightValue float32, padding GuiPadding, parent GuiElementId) (row GuiElementId) {
	row = guiState.CreatePane(id, CreateSizeType(SizeMode_Fill, heightMode, FPoint{1, heightValue}), GuiPanelDirection_Horizontal, padding, parent)
	return row
}

// Row with Auto as widthMode
func (guiState *GuiState) CreateAutoRow(id GuiElementId, heightMode SizeMode, heightValue float32, padding GuiPadding, parent GuiElementId) (row GuiElementId) {
	row = guiState.CreatePane(id, CreateSizeType(SizeMode_Auto, heightMode, FPoint{1, heightValue}), GuiPanelDirection_Horizontal, padding, parent)
	return row
}
func (guiState *GuiState) CreateFlexRow(id GuiElementId, heightMode SizeMode, heightValue float32, flexValue float32, padding GuiPadding, parent GuiElementId) (row GuiElementId) {
	row = guiState.CreatePane(id, CreateSizeType(SizeMode_Flex, heightMode, FPoint{flexValue, heightValue}), GuiPanelDirection_Horizontal, padding, parent)
	return row
}
func (guiState *GuiState) CreateColumn(id GuiElementId, widthMode SizeMode, widthValue float32, padding GuiPadding, parent GuiElementId) (col GuiElementId) {
	col = guiState.CreatePane(id, CreateSizeType(widthMode, SizeMode_Fill, FPoint{widthValue, 1}), GuiPanelDirection_Vertical, padding, parent)
	return col
}

// Column with Auto as heightMode
func (guiState *GuiState) CreateAutoColumn(id GuiElementId, widthMode SizeMode, widthValue float32, padding GuiPadding, parent GuiElementId) (col GuiElementId) {
	col = guiState.CreatePane(id, CreateSizeType(widthMode, SizeMode_Auto, FPoint{widthValue, 1}), GuiPanelDirection_Vertical, padding, parent)
	return col
}
func (guiState *GuiState) CreateFlexColumn(id GuiElementId, widthMode SizeMode, widthValue float32, padding GuiPadding, parent GuiElementId) (col GuiElementId) {
	col = guiState.CreatePane(id, CreateSizeType(widthMode, SizeMode_Flex, FPoint{widthValue, 1}), GuiPanelDirection_Vertical, padding, parent)
	return col
}

// Pane with border
func (guiState *GuiState) CreateBox(id GuiElementId, sizeType SizeType, border GuiBorder, direction GuiPanelDirection, padding GuiPadding, parent GuiElementId) (element GuiElementId) {
	element = guiState.CreatePane(id, sizeType, direction, padding, parent)
	guiState.SetBorder_all(id, border)

	return element
}

// TODO: CheckBoxText - default to UseGlobal
func (guiState *GuiState) CheckBox(id GuiElementId, checkboxColor Color, checkboxHoverColor Color, text string, textColor Color, textHover Color, font *Font, parent GuiElementId, isChecked bool, disabled bool) (element GuiElementId, checkContainer GuiElementId, check GuiElementId, textPane GuiElementId) {
	// checkboxColor := checkboxColor
	checkboxBackground := checkboxColor

	// textColor := textColor
	padding := float32(int(Scale_value(guiState, float32(4))))
	// TODO: make sure padding doesn't become too big and overtake the check size (particularly for small scale sizes)

	fontHeight := font.Font.LineSkip()
	// getFontHeight2(font, false)
	//height := float32(math.Round(float64(fontHeight)))
	height := float32(fontHeight)
	element = guiState.CreatePane(id, CreateSizeType(SizeMode_Auto, SizeMode_Pixel, FPoint{1, height + float32(padding)}), GuiPanelDirection_Horizontal, GuiPadding{}, parent)
	if !disabled && guiState.IsHovered(element) {
		checkboxColor = checkboxHoverColor
		checkboxBackground = checkboxHoverColor
		textColor = textHover
	}
	if !isChecked {
		checkboxBackground.A = 0
	}

	// Child index 0
	checkContainer = guiState.CreateBox(guiState.CreateId(id), CreateSizeType(SizeMode_Pixel, SizeMode_Fill, FPoint{height + float32(padding), 1}).SetMax(FPoint{Scale_value(guiState, float32(20)), 0}), GuiBorder{1, checkboxColor}, GuiPanelDirection_Vertical, GuiPadding{padding, padding, padding, padding}, element)
	check = guiState.CreatePane(guiState.CreateId(id), CreateSizeType(SizeMode_Fill, SizeMode_Fill, FPoint{1, 1}), GuiPanelDirection_Vertical, GuiPadding{}, checkContainer)
	guiState.SetBackground(check, checkboxBackground)
	//if isChecked do addBackground(child_2, checkboxColor); // TODO: Fix - something's wrong with layout system

	/*if !isChecked do hide(check);
	  else do show(check);*/

	// Child index 1
	textPane = guiState.CreatePane(guiState.CreateId(id), CreateSizeType(SizeMode_Auto, SizeMode_Fill, FPoint{1, 1}), GuiPanelDirection_Vertical, guiState.Scale_padding(GuiPadding{0, 0, 10, 10}), element)
	guiState.SetText(textPane, text, "", textColor, font, TextAlignment_LeftOrTop, TextAlignment_Middle, GuiTextOverflow_Clip, TextDirection_UseGlobal) // TODO: Left vs. Default

	return element, checkContainer, check, textPane
}
func (guiState *GuiState) ChangeCheckBoxColor_id(checkBox GuiElementId, color Color) {
	e := guiState.elements[checkBox]
	guiState.ChangeCheckBoxColor_ptr(e, color)
}
func (guiState *GuiState) ChangeCheckBoxColor_ptr(checkBox *GuiElement, color Color) {
	checkContainer := GetChild(checkBox, 0)
	checkContainer.borderLeft.Color = color
	checkContainer.borderRight.Color = color
	checkContainer.borderTop.Color = color
	checkContainer.borderBottom.Color = color
	check := GetChild(checkContainer, 0)
	guiState.SetBackground(check.id, color)
}

// ChangeCheckBoxColor :: proc{changeCheckBoxColor_id, changeCheckBoxColor_ptr};
func (guiState *GuiState) GetCheckBoxCheck_id(checkBox GuiElementId) GuiElementId {
	return guiState.GetCheckBoxCheck_ptr(guiState.GetElement(checkBox)).id
	//return fmt.tprintf("%s-check", checkBox);
}
func (guiState *GuiState) GetCheckBoxCheck_ptr(checkBox *GuiElement) *GuiElement {
	checkContainer := GetChild(checkBox, 0)
	return GetChild(checkContainer, 0)
}

// GetCheckBoxCheck :: proc{getCheckBoxCheck_id, getCheckBoxCheck_ptr};
func (guiState *GuiState) CheckCheckBox(checkBox GuiElementId) {
	check := guiState.GetCheckBoxCheck_id(checkBox)
	guiState.Show(check)
}
func (guiState *GuiState) UncheckCheckBox(checkBox GuiElementId) {
	check := guiState.GetCheckBoxCheck_id(checkBox)
	guiState.Hide(check)
}
func (guiState *GuiState) ToggleCheckBox(checkBox GuiElementId) {
	check := guiState.GetCheckBoxCheck_id(checkBox)
	guiState.ToggleVisibility(check)
}
func (guiState *GuiState) GetCheckBoxText_id(checkBox GuiElementId) GuiElementId {
	//return fmt.tprintf("%s-text", checkBox);
	return guiState.GetCheckBoxText_ptr(guiState.GetElement(checkBox)).id
}
func (guiState *GuiState) GetCheckBoxText_ptr(checkBox *GuiElement) *GuiElement {
	textPane := GetChild(checkBox, 1)
	return textPane
}

// GetCheckBoxText :: proc{getCheckBoxText_id, getCheckBoxText_ptr};

// TODO: Add textOffset to GuiElement so that we can render text at an offset.
//
//	Also need to clip text offset past the left side of the container
func (guiState *GuiState) CreateTextBox(id GuiElementId, sizeType SizeType, borderUnfocused GuiBorder, borderFocused GuiBorder, borderHover GuiBorder, padding GuiPadding, placeholderText string, placeholderTextColor Color, textColor Color, font *Font, textPtr *strings.Builder, direction TextDirection, parent GuiElementId) (element GuiElementId, focused bool) {
	focused = false
	// direction := direction
	if direction == TextDirection_UseGlobal {
		direction = guiState.globalTextDirection
	}

	// textColor := textColor
	element = guiState.CreateBox(id, sizeType, borderUnfocused, GuiPanelDirection_Horizontal, padding, parent)
	if guiState.IsFocused(element) {
		guiState.SetBorder_all(element, borderFocused)
		focused = true
	} else if guiState.IsHovered(element) {
		guiState.SetBorder_all(element, borderHover)
	}
	textString := textPtr.String()
	if textString == "" {
		/*textColor.r = (textColor.r + 255 - 40) % 255;
		  textColor.g = (textColor.g + 255 - 40) % 255;
		  textColor.b = (textColor.b + 255 - 40) % 255;*/
		guiState.SetText(element, placeholderText, "", placeholderTextColor, font, TextAlignment_Default, TextAlignment_Middle, GuiTextOverflow_Clip, direction)
	} else {
		guiState.SetText(element, textString, "", textColor, font, TextAlignment_Default, TextAlignment_Middle, GuiTextOverflow_Clip, direction)
	}

	return element, focused
}

/*
func CreateGrid($rows: int, $cols: int, spacing: GuiSpacing = {}) -> [rows][cols]GuiElementId {

}*/

// IDEA: All control create functions end with a style argument
// Note: focused = hover in this context.
// columnStart(), columnEnd()
// columnLayout() - will set the current/selected element to be Vertical (a column layout)
// Things that can accept children will have start and end function. Things that don't accept children only have one function.
// image(), text()
// buttonStart(), buttonEnd()
// controlStart(), controlEnd()

// UiCtrl
// * parent, childFirst, childLast, siblingPrev, siblingNext
// * layoutType, flags, focusState, scrollOffset
// * state - default, focused, active, or disabled
// * stateFlags - which states are enabled
// * styles ptr/array - indexed with state

// Flags - focusable, pressable, toggleable, selectable, scrollable_vertical, scrollable_horizontal
// styles - margin, padding, bgColor, borderColor, borderWidth, radius, etc.

// Frame - 1. Pass in Input State, 2. System handles Input on frame start
//  3. Layout the controls, 4. Rendering

// Input State
// setMouse(x, y)
// setActions(UiInputActions)
// addText(textInput) - text boxes, etc.

// UiInputActions - uses a bit_set - CtrlPressed, ShiftPressed, Left, Right, Up, Down,
//  Enter, Space, Backspace, Delete, Undo, Cut, Copy, Paste, SelectAll,
//  Redo, DeleteToStartOfWord (Ctrl+Backspace), DeleteToEndOfWord (Ctrl+Del)
//  WordStart, WordEnd, SelectLeft, SelectRight, SelectUp, SelectDown, SelectWordStart, SelectWordEnd
