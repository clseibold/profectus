# Profectus

Current Version: Beta 1.2.1
Copyright: 2024 Christian Lee Seibold
License: MIT

[Profectus on Flathub](https://flathub.org/apps/to.us.scrollprotocol.Profectus)<br>
[Download precompiled binaries via Gemini](gemini://scrollprotocol.us.to/precompiled/)

## What is Profectus?

Profectus is a new GUI client for the scroll, gemini, nex, and spartan protocols that is intended to rival Lagrange in performance, visual design, features, and cross-platform compatibility. It is written in Golang using SDL and a custom GUI. Font rendering is currently done with SDL-TTF, but will be moved to Harfbuzz eventually.

## Installation

### Linux

You will need to download the following dependencies:

- sdl2
- sdl2-image
- sdl2-ttf
- sdl2-gfx

And these are optional but recommended:

- libjxl
- libavif
- libwebp - for Beta 1.0, removed in Beta 1.1

For Debian-based distributions, you can run the following:

```
sudo apt install libsdl2 libsdl2-image libsdl2-gfx libsdl2-ttf libjxl
```

For Fedora-based distributions, run the following:

```
sudo dnf install SDL2 SDL2_image SDL2_gfx SDL2_ttf libjxl
```

Unzip the zip, and double click `profectus` to run the program. It runs in portable mode for now.

### Windows

Building for Windows requires mingw64, as well as mingw64-libwebp, and sdl2, sdl2-image, sdl2-ttf, and sdl2-gfx for mingw64.

To run the program on Windows, you should not need to download any dependencies, unless you want to be able to pipe streaming videos into a video player (mpv, vlc, or ffplay); you can download mpv, vlc, or ffmpeg through winget, choco, or some other method. Make sure they are added to your PATH.

Unzip the zip, and double click `profectus.exe` to run the program. It runs in portable mode for now.

### macOS

Profectus has not been tested with macOS, but you can try to manually build it for macOS by visiting the repository. You will need SDL2.0, SDL2.0-image, and SDL2.0-ttf libraries installed.
[https://gitlab.com/clseibold/profectus](Profectus Repository)

## Building

Dependencies:

- libjxl-devel
- sdl2.0-devel
- sdl2.0-image-devel
- sdl2.0-ttf-devel
- sdl2.0-gfx-devel

The minimum requried Golang version to build is 1.21.9.

### Cross-compile for Windows

Use mingw64 and install the mingw64 libraries for sdl2, sdl2-image, sdl2-ttf, and sdl2-gfx (or copy them into your mingw directory).
Also install the mingw64 library for libwebp. Finally, run `build_mingw.sh` on Linux to cross-compile for Windows.

## Getting Updates

You can get updates on the Scroll Protocol's software downloads page:
[gemini://scrollprotocol.us.to/software/precompiled/](Scroll Protocol Downloads via Gemini)

## Upcoming Features

- Scroll-Specific features:
  - Abstracts
  - Language Selection
- File Upload via Titan/Spartan, and Sensitive Input being hidden
- Downloads
- Bookmarks and history pages
- Sibling links
- TOFU verification
- Configure searchengine
- View pages as plain text
- Tabs and Tab dragging
- More document formats: markdown, asciidoc, Atom, and RSS
- Inline document viewing (for images and audio) - based on Lagrange
- Certificate management
- Use harfbuzz and bidi directly
- Gemsub feeds
- Custom fonts and more UI Customization
- UI Animations
- Split View
- Text-to-speech? (using rhasspy/piper and amitybell/piper)

## About the Name "Profectus"

Profectus in Latin means progress, or advancement. It is an intentional contrast to lack of change, or staying put, but it is not a dichotomy; progress requires both stability and change.

Staying the same, at the same points, in the same ranges, using the same tools, embracing the same limits, and keeping to the same sources of knowledge, means we do not learn from failures, we do not branch out, we do not explore and discover. Staying in constant motion, always advancing, chasing the next achievement, the next shiny object, the next best tech, means we do not have the chance to reflect on our choices and our past.

Scroll finds a balance between reflecting on the successes and failures of Gemini and Gopher, and advancing the idea of a reader-focused protocol. It offers stability and change. The stability is largely in the protocol that can be more widely utilized toward the specific goal of reading and media, and the change is in both the extended markup and the potential for media and other document formats. This change reflects on real-world usage of documents that are meant to be read. This advancement through stability and reflection perfectly encapsulates literature and media in general.
