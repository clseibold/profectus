package main

/*
import (
	"github.com/tidbyt/go-libwebp/webp"
	"github.com/veandco/go-sdl2/sdl"
)

type WebPAnimation struct {
	Data     *webp.Animation
	Frames   []*sdl.Surface
	Delays   []float32 // In Seconds
	frameImg *sdl.Surface
	Width    float32
	Height   float32
}

func NewWebPAnimation(data *webp.Animation) *WebPAnimation {
	surf, _ := sdl.CreateRGBSurfaceWithFormat(0, int32(data.CanvasWidth), int32(data.CanvasHeight), 64, uint32(sdl.PIXELFORMAT_RGBA32))
	surf.SetBlendMode(sdl.BLENDMODE_BLEND)

	anim := &WebPAnimation{
		Data:     data,
		frameImg: surf,
		Width:    float32(data.CanvasWidth),
		Height:   float32(data.CanvasHeight),
	}
	anim.Load()
	return anim
}

func (anim *WebPAnimation) Load() {
	defer anim.frameImg.Free()
	//empty := color.RGBA64{0, 0, 0, 0}

	prevTimestamp := 0

	for y := 0; y < anim.frameImg.Bounds().Dy(); y++ {
		for x := 0; x < anim.frameImg.Bounds().Dx(); x++ {
			anim.frameImg.Set(x, y, anim.Data.BackgroundColor)
		}
	}

	for index, frame := range anim.Data.Image {
		for y := 0; y < frame.Bounds().Dy(); y++ {
			for x := 0; x < frame.Bounds().Dx(); x++ {
				color := frame.At(x, y)
				anim.frameImg.Set(x, y, color)
			}
		}

		newSurf, _ := anim.frameImg.Duplicate()
		anim.Frames = append(anim.Frames, newSurf)

		delay := float32(anim.Data.Timestamp[index]-prevTimestamp) / 1000
		prevTimestamp = anim.Data.Timestamp[index]
		if delay <= 0 {
			delay = 0.01
		}
		anim.Delays = append(anim.Delays, delay)
	}
}

func (anim *WebPAnimation) Destroy() {
	for _, frame := range anim.Frames {
		if frame != nil {
			frame.Free()
		}
	}
}

type WebPPlayer struct {
	Animation    *WebPAnimation
	CurrentFrame int
	Timer        float32
	Frames       []*sdl.Texture
	renderer     *sdl.Renderer
}

func NewWebPPlayer(renderer *sdl.Renderer, webpAnim *WebPAnimation) *WebPPlayer {
	return &WebPPlayer{
		Animation: webpAnim,
		renderer:  renderer,
	}
}

func (webpPlayer *WebPPlayer) Update(dt float32) {
	webpPlayer.Timer += dt

	for webpPlayer.Timer >= webpPlayer.Animation.Delays[webpPlayer.CurrentFrame] {
		webpPlayer.Timer -= webpPlayer.Animation.Delays[webpPlayer.CurrentFrame]
		webpPlayer.CurrentFrame++
		if webpPlayer.CurrentFrame >= len(webpPlayer.Animation.Frames) {
			webpPlayer.CurrentFrame = 0
		}
	}
}

func (webpPlayer *WebPPlayer) Destroy(state *State) {
	webpPlayer.Animation.Destroy()
	for _, frame := range webpPlayer.Frames {
		if frame != nil {
			state.destroyer <- frame
		}
	}
}

func (webpPlayer *WebPPlayer) Texture() *sdl.Texture {
	for len(webpPlayer.Frames) <= webpPlayer.CurrentFrame {
		surface := webpPlayer.Animation.Frames[webpPlayer.CurrentFrame]
		surface.SetBlendMode(sdl.BLENDMODE_BLEND)
		frame, _ := webpPlayer.renderer.CreateTextureFromSurface(surface)
		frame.SetBlendMode(sdl.BLENDMODE_BLEND)
		webpPlayer.Frames = append(webpPlayer.Frames, frame)

		webpPlayer.Animation.Frames[webpPlayer.CurrentFrame] = nil
		surface.Free()
	}
	return webpPlayer.Frames[webpPlayer.CurrentFrame]
}
*/
