package main

import (
	"fmt"
	"path"
	"strings"
	"time"

	"github.com/gopxl/beep/speaker"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/clseibold/profectus/horusui"
	scroll "gitlab.com/clseibold/scroll-term/scroll_client"
)

// Setup Defaults and Animation Stuff here
func setupGui(state *State) {
}

func updateGui(state *State, deltaTime float64) {
	state.currentLanguageDirection = horusui.TextDirection_LTR_TTB
	activeTab := state.getActiveTab()
	guiState := state.guiState
	updateTheme(state)
	root := guiState.RootElement(currentTheme.Page.Panel.background[Selector_Default])
	guiState.SetDirection(root, horusui.GuiPanelDirection_Vertical)

	if currentTheme.Tabbar.Position == "above" {
		createTabBar(state, root, deltaTime, activeTab)
	}
	createTopBar(state, root, deltaTime, activeTab)
	if currentTheme.Tabbar.Position == "below" {
		createTabBar(state, root, deltaTime, activeTab)
	}

	mainAppContainer := guiState.Element(guiState.CreateId(root), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Flex, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Horizontal, root)
	guiState.SetPadding(mainAppContainer, guiState.Scale_padding(currentTheme.ContentContainerPadding))
	guiState.SetSpacing(mainAppContainer, guiState.Scale_spacing(horusui.GuiSpacing{X: currentTheme.SidebarDivider, Y: 14}))
	if state.currentLanguageDirection == horusui.TextDirection_RTL_TTB {
		guiState.SetFlip(mainAppContainer, true)
	}

	if currentTheme.Sidebar.Dock == "default" || currentTheme.Sidebar.Dock == "" || (currentTheme.Sidebar.Dock == "left" && state.currentLanguageDirection == horusui.TextDirection_LTR_TTB) || (currentTheme.Sidebar.Dock == "right" && state.currentLanguageDirection == horusui.TextDirection_RTL_TTB) {
		state.sidebarAnimation.Update(deltaTime * 1000)
		sidebarContainer := guiState.Element(guiState.CreateId(root), horusui.SizeColumn(horusui.SizeMode_Percent, 0.25).SetPercent(horusui.FPoint{X: state.sidebarAnimation.CurrentValue(), Y: 1}).SetMax(horusui.Scale_FPoint(guiState, horusui.FPoint{X: float32(currentTheme.Sidebar.MaxWidth), Y: 0})).SetMin(horusui.Scale_FPoint(guiState, horusui.FPoint{X: float32(currentTheme.Sidebar.MinWidth), Y: 0})), horusui.GuiPanelDirection_Horizontal, mainAppContainer)
		//guiState.SetBackground(sidebarContainer, horusui.White)
		//guiState.SetText(sidebarContainer, "Testing!", "", horusui.Black, state.defaultFont, horusui.TextAlignment_Middle, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Clip, horusui.TextDirection_LTR_TTB)
		if state.currentLanguageDirection == horusui.TextDirection_RTL_TTB {
			guiState.SetFlip(sidebarContainer, true)
		}
		/*if guiState.IsHovered(sidebarContainer) {
			guiState.SetBackground(sidebarContainer, horusui.Black)
		}*/
		createSidebar(state, sidebarContainer, deltaTime, activeTab)
		/*if state.guiState.windowSize.w <= scale(i32(732)) {
			setFloating(sidebarContainer, {}, mainAppContainer);
		}*/
	}

	content := guiState.Element(guiState.CreateId(root), horusui.SizeColumn(horusui.SizeMode_Flex, 2.5), horusui.GuiPanelDirection_Vertical, mainAppContainer)
	guiState.SetClickable(content, true, horusui.MouseButton_Left)

	documentPage(state, content, deltaTime, activeTab)

	if currentTheme.Sidebar.Dock == "opposite" || (currentTheme.Sidebar.Dock == "left" && state.currentLanguageDirection == horusui.TextDirection_RTL_TTB) || (currentTheme.Sidebar.Dock == "right" && state.currentLanguageDirection == horusui.TextDirection_LTR_TTB) {
		state.sidebarAnimation.Update(deltaTime * 1000)
		sidebarContainer := guiState.Element(guiState.CreateId(root), horusui.SizeColumn(horusui.SizeMode_Percent, 0.25).SetPercent(horusui.FPoint{X: state.sidebarAnimation.CurrentValue(), Y: 1}).SetMax(horusui.Scale_FPoint(guiState, horusui.FPoint{X: float32(currentTheme.Sidebar.MaxWidth), Y: 0})).SetMin(horusui.Scale_FPoint(guiState, horusui.FPoint{X: float32(currentTheme.Sidebar.MinWidth), Y: 0})), horusui.GuiPanelDirection_Horizontal, mainAppContainer)
		//guiState.SetBackground(sidebarContainer, horusui.White)
		//guiState.SetText(sidebarContainer, "Testing!", "", horusui.Black, state.defaultFont, horusui.TextAlignment_Middle, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Clip, horusui.TextDirection_LTR_TTB)
		if state.currentLanguageDirection == horusui.TextDirection_RTL_TTB {
			guiState.SetFlip(sidebarContainer, true)
		}
		/*if guiState.IsHovered(sidebarContainer) {
			guiState.SetBackground(sidebarContainer, horusui.Black)
		}*/
		createSidebar(state, sidebarContainer, deltaTime, activeTab)
		/*if state.guiState.windowSize.w <= scale(i32(732)) {
			setFloating(sidebarContainer, {}, mainAppContainer);
		}*/
	}
}

func createTopBar(state *State, parent horusui.GuiElementId, deltaTime float64, activeTab *Tab) {
	guiState := state.guiState
	topBar := guiState.Element(guiState.CreateId(parent), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1 /*horusui.Scale_value(guiState, float32(27))*/}), horusui.GuiPanelDirection_Horizontal, parent)
	guiState.SetBackground(topBar, currentTheme.Topbar.Panel.background[Selector_Default])
	guiState.SetSpacing(topBar, guiState.Scale_spacing(currentTheme.Topbar.Panel.spacing))

	if currentTheme.Tabbar.Position == "below" {
		guiState.SetBorder_sides(topBar, currentTheme.Topbar.Panel.border[Selector_Default], currentTheme.Topbar.Panel.border[Selector_Default], currentTheme.Topbar.Panel.border[Selector_Default], horusui.GuiBorder{})
	} else if currentTheme.Tabbar.Position == "above" {
		guiState.SetBorder_sides(topBar, horusui.GuiBorder{}, horusui.GuiBorder{}, horusui.GuiBorder{}, currentTheme.Topbar.Panel.border[Selector_Default])
	}

	guiState.SetPadding(topBar, guiState.Scale_padding(currentTheme.Topbar.Panel.padding))
	{
		if guiState.IsClicked(topBar, horusui.MouseButton_Back) {
			go backPage(state, activeTab)
		}
		if guiState.IsClicked(topBar, horusui.MouseButton_Forward) {
			go forwardPage(state, activeTab)
		}

		// Left Side Buttons
		//backBtn := createButton(state, guiState.CreateId(topBar), "<", topBar, true, true, state.topBarUiFont, currentTheme.Topbar)
		backBtn := createButton(state, guiState.CreateId(topBar), "\ue5c4", topBar, true, true, state.topBarIconsFont, currentTheme.Topbar)
		if guiState.IsClicked(backBtn, horusui.MouseButton_Left) {
			go backPage(state, activeTab)
		}

		forwardBtn := createButton(state, guiState.CreateId(topBar), "\ue5c8", topBar, true, true, state.topBarIconsFont, currentTheme.Topbar)
		if guiState.IsClicked(forwardBtn, horusui.MouseButton_Left) {
			go forwardPage(state, activeTab)
		}

		refreshBtn := createButton(state, guiState.CreateId(topBar), "\ue5d5", topBar, true, true, state.topBarIconsFont, currentTheme.Topbar)
		if guiState.IsClicked(refreshBtn, horusui.MouseButton_Left) {
			go refreshPage(state, activeTab)
		}

		// Left Spacer
		guiState.CreateFlexRow(guiState.CreateId(topBar), horusui.SizeMode_Auto, 1, 0.75, horusui.GuiPadding{}, topBar)

		// Address Textbox
		createTextbox_custom(state, guiState.CreateId(topBar), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}).SetMin(horusui.FPoint{X: horusui.Scale_value(guiState, float32(420)), Y: 0}), "Address", state.topBarUiFont, &activeTab.addressInput, topBar, currentTheme.Topbar)

		// Right Spacer
		guiState.CreateFlexRow(guiState.CreateId(topBar), horusui.SizeMode_Auto, 1, 0.75, horusui.GuiPadding{}, topBar)

		// Right Side Buttons
		homeBtn := createButton(state, guiState.CreateId(topBar), "\ue88a", topBar, true, true, state.topBarIconsFont, currentTheme.Topbar)
		if guiState.IsClicked(homeBtn, horusui.MouseButton_Left) {
			go getHomepage(state, activeTab)
		}
	}
}

func createTabBar(state *State, parent horusui.GuiElementId, deltaTime float64, activeTab *Tab) {
	guiState := state.guiState
	tabbar := guiState.Element(guiState.CreateId(parent), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Horizontal, parent)
	guiState.SetBackground(tabbar, currentTheme.Tabbar.Panel.background[Selector_Default])

	if currentTheme.Tabbar.Position == "below" {
		guiState.SetBorder_sides(tabbar, currentTheme.Topbar.Panel.border[Selector_Default], currentTheme.Topbar.Panel.border[Selector_Default], horusui.GuiBorder{}, currentTheme.Tabbar.Panel.border[Selector_Default])
	} else if currentTheme.Tabbar.Position == "above" {
		guiState.SetBorder_sides(tabbar, currentTheme.Topbar.Panel.border[Selector_Default], currentTheme.Topbar.Panel.border[Selector_Default], horusui.GuiBorder{}, currentTheme.Tabbar.Panel.border[Selector_Default])
	}

	guiState.SetPadding(tabbar, guiState.Scale_padding(currentTheme.Tabbar.Panel.padding))
	guiState.SetSpacing(tabbar, guiState.Scale_spacing(currentTheme.Tabbar.Panel.spacing))
	{
		// If add_button_position is set to "before_tabs"
		if currentTheme.Tabbar.AddButtonPosition == -1 {
			createTabBtn := createTabButton(state, guiState.CreateId(tabbar), "+", tabbar, state.arial15, false, false, false, true, 1, currentTheme.Tabbar.Panel.padding.Bottom > 0)
			guiState.AutoAsMin(createTabBtn, true)
			if guiState.IsClicked(createTabBtn, horusui.MouseButton_Left) {
				newTab := state.NewTab()
				state.setActiveTab(newTab)
			}
		}

		for i, tab := range state.tabs {
			state.guiState.SetLoopId(i)
			text := tab.currentPageCache.Title
			if tab.documentLoading {
				text = "Loading ..."
			} else if text == "" {
				if tab.currentPageCache.Url != nil {
					text = path.Base(tab.currentPageCache.Url.Path)
				}
			}
			tab.widthPercentAnimation.Update(deltaTime * 1000)
			tabBtn := createTabButton(state, guiState.CreateId(tabbar), text, tabbar, state.arial15, activeTab == tab, false, false, false, tab.widthPercentAnimation.CurrentValue(), currentTheme.Tabbar.Panel.padding.Bottom > 0)
			if guiState.IsClicked(tabBtn, horusui.MouseButton_Left) {
				state.setActiveTab(tab)
			}
			if guiState.IsClicked(tabBtn, horusui.MouseButton_Middle) {
				tab.widthPercentAnimation.Hide()
				go func(tab *Tab) {
					time.Sleep(time.Millisecond*time.Duration(tab.widthPercentAnimation.TotalMS()) + 5)
					tab.Close(state)
				}(tab)
			}
			/*if guiState.IsDragStart(tabBtn, horusui.MouseButton_Left) {
				fmt.Printf("Drag Start\n")
			} else if guiState.IsDragging(tabBtn, horusui.MouseButton_Left) {
				fmt.Printf("Dragging tab %d\n", 0)
			} else if guiState.IsDragEnd(tabBtn, horusui.MouseButton_Left) {
				fmt.Printf("Drag End\n")
			}*/
		}
		state.guiState.EndLoopId()

		guiState.Element(guiState.CreateId(tabbar), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Auto, horusui.FPoint{X: currentTheme.Tabbar.AddButtonPosition, Y: 1}), horusui.GuiPanelDirection_Horizontal, tabbar) // Spacer
		/*if currentTheme.Tabbar.AddButtonPosition >= 0 {
			guiState.SetPercentSize(spacer, horusui.FPoint{X: currentTheme.Tabbar.AddButtonPosition, Y: 1})
		}*/

		// If add_button_position is set to "after_tabs" or "opposite"
		if currentTheme.Tabbar.AddButtonPosition >= 0 {
			createTabBtn := createTabButton(state, guiState.CreateId(tabbar), "+", tabbar, state.arial15, false, false, false, true, 1, currentTheme.Tabbar.Panel.padding.Bottom > 0)
			guiState.AutoAsMin(createTabBtn, true)
			if guiState.IsClicked(createTabBtn, horusui.MouseButton_Left) {
				newTab := state.NewTab()
				state.setActiveTab(newTab)
			}
			if guiState.IsClicked(createTabBtn, horusui.MouseButton_Middle) {
				state.NewTab()
			}
		}

		guiState.Element(guiState.CreateId(tabbar), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Auto, horusui.FPoint{X: 1 - currentTheme.Tabbar.AddButtonPosition, Y: 1}), horusui.GuiPanelDirection_Horizontal, tabbar) // Spacer
	}
}

// Sidebar
func createSidebar(state *State, parent horusui.GuiElementId, deltaTime float64, activeTab *Tab) {
	guiState := state.guiState

	sidebar := guiState.CreateBox(guiState.CreateId(parent), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Fill, horusui.FPoint{X: 1, Y: 1}).SetPercent(horusui.FPoint{X: 1, Y: 1}), currentTheme.Sidebar.Panel.border[Selector_Default], horusui.GuiPanelDirection_Vertical, guiState.Scale_padding(currentTheme.Sidebar.Panel.padding), parent)
	if currentTheme.ContentContainerPadding.Top <= 1 {
		// If there's no top padding, then remove the top border of the sidebar.
		guiState.HideTopBorder(sidebar)
	}
	if currentTheme.ContentContainerPadding.Bottom == 0 {
		// If there's no bottom padding, then remove the top border of the sidebar.
		guiState.HideBottomBorder(sidebar)
	}
	if currentTheme.ContentContainerPadding.Left == 0 {
		// If there's no left padding, then remove the top border of the sidebar.
		guiState.HideLeftBorder(sidebar)
	}
	guiState.SetBackground(sidebar, currentTheme.Sidebar.Panel.background[Selector_Default])
	guiState.SetSpacing(sidebar, guiState.Scale_spacing(currentTheme.Sidebar.Panel.spacing))
	guiState.SetRounded(sidebar, currentTheme.Sidebar.Panel.radius)
	{
		if guiState.IsClicked(sidebar, horusui.MouseButton_Back) {
			go backPage(state, activeTab)
		}
		if guiState.IsClicked(sidebar, horusui.MouseButton_Forward) {
			go forwardPage(state, activeTab)
		}

		// -- Outline --
		outlineContainer := guiState.CreatePane(guiState.CreateId(sidebar), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Flex, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, horusui.GuiPadding{Top: 0, Bottom: 0, Left: 0, Right: 0}, sidebar)
		if guiState.IsScrolled(outlineContainer) {
			prevScrollValue := activeTab.outlineScroll.CurrentValue()
			newValue := prevScrollValue - (float32(guiState.GetScroll()) * 2.75 * (float32(state.arial15.Font.LineSkip()) + horusui.Scale_value(guiState, float32(currentTheme.Sidebar.List.padding.Top+currentTheme.Sidebar.List.padding.Bottom))))
			if newValue < 0 {
				newValue = 0
			}
			activeTab.outlineScroll.SaveCurrentAndSetNewEnd(newValue)
		}
		activeTab.outlineScroll.Update(deltaTime * 1000)
		guiState.SetScrollPixelOffset(outlineContainer, activeTab.outlineScroll.CurrentValue())

		// Page Details
		metadataTitle := guiState.CreatePane(guiState.CreateId(outlineContainer), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, guiState.Scale_padding(horusui.GuiPadding{Top: 0, Bottom: 0, Left: 7, Right: 7}), outlineContainer)
		guiState.SetText(metadataTitle, "Metadata", "", currentTheme.Sidebar.Panel.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Middle, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Clip, horusui.TextDirection_UseGlobal)
		_, metadataDivider := guiState.CreateHDivider(guiState.CreateId(outlineContainer), CheckboxColor, guiState.Scale_padding(horusui.GuiPadding{Bottom: 7, Top: 4.2}), outlineContainer)
		guiState.Hide(metadataTitle)
		guiState.Hide(metadataDivider)
		outlineTopPadding := float32(0)

		if activeTab.currentPageCache.Author != "" {
			str := fmt.Sprintf("Authored by %s", activeTab.currentPageCache.Author)
			author := guiState.CreatePane(guiState.CreateId(outlineContainer), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, guiState.Scale_padding(currentTheme.Sidebar.List.padding), outlineContainer)
			guiState.SetText(author, str, "", currentTheme.Sidebar.List.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Default, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			guiState.Show(metadataTitle)
			guiState.Show(metadataDivider)
			outlineTopPadding = 14
		}
		if (activeTab.currentPageCache.PublishDate != time.Time{}) {
			str := fmt.Sprintf("Published %s", activeTab.currentPageCache.PublishDate.Local().Format("Jan 2 2006 3:04 PM"))
			publishDate := guiState.CreatePane(guiState.CreateId(outlineContainer), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, guiState.Scale_padding(currentTheme.Sidebar.List.padding), outlineContainer)
			guiState.SetText(publishDate, str, "", currentTheme.Sidebar.List.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Default, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			guiState.Show(metadataTitle)
			guiState.Show(metadataDivider)
			outlineTopPadding = 14
		}
		if (activeTab.currentPageCache.ModificationDate != time.Time{}) {
			str := fmt.Sprintf("Modified %s", activeTab.currentPageCache.ModificationDate.Local().Format("Jan 2 2006 3:04 PM"))
			modificationDate := guiState.CreatePane(guiState.CreateId(outlineContainer), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, guiState.Scale_padding(currentTheme.Sidebar.List.padding), outlineContainer)
			guiState.SetText(modificationDate, str, "", currentTheme.Sidebar.List.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Default, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			guiState.Show(metadataTitle)
			guiState.Show(metadataDivider)
			outlineTopPadding = 14
		}
		if activeTab.currentPageCache.UDCClass != -1 {
			str := fmt.Sprintf("UDC Class %d", activeTab.currentPageCache.UDCClass)
			udcclass := guiState.CreatePane(guiState.CreateId(outlineContainer), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, guiState.Scale_padding(currentTheme.Sidebar.List.padding), outlineContainer)
			guiState.SetText(udcclass, str, "", currentTheme.Sidebar.List.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Default, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			guiState.Show(metadataTitle)
			guiState.Show(metadataDivider)
			outlineTopPadding = 14
		}

		outlineTitle := guiState.CreatePane(guiState.CreateId(outlineContainer), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, guiState.Scale_padding(horusui.GuiPadding{Top: outlineTopPadding, Bottom: 0, Left: 7, Right: 7}), outlineContainer)
		guiState.SetText(outlineTitle, "Outline", "", currentTheme.Sidebar.Panel.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Middle, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Clip, horusui.TextDirection_UseGlobal)
		guiState.CreateHDivider(guiState.CreateId(outlineContainer), CheckboxColor, guiState.Scale_padding(horusui.GuiPadding{Bottom: 7, Top: 4.2}), outlineContainer)

		if !activeTab.documentLoading {
			doResetCursor := false
			firstLevel1Heading := true
			guiState.GrowElementBacking(len(activeTab.currentPageCache.Headings))
			heading_i := 0
			for i, line := range activeTab.currentPageCache.Headings {
				guiState.SetLoopId(i)
				indentLevel := float32(line.Level - 2)
				if indentLevel < 0 {
					indentLevel = 0
				}
				row := guiState.CreateRow(guiState.CreateId(outlineContainer), horusui.SizeMode_Auto, 1, guiState.Scale_padding(horusui.GuiPadding{Top: currentTheme.Sidebar.List.padding.Top, Bottom: currentTheme.Sidebar.List.padding.Bottom, Left: currentTheme.Sidebar.List.padding.Left + (currentTheme.Sidebar.List.indentSize * indentLevel), Right: currentTheme.Sidebar.List.padding.Right}), outlineContainer)
				guiState.AutoAsMin(row, true)
				guiState.SetRounded(row, currentTheme.Sidebar.List.radius)
				font := state.arial15
				textColor := currentTheme.Sidebar.List.textColors[Selector_Default]
				if line.Level == 1 && firstLevel1Heading && line.Text != "/" && line.Text != ".." {
					font = state.bold_arial15
					firstLevel1Heading = false
				}
				if guiState.IsHoverExit(row) {
					doResetCursor = true
				}
				if guiState.IsHovered(row) {
					guiState.SetBackground(row, currentTheme.Sidebar.List.background[Selector_Hover])
					textColor = currentTheme.Sidebar.List.textColors[Selector_Hover]
					setCursor(state, sdl.SYSTEM_CURSOR_HAND)
					doResetCursor = false
				}

				if line.Text == "/" {
					line.Text = "Root"
				} else if line.Text == ".." {
					line.Text = "Up"
				}

				guiState.SetText(row, line.Text, "", textColor, font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Clip, horusui.TextDirection_UseGlobal)

				if line.Text == "Root" {
					if guiState.IsClicked(row, horusui.MouseButton_Left) {
						go rootPage(state, activeTab)
					}
					heading_i--
				} else if line.Text == "Up" {
					if guiState.IsClicked(row, horusui.MouseButton_Left) {
						go upPage(state, activeTab)
					}
					heading_i--
				} else {
					if guiState.IsClicked(row, horusui.MouseButton_Left) && activeTab.posCache != nil {
						activeTab.scrollAnimation.SetNewTotalMS(300, false)
						activeTab.scrollAnimation.SaveCurrentAndSetNewEnd(activeTab.posCache[heading_i].Y)
						activeTab.currentPageCache.Scroll = activeTab.posCache[heading_i].Y
					}
				}
				heading_i++
			}
			guiState.EndLoopId()
			if doResetCursor {
				resetCursor(state)
			}
		}

		if state.currentStream != nil {
			state.audioPlayerAnimation.Update(deltaTime * 1000)
			sizeType := horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}).SetPercent(horusui.FPoint{X: 1, Y: state.audioPlayerAnimation.CurrentValue()})
			musicPlayer := guiState.CreatePane(guiState.CreateId(sidebar), sizeType, horusui.GuiPanelDirection_Horizontal, guiState.Scale_padding(horusui.GuiPadding{Top: 14, Bottom: 14, Left: 7, Right: 7}), sidebar)
			guiState.SetSpacing(musicPlayer, horusui.GuiSpacing{X: horusui.Scale_value(guiState, float32(7)), Y: 0})
			guiState.SetBorder_sides(musicPlayer, horusui.GuiBorder{}, horusui.GuiBorder{}, horusui.GuiBorder{Thickness: 1, Color: horusui.Gray}, horusui.GuiBorder{Thickness: 1, Color: horusui.Gray})

			musicPlayer_container := guiState.CreatePane(guiState.CreateId(musicPlayer), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Pixel, horusui.FPoint{X: 1, Y: float32(state.arial15.Font.LineSkip()) * 2}), horusui.GuiPanelDirection_Vertical, horusui.GuiPadding{}, musicPlayer)
			musicPlayer_title := guiState.CreateRow(guiState.CreateId(sidebar), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, musicPlayer_container)
			guiState.SetText(musicPlayer_title, "Playing Music:", "", currentTheme.Sidebar.Panel.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Middle, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)

			var text string
			if state.currentStream.Err() != nil {
				text = fmt.Sprintf("Failed to stream: %s", state.currentStream.Err().Error()) // Click here to reload.
			} else {
				speaker.Lock()
				pos := state.currentStream.Position()
				speaker.Unlock()
				seconds := state.currentStreamFormat.SampleRate.D(pos).Round(time.Second)
				text = fmt.Sprintf("%d s / %d s", int(seconds.Seconds())-1, int(seconds.Seconds()))
			}

			musicPlayer_pos := guiState.CreateRow(guiState.CreateId(sidebar), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, musicPlayer_container)
			guiState.SetText(musicPlayer_pos, text, "", currentTheme.Sidebar.Panel.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Middle, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)

			musicPlayer_side := guiState.CreatePane(guiState.CreateId(musicPlayer), horusui.CreateSizeType(horusui.SizeMode_Auto, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, horusui.GuiPadding{}, musicPlayer)
			guiState.AutoAsMin(musicPlayer_side, true)
			musicPlayer_close := createButton(state, guiState.CreateId(sidebar), "X", musicPlayer_side, true, true, state.tinyFont, currentTheme.Sidebar.SectionTheme)
			if guiState.IsClicked(musicPlayer_close, horusui.MouseButton_Left) {
				state.audioPlayerAnimation.Hide()
				go func() {
					// Give audio player animation a chance to run before closing the audio.
					time.Sleep(time.Millisecond * time.Duration(state.audioPlayerAnimation.TotalMS()+5))
					speaker.Lock()
					state.audioReader.Close()
					state.currentStream.Close()
					speaker.Unlock()
					speaker.Suspend()
					state.currentStream = nil
				}()
				//state.currentStreamVolume = effects.Volume{}
			}
		}

		if state.videoPlaying != nil {
			state.videoPlayerAnimation.Update(deltaTime * 1000)
			sizeType := horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}).SetPercent(horusui.FPoint{X: 1, Y: state.videoPlayerAnimation.CurrentValue()})
			videoPlayer := guiState.CreatePane(guiState.CreateId(sidebar), sizeType, horusui.GuiPanelDirection_Horizontal, guiState.Scale_padding(horusui.GuiPadding{Top: 14, Bottom: 14, Left: 7, Right: 7}), sidebar)
			guiState.SetSpacing(videoPlayer, horusui.GuiSpacing{X: horusui.Scale_value(guiState, float32(7)), Y: 0})
			guiState.SetBorder_sides(videoPlayer, horusui.GuiBorder{}, horusui.GuiBorder{}, horusui.GuiBorder{Thickness: 1, Color: horusui.Gray}, horusui.GuiBorder{Thickness: 1, Color: horusui.Gray})

			videoPlayer_container := guiState.CreatePane(guiState.CreateId(videoPlayer), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Pixel, horusui.FPoint{X: 1, Y: float32(state.arial15.Font.LineSkip()) * 2}), horusui.GuiPanelDirection_Vertical, horusui.GuiPadding{}, videoPlayer)
			guiState.SetText(videoPlayer_container, "Streaming Video", "", currentTheme.Sidebar.Panel.textColors[Selector_Default], state.arial15, horusui.TextAlignment_Middle, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)

			videolayer_side := guiState.CreatePane(guiState.CreateId(videoPlayer), horusui.CreateSizeType(horusui.SizeMode_Auto, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, horusui.GuiPadding{}, videoPlayer)
			guiState.AutoAsMin(videolayer_side, true)
			videoPlayer_close := createButton(state, guiState.CreateId(sidebar), "X", videolayer_side, true, true, state.tinyFont, currentTheme.Sidebar.SectionTheme)
			if guiState.IsClicked(videoPlayer_close, horusui.MouseButton_Left) {
				state.videoPlayerAnimation.Hide()
				go func() {
					// Give video player animation a chance to run before closing the video.
					time.Sleep(time.Millisecond * time.Duration(state.videoPlayerAnimation.TotalMS()+5))
					state.videoPlaying.Close()
					state.videoPlaying = nil
				}()
			}
		}

		helpBtn := createButton(state, guiState.CreateId(sidebar), "Help", sidebar, false, true, state.arial15, currentTheme.Sidebar.SectionTheme)
		if guiState.IsClicked(helpBtn, horusui.MouseButton_Left) {
			//toggleSettingsbar(state)
			//fmt.Printf("Button clicked.\n")
			go getPage(state, activeTab, "about:help", false, "")
		}
	}

	//animateElementWithAnimationSet(deltaTime*1000, &state.sidebarAnimations, sidebar)
}

func documentPage(state *State, parent horusui.GuiElementId, deltaTime float64, activeTab *Tab) {
	guiState := state.guiState
	page_width := horusui.Scale_value(guiState, float32(currentTheme.Page.MaxWidth))
	if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_Nex {
		space_metrics, _ := state.documentFonts[ScrollLineType_Preformat].Font.GlyphMetrics(' ')
		page_width = float32(space_metrics.Advance * currentTheme.Page.NexCharWidth)
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_InputPrompt || activeTab.currentPageCache.DocType == DocumentType_InputPrompt_Sensitive {
		space_metrics, _ := state.documentFonts[ScrollLineType_Preformat].Font.GlyphMetrics(' ')
		page_width = float32(space_metrics.Advance * currentTheme.Page.NexCharWidth)
	} else if !activeTab.documentLoading && (activeTab.currentPageCache.DocType == DocumentType_Image || activeTab.currentPageCache.DocType == DocumentType_PNG || activeTab.currentPageCache.DocType == DocumentType_GIF || activeTab.currentPageCache.DocType == DocumentType_WebP || activeTab.currentPageCache.DocType == DocumentType_JXL || activeTab.currentPageCache.DocType == DocumentType_AVIF) {
		page_width = horusui.Scale_value(guiState, float32(currentTheme.Page.MaxImageWidth))
	}

	// Center the document inside a container
	container := guiState.CreatePane(guiState.CreateId(parent), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Fill, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Horizontal, horusui.GuiPadding{}, parent)
	guiState.Element(guiState.CreateId(parent), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Fill, horusui.FPoint{X: 1, Y: 1}).SetPercent(horusui.FPoint{X: currentTheme.Page.Center, Y: 1}), horusui.GuiPanelDirection_Vertical, container)
	document := guiState.Element(guiState.CreateId(parent), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Fill, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, container)
	guiState.SetPadding(document, guiState.Scale_padding(currentTheme.Page.Panel.padding))
	guiState.Element(guiState.CreateId(parent), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Fill, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Vertical, container)

	if activeTab.currentPageCache.DocType != DocumentType_CSV {
		guiState.SetSizeWidth(document, horusui.SizeMode_Pixel, page_width)
		guiState.SetMaxSize(document, horusui.FPoint{X: page_width, Y: 900000})
	}
	guiState.SetSpacing(document, currentTheme.Page.Panel.spacing)

	//guiState.GetElement(document).ScrollElement_StartIndex = &activeTab.ScrollOffset_StartIndex
	//guiState.GetElement(document).ScrollElement_StopIndex = &activeTab.ScrollOffset_StopIndex
	//fmt.Printf("%v; %v\n", activeTab.ScrollOffset_StartIndex, activeTab.ScrollOffset_StopIndex)
	activeTab.scrollAnimation.Update(deltaTime * 1000)
	if guiState.IsScrolled(parent) {
		activeTab.currentPageCache.Scroll -= float32(guiState.GetScroll()) * 2.75 * float32(state.documentFonts[ScrollLineType_Body].Font.LineSkip()) /*horusui.Scale_value(guiState, float32(37))*/
		if activeTab.currentPageCache.Scroll < 0 {
			activeTab.currentPageCache.Scroll = 0
		}
		activeTab.scrollAnimation.SaveCurrentAndSetNewEnd(activeTab.currentPageCache.Scroll)
	}
	guiState.SetScrollPixelOffset(document, activeTab.scrollAnimation.CurrentValue())
	//guiState.SetScrollPixelOffset(document, activeTab.currentPageCache.Scroll)
	if guiState.IsClicked(parent, horusui.MouseButton_Back) {
		go backPage(state, activeTab)
	}
	if guiState.IsClicked(parent, horusui.MouseButton_Forward) {
		go forwardPage(state, activeTab)
	}

	// Document padding
	guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Pixel, horusui.Scale_value(guiState, float32(15)), horusui.GuiPadding{}, document)

	if !activeTab.documentLoading && (activeTab.currentPageCache.DocType == DocumentType_PlainText || activeTab.currentPageCache.DocType == DocumentType_Nex || activeTab.currentPageCache.DocType == DocumentType_Scroll || activeTab.currentPageCache.DocType == DocumentType_Error || activeTab.currentPageCache.DocType == DocumentType_InputPrompt || activeTab.currentPageCache.DocType == DocumentType_InputPrompt_Sensitive || activeTab.currentPageCache.DocType == DocumentType_Video || activeTab.currentPageCache.DocType == DocumentType_Audio || activeTab.currentPageCache.DocType == DocumentType_OtherBinary) {
		scrollDocument(state, document, activeTab)
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_BuiltinInteractive {
		if activeTab.currentPageCache.Url.String() == "about:theme-builder" {
			themeBuilderDocument(state, document, activeTab)
		}
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_GIF {
		if activeTab.documentGifPlayer != nil {
			activeTab.documentGifPlayer.Update(float32(deltaTime))
		}
		guiState.SetCustomRender(document, func(renderer *sdl.Renderer, element *horusui.GuiElement, bounds sdl.Rect, innerBounds horusui.Rect[float32]) {
			if activeTab.documentGifPlayer != nil && !activeTab.documentLoading {
				t := activeTab.documentGifPlayer.Texture()
				t.SetBlendMode(sdl.BLENDMODE_BLEND)
				frame_width, frame_height := activeTab.documentGifPlayer.Animation.Width, activeTab.documentGifPlayer.Animation.Height

				aspectRatio := float32(frame_height) / float32(frame_width)
				width := int32(float64(frame_width) * (float64(innerBounds.W) / float64(frame_width)))
				height := int32(float32(width) * aspectRatio)
				state.renderer.Copy(t, nil, &sdl.Rect{X: int32(innerBounds.X), Y: int32(innerBounds.Y), W: width, H: height})
			}
		})
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_PNG {
		if activeTab.documentAPNGPlayer != nil {
			activeTab.documentAPNGPlayer.Update(float32(deltaTime))
		}
		guiState.SetCustomRender(document, func(renderer *sdl.Renderer, element *horusui.GuiElement, bounds sdl.Rect, innerBounds horusui.Rect[float32]) {
			if activeTab.documentAPNGPlayer != nil && !activeTab.documentLoading {
				t := activeTab.documentAPNGPlayer.Texture()
				t.SetBlendMode(sdl.BLENDMODE_BLEND)
				frame_width, frame_height := activeTab.documentAPNGPlayer.Animation.Width, activeTab.documentAPNGPlayer.Animation.Height

				aspectRatio := float32(frame_height) / float32(frame_width)
				width := int32(float64(frame_width) * (float64(innerBounds.W) / float64(frame_width)))
				height := int32(float32(width) * aspectRatio)
				state.renderer.Copy(t, nil, &sdl.Rect{X: int32(innerBounds.X), Y: int32(innerBounds.Y), W: width, H: height})
			}
		})
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_JXL {
		if activeTab.documentJxlPlayer != nil {
			activeTab.documentJxlPlayer.Update(float32(deltaTime))
		}
		guiState.SetCustomRender(document, func(renderer *sdl.Renderer, element *horusui.GuiElement, bounds sdl.Rect, innerBounds horusui.Rect[float32]) {
			if activeTab.documentJxlPlayer != nil && !activeTab.documentLoading {
				t := activeTab.documentJxlPlayer.Texture()
				t.SetBlendMode(sdl.BLENDMODE_BLEND)
				frame_width, frame_height := activeTab.documentJxlPlayer.Animation.Width, activeTab.documentJxlPlayer.Animation.Height

				aspectRatio := float32(frame_height) / float32(frame_width)
				width := int32(float64(frame_width) * (float64(innerBounds.W) / float64(frame_width)))
				height := int32(float32(width) * aspectRatio)
				state.renderer.Copy(t, nil, &sdl.Rect{X: int32(innerBounds.X), Y: int32(innerBounds.Y), W: width, H: height})
			}
		})
		/*} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_WebP {
		if activeTab.documentWebPPlayer != nil {
			activeTab.documentWebPPlayer.Update(float32(deltaTime))
		}
		guiState.SetCustomRender(document, func(renderer *sdl.Renderer, element *horusui.GuiElement, bounds sdl.Rect, innerBounds horusui.Rect[float32]) {
			if activeTab.documentWebPPlayer != nil && !activeTab.documentLoading {
				t := activeTab.documentWebPPlayer.Texture()
				t.SetBlendMode(sdl.BLENDMODE_BLEND)
				frame_width, frame_height := activeTab.documentWebPPlayer.Animation.Width, activeTab.documentWebPPlayer.Animation.Height

				aspectRatio := float32(frame_height) / float32(frame_width)
				width := int32(float64(frame_width) * (float64(innerBounds.W) / float64(frame_width)))
				height := int32(float32(width) * aspectRatio)
				state.renderer.Copy(t, nil, &sdl.Rect{X: int32(innerBounds.X), Y: int32(innerBounds.Y), W: width, H: height})
			}
		})*/
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_AVIF {
		if activeTab.documentAVIFPlayer != nil {
			activeTab.documentAVIFPlayer.Update(float32(deltaTime))
		}
		guiState.SetCustomRender(document, func(renderer *sdl.Renderer, element *horusui.GuiElement, bounds sdl.Rect, innerBounds horusui.Rect[float32]) {
			if activeTab.documentAVIFPlayer != nil && !activeTab.documentLoading {
				t := activeTab.documentAVIFPlayer.Texture()
				t.SetBlendMode(sdl.BLENDMODE_BLEND)
				frame_width, frame_height := activeTab.documentAVIFPlayer.Animation.Width, activeTab.documentAVIFPlayer.Animation.Height

				aspectRatio := float32(frame_height) / float32(frame_width)
				width := int32(float64(frame_width) * (float64(innerBounds.W) / float64(frame_width)))
				height := int32(float32(width) * aspectRatio)
				state.renderer.Copy(t, nil, &sdl.Rect{X: int32(innerBounds.X), Y: int32(innerBounds.Y), W: width, H: height})
			}
		})
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_Image {
		guiState.SetCustomRender(document, func(renderer *sdl.Renderer, element *horusui.GuiElement, bounds sdl.Rect, innerBounds horusui.Rect[float32]) {
			if activeTab.documentImgSurface != nil && !activeTab.documentLoading {
				t, err := state.renderer.CreateTextureFromSurface(activeTab.documentImgSurface)
				t.SetBlendMode(sdl.BLENDMODE_BLEND)
				if err != nil {
					return
				}

				aspectRatio := float32(activeTab.documentImgSurface.H) / float32(activeTab.documentImgSurface.W)
				width := int32(float64(activeTab.documentImgSurface.W) * (float64(innerBounds.W) / float64(activeTab.documentImgSurface.W)))
				height := int32(float32(width) * aspectRatio)
				state.renderer.Copy(t, nil, &sdl.Rect{X: int32(innerBounds.X), Y: int32(innerBounds.Y), W: width, H: height})
				t.Destroy()
			}
		})
	} else if !activeTab.documentLoading && activeTab.currentPageCache.DocType == DocumentType_CSV {
		csvDocument(state, document, activeTab)
	}
}

func scrollDocument(state *State, document horusui.GuiElementId, activeTab *Tab) {
	guiState := state.guiState
	doResetCursor := false
	prevQuote := false
	var prevQuoteRow horusui.GuiElementId
	prevEmptyLine := false

	if activeTab.posCache == nil {
		guiState.CalculateEverything()
	} else if activeTab.firstFrame {
		guiState.CalculateEverything()
		activeTab.firstFrame = false
	}

	guiState.GrowElementBacking(len(activeTab.currentPageCache.Lines))
	heading_i := 0
	for i, line := range activeTab.currentPageCache.Lines {
		if _, currentIsQuote := line.(scroll.ScrollLine_Quote); prevQuote && !currentIsQuote {
			guiState.GetElement(prevQuoteRow).Padding.Bottom = horusui.Scale_value(guiState, currentTheme.Page.Document[ScrollLineType_Quote].padding.Bottom)
		}

		guiState.SetLoopId(i)

		// Add empty line before headings if it's not already there
		if _, ok := line.(scroll.ScrollLine_Heading); ok && !prevEmptyLine {
			font := state.documentFonts[ScrollLineType_Body]
			if activeTab.currentPageCache.DocType == DocumentType_PlainText || activeTab.currentPageCache.DocType == DocumentType_Nex {
				font = state.documentFonts[ScrollLineType_Preformat]
			}
			row := guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, document)
			guiState.SetText(row, " ", "", horusui.Black, font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			guiState.AutoAsMin(row, true)
		}

		row := guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, document)
		guiState.AutoAsMin(row, true)
		font := state.documentFonts[ScrollLineType_Body]

		switch line := line.(type) {
		case scroll.ScrollLine_Heading:
			var color horusui.Color
			topPadding := float32(1.5)
			bottomPadding := float32(0)
			if line.Level == 1 {
				font = state.documentFonts[ScrollLineType_Heading1]
				color = currentTheme.Page.Document[ScrollLineType_Heading1].textColors[Selector_Default]
			} else if line.Level == 2 {
				topPadding = 14
				font = state.documentFonts[ScrollLineType_Heading2]
				color = currentTheme.Page.Document[ScrollLineType_Heading2].textColors[Selector_Default]
			} else if line.Level == 3 {
				font = state.documentFonts[ScrollLineType_Heading3]
				color = currentTheme.Page.Document[ScrollLineType_Heading3].textColors[Selector_Default]
			} else if line.Level == 4 || line.Level == 5 {
				font = state.documentFonts[ScrollLineType_Heading4]
				color = currentTheme.Page.Document[ScrollLineType_Heading4].textColors[Selector_Default]
				bottomPadding = 4.2
			}
			guiState.SetPadding(row, guiState.Scale_padding(horusui.GuiPadding{Top: topPadding, Bottom: bottomPadding, Left: 0, Right: 0}))
			guiState.SetText(row, line.Text, "", color, font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)

			if activeTab.posCache != nil && heading_i < len(activeTab.posCache) {
				guiState.SetPosCache(row, &activeTab.posCache[heading_i])
			}

			heading_i++
			prevQuote = false
			prevEmptyLine = false
		case scroll.ScrollLine_Text:
			font = state.documentFonts[ScrollLineType_Body]
			if activeTab.currentPageCache.DocType == DocumentType_PlainText || activeTab.currentPageCache.DocType == DocumentType_Nex {
				font = state.documentFonts[ScrollLineType_Preformat]
			}
			text := string(line)
			if strings.TrimSpace(text) == "" {
				text = " "
				prevEmptyLine = true
			} else {
				prevEmptyLine = false
			}
			guiState.SetText(row, text, "", currentTheme.Page.Document[ScrollLineType_Body].textColors[Selector_Default], font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			prevQuote = false
		case scroll.ScrollLine_Quote:
			font = state.documentFonts[ScrollLineType_Quote]
			text := line.Text
			if text == "" {
				text = " "
			}

			var topPadding float32 = 0
			if !prevQuote {
				topPadding = horusui.Scale_value(guiState, currentTheme.Page.Document[ScrollLineType_Quote].padding.Top)
			}
			guiState.SetPadding(row, guiState.Scale_padding(horusui.GuiPadding{Top: topPadding /*horusui.Scale_value(guiState, float32(5))*/, Bottom: 0 /*horusui.Scale_value(guiState, float32(5))*/, Left: currentTheme.Page.Document[ScrollLineType_Quote].padding.Left + (currentTheme.Page.Document[ScrollLineType_Quote].indentSize * (float32(line.Level))), Right: 0}))
			guiState.SetBorder_sides(row, currentTheme.Page.Document[ScrollLineType_Quote].border[Selector_Default], horusui.GuiBorder{}, horusui.GuiBorder{}, horusui.GuiBorder{})
			prevQuoteRow = row

			for j := 0; j < line.Level; j++ {
				guiState.SetLoopId(i, j)
				inner := j == line.Level-1
				padding := guiState.Scale_padding(horusui.GuiPadding{Top: 0, Bottom: 0, Left: currentTheme.Page.Document[ScrollLineType_Quote].padding.Left + (currentTheme.Page.Document[ScrollLineType_Quote].indentSize * float32(j)) + 1, Right: 0})
				if inner {
					padding = horusui.GuiPadding{}
				} else {
					guiState.SetBorder_sides(row, currentTheme.Page.Document[ScrollLineType_Quote].border[Selector_Default], horusui.GuiBorder{}, horusui.GuiBorder{}, horusui.GuiBorder{})
				}
				row = guiState.CreateRow(guiState.CreateId(row), horusui.SizeMode_Auto, 1, padding, row)
				guiState.AutoAsMin(row, true)
			}
			guiState.SetLoopId(i)
			guiState.SetText(row, text, "", currentTheme.Page.Document[ScrollLineType_Quote].textColors[Selector_Default], font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)

			prevQuote = true
			prevEmptyLine = false
		case scroll.ScrollLine_Link:
			if line.SpartanInput {
				if builder, ok := activeTab.spartanLineInputs[line.Url]; ok {
					guiState.SetPadding(row, guiState.Scale_padding(horusui.GuiPadding{Top: 7, Bottom: 7}))
					createTextbox_custom(state, guiState.CreateId(document), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}).SetMax(horusui.FPoint{X: horusui.Scale_value(guiState, float32(280)), Y: 0}), line.Title, state.documentFonts[ScrollLineType_Body], builder, row, currentTheme.Page.SectionTheme)
				}
			} else {
				guiState.SetPadding(row, guiState.Scale_padding(horusui.GuiPadding{Top: horusui.Scale_value(guiState, float32(1.4)), Bottom: horusui.Scale_value(guiState, float32(1.4)), Left: 0, Right: 0}))
				font = state.documentFonts[ScrollLineType_Link]
				var color = currentTheme.Page.Document[ScrollLineType_Link].textColors[Selector_Default]
				text := ">  " + line.Title
				if line.Title == "" {
					text = ">  " + line.Url // ➤
				}

				var link = row
				if activeTab.currentPageCache.DocType == DocumentType_Nex {
					guiState.SetSpacing(row, guiState.Scale_spacing(currentTheme.Page.Document[ScrollLineType_Link_Nex].spacing))
					font = state.documentFonts[ScrollLineType_Link_Nex]
					color = currentTheme.Page.Document[ScrollLineType_Link_Nex].textColors[Selector_Default]
					link = guiState.CreateAutoRow(guiState.CreateId(row), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, row)
					text = "=> " + line.Url

					if line.Title != "" {
						title := guiState.CreateAutoRow(guiState.CreateId(row), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, row)
						//guiState.AutoAsMin(title, true)
						guiState.SetText(title, line.Title, "", currentTheme.Page.Document[ScrollLineType_Body].textColors[Selector_Default], state.documentFonts[ScrollLineType_Preformat], horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
					}
				}

				if guiState.IsHoverExit(link) {
					doResetCursor = true
				}
				if guiState.IsHovered(link) {
					setCursor(state, sdl.SYSTEM_CURSOR_HAND)
					color = currentTheme.Page.Document[ScrollLineType_Link].textColors[Selector_Hover]
					doResetCursor = false
				}
				if guiState.IsClicked(link, horusui.MouseButton_Left) && !state.ctrl {
					go getPage(state, activeTab, line.Url, false, "")
				}
				if guiState.IsClicked(link, horusui.MouseButton_Middle) || (guiState.IsClicked(link, horusui.MouseButton_Left) && state.ctrl) {
					go func(setActive bool) {
						tab := state.NewTabWithPage(line.Url)
						if setActive {
							state.setActiveTab(tab)
						}
					}(state.shift)

				}
				guiState.SetText(link, text, "", color, font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			}
			prevQuote = false
			prevEmptyLine = false
		case scroll.ScrollLine_OrderedListItem:
			font = state.documentFonts[ScrollLineType_Body]
			text := line.Label + ". " + line.Text
			if text == "" {
				text = " "
			}
			guiState.SetPadding(row, guiState.Scale_padding(horusui.GuiPadding{Top: 0, Bottom: 0, Left: currentTheme.Page.Document[ScrollLineType_ListItem].indentSize * (float32(line.Level) - 1), Right: 0}))
			guiState.SetText(row, text, "", currentTheme.Page.Document[ScrollLineType_ListItem].textColors[Selector_Default], font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			prevQuote = false
		case scroll.ScrollLine_UnorderedListItem:
			font = state.documentFonts[ScrollLineType_Body]
			text := "• " + line.Text
			if text == "" {
				text = " "
			}
			guiState.SetPadding(row, guiState.Scale_padding(horusui.GuiPadding{Top: 0, Bottom: 0, Left: currentTheme.Page.Document[ScrollLineType_ListItem].indentSize * (float32(line.Level) - 1), Right: 0}))
			guiState.SetText(row, text, "", currentTheme.Page.Document[ScrollLineType_ListItem].textColors[Selector_Default], font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			prevQuote = false
			prevEmptyLine = false
		case scroll.ScrollLine_PreformattedText:
			font = state.documentFonts[ScrollLineType_Preformat]
			text := string(line)
			if text == "" {
				text = " "
			}
			guiState.SetText(row, text, "", currentTheme.Page.Document[ScrollLineType_Preformat].textColors[Selector_Default], font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			prevQuote = false
			prevEmptyLine = false
		case scroll.ScrollLine_PreformattedToggle:
			font = state.documentFonts[ScrollLineType_Preformat]
			text := "```" + string(line)
			guiState.SetText(row, text, "", currentTheme.Page.Document[ScrollLineType_Preformat].textColors[Selector_Default], font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			prevQuote = false
			prevEmptyLine = false
		case scroll.ScrollLine_ThematicBreak:
			guiState.CreateHDivider(guiState.CreateId(row), currentTheme.Page.Document[ScrollLineType_Body].textColors[Selector_Default], guiState.Scale_padding(horusui.GuiPadding{Top: 21, Bottom: 21, Left: 35, Right: 35}), row)
			prevEmptyLine = false
		}
	}
	guiState.EndLoopId()
	if doResetCursor {
		resetCursor(state)
	}

	// Add Input textbox for input prompts
	if activeTab.currentPageCache.DocType == DocumentType_InputPrompt {
		row := guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Auto, 1, guiState.Scale_padding(horusui.GuiPadding{Top: 7, Bottom: 7}), document)
		guiState.AutoAsMin(row, true)
		createTextbox_custom(state, guiState.CreateId(document), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), "Input", state.documentFonts[ScrollLineType_Body], &activeTab.documentInput, document, currentTheme.Page.SectionTheme)
	} else if activeTab.currentPageCache.DocType == DocumentType_InputPrompt_Sensitive {
		row := guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Auto, 1, guiState.Scale_padding(horusui.GuiPadding{Top: 7, Bottom: 7}), document)
		guiState.AutoAsMin(row, true)
		createTextbox_custom(state, guiState.CreateId(document), horusui.CreateSizeType(horusui.SizeMode_Fill, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}), "Input", state.documentFonts[ScrollLineType_Body], &activeTab.documentInput, document, currentTheme.Page.SectionTheme)
	}
}

func csvDocument(state *State, document horusui.GuiElementId, activeTab *Tab) {
	guiState := state.guiState
	for i, row := range activeTab.currentPageCache.CSV {
		guiState.SetLoopId(i)
		rowElem := guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, document)
		guiState.SetBorder_sides(rowElem, horusui.GuiBorder{}, horusui.GuiBorder{Thickness: 1, Color: horusui.Gray}, horusui.GuiBorder{}, horusui.GuiBorder{Thickness: 1, Color: horusui.Gray})
		guiState.AutoAsMin(rowElem, true)
		font := state.documentFonts[ScrollLineType_Body]
		color := state.documentColors[ScrollLineType_Body]

		for j := 0; j < activeTab.currentPageCache.CSVWidth; j++ {
			guiState.SetLoopId(i, j)
			var col string
			if j >= len(row) {
				col = ""
			} else {
				col = row[j]
			}

			colElem := guiState.CreatePane(guiState.CreateId(document), horusui.CreateSizeType(horusui.SizeMode_Flex, horusui.SizeMode_Pixel, horusui.FPoint{X: 1, Y: float32(font.Font.LineSkip()*2) + horusui.Scale_value(guiState, float32(14))}), horusui.GuiPanelDirection_Horizontal, guiState.Scale_padding(horusui.GuiPadding{Top: 7, Bottom: 7, Left: 7, Right: 7}), rowElem)
			//colElem := guiState.CreateAutoColumn(guiState.CreateId(document), horusui.SizeMode_Flex, 1, horusui.GuiPadding{}, rowElem)
			guiState.SetText(colElem, col, "", color, font, horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
			guiState.SetBorder_sides(colElem, horusui.GuiBorder{Thickness: 1, Color: horusui.Gray}, horusui.GuiBorder{}, horusui.GuiBorder{}, horusui.GuiBorder{})
			guiState.AutoAsMin(colElem, true)
		}
	}
	guiState.EndLoopId()
}

func themeBuilderDocument(state *State, document horusui.GuiElementId, activeTab *Tab) {
	guiState := state.guiState
	// bodyFont := state.documentFonts[ScrollLineType_Body]

	// Page Title
	pageTitle := guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Auto, 1, guiState.Scale_padding(horusui.GuiPadding{Top: 1.5, Bottom: 0, Left: 0, Right: 0}), document)
	guiState.AutoAsMin(pageTitle, true)
	guiState.SetText(pageTitle, "Theme Builder", "", currentTheme.Page.Document[ScrollLineType_Heading1].textColors[Selector_Default], state.documentFonts[ScrollLineType_Heading1], horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)

	bodyText := guiState.CreateRow(guiState.CreateId(document), horusui.SizeMode_Auto, 1, horusui.GuiPadding{}, document)
	guiState.AutoAsMin(bodyText, true)
	guiState.SetText(bodyText, "Coming Soon.", "", currentTheme.Page.Document[ScrollLineType_Body].textColors[Selector_Default], state.documentFonts[ScrollLineType_Body], horusui.TextAlignment_Default, horusui.TextAlignment_Default, horusui.GuiTextOverflow_Wrap, horusui.TextDirection_UseGlobal)
}
