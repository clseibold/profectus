package main

import (
	"image/color"

	"github.com/gen2brain/avif"
	"github.com/veandco/go-sdl2/sdl"
)

type AVIFAnimation struct {
	Data     *avif.AVIF
	Frames   []*sdl.Surface
	Delays   []float32 // In Seconds
	frameImg *sdl.Surface
	Width    float32
	Height   float32
}

func NewAVIFAnimation(data *avif.AVIF) *AVIFAnimation {
	surf, _ := sdl.CreateRGBSurfaceWithFormat(0, int32(data.Image[0].Bounds().Dx()), int32(data.Image[0].Bounds().Dy()), 64, uint32(sdl.PIXELFORMAT_RGBA32))
	surf.SetBlendMode(sdl.BLENDMODE_BLEND)

	anim := &AVIFAnimation{
		Data:     data,
		frameImg: surf,
		Width:    float32(data.Image[0].Bounds().Dx()),
		Height:   float32(data.Image[0].Bounds().Dy()),
	}
	anim.Load()
	return anim
}

// Load loads the frames of the GIF animation.
func (avifAnim *AVIFAnimation) Load() {
	defer avifAnim.frameImg.Free()
	empty := color.RGBA{0, 0, 0, 0}

	for y := 0; y < avifAnim.frameImg.Bounds().Dy(); y++ {
		for x := 0; x < avifAnim.frameImg.Bounds().Dx(); x++ {
			avifAnim.frameImg.Set(x, y, empty)
		}
	}

	for index, img := range avifAnim.Data.Image {
		for y := 0; y < img.Bounds().Dy(); y++ {
			for x := 0; x < img.Bounds().Dx(); x++ {
				color := img.At(x, y)
				avifAnim.frameImg.Set(x, y, color)
			}
		}

		newSurf, _ := avifAnim.frameImg.Duplicate()
		avifAnim.Frames = append(avifAnim.Frames, newSurf)

		delay := float32(avifAnim.Data.Delay[index])

		/*if delay <= 0 {
		delay = 0.1
		}*/

		avifAnim.Delays = append(avifAnim.Delays, delay)
	}
}

func (avifAnim *AVIFAnimation) Destroy() {
	for _, frame := range avifAnim.Frames {
		if frame != nil {
			frame.Free()
		}
	}
}

type AVIFPlayer struct {
	Animation    *AVIFAnimation
	CurrentFrame int
	Timer        float32
	Frames       []*sdl.Texture
	renderer     *sdl.Renderer
}

func NewAVIFPlayer(renderer *sdl.Renderer, avifAnim *AVIFAnimation) *AVIFPlayer {
	return &AVIFPlayer{
		Animation: avifAnim,
		renderer:  renderer,
	}
}

func (avifPlayer *AVIFPlayer) Update(dt float32) {
	avifPlayer.Timer += dt

	for avifPlayer.Timer >= avifPlayer.Animation.Delays[avifPlayer.CurrentFrame] {
		avifPlayer.Timer -= avifPlayer.Animation.Delays[avifPlayer.CurrentFrame]
		avifPlayer.CurrentFrame++
		if avifPlayer.CurrentFrame >= len(avifPlayer.Animation.Frames) {
			avifPlayer.CurrentFrame = 0
		}
	}
}

func (avifPlayer *AVIFPlayer) Destroy(state *State) {
	avifPlayer.Animation.Destroy()
	for _, frame := range avifPlayer.Frames {
		if frame != nil {
			state.destroyer <- frame
		}
	}
}

func (avifPlayer *AVIFPlayer) Texture() *sdl.Texture {
	for len(avifPlayer.Frames) <= avifPlayer.CurrentFrame {
		surface := avifPlayer.Animation.Frames[avifPlayer.CurrentFrame]
		surface.SetBlendMode(sdl.BLENDMODE_BLEND)
		frame, _ := avifPlayer.renderer.CreateTextureFromSurface(surface)
		frame.SetBlendMode(sdl.BLENDMODE_BLEND)
		avifPlayer.Frames = append(avifPlayer.Frames, frame)

		avifPlayer.Animation.Frames[avifPlayer.CurrentFrame] = nil
		surface.Free()
	}
	return avifPlayer.Frames[avifPlayer.CurrentFrame]
}
