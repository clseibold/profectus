// Source: https://github.com/makew0rld/go-gemini/blob/master/client.go
// Copyright (c) 2020, makeworld - See License info in LICENSE-CLIENT
// Copyright (c) 2023, Christian Lee Seibold - Licensed BSD-3-Clause, as per LICENSE file
//
// 9-28-2023 Christian Lee Seibold - Made major changes to turn this client into a misfin client.
//

package nex_client

import (
	"fmt"
	"io"
	"net"
	"net/url"
	"time"

	"golang.org/x/net/idna"
)

var URLMaxLength int = 1024

//var MetaMaxLength int = URLMaxLength - 3

func punycodeHost(host string) (string, error) {
	hostname, port, err := net.SplitHostPort(host)
	if err != nil {
		// Likely means no port
		hostname = host
		port = ""
	}

	if net.ParseIP(hostname) != nil {
		// Hostname is IP address, not domain
		return host, nil
	}
	pc, err := idna.ToASCII(hostname)
	if err != nil {
		return host, err
	}
	if port == "" {
		return pc, nil
	}
	return net.JoinHostPort(pc, port), nil
}

func punycodeHostFromURL(u string) (string, error) {
	parsed, err := url.Parse(u)
	if err != nil {
		return "", err
	}
	return punycodeHost(parsed.Host)
}

// GetPunycodeURL takes a full URL that potentially has Unicode in the
// domain name, and returns a URL with the domain punycoded.
func GetPunycodeURL(u string) (string, error) {
	parsed, err := url.Parse(u)
	if err != nil {
		return "", nil
	}
	host, err := punycodeHostFromURL(u)
	if err != nil {
		return "", err
	}
	parsed.Host = host
	return parsed.String(), nil
}

// ProxyFunc. See Client documentation
type ProxyFunc func(dialer *net.Dialer, address string) (net.Conn, error)

type Client struct {
	// ConnectTimeout is equivalent to the Timeout field in net.Dialer.
	// It's the max amount of time allowed for the initial connection/handshake.
	// The timeout of the DefaultClient is 15 seconds.
	//
	// If ReadTimeout is not set, then this value is also used to time out on getting
	// the header after the connection is made.
	ConnectTimeout time.Duration

	// ReadTimeout is the max amount of time reading to a server can take.
	// This should not be set if you want to support streams.
	// It is equivalent to net.Conn.SetDeadline, see that func for more documentation.
	//
	// For example, if this is set to 30 seconds, then no more reading from the connection
	// can happen 30 seconds after the initial handshake.
	ReadTimeout time.Duration

	// Proxy is a function that returns an existing connection. The TLS client
	// will use this as the underlying transport, instead of making a direct TCP
	// connection.
	//
	// go-gemini requires setting a dialer on the underlying connection, to impose
	// a timeout on making the initial connection. This dialer is provided as an
	// argument to the proxy function.
	//
	// The other argument provided is the address being connected to. For example
	// "example.com:1900".
	//
	// Any errors returned will prevent a connection from occurring.
	//
	// This is not "gemini proxying", aka the proxying functionality built in to
	// the Gemini protocol. This is for proxying requests over TOR, or SOCKS5, etc.
	//
	//     func(dialer *net.Dialer, address string) (net.Conn, error)
	//
	Proxy ProxyFunc
}

var DefaultClient = &Client{ConnectTimeout: 15 * time.Second}

// getHost returns a full host for the given URL, always including a port.
// It also punycodes the host, in case it contains Unicode.
func getHost(parsedURL *url.URL) string {
	host, _ := punycodeHostFromURL(parsedURL.String())
	if parsedURL.Port() == "" {
		host = net.JoinHostPort(parsedURL.Hostname(), "1900")
	}
	return host
}

func (c *Client) Request(rawURL string) (net.Conn, error) {
	parsedURL, err := url.Parse(rawURL)
	if err != nil {
		return nil, fmt.Errorf("failed to parse URL: %w", err)
	}
	return c.RequestWithHost(getHost(parsedURL), rawURL)
}

// Sends a message to a misfin server at the given host, with the given URL.
// This can be used for misfin proxying, where the URL host and actual server don't match.
// It assumes the host is using port 1900 if no port number is provided.
func (c *Client) RequestWithHost(host, rawURL string) (net.Conn, error) {
	u, err := GetPunycodeURL(rawURL)
	if err != nil {
		return nil, fmt.Errorf("error when punycoding URL: %w", err)
	}
	parsedURL, _ := url.Parse(u)

	if len(u) > URLMaxLength {
		// Out of spec
		return nil, fmt.Errorf("url is too long")
	}

	// Add port to host if needed
	_, _, err = net.SplitHostPort(host)
	if err != nil {
		// Error likely means there's no port in the host
		host = net.JoinHostPort(host, "1900")
	}
	ogHost := host
	host, err = punycodeHost(host)
	if err != nil {
		return nil, fmt.Errorf("failed to punycode host %s: %w", ogHost, err)
	}

	// Connect
	start := time.Now()
	conn, err := c.connect(host, parsedURL)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to the server: %w", err)
	}

	// Send request
	if c.ReadTimeout == 0 && c.ConnectTimeout != 0 {
		// No r/w timeout, so a timeout for sending the request must be set
		conn.SetDeadline(start.Add(c.ConnectTimeout))
	}
	err = sendRequest(conn, parsedURL.Path)
	if err != nil {
		conn.Close()
		return nil, err
	}
	if c.ReadTimeout == 0 && c.ConnectTimeout != 0 {
		// Undo deadline
		conn.SetDeadline(time.Time{})
	}

	//conn.Close()
	return conn, nil
}

func Request(rawURL string) (net.Conn, error) {
	return DefaultClient.Request(rawURL)
}

// Sends a message to a misfin server at the given host, with the given URL.
// This can be used for misfin proxying, where the URL host and actual server don't match.
// It assumes the host is using port 1900 if no port number is provided.
func RequestWithHost(host, url string) (net.Conn, error) {
	return DefaultClient.RequestWithHost(host, url)
}

func (c *Client) connect(host string, parsedURL *url.URL) (net.Conn, error) {
	var conn net.Conn
	var err error
	dialer := &net.Dialer{Timeout: c.ConnectTimeout}
	conn, err = dialer.Dial("tcp", host)
	if err != nil {
		return conn, err
	}

	/*var conn *tls.Conn
	var err error
	if c.Proxy == nil {
		// Dialer timeout for handshake
		conn, err = tls.DialWithDialer(&net.Dialer{Timeout: c.ConnectTimeout}, "tcp", host, conf)
		res.conn = conn
		if err != nil {
			return conn, err
		}
	} else {
		// Use proxy
		proxyConn, err := c.Proxy(&net.Dialer{Timeout: c.ConnectTimeout}, host)
		if err != nil {
			return nil, err
		}
		conn = tls.Client(proxyConn, conf)
		// Make handshake manually to start connection, so later call to
		// conn.ConnectionState() works
		if err := conn.Handshake(); err != nil {
			return nil, err
		}
	}*/

	if c.ReadTimeout != 0 {
		conn.SetDeadline(time.Now().Add(c.ReadTimeout))
	}

	return conn, nil
}

func sendRequest(conn io.Writer, path string) error {
	_, err := fmt.Fprintf(conn, "%s\n", path)
	if err != nil {
		return fmt.Errorf("could not send request to the server: %w", err)
	}
	return nil
}
