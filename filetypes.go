package main

func IsSupportedAudio(mediatype string) bool {
	if mediatype == "audio/mpeg" || mediatype == "audio/mp3" || mediatype == "audio/x-mpeg" || mediatype == "audio/ogg" || mediatype == "audio/x-ogg" || mediatype == "application/ogg" || mediatype == "application/x-ogg" || mediatype == "audio/flac" || mediatype == "audio/x-flac" || mediatype == "audio/wav" || mediatype == "audio/wave" || mediatype == "audio/x-wav" || mediatype == "audio/vnd.wave" {
		return true
	}
	return false
}
