package main

import (
	"encoding/json"
	"fmt"
	"os"
)

var DefaultSettings = Settings{
	Homepage:  "scroll://scrollprotocol.us.to/",
	SearchURL: "scroll://auragem.ddns.net/search/s",
	ThemeName: "Dark",
}

type Settings struct {
	Homepage  string `json:"homepage"`
	SearchURL string `json:"search_url"`
	ThemeName string `json:"theme"`
}

// Loads a theme from a json file.
func LoadSettingsFromFile(filepath string) Settings {
	data, err := os.ReadFile(filepath)
	if err != nil {
		SaveSettingsToFile(filepath, DefaultSettings)
		return DefaultSettings
	}
	var settings Settings
	err = json.Unmarshal(data, &settings)
	if err != nil {
		fmt.Printf("Error loading theme '%s'.\n", filepath)
		return DefaultSettings
	}

	return settings
}

func SaveSettingsToFile(filepath string, settings Settings) {
	data, err := json.MarshalIndent(settings, "", "    ")
	if err != nil {
		panic(err)
	}
	os.WriteFile(filepath, data, 0600)
}
