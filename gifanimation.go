package main

import (
	"image/color"
	"image/gif"

	"github.com/veandco/go-sdl2/sdl"
)

// Taken from Masterplan for non-commercial use: https://github.com/SolarLune/masterplan/tree/sdl-rework
// Modified to not use a goroutine on GifAnimation.Load() and to remove the progress channel.
// License:
// MasterPlan is copyright, All Rights Reserved, SolarLune Games 2019-2021.
//
// Feel free to use the program itself and the generated plan files in the development of projects, commercial or otherwise, as that's the point of the tool, haha. You can also build MasterPlan yourself using the repo here, contribute to its development, and fork it freely, but you may not use any assets (graphics files, sound files, code, etc) from this repository in any commercial creations. Please also do not distribute builds of MasterPlan to others.
//
// Special thanks to raysan5 for creating Raylib, as it was pretty key to the development of MasterPlan, and works rather well! Also thanks to the SDL development team, and the go-sdl2 maintenance team!

type GifAnimation struct {
	Data     *gif.GIF
	Frames   []*sdl.Surface
	Delays   []float32 // In Seconds
	frameImg *sdl.Surface
	Width    float32
	Height   float32
}

func NewGifAnimation(data *gif.GIF) *GifAnimation {
	surf, _ := sdl.CreateRGBSurfaceWithFormat(0, int32(data.Image[0].Rect.Dx()), int32(data.Image[0].Rect.Dy()), 32, uint32(sdl.PIXELFORMAT_RGBA8888))

	anim := &GifAnimation{
		Data:     data,
		frameImg: surf,
		Width:    float32(data.Image[0].Rect.Dx()),
		Height:   float32(data.Image[0].Rect.Dy()),
	}
	anim.Load()
	return anim
}

// Load loads the frames of the GIF animation.
func (gifAnim *GifAnimation) Load() {
	defer gifAnim.frameImg.Free()
	empty := color.RGBA{0, 0, 0, 0}
	for index, img := range gifAnim.Data.Image {
		// After decoding, we have to manually create a new image and plot each frame of the GIF because transparent GIFs
		// can only have frames that account for changed pixels (i.e. if you have a 320x240 GIF, but on frame
		// 17 only one pixel changes, the image generated for frame 17 will be 1x1 for Bounds.Size()).

		prev := 0
		disposalMode := byte(gif.DisposalNone)
		if index > 0 {
			disposalMode = gifAnim.Data.Disposal[index]
		}

		for y := 0; y < gifAnim.frameImg.Bounds().Size().Y; y++ {
			for x := 0; x < gifAnim.frameImg.Bounds().Size().X; x++ {
				// We clear each pixel of each frame, but only the pixels within the rectangle specified by the frame is plotted below, as
				// some frames of GIFs can have a "changed rectangle", indicating which pixels in which rectangle need to actually change.
				if disposalMode == gif.DisposalBackground {
					gifAnim.frameImg.Set(x, y, empty)
				} else if disposalMode == gif.DisposalPrevious {
					r, g, b, a := gifAnim.Data.Image[prev].At(x, y).RGBA()
					gifAnim.frameImg.Set(x, y, color.RGBA{byte(r), byte(g), byte(b), byte(a)})
				}

				if disposalMode != gif.DisposalPrevious {
					prev = index
				}

				if x >= img.Bounds().Min.X && x < img.Bounds().Max.X && y >= img.Bounds().Min.Y && y < img.Bounds().Max.Y {
					c := img.At(x, y)
					if _, _, _, alpha := c.RGBA(); alpha > 0 {
						r, g, b, a := c.RGBA()
						gifAnim.frameImg.Set(x, y, color.RGBA{byte(r), byte(g), byte(b), byte(a)})
					}
				}
			}
		}

		newSurf, _ := gifAnim.frameImg.Duplicate()
		gifAnim.Frames = append(gifAnim.Frames, newSurf)

		delay := float32(gifAnim.Data.Delay[index]) / 100

		if delay <= 0 {
			delay = 0.1
		}

		gifAnim.Delays = append(gifAnim.Delays, delay)
	}
}

func (gifAnim *GifAnimation) Destroy(state *State) {
	for _, frame := range gifAnim.Frames {
		if frame != nil {
			frame.Free()
		}
	}
}

type GifPlayer struct {
	Animation    *GifAnimation
	CurrentFrame int
	Timer        float32
	Frames       []*sdl.Texture
	renderer     *sdl.Renderer
}

func NewGifPlayer(renderer *sdl.Renderer, gifAnim *GifAnimation) *GifPlayer {
	return &GifPlayer{
		Animation: gifAnim,
		renderer:  renderer,
	}
}

func (gifPlayer *GifPlayer) Update(dt float32) {
	gifPlayer.Timer += dt

	for gifPlayer.Timer >= gifPlayer.Animation.Delays[gifPlayer.CurrentFrame] {
		gifPlayer.Timer -= gifPlayer.Animation.Delays[gifPlayer.CurrentFrame]
		gifPlayer.CurrentFrame++
		if gifPlayer.CurrentFrame >= len(gifPlayer.Animation.Frames) {
			gifPlayer.CurrentFrame = 0
		}
	}
}

func (gifPlayer *GifPlayer) Destroy(state *State) {
	gifPlayer.Animation.Destroy(state)
	for _, frame := range gifPlayer.Frames {
		if frame != nil {
			state.destroyer <- frame
		}
		// err := frame.Destroy()
		//fmt.Printf("Destroyed texture %d of %d.\n", i, len(gifPlayer.Frames))
	}
}

func (gifPlayer *GifPlayer) Texture() *sdl.Texture {
	for len(gifPlayer.Frames) <= gifPlayer.CurrentFrame {
		surface := gifPlayer.Animation.Frames[gifPlayer.CurrentFrame]
		frame, _ := gifPlayer.renderer.CreateTextureFromSurface(surface)
		gifPlayer.Frames = append(gifPlayer.Frames, frame)

		gifPlayer.Animation.Frames[gifPlayer.CurrentFrame] = nil
		surface.Free()
		//fmt.Printf("Freed frame %d out of %d total.\n", gifPlayer.CurrentFrame, len(gifPlayer.Animation.Frames))
	}
	return gifPlayer.Frames[gifPlayer.CurrentFrame]
}
