//go:build !portable
// +build !portable

package main

import (
	"os"
	"path/filepath"
)

func getConfigDirectory() string {
	config, _ := os.UserConfigDir()
	return filepath.Join(config, "profectus")
}

func getUserThemesDirectory() string {
	os.MkdirAll(filepath.Join(getConfigDirectory(), "themes"), 0700)
	return filepath.Join(getConfigDirectory(), "themes")
}

func getUserFontsDirectory() string {
	os.MkdirAll(filepath.Join(getConfigDirectory(), "fonts"), 0700)
	return filepath.Join(getConfigDirectory(), "fonts")
}
