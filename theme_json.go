package main

import (
	"fmt"
	"strings"

	"gitlab.com/clseibold/profectus/horusui"
)

type ThemeJson struct {
	Name                    string           `json:"name"`
	ContentContainerPadding *PaddingJson     `json:"content_container_padding,omitempty"`
	SidebarDivider          int              `json:"content_container_spacing,omitempty"`
	Default                 SectionThemeJson `json:"default"` // Default Fallback
	Topbar                  SectionThemeJson `json:"topbar"`
	Tabbar                  TabbarThemeJson  `json:"tabbar"`
	Sidebar                 SidebarThemeJson `json:"sidebar"`
	Page                    PageThemeJson    `json:"page"`
	PageInlined             SectionThemeJson `json:"page_inlined"`
	Popup                   SectionThemeJson `json:"popup"`

	// AnimationTimings: tab_open_close, sidebar_open_close
	// Sidebar Spacing and Position/Dock, Tabbar Position
	// Document alignment (centering/left/right of page, or entire screen like in Kristall) and max-width
	// Document background image
}

type TabbarThemeJson struct {
	Position          string `json:"position"`
	AddButtonPosition string `json:"add_button_position"`
	SectionThemeJson
}

type SidebarThemeJson struct {
	MaxWidth int    `json:"max_width,omitempty"`
	MinWidth int    `json:"min_width,omitempty"`
	Dock     string `json:"dock,omitempty"` // default, opposite, left, right
	SectionThemeJson
}

type PageThemeJson struct {
	MaxWidth      int  `json:"max_width,omitempty"`
	MaxImageWidth int  `json:"max_image_width,omitempty"`
	NexCharWidth  int  `json:"nex_char_width,omitempty"`
	Center        bool `json:"center"`
	SectionThemeJson
	Document map[string]ElementThemeJson `json:"document"`
}

type SectionThemeJson struct {
	// animationTotalMS float32 // TODO: Might be unnecessary to store this here
	Panel    ElementThemeJson `json:"panel"`
	Button   ButtonThemeJson  `json:"button,omitempty"`
	Textbox  ElementThemeJson `json:"textbox,omitempty"`
	Checkbox ElementThemeJson `json:"checkbox,omitempty"`
	List     ElementThemeJson `json:"list_item,omitempty"`
}

type ButtonThemeJson struct {
	Outset bool `json:"outset,omitempty"`
	ElementThemeJson
}

type ElementThemeJson struct {
	Radius     int16   `json:"radius,omitempty"`
	IndentSize float32 `json:"indent_size,omitempty"`

	Background map[string]ColorJson  `json:"backgrounds,omitempty"` // Change to ColorAnimation
	Border     map[string]BorderJson `json:"borders,omitempty"`     // Animate Border Color at the very least
	TextColors map[string]ColorJson  `json:"textcolors,omitempty"`  // Change to ColorAnimation
	//textFonts  [Selector_Max]horusui.Font

	Padding *PaddingJson `json:"padding,omitempty"`
	Spacing []float32    `json:"spacing,omitempty"`
}

// Hex encoded string
type ColorJson string

type BorderJson struct {
	Thickness uint16    `json:"thickness"`
	Color     ColorJson `json:"color"`
}

type PaddingJson struct {
	Top    float32 `json:"top,omitempty"`
	Bottom float32 `json:"bottom,omitempty"`
	Left   float32 `json:"left,omitempty"`
	Right  float32 `json:"right,omitempty"`
}

func ThemeJsonToTheme(theme ThemeJson) (result Theme) {
	result.Name = theme.Name // TODO: Make sure not empty and doesn't collide with existing name
	result.ContentContainerPadding = PaddingJsonToPadding(theme.ContentContainerPadding, DefaultDark.ContentContainerPadding)
	result.SidebarDivider = float32(theme.SidebarDivider)

	result.Default = SectionThemeJsonToSectionTheme(theme.Default, SectionTheme{})
	result.Topbar = SectionThemeJsonToSectionTheme(theme.Topbar, result.Default)
	result.Tabbar = TabbarThemeJsonToTabbarTheme(theme.Tabbar, result.Default)
	result.Sidebar = SidebarThemeJsonToSidebarTheme(theme.Sidebar, result.Default)
	result.Page = PageThemeJsonToPageTheme(theme.Page, result.Default)
	result.PageInlined = SectionThemeJsonToSectionTheme(theme.PageInlined, result.Default)
	result.Popup = SectionThemeJsonToSectionTheme(theme.Popup, result.Default)

	return result
}

func TabbarThemeJsonToTabbarTheme(tabbarTheme TabbarThemeJson, defaultSection SectionTheme) (result TabbarTheme) {
	result.SectionTheme = SectionThemeJsonToSectionTheme(tabbarTheme.SectionThemeJson, defaultSection)

	result.Position = tabbarTheme.Position
	if tabbarTheme.Position == "" {
		result.Position = "below"
	}

	if tabbarTheme.AddButtonPosition == "" || tabbarTheme.AddButtonPosition == "opposite" {
		result.AddButtonPosition = 1
	} else if tabbarTheme.AddButtonPosition == "after_tabs" {
		result.AddButtonPosition = 0
	} else if tabbarTheme.AddButtonPosition == "before_tabs" {
		result.AddButtonPosition = -1
	}

	return result
}

func SidebarThemeJsonToSidebarTheme(sidebarTheme SidebarThemeJson, defaultSection SectionTheme) (result SidebarTheme) {
	result.SectionTheme = SectionThemeJsonToSectionTheme(sidebarTheme.SectionThemeJson, defaultSection)

	result.MaxWidth = sidebarTheme.MaxWidth
	if result.MaxWidth == 0 {
		result.MaxWidth = 175
	}
	result.MinWidth = sidebarTheme.MinWidth

	result.Dock = strings.ToLower(sidebarTheme.Dock)
	if sidebarTheme.Dock == "" {
		result.Dock = "default"
	}

	return result
}

func PageThemeJsonToPageTheme(pageTheme PageThemeJson, defaultSection SectionTheme) (result PageTheme) {
	result.SectionTheme = SectionThemeJsonToSectionTheme(pageTheme.SectionThemeJson, defaultSection)

	// TODO: document element backgrounds should default to background of the sectionTheme

	// Document
	if len(pageTheme.Document) > 0 {
		result.Document[ScrollLineType_Heading1] = ElementThemeJsonToElementTheme(pageTheme.Document["heading1"], DefaultDark.Page.Document[ScrollLineType_Heading1])
		result.Document[ScrollLineType_Heading2] = ElementThemeJsonToElementTheme(pageTheme.Document["heading2"], DefaultDark.Page.Document[ScrollLineType_Heading2])
		result.Document[ScrollLineType_Heading3] = ElementThemeJsonToElementTheme(pageTheme.Document["heading3"], DefaultDark.Page.Document[ScrollLineType_Heading3])
		result.Document[ScrollLineType_Heading4] = ElementThemeJsonToElementTheme(pageTheme.Document["heading4"], DefaultDark.Page.Document[ScrollLineType_Heading4])

		result.Document[ScrollLineType_Body] = ElementThemeJsonToElementTheme(pageTheme.Document["body"], DefaultDark.Page.Document[ScrollLineType_Body])
		result.Document[ScrollLineType_Quote] = ElementThemeJsonToElementTheme(pageTheme.Document["blockquote"], DefaultDark.Page.Document[ScrollLineType_Quote])
		result.Document[ScrollLineType_Preformat] = ElementThemeJsonToElementTheme(pageTheme.Document["preformat"], DefaultDark.Page.Document[ScrollLineType_Preformat])

		result.Document[ScrollLineType_Link] = ElementThemeJsonToElementTheme(pageTheme.Document["link"], DefaultDark.Page.Document[ScrollLineType_Link])
		result.Document[ScrollLineType_Link_Nex] = ElementThemeJsonToElementTheme(pageTheme.Document["link_nex"], DefaultDark.Page.Document[ScrollLineType_Link_Nex])
		//result.Document[ScrollLineType_Link_Hover] = ElementThemeJsonToElementTheme(pageTheme.Document["link_hover"], DefaultDark.Page.Document[ScrollLineType_Link_Hover])

		result.Document[ScrollLineType_ListItem] = ElementThemeJsonToElementTheme(pageTheme.Document["list_item"], DefaultDark.Page.Document[ScrollLineType_ListItem])
	}

	result.MaxWidth = pageTheme.MaxWidth
	if pageTheme.MaxWidth == 0 {
		result.MaxWidth = 999999
	}
	result.MaxImageWidth = pageTheme.MaxImageWidth
	if result.MaxImageWidth == 0 {
		result.MaxImageWidth = result.MaxWidth
	}

	result.NexCharWidth = pageTheme.NexCharWidth
	if pageTheme.NexCharWidth == 0 {
		result.NexCharWidth = 80
	}

	if pageTheme.Center {
		result.Center = 1
	} else {
		result.Center = 0
	}

	return result
}

func SectionThemeJsonToSectionTheme(section SectionThemeJson, defaultSection SectionTheme) (result SectionTheme) {
	result.Panel = ElementThemeJsonToElementTheme(section.Panel, defaultSection.Panel)
	result.Button = ButtonThemeJsonToButtonTheme(section.Button, defaultSection.Button) // TODO: Default should be the Panel
	result.Textbox = ElementThemeJsonToElementTheme(section.Textbox, defaultSection.Textbox)
	result.Checkbox = ElementThemeJsonToElementTheme(section.Checkbox, defaultSection.Checkbox)
	result.List = ElementThemeJsonToElementTheme(section.List, defaultSection.List)

	if result.Panel.jsonOmitted && result.Button.jsonOmitted && result.Textbox.jsonOmitted && result.Checkbox.jsonOmitted && result.List.jsonOmitted {
		result.jsonOmitted = true
	}

	return result
}

func ButtonThemeJsonToButtonTheme(button ButtonThemeJson, defaultButton ButtonTheme) (result ButtonTheme) {
	result.ElementTheme = ElementThemeJsonToElementTheme(button.ElementThemeJson, defaultButton.ElementTheme)

	result.Outset = button.Outset

	return result
}

func ElementThemeJsonToElementTheme(element ElementThemeJson, defaultElement ElementTheme) (result ElementTheme) {
	// Empty value
	if len(element.Background)+len(element.Border)+len(element.TextColors) == 0 && (element.Padding == nil) && (element.Spacing == nil) && element.IndentSize == 0 && element.Radius == 0 {
		result.jsonOmitted = true
		return defaultElement
	}

	if element.Background == nil {
		result.background = defaultElement.background
	} else {
		result.background[Selector_Default] = ColorJsonToColor(element.Background["default"], defaultElement.background[Selector_Default])
		result.background[Selector_Hover] = ColorJsonToColor(element.Background["hover"], result.background[Selector_Default])
		result.background[Selector_DragHover] = ColorJsonToColor(element.Background["drag_hover"], result.background[Selector_Default])
		result.background[Selector_Dragged] = ColorJsonToColor(element.Background["dragged"], result.background[Selector_Default])
		result.background[Selector_Active] = ColorJsonToColor(element.Background["active"], result.background[Selector_Default])
		result.background[Selector_Focused] = ColorJsonToColor(element.Background["focused"], result.background[Selector_Default])
		result.background[Selector_Placeholder] = ColorJsonToColor(element.Background["placeholder"], result.background[Selector_Default])
		result.background[Selector_Checked] = ColorJsonToColor(element.Background["checked"], result.background[Selector_Default])
		result.background[Selector_Disabled] = ColorJsonToColor(element.Background["disabled"], result.background[Selector_Default])
	}

	if element.Border == nil {
		result.border = defaultElement.border
	} else {
		result.border[Selector_Default] = BorderJsonToBorder(element.Border["default"], defaultElement.border[Selector_Default])
		result.border[Selector_Hover] = BorderJsonToBorder(element.Border["hover"], result.border[Selector_Default])
		result.border[Selector_DragHover] = BorderJsonToBorder(element.Border["drag_hover"], result.border[Selector_Default])
		result.border[Selector_Dragged] = BorderJsonToBorder(element.Border["dragged"], result.border[Selector_Default])
		result.border[Selector_Active] = BorderJsonToBorder(element.Border["active"], result.border[Selector_Default])
		result.border[Selector_Focused] = BorderJsonToBorder(element.Border["focused"], result.border[Selector_Default])
		result.border[Selector_Placeholder] = BorderJsonToBorder(element.Border["placeholder"], result.border[Selector_Default])
		result.border[Selector_Checked] = BorderJsonToBorder(element.Border["checked"], result.border[Selector_Default])
		result.border[Selector_Disabled] = BorderJsonToBorder(element.Border["disabled"], result.border[Selector_Default])
	}

	if element.TextColors == nil {
		result.textColors = defaultElement.textColors
	} else {
		result.textColors[Selector_Default] = ColorJsonToColor(element.TextColors["default"], defaultElement.textColors[Selector_Default])
		result.textColors[Selector_Hover] = ColorJsonToColor(element.TextColors["hover"], result.textColors[Selector_Default])
		result.textColors[Selector_DragHover] = ColorJsonToColor(element.TextColors["drag_hover"], result.textColors[Selector_Default])
		result.textColors[Selector_Dragged] = ColorJsonToColor(element.TextColors["dragged"], result.textColors[Selector_Default])
		result.textColors[Selector_Active] = ColorJsonToColor(element.TextColors["active"], result.textColors[Selector_Default])
		result.textColors[Selector_Focused] = ColorJsonToColor(element.TextColors["focused"], result.textColors[Selector_Default])
		result.textColors[Selector_Placeholder] = ColorJsonToColor(element.TextColors["placeholder"], result.textColors[Selector_Default])
		result.textColors[Selector_Checked] = ColorJsonToColor(element.TextColors["checked"], result.textColors[Selector_Default])
		result.textColors[Selector_Disabled] = ColorJsonToColor(element.TextColors["disabled"], result.textColors[Selector_Default])
	}

	result.padding = PaddingJsonToPadding(element.Padding, defaultElement.padding)
	result.spacing = SpacingJsonToSpacing(element.Spacing, defaultElement.spacing)

	result.indentSize = element.IndentSize
	result.radius = element.Radius
	if element.Radius == 1 {
		result.radius = 0
	}

	return result
}

func ColorJsonToColor(color ColorJson, defaultColor horusui.Color) (result horusui.Color) {
	if color == "" {
		return defaultColor
	} else if color == "$panel" {
		// TODO: Panel's color
	}
	// TODO: $panel-scalar => panel color with rgb subtracted by scalar.
	// TODO: $panel+scalar => panel color with rgb added by scalar.
	// TODO: $accent => accent color.
	// TODO: $accent +/- scalar => accent color with rgb added/subtracted by scalar.

	parsedColor, err := ParseHexColorFast(string(color))
	if err != nil {
		panic(err)
	}
	result.R = parsedColor.R
	result.G = parsedColor.G
	result.B = parsedColor.B
	result.A = parsedColor.A

	return result
}

func BorderJsonToBorder(border BorderJson, defaultBorder horusui.GuiBorder) (result horusui.GuiBorder) {
	if (border == BorderJson{}) {
		return defaultBorder
	}

	result.Thickness = border.Thickness
	result.Color = ColorJsonToColor(border.Color, horusui.Black)

	return result
}

func PaddingJsonToPadding(padding *PaddingJson, defaultPadding horusui.GuiPadding) (result horusui.GuiPadding) {
	if padding == nil {
		return defaultPadding
	}

	result.Top = padding.Top
	result.Bottom = padding.Bottom
	result.Left = padding.Left
	result.Right = padding.Right

	return result
}

func SpacingJsonToSpacing(spacing []float32, defaultSpacing horusui.GuiSpacing) (result horusui.GuiSpacing) {
	if len(spacing) == 0 {
		return defaultSpacing
	}

	result.X = spacing[0]
	if len(spacing) >= 2 {
		result.Y = spacing[1]
	}

	return result
}

func ThemeToThemeJson(theme Theme) (result ThemeJson) {
	result.Name = theme.Name
	result.ContentContainerPadding = PaddingToPaddingJson(theme.ContentContainerPadding)
	result.SidebarDivider = int(theme.SidebarDivider)

	result.Default = SectionThemeToSectionThemeJson(theme.Default, SectionThemeToSectionThemeJson(DefaultDark.Default, SectionThemeJson{}))
	result.Topbar = SectionThemeToSectionThemeJson(theme.Topbar, SectionThemeJson{})
	result.Tabbar = TabbarThemeToTabbarThemeJson(theme.Tabbar, SectionThemeJson{})
	result.Sidebar = SidebarThemeToSidebarThemeJson(theme.Sidebar, SectionThemeJson{})
	result.Page = PageThemeToPageThemeJson(theme.Page, result.Default)
	result.PageInlined = SectionThemeToSectionThemeJson(theme.PageInlined, SectionThemeJson{})
	result.Popup = SectionThemeToSectionThemeJson(theme.Popup, SectionThemeJson{})

	return result
}

func TabbarThemeToTabbarThemeJson(tabbarTheme TabbarTheme, defaultSection SectionThemeJson) (result TabbarThemeJson) {
	result.SectionThemeJson = SectionThemeToSectionThemeJson(tabbarTheme.SectionTheme, defaultSection)

	result.Position = tabbarTheme.Position
	result.AddButtonPosition = "opposite"
	if tabbarTheme.AddButtonPosition == 1 {
		result.AddButtonPosition = "opposite"
	} else if tabbarTheme.AddButtonPosition == 0 {
		result.AddButtonPosition = "after_tabs"
	} else if tabbarTheme.AddButtonPosition == -1 {
		result.AddButtonPosition = "before_tabs"
	}

	return result
}

func SidebarThemeToSidebarThemeJson(sidebarTheme SidebarTheme, defaultSection SectionThemeJson) (result SidebarThemeJson) {
	result.SectionThemeJson = SectionThemeToSectionThemeJson(sidebarTheme.SectionTheme, defaultSection)

	result.MaxWidth = sidebarTheme.MaxWidth
	result.MinWidth = sidebarTheme.MinWidth
	result.Dock = sidebarTheme.Dock

	return result
}

func PageThemeToPageThemeJson(pageTheme PageTheme, defaultSection SectionThemeJson) (result PageThemeJson) {
	result.SectionThemeJson = SectionThemeToSectionThemeJson(pageTheme.SectionTheme, defaultSection)

	result.Document = make(map[string]ElementThemeJson)
	for linetype := ScrollLineType_Heading1; linetype < ScrollLineType_Max; linetype++ {
		elementJson := ElementThemeToElementThemeJson(pageTheme.Document[linetype])
		if elementJson.Background == nil && elementJson.Border == nil && elementJson.TextColors == nil && elementJson.IndentSize == 0 && elementJson.Radius == 0 && elementJson.Padding == nil && len(elementJson.Spacing) == 0 {
			if DefaultDark.Page.Document[linetype].jsonOmitted {
				continue
			}
			result.Document[LinetypeToString(linetype)] = ElementThemeToElementThemeJson(DefaultDark.Page.Document[linetype])
		} else {
			result.Document[LinetypeToString(linetype)] = elementJson
		}
	}

	result.MaxWidth = pageTheme.MaxWidth
	if pageTheme.MaxWidth == 0 {
		result.MaxWidth = 999999
	}
	if pageTheme.MaxImageWidth != pageTheme.MaxWidth {
		result.MaxImageWidth = pageTheme.MaxImageWidth
	}

	result.NexCharWidth = pageTheme.NexCharWidth
	if pageTheme.NexCharWidth == 80 {
		result.NexCharWidth = 0
	}

	if pageTheme.Center >= 1 {
		result.Center = true
	}

	return result
}

func LinetypeToString(linetype ScrollLineType) string {
	switch linetype {
	case ScrollLineType_Heading1:
		return "heading1"
	case ScrollLineType_Heading2:
		return "heading2"
	case ScrollLineType_Heading3:
		return "heading3"
	case ScrollLineType_Heading4:
		return "heading4"
	case ScrollLineType_Body:
		return "body"
	case ScrollLineType_Quote:
		return "blockquote"
	case ScrollLineType_Preformat:
		return "preformat"
	case ScrollLineType_Link:
		return "link"
	case ScrollLineType_Link_Nex:
		return "link_nex"
	case ScrollLineType_ListItem:
		return "list_item"
	}

	return ""
}

func SectionThemeToSectionThemeJson(section SectionTheme, defaultSection SectionThemeJson) (result SectionThemeJson) {
	if section.jsonOmitted {
		return defaultSection
	}

	result.Panel = ElementThemeToElementThemeJson(section.Panel)
	result.Button = ButtonThemeToButtonThemeJson(section.Button)
	result.Textbox = ElementThemeToElementThemeJson(section.Textbox)
	result.Checkbox = ElementThemeToElementThemeJson(section.Checkbox)
	result.List = ElementThemeToElementThemeJson(section.List)

	return result
}

func ButtonThemeToButtonThemeJson(button ButtonTheme) (result ButtonThemeJson) {
	result.ElementThemeJson = ElementThemeToElementThemeJson(button.ElementTheme)

	result.Outset = button.Outset

	return result
}

func ElementThemeToElementThemeJson(element ElementTheme) (result ElementThemeJson) {
	if element.jsonOmitted {
		return ElementThemeJson{}
	}

	result.Background = make(map[string]ColorJson)
	for selector := Selector_Default; selector < Selector_Max; selector++ {
		colorJson := ColorToColorJson(element.background[selector])
		if colorJson == "" {
			continue
		}
		result.Background[SelectorToString(selector)] = colorJson
	}

	result.Border = make(map[string]BorderJson)
	for selector := Selector_Default; selector < Selector_Max; selector++ {
		borderJson := BorderToBorderJson(element.border[selector])
		if (borderJson == BorderJson{}) {
			continue
		}
		result.Border[SelectorToString(selector)] = borderJson
	}

	result.TextColors = make(map[string]ColorJson)
	for selector := Selector_Default; selector < Selector_Max; selector++ {
		colorJson := ColorToColorJson(element.textColors[selector])
		if colorJson == "" {
			continue
		}
		result.TextColors[SelectorToString(selector)] = colorJson
	}

	result.Padding = PaddingToPaddingJson(element.padding)
	result.Spacing = SpacingToSpacingJson(element.spacing)

	result.IndentSize = element.indentSize
	result.Radius = element.radius
	if element.radius == 1 {
		result.Radius = 0
	}

	return result
}

func SelectorToString(selector Selector) string {
	switch selector {
	case Selector_Default:
		return "default"
	case Selector_Hover:
		return "hover"
	case Selector_DragHover:
		return "drag_hover"
	case Selector_Dragged:
		return "dragged"
	case Selector_Active:
		return "active"
	case Selector_Focused:
		return "focused"
	case Selector_Placeholder:
		return "placeholder"
	case Selector_Checked:
		return "checked"
	case Selector_Disabled:
		return "disabled"
	}

	return ""
}

func ColorToColorJson(color horusui.Color) (result ColorJson) {
	if (color == horusui.Color{}) {
		return ""
	}

	return ColorJson(fmt.Sprintf("#%02x%02x%02x%02x", color.R, color.G, color.B, color.A))
}

func BorderToBorderJson(border horusui.GuiBorder) (result BorderJson) {
	if (border == horusui.GuiBorder{}) {
		return result
	}

	result.Thickness = border.Thickness
	result.Color = ColorToColorJson(border.Color)

	return result
}

func PaddingToPaddingJson(padding horusui.GuiPadding) (result *PaddingJson) {
	if (padding == horusui.GuiPadding{}) {
		return nil
	}

	result = &PaddingJson{}
	result.Top = padding.Top
	result.Bottom = padding.Bottom
	result.Left = padding.Left
	result.Right = padding.Right

	return result
}

func SpacingToSpacingJson(spacing horusui.GuiSpacing) (result []float32) {
	if (spacing == horusui.GuiSpacing{}) {
		return nil
	}

	slice := &[2]float32{}
	result = slice[:]
	result[0] = spacing.X
	result[1] = spacing.Y

	return result
}
