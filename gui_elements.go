package main

import (
	"strings"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/clseibold/profectus/horusui"
)

func createTabButton(state *State, id horusui.GuiElementId, text string, tabbar horusui.GuiElementId, font *horusui.Font, active bool, noLeftBorder bool, noRightBorder bool, autoWidth bool, percentWidth float32, detached bool) horusui.GuiElementId {
	guiState := state.guiState
	tabButtonTheme := currentTheme.Tabbar.Button
	borderLeft := tabButtonTheme.border[Selector_Default]
	borderRight := tabButtonTheme.border[Selector_Default]
	borderTop := tabButtonTheme.border[Selector_Default]
	borderBottom := tabButtonTheme.border[Selector_Default]
	textColor := tabButtonTheme.textColors[Selector_Default]
	if active {
		borderLeft = tabButtonTheme.border[Selector_Active]
		borderRight = tabButtonTheme.border[Selector_Active]
		borderTop = tabButtonTheme.border[Selector_Active]
		borderBottom = tabButtonTheme.border[Selector_Active]
		textColor = tabButtonTheme.textColors[Selector_Active]
	}
	if tabButtonTheme.Outset {
		// borderRight.Color = horusui.Black

		max := float64(13)
		borderRight.Color = horusui.Color{R: uint8(float64(borderTop.Color.R) / 255 * max), G: uint8(float64(borderTop.Color.G) / 255 * max), B: uint8(float64(borderTop.Color.B) / 255 * max), A: 255} //horusui.Black
	}
	if noLeftBorder {
		borderLeft = horusui.GuiBorder{}
	}
	if noRightBorder {
		borderRight = horusui.GuiBorder{}
	}
	if !detached {
		borderBottom = horusui.GuiBorder{}
	}

	var sizeType horusui.SizeType
	if autoWidth {
		sizeType = horusui.CreateSizeType(horusui.SizeMode_Auto, horusui.SizeMode_Auto, horusui.FPoint{X: 1, Y: 1}).SetMin(horusui.FPoint{X: horusui.Scale_value(guiState, float32(70)), Y: 0}).SetPercent(horusui.FPoint{X: percentWidth, Y: 1})
	} else {
		sizeType = horusui.CreateSizeType(horusui.SizeMode_Pixel, horusui.SizeMode_Auto, horusui.FPoint{X: horusui.Scale_value(guiState, float32(168)), Y: 1}).SetMin(horusui.FPoint{X: horusui.Scale_value(guiState, float32(70)), Y: 0}).SetPercent(horusui.FPoint{X: percentWidth, Y: 1})
	}
	btn := guiState.Element(id, sizeType, horusui.GuiPanelDirection_Horizontal, tabbar)
	guiState.SetRounded(btn, currentTheme.Tabbar.Button.radius)
	guiState.SetPadding(btn, guiState.Scale_padding(currentTheme.Tabbar.Button.padding))
	guiState.SetBorder_sides(btn, borderLeft, borderRight, borderTop, borderBottom)
	guiState.SetText(btn, text, "", textColor, state.arial15, horusui.TextAlignment_Middle, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Clip, horusui.TextDirection_UseGlobal)
	if active {
		guiState.SetBackground(btn, currentTheme.Tabbar.Button.background[Selector_Active])
	} else {
		guiState.SetBackground(btn, currentTheme.Tabbar.Button.background[Selector_Default])
	}

	if guiState.IsHovered(btn) && !active {
		borderLeft := currentTheme.Tabbar.Button.border[Selector_Hover]
		borderRight := currentTheme.Tabbar.Button.border[Selector_Hover]
		borderBottom := currentTheme.Tabbar.Button.border[Selector_Hover]
		if noLeftBorder {
			borderLeft = horusui.GuiBorder{}
		}
		if noRightBorder {
			borderRight = horusui.GuiBorder{}
		}

		if !detached {
			borderBottom = horusui.GuiBorder{}
		}

		guiState.SetBackground(btn, currentTheme.Tabbar.Button.background[Selector_Hover])
		guiState.SetBorder_sides(btn, borderLeft, borderRight, currentTheme.Tabbar.Button.border[Selector_Hover], borderBottom)
	}

	return btn
}

func createButton(state *State, id horusui.GuiElementId, text string, parent horusui.GuiElementId, autoWidth bool, autoHeight bool, font *horusui.Font, sectionTheme SectionTheme) horusui.GuiElementId {
	guiState := state.guiState
	buttonTheme := sectionTheme.Button
	borderBottom := buttonTheme.border[Selector_Default]
	borderRight := buttonTheme.border[Selector_Default]
	if buttonTheme.Outset {
		//max := math.Max(math.Max(255-float64(buttonTheme.border[Selector_Default].Color.R), 255-float64(buttonTheme.border[Selector_Default].Color.G)), 255-float64(buttonTheme.border[Selector_Default].Color.B))
		max := float64(13)
		borderBottom.Color = horusui.Color{R: uint8(float64(buttonTheme.border[Selector_Default].Color.R) / 255 * max), G: uint8(float64(buttonTheme.border[Selector_Default].Color.G) / 255 * max), B: uint8(float64(buttonTheme.border[Selector_Default].Color.B) / 255 * max), A: 255} //horusui.Black
		borderRight.Color = borderBottom.Color
	}

	sizeMode_W := horusui.SizeMode_Auto
	if !autoWidth {
		sizeMode_W = horusui.SizeMode_Fill
	}
	sizeMode_H := horusui.SizeMode_Auto
	if !autoHeight {
		sizeMode_H = horusui.SizeMode_Fill
	}
	btn := guiState.CreatePane(id, horusui.CreateSizeType(sizeMode_W, sizeMode_H, horusui.FPoint{X: 1, Y: 1}), horusui.GuiPanelDirection_Horizontal, guiState.Scale_padding(buttonTheme.padding), parent)
	guiState.SetBackground(btn, buttonTheme.background[Selector_Default])
	guiState.SetRounded(btn, buttonTheme.radius)

	guiState.SetBorder_sides(btn, buttonTheme.border[Selector_Default], borderRight, buttonTheme.border[Selector_Default], borderBottom)
	guiState.SetText(btn, text, "", buttonTheme.textColors[Selector_Default], font, horusui.TextAlignment_Middle, horusui.TextAlignment_Middle, horusui.GuiTextOverflow_Clip, horusui.TextDirection_UseGlobal)
	if guiState.IsHovered(btn) {
		guiState.SetBackground(btn, buttonTheme.background[Selector_Hover])

		guiState.SetBorder_all(btn, buttonTheme.border[Selector_Hover])
	}

	return btn
}

func createTextbox_custom(state *State, id horusui.GuiElementId, sizeType horusui.SizeType, placeholderText string, font *horusui.Font, textPtr *strings.Builder, parent horusui.GuiElementId, sectionTheme SectionTheme) horusui.GuiElementId {
	guiState := state.guiState
	textboxTheme := sectionTheme.Textbox

	textbox, _ := guiState.CreateTextBox(id, sizeType, textboxTheme.border[Selector_Default], textboxTheme.border[Selector_Focused], textboxTheme.border[Selector_Hover], guiState.Scale_padding(textboxTheme.padding), placeholderText, textboxTheme.textColors[Selector_Placeholder], textboxTheme.textColors[Selector_Default], font, textPtr, state.currentLanguageDirection, parent)
	guiState.SetBackground(textbox, textboxTheme.background[Selector_Default])
	guiState.SetRounded(textbox, textboxTheme.radius)
	if guiState.IsHoverEnter(textbox) {
		setCursor(state, sdl.SYSTEM_CURSOR_IBEAM)
	} else if guiState.IsHoverExit(textbox) {
		resetCursor(state)
	}

	if guiState.IsClicked(textbox, horusui.MouseButton_Middle) {
		clipboardText, err := sdl.GetClipboardText()
		if err == nil {
			textPtr.WriteString(clipboardText)
			state.focusedInputCursor += len(clipboardText)
		}
	}

	// TODO: Put cursor stuff inside guiState!
	if guiState.IsFocused(textbox) {
		guiState.SetBackground(textbox, textboxTheme.background[Selector_Focused])
		guiState.SetCursorIndex(textbox, state.focusedInputCursor)
	} else if guiState.IsHovered(textbox) {
		guiState.SetBackground(textbox, textboxTheme.background[Selector_Hover])
	}
	if guiState.IsFocusEnter(textbox) {
		setInputFocus(state, textPtr)
		guiState.SetCursorIndex(textbox, state.focusedInputCursor)
	} else if guiState.IsFocusExit(textbox) {
		resetInputFocus(state)
	}

	return textbox
}

func setInputFocus(state *State, textPtr *strings.Builder) {
	state.inputFocusSet = true
	state.focusedInput = textPtr
	state.focusedInputCursor = textPtr.Len() // TODO: Instead of this, get the current cursor from stored state
	sdl.StartTextInput()
}
func resetInputFocus(state *State) {
	state.inputFocusReset = true
}

// Call at start of frame to reset
func handleFocusExit_startFrame(state *State) {
	state.inputFocusSet = false
	state.inputFocusReset = false
}

// Call at end of frame
func handleFocusExit_endFrame(state *State) {
	// Reset input if a new input has not already been set
	if !state.inputFocusSet && state.inputFocusReset {
		//if state.focusedInput == textPtr {
		state.focusedInput = nil
		state.focusedInputCursor = 0
		//}
		sdl.StopTextInput()
	}
}
