package main

import (
	"image/color"

	"github.com/gen2brain/jpegxl"
	"github.com/veandco/go-sdl2/sdl"
)

type JxlAnimation struct {
	Data     *jpegxl.JXL
	Frames   []*sdl.Surface
	Delays   []float32 // In Seconds
	frameImg *sdl.Surface
	Width    float32
	Height   float32
}

func NewJxlAnimation(data *jpegxl.JXL) *JxlAnimation {
	surf, _ := sdl.CreateRGBSurfaceWithFormat(0, int32(data.Image[0].Bounds().Dx()), int32(data.Image[0].Bounds().Dy()), 64, uint32(sdl.PIXELFORMAT_RGBA32))
	surf.SetBlendMode(sdl.BLENDMODE_BLEND)

	anim := &JxlAnimation{
		Data:     data,
		frameImg: surf,
		Width:    float32(data.Image[0].Bounds().Dx()),
		Height:   float32(data.Image[0].Bounds().Dy()),
	}
	anim.Load()
	return anim
}

func (anim *JxlAnimation) Load() {
	defer anim.frameImg.Free()
	empty := color.RGBA64{0, 0, 0, 0}

	for y := 0; y < anim.frameImg.Bounds().Dy(); y++ {
		for x := 0; x < anim.frameImg.Bounds().Dx(); x++ {
			anim.frameImg.Set(x, y, empty)
		}
	}

	for index, frame := range anim.Data.Image {
		for y := 0; y < frame.Bounds().Dy(); y++ {
			for x := 0; x < frame.Bounds().Dx(); x++ {
				color := frame.At(x, y)
				anim.frameImg.Set(x, y, color)
			}
		}

		newSurf, _ := anim.frameImg.Duplicate()
		anim.Frames = append(anim.Frames, newSurf)

		delay := float32(anim.Data.Delay[index]) / 1000
		if delay <= 0 {
			delay = 0.01
		}
		anim.Delays = append(anim.Delays, delay)
	}
}

func (anim *JxlAnimation) Destroy() {
	for _, frame := range anim.Frames {
		if frame != nil {
			frame.Free()
		}
	}
}

type JxlPlayer struct {
	Animation    *JxlAnimation
	CurrentFrame int
	Timer        float32
	Frames       []*sdl.Texture
	renderer     *sdl.Renderer
}

func NewJxlPlayer(renderer *sdl.Renderer, jxlAnim *JxlAnimation) *JxlPlayer {
	return &JxlPlayer{
		Animation: jxlAnim,
		renderer:  renderer,
	}
}

func (jxlPlayer *JxlPlayer) Update(dt float32) {
	jxlPlayer.Timer += dt

	for jxlPlayer.Timer >= jxlPlayer.Animation.Delays[jxlPlayer.CurrentFrame] {
		jxlPlayer.Timer -= jxlPlayer.Animation.Delays[jxlPlayer.CurrentFrame]
		jxlPlayer.CurrentFrame++
		if jxlPlayer.CurrentFrame >= len(jxlPlayer.Animation.Frames) {
			jxlPlayer.CurrentFrame = 0
		}
	}
}

func (jxlPlayer *JxlPlayer) Destroy(state *State) {
	jxlPlayer.Animation.Destroy()
	for _, frame := range jxlPlayer.Frames {
		if frame != nil {
			state.destroyer <- frame
		}
	}
}

func (jxlPlayer *JxlPlayer) Texture() *sdl.Texture {
	for len(jxlPlayer.Frames) <= jxlPlayer.CurrentFrame {
		surface := jxlPlayer.Animation.Frames[jxlPlayer.CurrentFrame]
		surface.SetBlendMode(sdl.BLENDMODE_BLEND)
		frame, _ := jxlPlayer.renderer.CreateTextureFromSurface(surface)
		frame.SetBlendMode(sdl.BLENDMODE_BLEND)
		jxlPlayer.Frames = append(jxlPlayer.Frames, frame)

		jxlPlayer.Animation.Frames[jxlPlayer.CurrentFrame] = nil
		surface.Free()
	}
	return jxlPlayer.Frames[jxlPlayer.CurrentFrame]
}
