package main

import (
	"image/color"

	"github.com/kettek/apng"
	"github.com/veandco/go-sdl2/sdl"
)

type APNGAnimation struct {
	Data     apng.APNG
	Frames   []*sdl.Surface
	Delays   []float32 // In Seconds
	frameImg *sdl.Surface
	Width    float32
	Height   float32
}

func NewAPNGAnimation(data apng.APNG) *APNGAnimation {
	surf, _ := sdl.CreateRGBSurfaceWithFormat(0, int32(data.Frames[0].Image.Bounds().Dx()), int32(data.Frames[0].Image.Bounds().Dy()), 32, uint32(sdl.PIXELFORMAT_RGBA8888))

	anim := &APNGAnimation{
		Data:     data,
		frameImg: surf,
		Width:    float32(data.Frames[0].Image.Bounds().Dx()),
		Height:   float32(data.Frames[0].Image.Bounds().Dy()),
	}
	anim.Load()
	return anim
}

func (anim *APNGAnimation) Load() {
	defer anim.frameImg.Free()
	prev := 0
	empty := color.RGBA{0, 0, 0, 0}

	for y := 0; y < anim.frameImg.Bounds().Dy(); y++ {
		for x := 0; x < anim.frameImg.Bounds().Dx(); x++ {
			anim.frameImg.Set(x, y, empty)
		}
	}

	for index, frame := range anim.Data.Frames {
		/*if frame.IsDefault {
			continue
		}*/

		disposalMode := byte(frame.DisposeOp)
		if index == len(anim.Data.Frames)-1 {
			disposalMode = apng.DISPOSE_OP_BACKGROUND
		}

		for y := 0; y < frame.Image.Bounds().Dy(); y++ {
			for x := 0; x < frame.Image.Bounds().Dx(); x++ {
				c := frame.Image.At(x, y)
				if frame.BlendOp == apng.BLEND_OP_SOURCE {
					// r, g, b, a := color.RGBA()
					anim.frameImg.Set(x+frame.XOffset, y+frame.YOffset, c)
				} else if frame.BlendOp == apng.BLEND_OP_OVER {
					blendedColor := blendOver(c, anim.frameImg.At(x+frame.XOffset, y+frame.YOffset))
					anim.frameImg.Set(x+frame.XOffset, y+frame.YOffset, color.RGBA{byte(blendedColor.R), byte(blendedColor.G), byte(blendedColor.B), byte(blendedColor.A)})
				}
			}
		}

		newSurf, _ := anim.frameImg.Duplicate()
		anim.Frames = append(anim.Frames, newSurf)

		delay := float32(frame.GetDelay())
		if delay <= 0 {
			delay = 0.01
		}
		anim.Delays = append(anim.Delays, delay)

		if disposalMode == apng.DISPOSE_OP_BACKGROUND {
			//fmt.Printf("Dispose Background\n")
			for y := 0; y < frame.Image.Bounds().Dy(); y++ {
				for x := 0; x < frame.Image.Bounds().Dx(); x++ {
					anim.frameImg.Set(x+frame.XOffset, y+frame.YOffset, empty)
				}
			}
		} else if disposalMode == apng.DISPOSE_OP_PREVIOUS {
			for y := 0; y < frame.Image.Bounds().Dy(); y++ {
				for x := 0; x < frame.Image.Bounds().Dx(); x++ {
					prev_frame := anim.Data.Frames[prev]
					r, g, b, a := prev_frame.Image.At(x+(frame.XOffset-prev_frame.XOffset), y+(frame.XOffset-prev_frame.YOffset)).RGBA()
					anim.frameImg.Set(x+frame.XOffset, y+frame.YOffset, color.RGBA{byte(r), byte(g), byte(b), byte(a)})
				}
			}
		}

		if disposalMode != apng.DISPOSE_OP_PREVIOUS {
			prev = index
		}

		// prev = index
	}
}

func (anim *APNGAnimation) Destroy() {
	for _, frame := range anim.Frames {
		if frame != nil {
			frame.Free()
		}
	}
}

type APNGPlayer struct {
	Animation    *APNGAnimation
	CurrentFrame int
	Timer        float32
	Frames       []*sdl.Texture
	renderer     *sdl.Renderer
	CurrentLoop  int
}

func NewAPNGPlayer(renderer *sdl.Renderer, apngAnim *APNGAnimation) *APNGPlayer {
	return &APNGPlayer{
		Animation: apngAnim,
		renderer:  renderer,
	}
}

func (player *APNGPlayer) Update(dt float32) {
	player.Timer += dt

	for player.Timer >= player.Animation.Delays[player.CurrentFrame] {
		player.Timer -= player.Animation.Delays[player.CurrentFrame]
		player.CurrentFrame++
		if player.CurrentFrame >= len(player.Animation.Frames) {
			player.CurrentFrame = 0
		}
	}
}

func (player *APNGPlayer) Destroy(state *State) {
	player.Animation.Destroy()
	for _, frame := range player.Frames {
		if frame != nil {
			state.destroyer <- frame
		}
	}
}

func (player *APNGPlayer) Texture() *sdl.Texture {
	for len(player.Frames) <= player.CurrentFrame {
		surface := player.Animation.Frames[player.CurrentFrame]
		frame, _ := player.renderer.CreateTextureFromSurface(surface)
		player.Frames = append(player.Frames, frame)

		player.Animation.Frames[player.CurrentFrame] = nil
		surface.Free()
	}
	return player.Frames[player.CurrentFrame]
}

// Blends a over b.
func blendOver(a color.Color, b color.Color) color.RGBA {
	var result color.RGBA

	ra, ga, ba, aa := a.RGBA()
	rb, gb, bb, ab := b.RGBA()

	resultAlpha := aa + (ab * (1 - aa))
	result.A = uint8(resultAlpha)
	result.R = uint8(((ra * aa) + ((rb * ab) * (1 - aa))) / resultAlpha)
	result.G = uint8(((ga * aa) + ((gb * ab) * (1 - aa))) / resultAlpha)
	result.B = uint8(((ba * aa) + ((bb * ab) * (1 - aa))) / resultAlpha)

	return result
}
