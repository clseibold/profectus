//go:build windows
// +build windows

package main

import "github.com/gonutz/w32/v2"

func setupPlatform() {
	w32.SetProcessDpiAwareness(w32.PROCESS_PER_MONITOR_DPI_AWARE)
}
