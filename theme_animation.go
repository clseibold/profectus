package main

import (
	"gitlab.com/clseibold/profectus/horusui"
)

type ThemeAnimation struct {
	Animating               bool
	ContentContainerPadding horusui.PaddingAnimation
	SidebarDivider          horusui.NumberAnimation[float32]

	Default     SectionThemeAnimation // Default Fallback
	Topbar      SectionThemeAnimation
	Tabbar      TabbarThemeAnimation // Add location setting (below_topbar, above_topbar, bottom, top)
	Sidebar     SidebarThemeAnimation
	Page        PageThemeAnimation
	PageInlined SectionThemeAnimation
	Popup       SectionThemeAnimation
}

type TabbarThemeAnimation struct {
	AddButtonPosition horusui.NumberAnimation[float32]
	SectionThemeAnimation
}

type SidebarThemeAnimation struct {
	MaxWidth horusui.NumberAnimation[int]
	MinWidth horusui.NumberAnimation[int]
	SectionThemeAnimation
}

type PageThemeAnimation struct {
	MaxWidth      horusui.NumberAnimation[int]
	MaxImageWidth horusui.NumberAnimation[int]
	NexCharWidth  horusui.NumberAnimation[int]
	Center        horusui.ShowHideAnimation // TODO
	SectionThemeAnimation
	Document [ScrollLineType_Max]ElementThemeAnimation
}

type SectionThemeAnimation struct {
	Panel    ElementThemeAnimation
	Button   ButtonThemeAnimation
	Textbox  ElementThemeAnimation
	Checkbox ElementThemeAnimation
	List     ElementThemeAnimation
}

type ButtonThemeAnimation struct {
	ElementThemeAnimation
}

type ElementThemeAnimation struct {
	background [Selector_Max]horusui.ColorAnimation // Change to ColorAnimation
	border     [Selector_Max]horusui.BorderAnimation
	textColors [Selector_Max]horusui.ColorAnimation // Change to ColorAnimation

	padding    horusui.PaddingAnimation
	spacing    horusui.SpacingAnimation
	indentSize horusui.NumberAnimation[float32]
	radius     horusui.NumberAnimation[int16]
}

// Converts Theme to ThemeAnimated at already-finished state.
func NewThemeAnimation(startTheme Theme, endTheme Theme, ms float64) (result ThemeAnimation) {
	result.Animating = true

	sizeAnimationType := horusui.AnimationType_EaseOutQuad
	colorAnimationType := horusui.AnimationType_EaseInOutQuad
	documentSizeAnimationType := horusui.AnimationType_Step_5
	textColorAnimationType := horusui.AnimationType_Step_5

	// Get non-animated things out of the way first
	currentTheme.Name = endTheme.Name
	currentTheme.Filepath = endTheme.Filepath
	currentTheme.Tabbar.Position = endTheme.Tabbar.Position
	currentTheme.Sidebar.Dock = endTheme.Sidebar.Dock

	currentTheme.Default.Button.Outset = endTheme.Default.Button.Outset
	currentTheme.Topbar.Button.Outset = endTheme.Topbar.Button.Outset
	currentTheme.Tabbar.Button.Outset = endTheme.Tabbar.Button.Outset
	currentTheme.Sidebar.Button.Outset = endTheme.Sidebar.Button.Outset
	currentTheme.Page.Button.Outset = endTheme.Page.Button.Outset
	currentTheme.PageInlined.Button.Outset = endTheme.PageInlined.Button.Outset
	currentTheme.Popup.Button.Outset = endTheme.Popup.Button.Outset

	// ----- Animated Stuff -----
	result.ContentContainerPadding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.ContentContainerPadding, endTheme.ContentContainerPadding)
	result.SidebarDivider = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.SidebarDivider), float32(endTheme.SidebarDivider))

	// Padding Animation
	result.Default.Panel.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Default.Panel.padding, endTheme.Default.Panel.padding)
	result.Default.Button.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Default.Button.padding, endTheme.Default.Button.padding)
	result.Default.Textbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Default.Textbox.padding, endTheme.Default.Textbox.padding)
	result.Default.Checkbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Default.Checkbox.padding, endTheme.Default.Checkbox.padding)
	result.Default.List.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Default.List.padding, endTheme.Default.List.padding)

	result.Topbar.Panel.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Topbar.Panel.padding, endTheme.Topbar.Panel.padding)
	result.Topbar.Button.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Topbar.Button.padding, endTheme.Topbar.Button.padding)
	result.Topbar.Textbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Topbar.Textbox.padding, endTheme.Topbar.Textbox.padding)
	result.Topbar.Checkbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Topbar.Checkbox.padding, endTheme.Topbar.Checkbox.padding)
	result.Topbar.List.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Topbar.List.padding, endTheme.Topbar.List.padding)

	result.Tabbar.Panel.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Panel.padding, endTheme.Tabbar.Panel.padding)
	result.Tabbar.Button.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Button.padding, endTheme.Tabbar.Button.padding)
	result.Tabbar.Textbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Textbox.padding, endTheme.Tabbar.Textbox.padding)
	result.Tabbar.Checkbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Checkbox.padding, endTheme.Tabbar.Checkbox.padding)
	result.Tabbar.List.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Tabbar.List.padding, endTheme.Tabbar.List.padding)

	result.Sidebar.Panel.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Panel.padding, endTheme.Sidebar.Panel.padding)
	result.Sidebar.Button.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Button.padding, endTheme.Sidebar.Button.padding)
	result.Sidebar.Textbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Textbox.padding, endTheme.Sidebar.Textbox.padding)
	result.Sidebar.Checkbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Checkbox.padding, endTheme.Sidebar.Checkbox.padding)
	result.Sidebar.List.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Sidebar.List.padding, endTheme.Sidebar.List.padding)

	result.Page.Panel.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Page.Panel.padding, endTheme.Page.Panel.padding)
	result.Page.Button.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Page.Button.padding, endTheme.Page.Button.padding)
	result.Page.Textbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Page.Textbox.padding, endTheme.Page.Textbox.padding)
	result.Page.Checkbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Page.Checkbox.padding, endTheme.Page.Checkbox.padding)
	result.Page.List.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Page.List.padding, endTheme.Page.List.padding)

	result.PageInlined.Panel.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Panel.padding, endTheme.PageInlined.Panel.padding)
	result.PageInlined.Button.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Button.padding, endTheme.PageInlined.Button.padding)
	result.PageInlined.Textbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Textbox.padding, endTheme.PageInlined.Textbox.padding)
	result.PageInlined.Checkbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Checkbox.padding, endTheme.PageInlined.Checkbox.padding)
	result.PageInlined.List.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.PageInlined.List.padding, endTheme.PageInlined.List.padding)

	result.Popup.Panel.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Popup.Panel.padding, endTheme.Popup.Panel.padding)
	result.Popup.Button.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Popup.Button.padding, endTheme.Popup.Button.padding)
	result.Popup.Textbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Popup.Textbox.padding, endTheme.Popup.Textbox.padding)
	result.Popup.Checkbox.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Popup.Checkbox.padding, endTheme.Popup.Checkbox.padding)
	result.Popup.List.padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Popup.List.padding, endTheme.Popup.List.padding)

	// Spacing Animation
	result.Default.Panel.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Default.Panel.spacing, endTheme.Default.Panel.spacing)
	result.Default.Button.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Default.Button.spacing, endTheme.Default.Button.spacing)
	result.Default.Textbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Default.Textbox.spacing, endTheme.Default.Textbox.spacing)
	result.Default.Checkbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Default.Checkbox.spacing, endTheme.Default.Checkbox.spacing)
	result.Default.List.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Default.List.spacing, endTheme.Default.List.spacing)

	result.Topbar.Panel.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Topbar.Panel.spacing, endTheme.Topbar.Panel.spacing)
	result.Topbar.Button.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Topbar.Button.spacing, endTheme.Topbar.Button.spacing)
	result.Topbar.Textbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Topbar.Textbox.spacing, endTheme.Topbar.Textbox.spacing)
	result.Topbar.Checkbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Topbar.Checkbox.spacing, endTheme.Topbar.Checkbox.spacing)
	result.Topbar.List.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Topbar.List.spacing, endTheme.Topbar.List.spacing)

	result.Tabbar.Panel.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Panel.spacing, endTheme.Tabbar.Panel.spacing)
	result.Tabbar.Button.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Button.spacing, endTheme.Tabbar.Button.spacing)
	result.Tabbar.Textbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Textbox.spacing, endTheme.Tabbar.Textbox.spacing)
	result.Tabbar.Checkbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Tabbar.Checkbox.spacing, endTheme.Tabbar.Checkbox.spacing)
	result.Tabbar.List.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Tabbar.List.spacing, endTheme.Tabbar.List.spacing)

	result.Sidebar.Panel.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Panel.spacing, endTheme.Sidebar.Panel.spacing)
	result.Sidebar.Button.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Button.spacing, endTheme.Sidebar.Button.spacing)
	result.Sidebar.Textbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Textbox.spacing, endTheme.Sidebar.Textbox.spacing)
	result.Sidebar.Checkbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Sidebar.Checkbox.spacing, endTheme.Sidebar.Checkbox.spacing)
	result.Sidebar.List.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Sidebar.List.spacing, endTheme.Sidebar.List.spacing)

	result.Page.Panel.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Page.Panel.spacing, endTheme.Page.Panel.spacing)
	result.Page.Button.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Page.Button.spacing, endTheme.Page.Button.spacing)
	result.Page.Textbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Page.Textbox.spacing, endTheme.Page.Textbox.spacing)
	result.Page.Checkbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Page.Checkbox.spacing, endTheme.Page.Checkbox.spacing)
	result.Page.List.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Page.List.spacing, endTheme.Page.List.spacing)

	result.PageInlined.Panel.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Panel.spacing, endTheme.PageInlined.Panel.spacing)
	result.PageInlined.Button.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Button.spacing, endTheme.PageInlined.Button.spacing)
	result.PageInlined.Textbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Textbox.spacing, endTheme.PageInlined.Textbox.spacing)
	result.PageInlined.Checkbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.PageInlined.Checkbox.spacing, endTheme.PageInlined.Checkbox.spacing)
	result.PageInlined.List.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.PageInlined.List.spacing, endTheme.PageInlined.List.spacing)

	result.Popup.Panel.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Popup.Panel.spacing, endTheme.Popup.Panel.spacing)
	result.Popup.Button.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Popup.Button.spacing, endTheme.Popup.Button.spacing)
	result.Popup.Textbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Popup.Textbox.spacing, endTheme.Popup.Textbox.spacing)
	result.Popup.Checkbox.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Popup.Checkbox.spacing, endTheme.Popup.Checkbox.spacing)
	result.Popup.List.spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Popup.List.spacing, endTheme.Popup.List.spacing)

	// Indent Size Animation
	result.Default.Panel.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Default.Panel.indentSize), float32(endTheme.Default.Panel.indentSize))
	result.Default.Button.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Default.Button.indentSize), float32(endTheme.Default.Button.indentSize))
	result.Default.Textbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Default.Textbox.indentSize), float32(endTheme.Default.Textbox.indentSize))
	result.Default.Checkbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Default.Checkbox.indentSize), float32(endTheme.Default.Checkbox.indentSize))
	result.Default.List.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Default.List.indentSize), float32(endTheme.Default.List.indentSize))

	result.Topbar.Panel.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Topbar.Panel.indentSize), float32(endTheme.Topbar.Panel.indentSize))
	result.Topbar.Button.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Topbar.Button.indentSize), float32(endTheme.Topbar.Button.indentSize))
	result.Topbar.Textbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Topbar.Textbox.indentSize), float32(endTheme.Topbar.Textbox.indentSize))
	result.Topbar.Checkbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Topbar.Checkbox.indentSize), float32(endTheme.Topbar.Checkbox.indentSize))
	result.Topbar.List.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Topbar.List.indentSize), float32(endTheme.Topbar.List.indentSize))

	result.Tabbar.Panel.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Tabbar.Panel.indentSize), float32(endTheme.Tabbar.Panel.indentSize))
	result.Tabbar.Button.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Tabbar.Button.indentSize), float32(endTheme.Tabbar.Button.indentSize))
	result.Tabbar.Textbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Tabbar.Textbox.indentSize), float32(endTheme.Tabbar.Textbox.indentSize))
	result.Tabbar.Checkbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Tabbar.Checkbox.indentSize), float32(endTheme.Tabbar.Checkbox.indentSize))
	result.Tabbar.List.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Tabbar.List.indentSize), float32(endTheme.Tabbar.List.indentSize))

	result.Sidebar.Panel.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Sidebar.Panel.indentSize), float32(endTheme.Sidebar.Panel.indentSize))
	result.Sidebar.Button.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Sidebar.Button.indentSize), float32(endTheme.Sidebar.Button.indentSize))
	result.Sidebar.Textbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Sidebar.Textbox.indentSize), float32(endTheme.Sidebar.Textbox.indentSize))
	result.Sidebar.Checkbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Sidebar.Checkbox.indentSize), float32(endTheme.Sidebar.Checkbox.indentSize))
	result.Sidebar.List.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Sidebar.List.indentSize), float32(endTheme.Sidebar.List.indentSize))

	result.Page.Panel.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Page.Panel.indentSize), float32(endTheme.Page.Panel.indentSize))
	result.Page.Button.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Page.Button.indentSize), float32(endTheme.Page.Button.indentSize))
	result.Page.Textbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Page.Textbox.indentSize), float32(endTheme.Page.Textbox.indentSize))
	result.Page.Checkbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Page.Checkbox.indentSize), float32(endTheme.Page.Checkbox.indentSize))
	result.Page.List.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Page.List.indentSize), float32(endTheme.Page.List.indentSize))

	result.PageInlined.Panel.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.PageInlined.Panel.indentSize), float32(endTheme.PageInlined.Panel.indentSize))
	result.PageInlined.Button.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.PageInlined.Button.indentSize), float32(endTheme.PageInlined.Button.indentSize))
	result.PageInlined.Textbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.PageInlined.Textbox.indentSize), float32(endTheme.PageInlined.Textbox.indentSize))
	result.PageInlined.Checkbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.PageInlined.Checkbox.indentSize), float32(endTheme.PageInlined.Checkbox.indentSize))
	result.PageInlined.List.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.PageInlined.List.indentSize), float32(endTheme.PageInlined.List.indentSize))

	result.Popup.Panel.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Popup.Panel.indentSize), float32(endTheme.Popup.Panel.indentSize))
	result.Popup.Button.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Popup.Button.indentSize), float32(endTheme.Popup.Button.indentSize))
	result.Popup.Textbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Popup.Textbox.indentSize), float32(endTheme.Popup.Textbox.indentSize))
	result.Popup.Checkbox.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Popup.Checkbox.indentSize), float32(endTheme.Popup.Checkbox.indentSize))
	result.Popup.List.indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Popup.List.indentSize), float32(endTheme.Popup.List.indentSize))

	// Radius
	result.Default.Panel.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Default.Panel.radius), int16(endTheme.Default.Panel.radius))
	result.Default.Button.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Default.Button.radius), int16(endTheme.Default.Button.radius))
	result.Default.Textbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Default.Textbox.radius), int16(endTheme.Default.Textbox.radius))
	result.Default.Checkbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Default.Checkbox.radius), int16(endTheme.Default.Checkbox.radius))
	result.Default.List.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Default.List.radius), int16(endTheme.Default.List.radius))

	result.Topbar.Panel.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Topbar.Panel.radius), int16(endTheme.Topbar.Panel.radius))
	result.Topbar.Button.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Topbar.Button.radius), int16(endTheme.Topbar.Button.radius))
	result.Topbar.Textbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Topbar.Textbox.radius), int16(endTheme.Topbar.Textbox.radius))
	result.Topbar.Checkbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Topbar.Checkbox.radius), int16(endTheme.Topbar.Checkbox.radius))
	result.Topbar.List.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Topbar.List.radius), int16(endTheme.Topbar.List.radius))

	result.Tabbar.Panel.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Tabbar.Panel.radius), int16(endTheme.Tabbar.Panel.radius))
	result.Tabbar.Button.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Tabbar.Button.radius), int16(endTheme.Tabbar.Button.radius))
	result.Tabbar.Textbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Tabbar.Textbox.radius), int16(endTheme.Tabbar.Textbox.radius))
	result.Tabbar.Checkbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Tabbar.Checkbox.radius), int16(endTheme.Tabbar.Checkbox.radius))
	result.Tabbar.List.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Tabbar.List.radius), int16(endTheme.Tabbar.List.radius))

	result.Sidebar.Panel.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Sidebar.Panel.radius), int16(endTheme.Sidebar.Panel.radius))
	result.Sidebar.Button.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Sidebar.Button.radius), int16(endTheme.Sidebar.Button.radius))
	result.Sidebar.Textbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Sidebar.Textbox.radius), int16(endTheme.Sidebar.Textbox.radius))
	result.Sidebar.Checkbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Sidebar.Checkbox.radius), int16(endTheme.Sidebar.Checkbox.radius))
	result.Sidebar.List.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Sidebar.List.radius), int16(endTheme.Sidebar.List.radius))

	result.Page.Panel.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Page.Panel.radius), int16(endTheme.Page.Panel.radius))
	result.Page.Button.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Page.Button.radius), int16(endTheme.Page.Button.radius))
	result.Page.Textbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Page.Textbox.radius), int16(endTheme.Page.Textbox.radius))
	result.Page.Checkbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Page.Checkbox.radius), int16(endTheme.Page.Checkbox.radius))
	result.Page.List.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Page.List.radius), int16(endTheme.Page.List.radius))

	result.PageInlined.Panel.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.PageInlined.Panel.radius), int16(endTheme.PageInlined.Panel.radius))
	result.PageInlined.Button.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.PageInlined.Button.radius), int16(endTheme.PageInlined.Button.radius))
	result.PageInlined.Textbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.PageInlined.Textbox.radius), int16(endTheme.PageInlined.Textbox.radius))
	result.PageInlined.Checkbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.PageInlined.Checkbox.radius), int16(endTheme.PageInlined.Checkbox.radius))
	result.PageInlined.List.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.PageInlined.List.radius), int16(endTheme.PageInlined.List.radius))

	result.Popup.Panel.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Popup.Panel.radius), int16(endTheme.Popup.Panel.radius))
	result.Popup.Button.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Popup.Button.radius), int16(endTheme.Popup.Button.radius))
	result.Popup.Textbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Popup.Textbox.radius), int16(endTheme.Popup.Textbox.radius))
	result.Popup.Checkbox.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Popup.Checkbox.radius), int16(endTheme.Popup.Checkbox.radius))
	result.Popup.List.radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Popup.List.radius), int16(endTheme.Popup.List.radius))

	// Special properties
	result.Tabbar.AddButtonPosition = horusui.CreateNumberAnimation(sizeAnimationType, ms, startTheme.Tabbar.AddButtonPosition, endTheme.Tabbar.AddButtonPosition)

	result.Sidebar.MaxWidth = horusui.CreateNumberAnimation(sizeAnimationType, ms, startTheme.Sidebar.MaxWidth, endTheme.Sidebar.MaxWidth)
	result.Sidebar.MinWidth = horusui.CreateNumberAnimation(sizeAnimationType, ms, startTheme.Sidebar.MinWidth, endTheme.Sidebar.MinWidth)

	result.Page.MaxWidth = horusui.CreateNumberAnimation(documentSizeAnimationType, ms, startTheme.Page.MaxWidth, endTheme.Page.MaxWidth)
	result.Page.MaxImageWidth = horusui.CreateNumberAnimation(documentSizeAnimationType, ms, startTheme.Page.MaxImageWidth, endTheme.Page.MaxImageWidth)
	result.Page.NexCharWidth = horusui.CreateNumberAnimation(documentSizeAnimationType, ms, startTheme.Page.NexCharWidth, endTheme.Page.NexCharWidth)
	result.Page.Center = horusui.CreateShowHideAnimation(sizeAnimationType, horusui.AnimationType_EaseOutQuad, ms, startTheme.Page.Center == 1)
	if startTheme.Page.Center != endTheme.Page.Center {
		result.Page.Center.Toggle()
	}

	for linetype := ScrollLineType_Heading1; linetype < ScrollLineType_Max; linetype++ {
		result.Page.Document[linetype].padding = horusui.CreatePaddingAnimation(sizeAnimationType, ms, startTheme.Page.Document[linetype].padding, startTheme.Page.Document[linetype].padding)
		result.Page.Document[linetype].spacing = horusui.CreateSpacingAnimation(sizeAnimationType, ms, startTheme.Page.Document[linetype].spacing, startTheme.Page.Document[linetype].spacing)
		result.Page.Document[linetype].indentSize = horusui.CreateNumberAnimation(sizeAnimationType, ms, float32(startTheme.Page.Document[linetype].indentSize), float32(startTheme.Page.Document[linetype].indentSize))
		result.Page.Document[linetype].radius = horusui.CreateNumberAnimation(sizeAnimationType, ms, int16(startTheme.Page.Document[linetype].radius), int16(startTheme.Page.Document[linetype].radius))
	}

	for selector := Selector_Default; selector < Selector_Max; selector++ {
		// Background Color Animation
		result.Default.Panel.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Default.Panel.background[selector], endTheme.Default.Panel.background[selector])
		result.Default.Button.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Default.Button.background[selector], endTheme.Default.Button.background[selector])
		result.Default.Textbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Default.Textbox.background[selector], endTheme.Default.Textbox.background[selector])
		result.Default.Checkbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Default.Checkbox.background[selector], endTheme.Default.Checkbox.background[selector])
		result.Default.List.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Default.List.background[selector], endTheme.Default.List.background[selector])

		result.Topbar.Panel.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Topbar.Panel.background[selector], endTheme.Topbar.Panel.background[selector])
		result.Topbar.Button.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Topbar.Button.background[selector], endTheme.Topbar.Button.background[selector])
		result.Topbar.Textbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Topbar.Textbox.background[selector], endTheme.Topbar.Textbox.background[selector])
		result.Topbar.Checkbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Topbar.Checkbox.background[selector], endTheme.Topbar.Checkbox.background[selector])
		result.Topbar.List.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Topbar.List.background[selector], endTheme.Topbar.List.background[selector])

		result.Tabbar.Panel.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Tabbar.Panel.background[selector], endTheme.Tabbar.Panel.background[selector])
		result.Tabbar.Button.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Tabbar.Button.background[selector], endTheme.Tabbar.Button.background[selector])
		result.Tabbar.Textbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Tabbar.Textbox.background[selector], endTheme.Tabbar.Textbox.background[selector])
		result.Tabbar.Checkbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Tabbar.Checkbox.background[selector], endTheme.Tabbar.Checkbox.background[selector])
		result.Tabbar.List.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Tabbar.List.background[selector], endTheme.Tabbar.List.background[selector])

		result.Sidebar.Panel.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Sidebar.Panel.background[selector], endTheme.Sidebar.Panel.background[selector])
		result.Sidebar.Button.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Sidebar.Button.background[selector], endTheme.Sidebar.Button.background[selector])
		result.Sidebar.Textbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Sidebar.Textbox.background[selector], endTheme.Sidebar.Textbox.background[selector])
		result.Sidebar.Checkbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Sidebar.Checkbox.background[selector], endTheme.Sidebar.Checkbox.background[selector])
		result.Sidebar.List.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Sidebar.List.background[selector], endTheme.Sidebar.List.background[selector])

		result.Page.Panel.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Page.Panel.background[selector], endTheme.Page.Panel.background[selector])
		result.Page.Button.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Page.Button.background[selector], endTheme.Page.Button.background[selector])
		result.Page.Textbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Page.Textbox.background[selector], endTheme.Page.Textbox.background[selector])
		result.Page.Checkbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Page.Checkbox.background[selector], endTheme.Page.Checkbox.background[selector])
		result.Page.List.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Page.List.background[selector], endTheme.Page.List.background[selector])

		result.PageInlined.Panel.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.PageInlined.Panel.background[selector], endTheme.PageInlined.Panel.background[selector])
		result.PageInlined.Button.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.PageInlined.Button.background[selector], endTheme.PageInlined.Button.background[selector])
		result.PageInlined.Textbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.PageInlined.Textbox.background[selector], endTheme.PageInlined.Textbox.background[selector])
		result.PageInlined.Checkbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.PageInlined.Checkbox.background[selector], endTheme.PageInlined.Checkbox.background[selector])
		result.PageInlined.List.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.PageInlined.List.background[selector], endTheme.PageInlined.List.background[selector])

		result.Popup.Panel.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Popup.Panel.background[selector], endTheme.Popup.Panel.background[selector])
		result.Popup.Button.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Popup.Button.background[selector], endTheme.Popup.Button.background[selector])
		result.Popup.Textbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Popup.Textbox.background[selector], endTheme.Popup.Textbox.background[selector])
		result.Popup.Checkbox.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Popup.Checkbox.background[selector], endTheme.Popup.Checkbox.background[selector])
		result.Popup.List.background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Popup.List.background[selector], endTheme.Popup.List.background[selector])

		// Border Animation
		result.Default.Panel.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Default.Panel.border[selector], endTheme.Default.Panel.border[selector])
		result.Default.Button.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Default.Button.border[selector], endTheme.Default.Button.border[selector])
		result.Default.Textbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Default.Textbox.border[selector], endTheme.Default.Textbox.border[selector])
		result.Default.Checkbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Default.Checkbox.border[selector], endTheme.Default.Checkbox.border[selector])
		result.Default.List.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Default.List.border[selector], endTheme.Default.List.border[selector])

		result.Topbar.Panel.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Topbar.Panel.border[selector], endTheme.Topbar.Panel.border[selector])
		result.Topbar.Button.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Topbar.Button.border[selector], endTheme.Topbar.Button.border[selector])
		result.Topbar.Textbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Topbar.Textbox.border[selector], endTheme.Topbar.Textbox.border[selector])
		result.Topbar.Checkbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Topbar.Checkbox.border[selector], endTheme.Topbar.Checkbox.border[selector])
		result.Topbar.List.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Topbar.List.border[selector], endTheme.Topbar.List.border[selector])

		result.Tabbar.Panel.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Tabbar.Panel.border[selector], endTheme.Tabbar.Panel.border[selector])
		result.Tabbar.Button.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Tabbar.Button.border[selector], endTheme.Tabbar.Button.border[selector])
		result.Tabbar.Textbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Tabbar.Textbox.border[selector], endTheme.Tabbar.Textbox.border[selector])
		result.Tabbar.Checkbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Tabbar.Checkbox.border[selector], endTheme.Tabbar.Checkbox.border[selector])
		result.Tabbar.List.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Tabbar.List.border[selector], endTheme.Tabbar.List.border[selector])

		result.Sidebar.Panel.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Sidebar.Panel.border[selector], endTheme.Sidebar.Panel.border[selector])
		result.Sidebar.Button.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Sidebar.Button.border[selector], endTheme.Sidebar.Button.border[selector])
		result.Sidebar.Textbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Sidebar.Textbox.border[selector], endTheme.Sidebar.Textbox.border[selector])
		result.Sidebar.Checkbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Sidebar.Checkbox.border[selector], endTheme.Sidebar.Checkbox.border[selector])
		result.Sidebar.List.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Sidebar.List.border[selector], endTheme.Sidebar.List.border[selector])

		result.Page.Panel.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Page.Panel.border[selector], endTheme.Page.Panel.border[selector])
		result.Page.Button.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Page.Button.border[selector], endTheme.Page.Button.border[selector])
		result.Page.Textbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Page.Textbox.border[selector], endTheme.Page.Textbox.border[selector])
		result.Page.Checkbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Page.Checkbox.border[selector], endTheme.Page.Checkbox.border[selector])
		result.Page.List.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Page.List.border[selector], endTheme.Page.List.border[selector])

		result.PageInlined.Panel.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.PageInlined.Panel.border[selector], endTheme.PageInlined.Panel.border[selector])
		result.PageInlined.Button.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.PageInlined.Button.border[selector], endTheme.PageInlined.Button.border[selector])
		result.PageInlined.Textbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.PageInlined.Textbox.border[selector], endTheme.PageInlined.Textbox.border[selector])
		result.PageInlined.Checkbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.PageInlined.Checkbox.border[selector], endTheme.PageInlined.Checkbox.border[selector])
		result.PageInlined.List.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.PageInlined.List.border[selector], endTheme.PageInlined.List.border[selector])

		result.Popup.Panel.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Popup.Panel.border[selector], endTheme.Popup.Panel.border[selector])
		result.Popup.Button.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Popup.Button.border[selector], endTheme.Popup.Button.border[selector])
		result.Popup.Textbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Popup.Textbox.border[selector], endTheme.Popup.Textbox.border[selector])
		result.Popup.Checkbox.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Popup.Checkbox.border[selector], endTheme.Popup.Checkbox.border[selector])
		result.Popup.List.border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Popup.List.border[selector], endTheme.Popup.List.border[selector])

		// Text Color Animation
		result.Default.Panel.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Default.Panel.textColors[selector], endTheme.Default.Panel.textColors[selector])
		result.Default.Button.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Default.Button.textColors[selector], endTheme.Default.Button.textColors[selector])
		result.Default.Textbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Default.Textbox.textColors[selector], endTheme.Default.Textbox.textColors[selector])
		result.Default.Checkbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Default.Checkbox.textColors[selector], endTheme.Default.Checkbox.textColors[selector])
		result.Default.List.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Default.List.textColors[selector], endTheme.Default.List.textColors[selector])

		result.Topbar.Panel.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Topbar.Panel.textColors[selector], endTheme.Topbar.Panel.textColors[selector])
		result.Topbar.Button.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Topbar.Button.textColors[selector], endTheme.Topbar.Button.textColors[selector])
		result.Topbar.Textbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Topbar.Textbox.textColors[selector], endTheme.Topbar.Textbox.textColors[selector])
		result.Topbar.Checkbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Topbar.Checkbox.textColors[selector], endTheme.Topbar.Checkbox.textColors[selector])
		result.Topbar.List.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Topbar.List.textColors[selector], endTheme.Topbar.List.textColors[selector])

		result.Tabbar.Panel.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Tabbar.Panel.textColors[selector], endTheme.Tabbar.Panel.textColors[selector])
		result.Tabbar.Button.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Tabbar.Button.textColors[selector], endTheme.Tabbar.Button.textColors[selector])
		result.Tabbar.Textbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Tabbar.Textbox.textColors[selector], endTheme.Tabbar.Textbox.textColors[selector])
		result.Tabbar.Checkbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Tabbar.Checkbox.textColors[selector], endTheme.Tabbar.Checkbox.textColors[selector])
		result.Tabbar.List.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Tabbar.List.textColors[selector], endTheme.Tabbar.List.textColors[selector])

		result.Sidebar.Panel.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Sidebar.Panel.textColors[selector], endTheme.Sidebar.Panel.textColors[selector])
		result.Sidebar.Button.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Sidebar.Button.textColors[selector], endTheme.Sidebar.Button.textColors[selector])
		result.Sidebar.Textbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Sidebar.Textbox.textColors[selector], endTheme.Sidebar.Textbox.textColors[selector])
		result.Sidebar.Checkbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Sidebar.Checkbox.textColors[selector], endTheme.Sidebar.Checkbox.textColors[selector])
		result.Sidebar.List.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Sidebar.List.textColors[selector], endTheme.Sidebar.List.textColors[selector])

		result.Page.Panel.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Page.Panel.textColors[selector], endTheme.Page.Panel.textColors[selector])
		result.Page.Button.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Page.Button.textColors[selector], endTheme.Page.Button.textColors[selector])
		result.Page.Textbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Page.Textbox.textColors[selector], endTheme.Page.Textbox.textColors[selector])
		result.Page.Checkbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Page.Checkbox.textColors[selector], endTheme.Page.Checkbox.textColors[selector])
		result.Page.List.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Page.List.textColors[selector], endTheme.Page.List.textColors[selector])

		result.PageInlined.Panel.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.PageInlined.Panel.textColors[selector], endTheme.PageInlined.Panel.textColors[selector])
		result.PageInlined.Button.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.PageInlined.Button.textColors[selector], endTheme.PageInlined.Button.textColors[selector])
		result.PageInlined.Textbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.PageInlined.Textbox.textColors[selector], endTheme.PageInlined.Textbox.textColors[selector])
		result.PageInlined.Checkbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.PageInlined.Checkbox.textColors[selector], endTheme.PageInlined.Checkbox.textColors[selector])
		result.PageInlined.List.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.PageInlined.List.textColors[selector], endTheme.PageInlined.List.textColors[selector])

		result.Popup.Panel.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Popup.Panel.textColors[selector], endTheme.Popup.Panel.textColors[selector])
		result.Popup.Button.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Popup.Button.textColors[selector], endTheme.Popup.Button.textColors[selector])
		result.Popup.Textbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Popup.Textbox.textColors[selector], endTheme.Popup.Textbox.textColors[selector])
		result.Popup.Checkbox.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Popup.Checkbox.textColors[selector], endTheme.Popup.Checkbox.textColors[selector])
		result.Popup.List.textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Popup.List.textColors[selector], endTheme.Popup.List.textColors[selector])

		for linetype := ScrollLineType_Heading1; linetype < ScrollLineType_Max; linetype++ {
			result.Page.Document[linetype].background[selector] = horusui.CreateColorAnimation(colorAnimationType, ms, startTheme.Page.Document[linetype].background[selector], endTheme.Page.Document[linetype].background[selector])
			result.Page.Document[linetype].border[selector] = horusui.CreateBorderAnimation(colorAnimationType, ms, startTheme.Page.Document[linetype].border[selector], endTheme.Page.Document[linetype].border[selector])
			result.Page.Document[linetype].textColors[selector] = horusui.CreateColorAnimation(textColorAnimationType, ms, startTheme.Page.Document[linetype].textColors[selector], endTheme.Page.Document[linetype].textColors[selector])
		}
	}

	return result
}

// Update the animation
func (themeAnimated *ThemeAnimation) Update(state *State, deltaTime float64) {
	if !themeAnimated.Animating {
		return
	}

	state.getActiveTab().firstFrame = true

	var finished bool // Used at end to set .Animating to false when finished.
	currentTheme.SidebarDivider, finished = themeAnimated.SidebarDivider.Update(deltaTime)
	currentTheme.ContentContainerPadding, _ = themeAnimated.ContentContainerPadding.Update(deltaTime)

	// Padding Animation
	currentTheme.Default.Panel.padding, _ = themeAnimated.Default.Panel.padding.Update(deltaTime)
	currentTheme.Default.Button.padding, _ = themeAnimated.Default.Button.padding.Update(deltaTime)
	currentTheme.Default.Textbox.padding, _ = themeAnimated.Default.Textbox.padding.Update(deltaTime)
	currentTheme.Default.Checkbox.padding, _ = themeAnimated.Default.Checkbox.padding.Update(deltaTime)
	currentTheme.Default.List.padding, _ = themeAnimated.Default.List.padding.Update(deltaTime)

	currentTheme.Topbar.Panel.padding, _ = themeAnimated.Topbar.Panel.padding.Update(deltaTime)
	currentTheme.Topbar.Button.padding, _ = themeAnimated.Topbar.Button.padding.Update(deltaTime)
	currentTheme.Topbar.Textbox.padding, _ = themeAnimated.Topbar.Textbox.padding.Update(deltaTime)
	currentTheme.Topbar.Checkbox.padding, _ = themeAnimated.Topbar.Checkbox.padding.Update(deltaTime)
	currentTheme.Topbar.List.padding, _ = themeAnimated.Topbar.List.padding.Update(deltaTime)

	currentTheme.Tabbar.Panel.padding, _ = themeAnimated.Tabbar.Panel.padding.Update(deltaTime)
	currentTheme.Tabbar.Button.padding, _ = themeAnimated.Tabbar.Button.padding.Update(deltaTime)
	currentTheme.Tabbar.Textbox.padding, _ = themeAnimated.Tabbar.Textbox.padding.Update(deltaTime)
	currentTheme.Tabbar.Checkbox.padding, _ = themeAnimated.Tabbar.Checkbox.padding.Update(deltaTime)
	currentTheme.Tabbar.List.padding, _ = themeAnimated.Tabbar.List.padding.Update(deltaTime)

	currentTheme.Sidebar.Panel.padding, _ = themeAnimated.Sidebar.Panel.padding.Update(deltaTime)
	currentTheme.Sidebar.Button.padding, _ = themeAnimated.Sidebar.Button.padding.Update(deltaTime)
	currentTheme.Sidebar.Textbox.padding, _ = themeAnimated.Sidebar.Textbox.padding.Update(deltaTime)
	currentTheme.Sidebar.Checkbox.padding, _ = themeAnimated.Sidebar.Checkbox.padding.Update(deltaTime)
	currentTheme.Sidebar.List.padding, _ = themeAnimated.Sidebar.List.padding.Update(deltaTime)

	currentTheme.Page.Panel.padding, _ = themeAnimated.Page.Panel.padding.Update(deltaTime)
	currentTheme.Page.Button.padding, _ = themeAnimated.Page.Button.padding.Update(deltaTime)
	currentTheme.Page.Textbox.padding, _ = themeAnimated.Page.Textbox.padding.Update(deltaTime)
	currentTheme.Page.Checkbox.padding, _ = themeAnimated.Page.Checkbox.padding.Update(deltaTime)
	currentTheme.Page.List.padding, _ = themeAnimated.Page.List.padding.Update(deltaTime)

	currentTheme.PageInlined.Panel.padding, _ = themeAnimated.PageInlined.Panel.padding.Update(deltaTime)
	currentTheme.PageInlined.Button.padding, _ = themeAnimated.PageInlined.Button.padding.Update(deltaTime)
	currentTheme.PageInlined.Textbox.padding, _ = themeAnimated.PageInlined.Textbox.padding.Update(deltaTime)
	currentTheme.PageInlined.Checkbox.padding, _ = themeAnimated.PageInlined.Checkbox.padding.Update(deltaTime)
	currentTheme.PageInlined.List.padding, _ = themeAnimated.PageInlined.List.padding.Update(deltaTime)

	currentTheme.Popup.Panel.padding, _ = themeAnimated.Popup.Panel.padding.Update(deltaTime)
	currentTheme.Popup.Button.padding, _ = themeAnimated.Popup.Button.padding.Update(deltaTime)
	currentTheme.Popup.Textbox.padding, _ = themeAnimated.Popup.Textbox.padding.Update(deltaTime)
	currentTheme.Popup.Checkbox.padding, _ = themeAnimated.Popup.Checkbox.padding.Update(deltaTime)
	currentTheme.Popup.List.padding, _ = themeAnimated.Popup.List.padding.Update(deltaTime)

	// Spacing Animation
	currentTheme.Default.Panel.spacing, _ = themeAnimated.Default.Panel.spacing.Update(deltaTime)
	currentTheme.Default.Button.spacing, _ = themeAnimated.Default.Button.spacing.Update(deltaTime)
	currentTheme.Default.Textbox.spacing, _ = themeAnimated.Default.Textbox.spacing.Update(deltaTime)
	currentTheme.Default.Checkbox.spacing, _ = themeAnimated.Default.Checkbox.spacing.Update(deltaTime)
	currentTheme.Default.List.spacing, _ = themeAnimated.Default.List.spacing.Update(deltaTime)

	currentTheme.Topbar.Panel.spacing, _ = themeAnimated.Topbar.Panel.spacing.Update(deltaTime)
	currentTheme.Topbar.Button.spacing, _ = themeAnimated.Topbar.Button.spacing.Update(deltaTime)
	currentTheme.Topbar.Textbox.spacing, _ = themeAnimated.Topbar.Textbox.spacing.Update(deltaTime)
	currentTheme.Topbar.Checkbox.spacing, _ = themeAnimated.Topbar.Checkbox.spacing.Update(deltaTime)
	currentTheme.Topbar.List.spacing, _ = themeAnimated.Topbar.List.spacing.Update(deltaTime)

	currentTheme.Tabbar.Panel.spacing, _ = themeAnimated.Tabbar.Panel.spacing.Update(deltaTime)
	currentTheme.Tabbar.Button.spacing, _ = themeAnimated.Tabbar.Button.spacing.Update(deltaTime)
	currentTheme.Tabbar.Textbox.spacing, _ = themeAnimated.Tabbar.Textbox.spacing.Update(deltaTime)
	currentTheme.Tabbar.Checkbox.spacing, _ = themeAnimated.Tabbar.Checkbox.spacing.Update(deltaTime)
	currentTheme.Tabbar.List.spacing, _ = themeAnimated.Tabbar.List.spacing.Update(deltaTime)

	currentTheme.Sidebar.Panel.spacing, _ = themeAnimated.Sidebar.Panel.spacing.Update(deltaTime)
	currentTheme.Sidebar.Button.spacing, _ = themeAnimated.Sidebar.Button.spacing.Update(deltaTime)
	currentTheme.Sidebar.Textbox.spacing, _ = themeAnimated.Sidebar.Textbox.spacing.Update(deltaTime)
	currentTheme.Sidebar.Checkbox.spacing, _ = themeAnimated.Sidebar.Checkbox.spacing.Update(deltaTime)
	currentTheme.Sidebar.List.spacing, _ = themeAnimated.Sidebar.List.spacing.Update(deltaTime)

	currentTheme.Page.Panel.spacing, _ = themeAnimated.Page.Panel.spacing.Update(deltaTime)
	currentTheme.Page.Button.spacing, _ = themeAnimated.Page.Button.spacing.Update(deltaTime)
	currentTheme.Page.Textbox.spacing, _ = themeAnimated.Page.Textbox.spacing.Update(deltaTime)
	currentTheme.Page.Checkbox.spacing, _ = themeAnimated.Page.Checkbox.spacing.Update(deltaTime)
	currentTheme.Page.List.spacing, _ = themeAnimated.Page.List.spacing.Update(deltaTime)

	currentTheme.PageInlined.Panel.spacing, _ = themeAnimated.PageInlined.Panel.spacing.Update(deltaTime)
	currentTheme.PageInlined.Button.spacing, _ = themeAnimated.PageInlined.Button.spacing.Update(deltaTime)
	currentTheme.PageInlined.Textbox.spacing, _ = themeAnimated.PageInlined.Textbox.spacing.Update(deltaTime)
	currentTheme.PageInlined.Checkbox.spacing, _ = themeAnimated.PageInlined.Checkbox.spacing.Update(deltaTime)
	currentTheme.PageInlined.List.spacing, _ = themeAnimated.PageInlined.List.spacing.Update(deltaTime)

	currentTheme.Popup.Panel.spacing, _ = themeAnimated.Popup.Panel.spacing.Update(deltaTime)
	currentTheme.Popup.Button.spacing, _ = themeAnimated.Popup.Button.spacing.Update(deltaTime)
	currentTheme.Popup.Textbox.spacing, _ = themeAnimated.Popup.Textbox.spacing.Update(deltaTime)
	currentTheme.Popup.Checkbox.spacing, _ = themeAnimated.Popup.Checkbox.spacing.Update(deltaTime)
	currentTheme.Popup.List.spacing, _ = themeAnimated.Popup.List.spacing.Update(deltaTime)

	// Indent Size Animation
	currentTheme.Default.Panel.indentSize, _ = themeAnimated.Default.Panel.indentSize.Update(deltaTime)
	currentTheme.Default.Button.indentSize, _ = themeAnimated.Default.Button.indentSize.Update(deltaTime)
	currentTheme.Default.Textbox.indentSize, _ = themeAnimated.Default.Textbox.indentSize.Update(deltaTime)
	currentTheme.Default.Checkbox.indentSize, _ = themeAnimated.Default.Checkbox.indentSize.Update(deltaTime)
	currentTheme.Default.List.indentSize, _ = themeAnimated.Default.List.indentSize.Update(deltaTime)

	currentTheme.Topbar.Panel.indentSize, _ = themeAnimated.Topbar.Panel.indentSize.Update(deltaTime)
	currentTheme.Topbar.Button.indentSize, _ = themeAnimated.Topbar.Button.indentSize.Update(deltaTime)
	currentTheme.Topbar.Textbox.indentSize, _ = themeAnimated.Topbar.Textbox.indentSize.Update(deltaTime)
	currentTheme.Topbar.Checkbox.indentSize, _ = themeAnimated.Topbar.Checkbox.indentSize.Update(deltaTime)
	currentTheme.Topbar.List.indentSize, _ = themeAnimated.Topbar.List.indentSize.Update(deltaTime)

	currentTheme.Tabbar.Panel.indentSize, _ = themeAnimated.Tabbar.Panel.indentSize.Update(deltaTime)
	currentTheme.Tabbar.Button.indentSize, _ = themeAnimated.Tabbar.Button.indentSize.Update(deltaTime)
	currentTheme.Tabbar.Textbox.indentSize, _ = themeAnimated.Tabbar.Textbox.indentSize.Update(deltaTime)
	currentTheme.Tabbar.Checkbox.indentSize, _ = themeAnimated.Tabbar.Checkbox.indentSize.Update(deltaTime)
	currentTheme.Tabbar.List.indentSize, _ = themeAnimated.Tabbar.List.indentSize.Update(deltaTime)

	currentTheme.Sidebar.Panel.indentSize, _ = themeAnimated.Sidebar.Panel.indentSize.Update(deltaTime)
	currentTheme.Sidebar.Button.indentSize, _ = themeAnimated.Sidebar.Button.indentSize.Update(deltaTime)
	currentTheme.Sidebar.Textbox.indentSize, _ = themeAnimated.Sidebar.Textbox.indentSize.Update(deltaTime)
	currentTheme.Sidebar.Checkbox.indentSize, _ = themeAnimated.Sidebar.Checkbox.indentSize.Update(deltaTime)
	currentTheme.Sidebar.List.indentSize, _ = themeAnimated.Sidebar.List.indentSize.Update(deltaTime)

	currentTheme.Page.Panel.indentSize, _ = themeAnimated.Page.Panel.indentSize.Update(deltaTime)
	currentTheme.Page.Button.indentSize, _ = themeAnimated.Page.Button.indentSize.Update(deltaTime)
	currentTheme.Page.Textbox.indentSize, _ = themeAnimated.Page.Textbox.indentSize.Update(deltaTime)
	currentTheme.Page.Checkbox.indentSize, _ = themeAnimated.Page.Checkbox.indentSize.Update(deltaTime)
	currentTheme.Page.List.indentSize, _ = themeAnimated.Page.List.indentSize.Update(deltaTime)

	currentTheme.PageInlined.Panel.indentSize, _ = themeAnimated.PageInlined.Panel.indentSize.Update(deltaTime)
	currentTheme.PageInlined.Button.indentSize, _ = themeAnimated.PageInlined.Button.indentSize.Update(deltaTime)
	currentTheme.PageInlined.Textbox.indentSize, _ = themeAnimated.PageInlined.Textbox.indentSize.Update(deltaTime)
	currentTheme.PageInlined.Checkbox.indentSize, _ = themeAnimated.PageInlined.Checkbox.indentSize.Update(deltaTime)
	currentTheme.PageInlined.List.indentSize, _ = themeAnimated.PageInlined.List.indentSize.Update(deltaTime)

	currentTheme.Popup.Panel.indentSize, _ = themeAnimated.Popup.Panel.indentSize.Update(deltaTime)
	currentTheme.Popup.Button.indentSize, _ = themeAnimated.Popup.Button.indentSize.Update(deltaTime)
	currentTheme.Popup.Textbox.indentSize, _ = themeAnimated.Popup.Textbox.indentSize.Update(deltaTime)
	currentTheme.Popup.Checkbox.indentSize, _ = themeAnimated.Popup.Checkbox.indentSize.Update(deltaTime)
	currentTheme.Popup.List.indentSize, _ = themeAnimated.Popup.List.indentSize.Update(deltaTime)

	// Radius
	currentTheme.Default.Panel.radius, _ = themeAnimated.Default.Panel.radius.Update(deltaTime)
	currentTheme.Default.Button.radius, _ = themeAnimated.Default.Button.radius.Update(deltaTime)
	currentTheme.Default.Textbox.radius, _ = themeAnimated.Default.Textbox.radius.Update(deltaTime)
	currentTheme.Default.Checkbox.radius, _ = themeAnimated.Default.Checkbox.radius.Update(deltaTime)
	currentTheme.Default.List.radius, _ = themeAnimated.Default.List.radius.Update(deltaTime)

	currentTheme.Topbar.Panel.radius, _ = themeAnimated.Topbar.Panel.radius.Update(deltaTime)
	currentTheme.Topbar.Button.radius, _ = themeAnimated.Topbar.Button.radius.Update(deltaTime)
	currentTheme.Topbar.Textbox.radius, _ = themeAnimated.Topbar.Textbox.radius.Update(deltaTime)
	currentTheme.Topbar.Checkbox.radius, _ = themeAnimated.Topbar.Checkbox.radius.Update(deltaTime)
	currentTheme.Topbar.List.radius, _ = themeAnimated.Topbar.List.radius.Update(deltaTime)

	currentTheme.Tabbar.Panel.radius, _ = themeAnimated.Tabbar.Panel.radius.Update(deltaTime)
	currentTheme.Tabbar.Button.radius, _ = themeAnimated.Tabbar.Button.radius.Update(deltaTime)
	currentTheme.Tabbar.Textbox.radius, _ = themeAnimated.Tabbar.Textbox.radius.Update(deltaTime)
	currentTheme.Tabbar.Checkbox.radius, _ = themeAnimated.Tabbar.Checkbox.radius.Update(deltaTime)
	currentTheme.Tabbar.List.radius, _ = themeAnimated.Tabbar.List.radius.Update(deltaTime)

	currentTheme.Sidebar.Panel.radius, _ = themeAnimated.Sidebar.Panel.radius.Update(deltaTime)
	currentTheme.Sidebar.Button.radius, _ = themeAnimated.Sidebar.Button.radius.Update(deltaTime)
	currentTheme.Sidebar.Textbox.radius, _ = themeAnimated.Sidebar.Textbox.radius.Update(deltaTime)
	currentTheme.Sidebar.Checkbox.radius, _ = themeAnimated.Sidebar.Checkbox.radius.Update(deltaTime)
	currentTheme.Sidebar.List.radius, _ = themeAnimated.Sidebar.List.radius.Update(deltaTime)

	currentTheme.Page.Panel.radius, _ = themeAnimated.Page.Panel.radius.Update(deltaTime)
	currentTheme.Page.Button.radius, _ = themeAnimated.Page.Button.radius.Update(deltaTime)
	currentTheme.Page.Textbox.radius, _ = themeAnimated.Page.Textbox.radius.Update(deltaTime)
	currentTheme.Page.Checkbox.radius, _ = themeAnimated.Page.Checkbox.radius.Update(deltaTime)
	currentTheme.Page.List.radius, _ = themeAnimated.Page.List.radius.Update(deltaTime)

	currentTheme.PageInlined.Panel.radius, _ = themeAnimated.PageInlined.Panel.radius.Update(deltaTime)
	currentTheme.PageInlined.Button.radius, _ = themeAnimated.PageInlined.Button.radius.Update(deltaTime)
	currentTheme.PageInlined.Textbox.radius, _ = themeAnimated.PageInlined.Textbox.radius.Update(deltaTime)
	currentTheme.PageInlined.Checkbox.radius, _ = themeAnimated.PageInlined.Checkbox.radius.Update(deltaTime)
	currentTheme.PageInlined.List.radius, _ = themeAnimated.PageInlined.List.radius.Update(deltaTime)

	currentTheme.Popup.Panel.radius, _ = themeAnimated.Popup.Panel.radius.Update(deltaTime)
	currentTheme.Popup.Button.radius, _ = themeAnimated.Popup.Button.radius.Update(deltaTime)
	currentTheme.Popup.Textbox.radius, _ = themeAnimated.Popup.Textbox.radius.Update(deltaTime)
	currentTheme.Popup.Checkbox.radius, _ = themeAnimated.Popup.Checkbox.radius.Update(deltaTime)
	currentTheme.Popup.List.radius, _ = themeAnimated.Popup.List.radius.Update(deltaTime)

	// Special properties
	currentTheme.Tabbar.AddButtonPosition, _ = themeAnimated.Tabbar.AddButtonPosition.Update(deltaTime)

	currentTheme.Sidebar.MaxWidth, _ = themeAnimated.Sidebar.MaxWidth.Update(deltaTime)
	currentTheme.Sidebar.MinWidth, _ = themeAnimated.Sidebar.MinWidth.Update(deltaTime)

	currentTheme.Page.MaxWidth, _ = themeAnimated.Page.MaxWidth.Update(deltaTime)
	currentTheme.Page.MaxImageWidth, _ = themeAnimated.Page.MaxImageWidth.Update(deltaTime)
	currentTheme.Page.NexCharWidth, _ = themeAnimated.Page.NexCharWidth.Update(deltaTime)
	currentTheme.Page.Center, _ = themeAnimated.Page.Center.Update(deltaTime)

	for linetype := ScrollLineType_Heading1; linetype < ScrollLineType_Max; linetype++ {
		currentTheme.Page.Document[linetype].padding, _ = themeAnimated.Page.Document[linetype].padding.Update(deltaTime)
		currentTheme.Page.Document[linetype].spacing, _ = themeAnimated.Page.Document[linetype].spacing.Update(deltaTime)
		currentTheme.Page.Document[linetype].indentSize, _ = themeAnimated.Page.Document[linetype].indentSize.Update(deltaTime)
		currentTheme.Page.Document[linetype].radius, _ = themeAnimated.Page.Document[linetype].radius.Update(deltaTime)
	}

	for selector := Selector_Default; selector < Selector_Max; selector++ {
		// Background Color Animation
		currentTheme.Default.Panel.background[selector], _ = themeAnimated.Default.Panel.background[selector].Update(deltaTime)
		currentTheme.Default.Button.background[selector], _ = themeAnimated.Default.Button.background[selector].Update(deltaTime)
		currentTheme.Default.Textbox.background[selector], _ = themeAnimated.Default.Textbox.background[selector].Update(deltaTime)
		currentTheme.Default.Checkbox.background[selector], _ = themeAnimated.Default.Checkbox.background[selector].Update(deltaTime)
		currentTheme.Default.List.background[selector], _ = themeAnimated.Default.List.background[selector].Update(deltaTime)

		currentTheme.Topbar.Panel.background[selector], _ = themeAnimated.Topbar.Panel.background[selector].Update(deltaTime)
		currentTheme.Topbar.Button.background[selector], _ = themeAnimated.Topbar.Button.background[selector].Update(deltaTime)
		currentTheme.Topbar.Textbox.background[selector], _ = themeAnimated.Topbar.Textbox.background[selector].Update(deltaTime)
		currentTheme.Topbar.Checkbox.background[selector], _ = themeAnimated.Topbar.Checkbox.background[selector].Update(deltaTime)
		currentTheme.Topbar.List.background[selector], _ = themeAnimated.Topbar.List.background[selector].Update(deltaTime)

		currentTheme.Tabbar.Panel.background[selector], _ = themeAnimated.Tabbar.Panel.background[selector].Update(deltaTime)
		currentTheme.Tabbar.Button.background[selector], _ = themeAnimated.Tabbar.Button.background[selector].Update(deltaTime)
		currentTheme.Tabbar.Textbox.background[selector], _ = themeAnimated.Tabbar.Textbox.background[selector].Update(deltaTime)
		currentTheme.Tabbar.Checkbox.background[selector], _ = themeAnimated.Tabbar.Checkbox.background[selector].Update(deltaTime)
		currentTheme.Tabbar.List.background[selector], _ = themeAnimated.Tabbar.List.background[selector].Update(deltaTime)

		currentTheme.Sidebar.Panel.background[selector], _ = themeAnimated.Sidebar.Panel.background[selector].Update(deltaTime)
		currentTheme.Sidebar.Button.background[selector], _ = themeAnimated.Sidebar.Button.background[selector].Update(deltaTime)
		currentTheme.Sidebar.Textbox.background[selector], _ = themeAnimated.Sidebar.Textbox.background[selector].Update(deltaTime)
		currentTheme.Sidebar.Checkbox.background[selector], _ = themeAnimated.Sidebar.Checkbox.background[selector].Update(deltaTime)
		currentTheme.Sidebar.List.background[selector], _ = themeAnimated.Sidebar.List.background[selector].Update(deltaTime)

		currentTheme.Page.Panel.background[selector], _ = themeAnimated.Page.Panel.background[selector].Update(deltaTime)
		currentTheme.Page.Button.background[selector], _ = themeAnimated.Page.Button.background[selector].Update(deltaTime)
		currentTheme.Page.Textbox.background[selector], _ = themeAnimated.Page.Textbox.background[selector].Update(deltaTime)
		currentTheme.Page.Checkbox.background[selector], _ = themeAnimated.Page.Checkbox.background[selector].Update(deltaTime)
		currentTheme.Page.List.background[selector], _ = themeAnimated.Page.List.background[selector].Update(deltaTime)

		currentTheme.PageInlined.Panel.background[selector], _ = themeAnimated.PageInlined.Panel.background[selector].Update(deltaTime)
		currentTheme.PageInlined.Button.background[selector], _ = themeAnimated.PageInlined.Button.background[selector].Update(deltaTime)
		currentTheme.PageInlined.Textbox.background[selector], _ = themeAnimated.PageInlined.Textbox.background[selector].Update(deltaTime)
		currentTheme.PageInlined.Checkbox.background[selector], _ = themeAnimated.PageInlined.Checkbox.background[selector].Update(deltaTime)
		currentTheme.PageInlined.List.background[selector], _ = themeAnimated.PageInlined.List.background[selector].Update(deltaTime)

		currentTheme.Popup.Panel.background[selector], _ = themeAnimated.Popup.Panel.background[selector].Update(deltaTime)
		currentTheme.Popup.Button.background[selector], _ = themeAnimated.Popup.Button.background[selector].Update(deltaTime)
		currentTheme.Popup.Textbox.background[selector], _ = themeAnimated.Popup.Textbox.background[selector].Update(deltaTime)
		currentTheme.Popup.Checkbox.background[selector], _ = themeAnimated.Popup.Checkbox.background[selector].Update(deltaTime)
		currentTheme.Popup.List.background[selector], _ = themeAnimated.Popup.List.background[selector].Update(deltaTime)

		// Border Animations
		currentTheme.Default.Panel.border[selector], _ = themeAnimated.Default.Panel.border[selector].Update(deltaTime)
		currentTheme.Default.Button.border[selector], _ = themeAnimated.Default.Button.border[selector].Update(deltaTime)
		currentTheme.Default.Textbox.border[selector], _ = themeAnimated.Default.Textbox.border[selector].Update(deltaTime)
		currentTheme.Default.Checkbox.border[selector], _ = themeAnimated.Default.Checkbox.border[selector].Update(deltaTime)
		currentTheme.Default.List.border[selector], _ = themeAnimated.Default.List.border[selector].Update(deltaTime)

		currentTheme.Topbar.Panel.border[selector], _ = themeAnimated.Topbar.Panel.border[selector].Update(deltaTime)
		currentTheme.Topbar.Button.border[selector], _ = themeAnimated.Topbar.Button.border[selector].Update(deltaTime)
		currentTheme.Topbar.Textbox.border[selector], _ = themeAnimated.Topbar.Textbox.border[selector].Update(deltaTime)
		currentTheme.Topbar.Checkbox.border[selector], _ = themeAnimated.Topbar.Checkbox.border[selector].Update(deltaTime)
		currentTheme.Topbar.List.border[selector], _ = themeAnimated.Topbar.List.border[selector].Update(deltaTime)

		currentTheme.Tabbar.Panel.border[selector], _ = themeAnimated.Tabbar.Panel.border[selector].Update(deltaTime)
		currentTheme.Tabbar.Button.border[selector], _ = themeAnimated.Tabbar.Button.border[selector].Update(deltaTime)
		currentTheme.Tabbar.Textbox.border[selector], _ = themeAnimated.Tabbar.Textbox.border[selector].Update(deltaTime)
		currentTheme.Tabbar.Checkbox.border[selector], _ = themeAnimated.Tabbar.Checkbox.border[selector].Update(deltaTime)
		currentTheme.Tabbar.List.border[selector], _ = themeAnimated.Tabbar.List.border[selector].Update(deltaTime)

		currentTheme.Sidebar.Panel.border[selector], _ = themeAnimated.Sidebar.Panel.border[selector].Update(deltaTime)
		currentTheme.Sidebar.Button.border[selector], _ = themeAnimated.Sidebar.Button.border[selector].Update(deltaTime)
		currentTheme.Sidebar.Textbox.border[selector], _ = themeAnimated.Sidebar.Textbox.border[selector].Update(deltaTime)
		currentTheme.Sidebar.Checkbox.border[selector], _ = themeAnimated.Sidebar.Checkbox.border[selector].Update(deltaTime)
		currentTheme.Sidebar.List.border[selector], _ = themeAnimated.Sidebar.List.border[selector].Update(deltaTime)

		currentTheme.Page.Panel.border[selector], _ = themeAnimated.Page.Panel.border[selector].Update(deltaTime)
		currentTheme.Page.Button.border[selector], _ = themeAnimated.Page.Button.border[selector].Update(deltaTime)
		currentTheme.Page.Textbox.border[selector], _ = themeAnimated.Page.Textbox.border[selector].Update(deltaTime)
		currentTheme.Page.Checkbox.border[selector], _ = themeAnimated.Page.Checkbox.border[selector].Update(deltaTime)
		currentTheme.Page.List.border[selector], _ = themeAnimated.Page.List.border[selector].Update(deltaTime)

		currentTheme.PageInlined.Panel.border[selector], _ = themeAnimated.PageInlined.Panel.border[selector].Update(deltaTime)
		currentTheme.PageInlined.Button.border[selector], _ = themeAnimated.PageInlined.Button.border[selector].Update(deltaTime)
		currentTheme.PageInlined.Textbox.border[selector], _ = themeAnimated.PageInlined.Textbox.border[selector].Update(deltaTime)
		currentTheme.PageInlined.Checkbox.border[selector], _ = themeAnimated.PageInlined.Checkbox.border[selector].Update(deltaTime)
		currentTheme.PageInlined.List.border[selector], _ = themeAnimated.PageInlined.List.border[selector].Update(deltaTime)

		currentTheme.Popup.Panel.border[selector], _ = themeAnimated.Popup.Panel.border[selector].Update(deltaTime)
		currentTheme.Popup.Button.border[selector], _ = themeAnimated.Popup.Button.border[selector].Update(deltaTime)
		currentTheme.Popup.Textbox.border[selector], _ = themeAnimated.Popup.Textbox.border[selector].Update(deltaTime)
		currentTheme.Popup.Checkbox.border[selector], _ = themeAnimated.Popup.Checkbox.border[selector].Update(deltaTime)
		currentTheme.Popup.List.border[selector], _ = themeAnimated.Popup.List.border[selector].Update(deltaTime)

		// Text Color Animation
		currentTheme.Default.Panel.textColors[selector], _ = themeAnimated.Default.Panel.textColors[selector].Update(deltaTime)
		currentTheme.Default.Button.textColors[selector], _ = themeAnimated.Default.Button.textColors[selector].Update(deltaTime)
		currentTheme.Default.Textbox.textColors[selector], _ = themeAnimated.Default.Textbox.textColors[selector].Update(deltaTime)
		currentTheme.Default.Checkbox.textColors[selector], _ = themeAnimated.Default.Checkbox.textColors[selector].Update(deltaTime)
		currentTheme.Default.List.textColors[selector], _ = themeAnimated.Default.List.textColors[selector].Update(deltaTime)

		currentTheme.Topbar.Panel.textColors[selector], _ = themeAnimated.Topbar.Panel.textColors[selector].Update(deltaTime)
		currentTheme.Topbar.Button.textColors[selector], _ = themeAnimated.Topbar.Button.textColors[selector].Update(deltaTime)
		currentTheme.Topbar.Textbox.textColors[selector], _ = themeAnimated.Topbar.Textbox.textColors[selector].Update(deltaTime)
		currentTheme.Topbar.Checkbox.textColors[selector], _ = themeAnimated.Topbar.Checkbox.textColors[selector].Update(deltaTime)
		currentTheme.Topbar.List.textColors[selector], _ = themeAnimated.Topbar.List.textColors[selector].Update(deltaTime)

		currentTheme.Tabbar.Panel.textColors[selector], _ = themeAnimated.Tabbar.Panel.textColors[selector].Update(deltaTime)
		currentTheme.Tabbar.Button.textColors[selector], _ = themeAnimated.Tabbar.Button.textColors[selector].Update(deltaTime)
		currentTheme.Tabbar.Textbox.textColors[selector], _ = themeAnimated.Tabbar.Textbox.textColors[selector].Update(deltaTime)
		currentTheme.Tabbar.Checkbox.textColors[selector], _ = themeAnimated.Tabbar.Checkbox.textColors[selector].Update(deltaTime)
		currentTheme.Tabbar.List.textColors[selector], _ = themeAnimated.Tabbar.List.textColors[selector].Update(deltaTime)

		currentTheme.Sidebar.Panel.textColors[selector], _ = themeAnimated.Sidebar.Panel.textColors[selector].Update(deltaTime)
		currentTheme.Sidebar.Button.textColors[selector], _ = themeAnimated.Sidebar.Button.textColors[selector].Update(deltaTime)
		currentTheme.Sidebar.Textbox.textColors[selector], _ = themeAnimated.Sidebar.Textbox.textColors[selector].Update(deltaTime)
		currentTheme.Sidebar.Checkbox.textColors[selector], _ = themeAnimated.Sidebar.Checkbox.textColors[selector].Update(deltaTime)
		currentTheme.Sidebar.List.textColors[selector], _ = themeAnimated.Sidebar.List.textColors[selector].Update(deltaTime)

		currentTheme.Page.Panel.textColors[selector], _ = themeAnimated.Page.Panel.textColors[selector].Update(deltaTime)
		currentTheme.Page.Button.textColors[selector], _ = themeAnimated.Page.Button.textColors[selector].Update(deltaTime)
		currentTheme.Page.Textbox.textColors[selector], _ = themeAnimated.Page.Textbox.textColors[selector].Update(deltaTime)
		currentTheme.Page.Checkbox.textColors[selector], _ = themeAnimated.Page.Checkbox.textColors[selector].Update(deltaTime)
		currentTheme.Page.List.textColors[selector], _ = themeAnimated.Page.List.textColors[selector].Update(deltaTime)

		currentTheme.PageInlined.Panel.textColors[selector], _ = themeAnimated.PageInlined.Panel.textColors[selector].Update(deltaTime)
		currentTheme.PageInlined.Button.textColors[selector], _ = themeAnimated.PageInlined.Button.textColors[selector].Update(deltaTime)
		currentTheme.PageInlined.Textbox.textColors[selector], _ = themeAnimated.PageInlined.Textbox.textColors[selector].Update(deltaTime)
		currentTheme.PageInlined.Checkbox.textColors[selector], _ = themeAnimated.PageInlined.Checkbox.textColors[selector].Update(deltaTime)
		currentTheme.PageInlined.List.textColors[selector], _ = themeAnimated.PageInlined.List.textColors[selector].Update(deltaTime)

		currentTheme.Popup.Panel.textColors[selector], _ = themeAnimated.Popup.Panel.textColors[selector].Update(deltaTime)
		currentTheme.Popup.Button.textColors[selector], _ = themeAnimated.Popup.Button.textColors[selector].Update(deltaTime)
		currentTheme.Popup.Textbox.textColors[selector], _ = themeAnimated.Popup.Textbox.textColors[selector].Update(deltaTime)
		currentTheme.Popup.Checkbox.textColors[selector], _ = themeAnimated.Popup.Checkbox.textColors[selector].Update(deltaTime)
		currentTheme.Popup.List.textColors[selector], _ = themeAnimated.Popup.List.textColors[selector].Update(deltaTime)

		// Document Stuff
		for linetype := ScrollLineType_Heading1; linetype < ScrollLineType_Max; linetype++ {
			currentTheme.Page.Document[linetype].background[selector], _ = themeAnimated.Page.Document[linetype].background[selector].Update(deltaTime)
			currentTheme.Page.Document[linetype].border[selector], _ = themeAnimated.Page.Document[linetype].border[selector].Update(deltaTime)
			currentTheme.Page.Document[linetype].textColors[selector], _ = themeAnimated.Page.Document[linetype].textColors[selector].Update(deltaTime)
		}
	}

	if finished {
		themeAnimated.Animating = false
	}
}
